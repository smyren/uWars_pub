# uWars - the microcontroller snake game

A snake game where players program microcontrollers to control a single snake given a radar image of the board.

The initial version is made for AVR microcontrollers, starting with the ATtiny817.  
The communication protocol is using UART or SPI so it is possible to port to all kinds of devices.

![screenshot_1|small](doc/screenshot_1.PNG "In-game screenshot")

## Project team

| **Name**           | **Responsibility**                | **Description**  |
| :----------------- | :--------------------------       | :--------------- |
| Steinar Myren      | game_host                         | Running and controlling the game and displaying graphics on display |
| Stig Larssen       | snake_controller (HW/SW)          | Interfacing commands between game_host and multiple snake_nodes |
| Carl Petter Levy   | snake_node (SW)                   | Microcontroller controlling one snake based on radar image input |
| Johan R Karlsen    | game_monitor                      | Monitor and display statistical information about the game |
| Ingebrigt Hole     | board_generator <br/> icon_editor | Generate game boards to be loaded by the game_host <br/> Generate colors and icons to be loaded by the snake_node |
| Ole Einar Salvesen | casing                            | Casing for the snake_controller and optionally the game_host |
| Egil Rotevatn      | snake_node (HW)                   | Snake node board design and layout |

## Contents

### game_host

The python software running the game and graphics.  
Check out [the readme](game_host/README.md) for more info.

### game_monitor

The python software taking care of the game statistics.  
Check out [the readme](game_monitor/README.md) for more info.

### snake_controller

The hardware and C firmware for the snake controller using SPI.  
{- TODO: Check out [the readme](snake_controller/README.md) for more info. -}

### snake_node

The hardware and C firmware framework for the snakes.  
Check out [the readme](snake_node/README.md) for more info.

### board_generator

Generate game boards and graphics to be used by the game.  
{- TODO: Check out [the readme](board_generator/README.md) for more info. -}

### icon_editor

GUI to design your snake colors and export to be used in the snake code.  
Check out [the readme](icon_editor/README.md) for more info.

## License

This project is made public under the GNU AGPLv3 license, check out [the license file](LICENSE.md) for more info.

## More screenshots

![screenshot_4|small](doc/screenshot_4.PNG "In-game screenshot")
![screenshot_3|small](doc/screenshot_3.PNG "In-game screenshot")
![screenshot_2|small](doc/screenshot_2.PNG "In-game screenshot")

