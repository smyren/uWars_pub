##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##



class Display_board_render():
    def __init__(self):
        self.fullscreen = False
        self.board_size = (36,54)           # Sprites (y,x)
        self.graphics = True                # Enable graphic update on screen
        self.enable_sound = False           #
        self.jingle_interval = 35           # In sec
        self.icon_scroll_speed = 100        # Pixels per sec
        self.block_size = 20
        
       
        
class Options():
    def __init__(self):
        self.board_generator_help_screen = "help_board_gen.png"
        self.save_warnings = True
        self.image_directory = 'images/components' # default/starting folder for images used in board generator
        self.map_icon_bg_color = (50,50,50)
        self.local_icon_bg_color = (50,50,85)
        
        self.undo_depth = 25
        self.undo_update_freq = 1