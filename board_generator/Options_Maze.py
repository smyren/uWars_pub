##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##


        
class Options():
    def __init__(self):
    
        # equal number in range to set specific size. If range a random number in range is choosen
        self.x_range           = (15,20)    # number of maze cells horizontally - translates to map size: (x+1)*width -1
        self.y_range           = (12,16)    # number of maze cells vertically   - translates to map size: (y+1)*height-1
        self.cell_width_range  = (12,16)    # between walls each map cell is width*height open spaces
        self.cell_height_range = (8,12)    # between walls each map cell is width*height open spaces
        
        self.shortcut_pct      =  40        # Percentage of walls where shortcuts should be opened.
        self.shortcut_size     = ( 3, 7)    # Size of shortcuts (will be limited by cell size)
        
        self.board_size        = (130, 75) # if defined will use cell size and this to get x_range and y_range
        #self.board_size        = None
        
        #self.random_seed       = 3
        self.random_seed       = None # if none default used (system clock based)
        
        self.image_directory   = 'images/maze_pacman' # default/starting folder for images used in board generator
        self.save_name         = 'generated_maze.pkl'
        self.image_style       = 2 # none or 1 use only one image for walls. 2 use five (see packman folder for example)
        