##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# # Main class for handling board generator

# #Import Modules
import os, sys
from os import walk
import pygame
from pygame.locals import *
import numpy as np
import tkinter as tk
from tkinter import filedialog
from tkinter import simpledialog
from tkinter import messagebox

sys.path.append("../game_host")

from Options import *
from src.Board import *
from Ruleset import *
from src.Wall import *


# mouse icons

#. . . . . . . . . . . . . . . .   0    0 
#. . 1 1 . . . . 1 1 . . . . . .  48  192
#. 1 . . 1 . . 1 . . . . . . . .  73    0 
#. 1 . . 1 . . 1 . . . . . . . .  73    0 
#. . . . . 1 1 . . . . . . . . .   6    0 

#. . . . . . . . . . . . . . . .   0   0 
#. 1 1 1 1 1 1 1 1 1 . . . . . . 127   0 
#. . . . . . . . . . . . . . . .   0   0 
#. . . . . . . . . . . . . . . .   0   0 
#. . . . . . . . . . . . . . . .   0   0 

#. 1 1 1 1 1 . . . . . . . . . . 124   0 
#. 1 1 1 1 1 . . . . . . . . . . 124   0 
#. 1 1 1 1 1 . . . . . . . . . . 124   0 
#. 1 1 1 1 1 . . . . . . . . . . 124   0 
#. 1 1 1 1 1 . . . . . . . . . . 124   0 

#. 1 1 1 . . . . . 1 1 1 . . . . 112   112 
#. 1 1 1 . 1 1 1 . 1 1 1 . . . . 119   112 
#. 1 1 1 . . . . . 1 1 1 . . . . 112   112 
#. . . . . . . . . . . . . . . .   0   0 
#. . . . . . . . . . . . . . . .   0   0 

#. . . . . . . . . . . . . 1 1 .   0   6  0   0  0   6 
#. . . . . . . . . . . . 1 0 0 1   0   9  0   6  0  15
#. . . . . . . . . . . 1 0 0 0 1   0  17  0  14  0  31
#. . . . . . . . . . 1 0 0 0 1 .   0  34  0  28  0  62  
#. . . . . . . . . 1 0 0 0 1 . .   0  68  0  56  0 124
#. . . . . . . . 1 0 0 0 1 . . .   0 136  0 112  0 248
#. . . . . . . 1 0 0 0 1 . . . .   1  16  0 224  1 240
#. . . . . . 1 1 1 0 1 . . . . .   3 160  0  64  3 224
#. . . . . . 1 1 1 1 . . . . . .   3 192  0   0  3 192
#. . . . . . 1 1 1 . . . . . . .   3 128  0   0  3 128
#. . . . . . . . . . . . . . . .   0   0  0   0  0   0



class Interface():
    draw_mode = {'freehand':0,'line':1,'box':2,'copy':3}
    
    draw_cursors = [[(16, 16), (7, 14), (0,0,48,192,73,0,73,0,6,0,0,6,0,9,0,17,0,34,0,68,0,136,1,16,3,160,3,192,3,128,0,0), (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)], 
                    [(16, 16), (7, 14), (0,0,127,0,0,0,0,0,0,0,0,6,0,9,0,17,0,34,0,68,0,136,1,16,3,160,3,192,3,128,0,0), (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)], 
                    [(16, 16), (7, 14), (124,0,124,0,124,0,124,0,124,0,0,6,0,9,0,17,0,34,0,68,0,136,1,16,3,160,3,192,3,128,0,0), (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)], 
                    [(16, 16), (7, 14), (112,112,119,112,112,112,0,0,0,0,0,6,0,9,0,17,0,34,0,68,0,136,1,16,3,160,3,192,3,128,0,0), (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)] ]            
  
    
    def __init__(self):
        pygame.init()
        pygame.mouse.set_visible(True)
        
        pygame.mouse.set_cursor(*self.draw_cursors[0]) # cross as mouse cursor

        # Using separate ruleset for board generator to disable full screen by default. Can be made more smooooooooth
        self.display = Display_board_render()   
        self.options = Options()
        self.ruleset = Ruleset()
        self.image_list = []
        self.local_image_list = []
        self.images = []
        self.local_images = []
        self.local_theme = True

        self.save_warnings = self.options.save_warnings    
        self.image_dir_relative = self.options.image_directory
        
        self.block_size = self.display.block_size
        self.wall_display_size = 30
        self.changes = False
        self.draw_mode = 'freehand'
        self.caption_x = 0
        self.caption_y = 0
        self.caption_coord = False
        
        self.start_x = 1
        self.start_y = 1        
        self.valid_start = False
        self.lean_line = True
        
        self.prev_x = 1
        self.prev_y = 1
        self.prev_valid = False
        
        # Build image palett
        self.image_dir_abs = os.path.join(map_dir, self.image_dir_relative)
        self.image_dir_abs = os.path.normpath(self.image_dir_abs)
        self.load_icon_bar()
        
        self.map = Map(self.local_image_list, self.image_dir_relative, no_walls = True)
        self.local_map = Map(self.local_image_list, self.image_dir_relative)
        self.walltype = 0
        self.wallrotation = 0
        # self.wall_in_map = False
        
    
        # Blatently using almost only features from game_host :)
        self.board_render = Board_Render(self.ruleset.board_id_base, self.display, self.map.map_images, self.ruleset.snake_radar, self.block_size)
        self.block_size = self.board_render._block_size
        self.board = Board(self.ruleset.board_id_base, self.display.board_size, self.board_render.get_number_of_active_objects() )
        self.wall = Wall( self.board.get_board_size(), generate = True, id = self.ruleset.board_id_base.board_object)
           
        self.map_actual = []
        self.map_effects_actual = []
  
        self.file_opt = options_file = {}
        options_file['defaultextension'] = '.pkl'
        options_file['initialdir'] = map_dir 
        
        self.dir_opt = options_dir = {}
        options_dir['initialdir'] = map_dir 
        
        root = tk.Tk()
        root.withdraw()
        
        self.update_wall_images()
        self.numwalls = len(self.local_map.map_images.walls)
        self.setup_screen()
        
        self.mark_valid = False
        self.mark_x_delta = 0
        self.mark_y_delta = 0
        self.mark_x = 0
        self.mark_y = 0
        
        self.map_list = []
        self.map_effects_list = []
        self.update_undo_map(clear = True)

    def load_icon_bar(self):
        if self.local_theme: self.local_image_list = []
        else: self.image_list = []
        f = []
        for (dirpath, dirnames, filenames) in walk(self.image_dir_abs):
            f.extend(filenames)
            break
            
        # running loop twice to get order correct
        for file in f:
            if file == '_bg.png':
                if self.local_theme: self.local_image_list.append(file)
                else: self.image_list.append(file)
                
        for file in f:
            if file == '_bg.png'or file == '_food.png'or file == '_talk.png'or file == 'body6.png' or file == 'head6.png' or file == '_bg_replace.png':
                continue
            else:
                if self.local_theme: self.local_image_list.append(file)
                else: self.image_list.append(file)
                
    def update_undo_map(self, clear = False):
        if clear:
            del self.map_list[:]
            del self.map_effects_list[:]
            self.map_id = 0
            self.numchanges = 0

        while len(self.map_list) > (self.map_id+1):
            del self.map_list[(len(self.map_list)-1)]
            del self.map_effects_list[(len(self.map_effects_list)-1)]
            
        self.map_list.append(self.wall.map.copy())
        self.map_effects_list.append(self.wall.map_effects.copy())
        if len(self.map_list) > self.options.undo_depth:
            del self.map_list[0]
            del self.map_effects_list[0]
        self.map_id = (len(self.map_list)-1)
        self.numchanges = 0 
       
    def undo(self):
        if self.numchanges > 0:
            self.update_undo_map()
        if len(self.map_list) > 0:
            if self.map_id > 0 and self.map_id <= len(self.map_list):
                self.map_id -= 1
                self.wall.map = []
                self.wall.map = self.map_list[self.map_id].copy()
                self.wall.map_effects = []
                self.wall.map_effects = self.map_effects_list[self.map_id].copy()
                self.wall.map_to_wall()
                self.board.update_object_radar( self.wall )
        
    def redo(self):
        if len(self.map_list) > (self.map_id+1) and self.map_id >=0:
            self.map_id += 1
            self.wall.map = []
            self.wall.map = self.map_list[self.map_id].copy()
            self.wall.map_effects = []
            self.wall.map_effects = self.map_effects_list[self.map_id].copy()
            self.wall.map_to_wall()
            self.board.update_object_radar( self.wall )
            
    def setup_screen(self):
        self.board.update_object_radar( self.wall ) 
        self.update_map = np.ones( (self.display.board_size[0], self.display.board_size[1]), dtype=np.uint8 )        
        self.border_width = self.board_render._border.width 
        self.border_height = self.board_render._border.height 
        self.block_size = self.board_render._block_size
    
        self.update_caption()
        
    def update_caption(self):
        output = 'uWars Board Editor '
        if self.caption_coord:
            output += '(x = %d, y = %d). ' % (self.caption_x, self.caption_y)
        else:
            output += ' (x = ?, y = ?). '
        output += '(x_size = %d, y_size = %d). Block size = %d' % (self.display.board_size[1], self.display.board_size[0], self.block_size)
        if self.local_theme: output += '. Local images'
        else: output += '. Map images'
        if self.draw_mode == 'freehand':
            output += '. Freehand'
        elif self.draw_mode == 'line':
            output += '. Line'
            if self.lean_line:
                output += ' (Lean)'
            else:
                output += ' (Thick)'
        elif self.draw_mode == 'box':
            output += '. Box'
        elif self.draw_mode == 'copy':
            output += '. Copy/Paste'   
            if (self.valid_start) or (self.mark_valid):
                output += '. Marked (d_x = %d, d_y =%d)' % (self.mark_x_delta, self.mark_y_delta)
        else:
            output += '. Unknown draw mode!'
        if self.changes:
            output += '(*)'
        pygame.display.set_caption(output)
       
    def update_wall_images(self):
        if self.local_theme:
            self.local_images = []
            self.local_images = self.local_map.update_wall_images(self.board_render._board_render_objects._gameregion_color, self.wall_display_size)
            self.board_render.draw_images_below_board(self.local_images, self.walltype, fill_color = self.options.local_icon_bg_color)
        else:
            self.images = []
            self.images = self.map.update_wall_images(self.board_render._board_render_objects._gameregion_color, self.wall_display_size)
            self.board_render.draw_images_below_board(self.images, self.walltype, fill_color = self.options.map_icon_bg_color) 
 
        self.board_render.update_scrollbar(0)
        
    def setup_new_sized_map(self):        
        self.wall.board_size = self.wall.map.shape
        self.display.board_size = self.wall.map.shape
        
        self.board_render.cleanup()
        self.board_render = Board_Render(self.ruleset.board_id_base, self.display, self.map.map_images, self.ruleset.snake_radar, self.block_size)
        self.board = None
        self.board = Board(self.ruleset.board_id_base, self.display.board_size, self.board_render.get_number_of_active_objects() )
        self.wall.map_to_wall()        
       
        
        self.update_wall_images()
        self.setup_screen()
        self.update_undo_map(clear = True)
    
    def save_file(self):           
        file_path = tk.filedialog.asksaveasfilename(**self.file_opt)   
        if (file_path != ''):
            self.map.save_map(file_path, self.wall)
            #self.wall.save_map(file_path)
            self.changes = False
            self.update_caption()
         
    def get_confirmation(self, msg1 = None, msg2 = None):
        if msg1 and msg2:
            confirmation = tk.messagebox.askyesno(msg1, msg2)
        else:
            if not self.save_warnings:
                confirmation = True
            elif (self.changes == True):
                confirmation = tk.messagebox.askyesno("Changes made since last save will be lost", "You have unsaved changes that will be lost if you continue. Proceed in any case?")
            else: 
                confirmation = True
        return confirmation

    def load_file(self):
        ask = self.get_confirmation()    
        if ask == True:
            file_path = tk.filedialog.askopenfilename(**self.file_opt)  
            if (file_path != ''):
                #self.wall.load(file_path) 
                #self.wall.load_map(file_path) 
                self.map.load_map(file_path, self.wall)
                self.numwalls = len(self.map.map_images.walls)
                self.walltype = 0
                self.wallrotation = 0
                self.local_theme = False
                self.setup_new_sized_map()    
                self.valid_start = False
                self.prev_valid = False
                self.changes = False
                self.update_caption()
                self.update_undo_map(clear = True)
                
                
    def new_icon_folder(self):
        folder = tk.filedialog.askdirectory(**self.dir_opt)
        if (folder != ''):
            folder = os.path.normpath(folder)
            self.image_dir_abs = folder
            folder = folder.replace(map_dir, '')     
            self.image_dir_relative = folder[1:]  #must also remove backslash from beginning of string - TODO add check for it!
            
            self.local_theme = True
            self.load_icon_bar()
            self.local_map = Map(self.local_image_list, self.image_dir_relative)
            self.update_wall_images()
            self.local_images = self.local_map.update_wall_images(self.board_render._board_render_objects._gameregion_color, self.wall_display_size)
            
            self.numwalls = len(self.local_map.map_images.walls)
            self.walltype = 0
            self.wallrotation = 0   
            
            self.board_render.draw_images_below_board(self.local_images, self.walltype, fill_color = self.options.local_icon_bg_color) # TODO add support for resizing also these images?
            self.board_render.update_scrollbar(0)
        
    def new_map(self, skip_ask = False): 
        if skip_ask:
            ask = True
        else:
            ask = self.get_confirmation()    
        if ask == True:
            self.wall.map = []
            self.wall.map = np.zeros( (self.display.board_size[0], self.display.board_size[1]), dtype=np.uint8 )
            self.wall.map_effects = []
            self.wall.map_effects = np.zeros( (self.display.board_size[0], self.display.board_size[1]), dtype=np.uint8 )
            self.local_theme = True
            self.setup_new_sized_map()  
            self.valid_start = False
            self.prev_valid = False
            self.changes = False
            self.update_caption()
          
    def set_walltype(self, wall_id):
        if (wall_id >= 0) and (wall_id <= self.numwalls):
            self.walltype = wall_id
            self.wallrotation = self.map.get_rotation(wall_id)
            self.prev_valid = False
            self.valid_start = False
            if self.local_theme: self.board_render.draw_images_below_board(self.local_images, self.walltype, fill_color = self.options.local_icon_bg_color)
            else: self.board_render.draw_images_below_board(self.images, self.walltype, fill_color = self.options.map_icon_bg_color)
        
    def add_wall(self, x_seg, y_seg, delete = False):
        if delete:
            self.wall.set_wall(x_seg, y_seg, 0)
        else:
            self.wall.set_wall(x_seg, y_seg, self.walltype, self.map.get_rotation(self.walltype))
        self.wall.map_to_wall()
        self.board.update_object_radar( self.wall )
        if (self.changes==False):
            self.changes = True
            self.update_caption()
                
    def mouse_click(self, val = 0, move = False):
        x, y = pygame.mouse.get_pos()
        x_obj = int(np.floor( (x-self.border_width)/self.block_size))
        y_obj = int(np.floor( (y-self.border_height)/self.block_size))  
        
        if (x_obj >= 0) and (x_obj < self.display.board_size[1]) and (y_obj >= 0) and (y_obj < self.display.board_size[0]):
            if (val == 2):
                if (self.draw_mode == 'copy'):
                    if (self.mark_valid == True):
                        self.copy_box()
                        self.numchanges += (self.options.undo_update_freq+1)
                    
                elif (move == False):
                    self.rotate(x_obj, y_obj)
                    self.numchanges += 1
            else:
                if self.local_theme:
                    if self.walltype == 0:
                        update = True
                        if not self.map.split_path(self.image_dir_relative) == self.map.rel_dir_main:                           
                            #update = self.get_confirmation("Update map theme?", "Change map background to current theme?")
                            #if update:
                            self.map.update_dir(self.image_dir_relative)
                            self.setup_new_sized_map()
                                
                        if update:
                            self.local_theme = False
                            self.walltype = 0
                            self.update_wall_images()
                            self.update_caption()
                    else:
                        in_map, id = self.map.is_image_in_map(os.path.join(map_dir, self.image_dir_relative), self.local_image_list[self.walltype])
                        if not in_map:
                            id = self.map.add_image_to_map(self.image_dir_relative, self.local_image_list[self.walltype])
                            self.setup_new_sized_map()
                            
                        self.local_theme = False
                        self.walltype = id
                        self.update_wall_images()
                        self.numwalls = len(self.map.map_images.walls)
                        self.update_caption()
                            
                if (self.draw_mode == 'line') or (self.draw_mode == 'box'):
                    self.start_x = x
                    self.start_y = y
                    self.valid_start = True
                    self.map_actual = [] 
                    self.map_actual = self.wall.map.copy()
                    self.map_effects_actual = []
                    self.map_effects_actual = self.wall.map_effects.copy()
                elif (self.draw_mode == 'copy'):
                    self.mark_valid = False
                    self.start_x = x
                    self.start_y = y
                    self.valid_start = True
                else:
                    self.add_wall_segment(x_obj, y_obj, x, y, False)
                    self.numchanges += 1
            if self.numchanges > self.options.undo_update_freq:
                self.update_undo_map()        
                    
        elif (y_obj >= self.display.board_size[0]):
            x_wall = int(np.floor( (x-self.board_render._user_icon_section.spacing/2) / (self.wall_display_size + self.board_render._user_icon_section.spacing)  ))
            if (val == 2):
                if (move == False):
                    self.map.rotate_map_object(x_wall)
                    self.update_wall_images()
            
            self.set_walltype(x_wall)    
                
            if (self.draw_mode == 'line') or (self.draw_mode == 'box'):
                self.map_actual = [] 
                self.map_actual = self.wall.map.copy()
                self.map_effects_actual = []
                self.map_effects_actual = self.wall.map_effects.copy()
            self.valid_start = False   
    
    def mouse_wheel(self, val = 0):
        x, y = pygame.mouse.get_pos()
        x_obj = int(np.floor( (x-self.border_width)/self.block_size))
        y_obj = int(np.floor( (y-self.border_height)/self.block_size))  
        
        if (x_obj >= 0) and (x_obj < self.display.board_size[1]) and (y_obj >= 0) and (y_obj < self.display.board_size[0]):
            if val < 0:
                self.toggle_draw_mode()
            else:
                self.toggle_draw_mode(True)
        elif (y_obj >= self.display.board_size[0]):
            new_wall = self.walltype + val
            self.set_walltype(new_wall)
            
    def add_wall_segment(self, x_obj, y_obj, x, y, delete = False):
        if (self.prev_x != x_obj) or (self.prev_y != y_obj) or (self.prev_valid == False) or delete:
            if delete:
                self.add_wall(x_obj, y_obj, delete)
            else:
                self.add_wall(x_obj, y_obj)    
                self.prev_x = x_obj
                self.prev_y = y_obj  
                self.prev_valid = True
                
    def rotate(self, x_obj, y_obj):
        self.prev_valid = False
        self.wall.rotate(x_obj, y_obj)
        
        self.wall.map_to_wall()
        self.board.update_object_radar( self.wall )
        if (self.changes==False):
            self.changes = True
            self.update_caption()
            
    # Drawing line of walls from point (a,b) to (c,d) if order True, otherwise from (b,a) to (d,c)
    def traverse_line(self, a, b, c, d, order): 
        x = - 1
        y = - 1
        if (self.lean_line):
            e = a
            done = False
            while (done == False):
                
                if e > c:
                    e = c
                f = b + int(np.floor((d - b) * (a-e) / (a - c) ) )
                if order == True:
                    x_obj = int(np.floor( (e-self.border_width)/self.block_size))
                    y_obj = int(np.floor( (f-self.border_height)/self.block_size)) 
                else:
                    x_obj = int(np.floor( (f-self.border_width)/self.block_size))
                    y_obj = int(np.floor( (e-self.border_height)/self.block_size))
                self.wall.set_wall(x_obj, y_obj, self.walltype, self.map.get_rotation(self.walltype))
                if e >= c:
                    done = True
                e += self.block_size
        else:
            for e in range (a , c):
                if abs(c - a) > 0:
                    f = b + int(np.floor((d - b) * (a-e) / (a - c) ) )
                    if order == True:
                        x_obj = int(np.floor( (e-self.border_width)/self.block_size))
                        y_obj = int(np.floor( (f-self.border_height)/self.block_size)) 
                    else:
                        x_obj = int(np.floor( (f-self.border_width)/self.block_size))
                        y_obj = int(np.floor( (e-self.border_height)/self.block_size))     
                    if (x_obj != x) or (y_obj != y):
                        self.wall.set_wall(x_obj, y_obj, self.walltype, self.map.get_rotation(self.walltype))
                        x = x_obj
                        y = y_obj     
                        
        self.wall.map_to_wall()
        self.board.update_object_radar( self.wall )
        
    def update_coordinates(self):
        x, y = pygame.mouse.get_pos()            
        x_obj = int(np.floor( (x-self.border_width)/self.block_size))
        y_obj = int(np.floor( (y-self.border_height)/self.block_size))                
        if (x_obj > -1) and (x_obj < self.display.board_size[1]) and (y_obj > -1) and (y_obj < self.display.board_size[0]):
            self.caption_x = x_obj + 1
            self.caption_y = y_obj + 1
            self.caption_coord = True
        else:
            self.caption_coord = False
        self.update_caption()
        
    # Check if there is a valid start point for line, and if marked point also valid then draw line
    # Incrementing along the axis with greatest delta, and starting from point with low value since
    # Python likes for loops to increment only
    def add_wall_line(self, preview = False):
        if self.valid_start == True:
            self.wall.map = []
            self.wall.map = self.map_actual.copy()
            self.wall.map_effects = []
            self.wall.map_effects = self.map_effects_actual.copy()
            
            x, y = pygame.mouse.get_pos()
            
            x_obj = int(np.floor( (x-self.border_width)/self.block_size))
            y_obj = int(np.floor( (y-self.border_height)/self.block_size))                
            if (x_obj > -1) and (x_obj < self.display.board_size[1]) and (y_obj > -1) and (y_obj < self.display.board_size[0]):
                if abs(x-self.start_x) > abs (y-self.start_y):
                    if x > self.start_x:
                        self.traverse_line(self.start_x, self.start_y, x, y, True)
                    elif self.start_x > x:
                        self.traverse_line(x, y, self.start_x, self.start_y, True)
                else:
                    if y > self.start_y:
                        self.traverse_line(self.start_y, self.start_x, y, x, False)
                    elif self.start_y > y:
                        self.traverse_line(y, x, self.start_y, self.start_x, False)                    
                
            if (preview == False):
                self.valid_start = False
                self.start_x = x
                self.start_y = y
                self.changes = True  
                self.update_caption()
                self.numchanges += 2
                if self.numchanges > self.options.undo_update_freq:
                    self.update_undo_map()
            
            
            
    def mark_box(self, preview = False):
        if self.valid_start == True:
            x, y = pygame.mouse.get_pos()
            x_obj = int(np.floor( (x-self.border_width)/self.block_size))
            y_obj = int(np.floor( (y-self.border_height)/self.block_size))                
            if (x_obj > -1) and (x_obj < self.display.board_size[1]) and (y_obj > -1) and (y_obj < self.display.board_size[0]):
                x_obj_start = int(np.floor( (self.start_x-self.border_width)/self.block_size))
                y_obj_start = int(np.floor( (self.start_y-self.border_height)/self.block_size))         
                min_x = min(x_obj, x_obj_start)
                max_x = max(x_obj, x_obj_start)
                min_y = min(y_obj, y_obj_start)
                max_y = max(y_obj, y_obj_start)
                self.mark_x = min_x
                self.mark_y = min_y
                self.mark_x_delta = max_x - min_x + 1
                self.mark_y_delta = max_y - min_y + 1  
                self.update_caption()
                
            if (preview == False):
                self.valid_start = False
                self.mark_valid = True
                self.start_x = x
                self.start_y = y
                self.changes = True  
                self.update_caption() 
                self.wall.copy(self.mark_x, self.mark_y, self.mark_x_delta, self.mark_y_delta)
            
                
    def copy_box(self):        
        x, y = pygame.mouse.get_pos()
            
        x_obj = int(np.floor( (x-self.border_width)/self.block_size))
        y_obj = int(np.floor( (y-self.border_height)/self.block_size))
        if (x_obj > -1) and (x_obj < self.display.board_size[1]) and (y_obj > -1) and (y_obj < self.display.board_size[0]):
            self.wall.paste(x_obj,y_obj)
        
        self.wall.map_to_wall()
        self.board.update_object_radar( self.wall )    
        
    def delete_box(self):
        if self.mark_valid:
            self.wall.copy(self.mark_x, self.mark_y, self.mark_x_delta, self.mark_y_delta, True)
            
            self.wall.map_to_wall()
            self.board.update_object_radar( self.wall ) 
        
    def add_box(self, preview = False):
        if self.valid_start == True:       
            self.wall.map = []
            self.wall.map = self.map_actual.copy()
            self.wall.map_effects = []
            self.wall.map_effects = self.map_effects_actual.copy()
            
            x, y = pygame.mouse.get_pos()
            
            x_obj = int(np.floor( (x-self.border_width)/self.block_size))
            y_obj = int(np.floor( (y-self.border_height)/self.block_size))                
            if (x_obj > -1) and (x_obj < self.display.board_size[1]) and (y_obj > -1) and (y_obj < self.display.board_size[0]):
                x_obj_start = int(np.floor( (self.start_x-self.border_width)/self.block_size))
                y_obj_start = int(np.floor( (self.start_y-self.border_height)/self.block_size))         
                min_x = min(x_obj, x_obj_start)
                max_x = max(x_obj, x_obj_start)
                min_y = min(y_obj, y_obj_start)
                max_y = max(y_obj, y_obj_start)
                self.draw_box(min_x, min_y, max_x, max_y)                   
                
            if (preview == False):
                self.valid_start = False
                self.start_x = x
                self.start_y = y
                self.changes = True  
                self.update_caption() 
                self.numchanges += 2
                if self.numchanges > self.options.undo_update_freq:
                    self.update_undo_map()                
                
    def draw_box(self, min_x, min_y, max_x, max_y):
        for x in range(min_x, max_x + 1):
            for y in range(min_y, max_y + 1):
                self.wall.set_wall(x, y, self.walltype, self.map.get_rotation(self.walltype))
        self.wall.map_to_wall()
        self.board.update_object_radar( self.wall )
        
    def draw(self):
        self.board_render.draw_background()        
        self.board_render.draw_board( self.board.get_board(), self.update_map, self.board._board_object_rotation, self.board._board_object_sub_id )
        if (self.draw_mode == 'copy'):
            if (self.valid_start or self.mark_valid):
                self.board_render.draw_copy_paste_box(self.mark_x, self.mark_y, self.mark_x_delta, self.mark_y_delta)
        self.board_render.draw_wall_images()  
        self.board_render.draw_help_screen(self.options.board_generator_help_screen)        
        
        self.board_render.copy_screen_buf_to_screen_hw()        
        self.board_render.flip()
           
    def scale(self, increment):
        self.block_size += increment    
        if self.block_size < 5:
            self.block_size = 5
        self.setup_new_sized_map()
        
    def set_description(self):
        description = tk.simpledialog.askstring('Describe map', 'Enter a description of the map for game host and hall of fame')
        if description:
            self.map.set_description(description)

    def resize(self, axis):        
        x = self.display.board_size[1]
        y = self.display.board_size[0]
        ask = True
        if axis == 'x':
            new_x = tk.simpledialog.askinteger('Set X', 'Enter board size: x', initialvalue=self.display.board_size[1])
            if new_x:
                if new_x < x:
                    ask = self.get_confirmation()    # Ask for confirmation since fiels in old map that are outside new map will be lost
                self.display.board_size = (y, new_x)
        else:
            new_y = tk.simpledialog.askinteger('Set Y', 'Enter board size: y', initialvalue=self.display.board_size[0])
            if new_y:
                if new_y < y:
                    ask = self.get_confirmation() 
                self.display.board_size = (new_y, x)
        
        new_map = np.zeros( (self.display.board_size[0], self.display.board_size[1]), dtype=np.uint8 )
        new_map_effects = np.zeros( (self.display.board_size[0], self.display.board_size[1]), dtype=np.uint8 )
        max_x = min( new_map.shape[1], self.wall.map.shape[1])
        max_y = min( new_map.shape[0], self.wall.map.shape[0])
        
        # Move walls to new map (only for fields that exist in both)
        for a in range(max_x):
            for b in range(max_y):
                new_map[b,a] = self.wall.map[b,a]
                new_map_effects[b,a] = self.wall.map_effects[b,a]
                
        self.wall.map = []
        self.wall.map = new_map
        self.wall.map_effects = []
        self.wall.map_effects = new_map_effects
        self.setup_new_sized_map()  
        self.valid_start = False
        self.prev_valid = False
        self.changes = True
        self.update_caption()
        
    def toggle_line_style(self):
        self.lean_line = not self.lean_line
        self.update_caption()
        
    def toggle_images(self):
        self.local_theme = not self.local_theme
        
        if self.local_theme:
            self.numwalls = len(self.local_map.map_images.walls)
            if self.walltype >= len(self.local_images):
                self.walltype = len(self.local_images) - 1
            self.board_render.draw_images_below_board(self.local_images, self.walltype, fill_color = self.options.local_icon_bg_color)
        else:
            self.numwalls = len(self.map.map_images.walls)
            if self.walltype >= self.numwalls:
                self.walltype = self.numwalls
            self.board_render.draw_images_below_board(self.images, self.walltype, fill_color = self.options.map_icon_bg_color)
        
        self.update_caption()
        
    def toggle_draw_mode(self, back = False):
        if self.draw_mode == 'freehand': # Not a elegant solution. Is there a way to increment position in dictionary?
            if back:
                self.draw_mode = 'copy'
                self.mark_valid = False
                pygame.mouse.set_cursor(*self.draw_cursors[3])
            else:
                self.draw_mode = 'line'
                pygame.mouse.set_cursor(*self.draw_cursors[1])
        elif self.draw_mode == 'line':
            if back:
                self.draw_mode = 'freehand'
                pygame.mouse.set_cursor(*self.draw_cursors[0])
            else:
                self.draw_mode = 'box'
                pygame.mouse.set_cursor(*self.draw_cursors[2])
        elif self.draw_mode == 'box':
            if back:
                self.draw_mode = 'line'
                pygame.mouse.set_cursor(*self.draw_cursors[1])
            else:
                self.draw_mode = 'copy'
                self.mark_valid = False
                pygame.mouse.set_cursor(*self.draw_cursors[3])
        elif self.draw_mode == 'copy':
            if back:
                self.draw_mode = 'box'
                pygame.mouse.set_cursor(*self.draw_cursors[2])
            else:
                self.draw_mode = 'freehand'
                pygame.mouse.set_cursor(*self.draw_cursors[0])        
        else:
            self.draw_mode == 'freehand'
            pygame.mouse.set_cursor(*self.draw_cursors[0])
        self.update_caption()
        
    def toggle_help_screen(self):    
        self.board_render.toggle_help_screen()
        
    def add_all_images(self):    
        if self.local_theme and self.numwalls > 0:
            self.walltype = 1
            for img in range (1, self.numwalls+1):
                in_map, id = self.map.is_image_in_map(os.path.join(map_dir, self.image_dir_relative), self.local_image_list[img])
                if not in_map:
                    id = self.map.add_image_to_map(self.image_dir_relative, self.local_image_list[img])
                    self.setup_new_sized_map()
                self.walltype = id
                
            self.local_theme = False
            self.update_wall_images()
            self.numwalls = len(self.map.map_images.walls)
            self.update_caption()    
                    
