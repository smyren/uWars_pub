##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Main loop

#Import Modules
import pygame
from pygame.locals import *
if not pygame.font: print ('Warning, fonts disabled')

from editor.Interface import *

def main():   
    interface = Interface()

    going = True
    
    # Main board generator loop
    while going: 
        events = pygame.event.get()        
      
        # All input controls. Numbers to change walltype
        for event in events:            
            if event.type == KEYDOWN:   
                if event.key == K_ESCAPE:
                    ask = interface.get_confirmation()
                    if ask == True:
                        going = False
                elif event.key == K_1:
                    interface.set_walltype(0)   
                elif event.key == K_2:
                    interface.set_walltype(1)   
                elif event.key == K_3:   
                    interface.set_walltype(2)   
                elif event.key == K_4:  
                    interface.set_walltype(3)   
                elif event.key == K_5:
                    interface.set_walltype(4)        
                elif event.key == K_6:
                    interface.set_walltype(5)   
                elif event.key == K_7:
                    interface.set_walltype(6)   
                elif event.key == K_8:   
                    interface.set_walltype(7)   
                elif event.key == K_9:  
                    interface.set_walltype(8)   
                elif event.key == K_0:
                    interface.set_walltype(9)                            
                elif event.key == K_p and pygame.key.get_mods() & pygame.KMOD_CTRL:    
                    interface.wall.print_ascii()
                    interface.map.print_ascii()
                elif event.key == K_s and pygame.key.get_mods() & pygame.KMOD_CTRL:       
                    interface.save_file()                               
                elif event.key == K_o and pygame.key.get_mods() & pygame.KMOD_CTRL:     
                    interface.load_file()                   
                elif event.key == K_n and pygame.key.get_mods() & pygame.KMOD_CTRL:     
                    interface.new_map()
                elif event.key == K_f and pygame.key.get_mods() & pygame.KMOD_CTRL:     
                    interface.new_icon_folder()  
                elif event.key == K_z and pygame.key.get_mods() & pygame.KMOD_CTRL:     
                    interface.undo()
                elif event.key == K_y and pygame.key.get_mods() & pygame.KMOD_CTRL:     
                    interface.redo()
                elif event.key == K_x and pygame.key.get_mods() & pygame.KMOD_CTRL:     
                    if interface.draw_mode == 'copy':
                        interface.delete_box()
                elif event.key == K_a and pygame.key.get_mods() & pygame.KMOD_CTRL:     
                    interface.add_all_images()
                elif event.key == K_d and pygame.key.get_mods() & pygame.KMOD_CTRL:        
                    interface.set_description()
                elif (event.key == K_PLUS) or (event.key == K_KP_PLUS) or (event.key == K_p):
                    interface.scale(5)
                elif (event.key == K_MINUS) or (event.key == K_KP_MINUS) or (event.key == K_m):
                    interface.scale(-5)  
                elif event.key == K_d:    
                    interface.toggle_draw_mode()   
                elif event.key == K_x:    
                    interface.resize('x')
                elif event.key == K_y:    
                    interface.resize('y')
               
                elif event.key == K_l:    
                    interface.toggle_line_style()
                elif event.key == K_h:    
                    interface.toggle_help_screen()
                elif event.key == K_i:    
                    interface.toggle_images()
                
                    
            if event.type == QUIT:
                ask = interface.get_confirmation()
                if ask == True:
                    going = False
                
            if event.type == MOUSEBUTTONDOWN:
                if pygame.mouse.get_pressed()==(1,0,0):
                     interface.mouse_click(0)
                elif pygame.mouse.get_pressed()==(0,0,1):
                     interface.mouse_click(2)
                elif event.button == 4:
                    interface.mouse_wheel(1)
                elif event.button == 5:
                    interface.mouse_wheel(-1)
                         
            if event.type == MOUSEBUTTONUP:
                if interface.draw_mode == 'line':
                    interface.add_wall_line()
                elif interface.draw_mode == 'box':
                    interface.add_box()   
                elif interface.draw_mode == 'copy':
                    interface.mark_box()        
                    
            if event.type == MOUSEMOTION:
                if interface.draw_mode == 'line':
                    if interface.valid_start:
                        interface.add_wall_line(True)
                elif interface.draw_mode == 'box':
                    if interface.valid_start:
                        interface.add_box(True)
                elif interface.draw_mode == 'copy':
                    if interface.valid_start:
                        interface.mark_box(True)
                else:
                    if pygame.mouse.get_pressed()==(1,0,0):
                        interface.mouse_click(0, True)
                        
                interface.update_coordinates()
                        
        interface.draw()        
                        
    pygame.quit()
    


           