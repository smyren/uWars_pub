##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Maze generator using recursive backtrack method

import os, sys
from os import walk
import random

sys.path.append("../game_host")
from src.Wall import *
from Ruleset import *

from Options_Maze import *


class Cell():
    def __init__(self):
        self.n = False
        self.s = False
        self.e = False
        self.w = False
        self.visit = False
        self.shortcut_s = False # only checking south and east when carving and shortcutting
        self.shortcut_e = False


class Recursive_Backtrack_Maze():    
    def __init__(self):   
        self.options = Options()
        
        if self.options.random_seed:
            random.seed(self.options.random_seed)
        
        
        self.width  = random.randrange(self.options.cell_width_range[0], self.options.cell_width_range[1] +1,1)
        self.height = random.randrange(self.options.cell_height_range[0], self.options.cell_height_range[1] +1,1)
        
        if self.options.board_size:
            self.x_size = int( (self.options.board_size[0] +1 ) / (self.width  + 1) )
            self.y_size = int( (self.options.board_size[1] +1 ) / (self.height + 1) )
        else:
            self.x_size = random.randrange(self.options.x_range[0], self.options.x_range[1] +1,1)
            self.y_size = random.randrange(self.options.y_range[0], self.options.y_range[1] +1,1)
        
        self.maze = [[Cell() for x in range(self.x_size)] for y in range(self.y_size)]
        self.map = None
        self.wall = None
        
        
        
    def generate_runtime(self):
        self.generate_maze()
        self.generate_map()
        return self.map, self.wall
        
    def generate_and_save(self):
        self.generate_maze()
        self.ascii() 
        self.generate_map()
        self.save_map()      
        
    def generate_maze(self):
        self.carve_passage_from(random.randrange(self.x_size),random.randrange(self.y_size))

    def carve_passage_from(self, x, y):
        self.maze[y][x].visit = True
        direction = [(0,-1),(0,1),(-1,0),(1,0)] # North, south, west, east
        random.shuffle(direction)
        
        for dir in direction:
            next_x = x + dir[0]
            next_y = y + dir[1]
            if next_x >= 0 and next_x < self.x_size and next_y >= 0 and next_y < self.y_size:
                if not self.maze[next_y][next_x].visit:
                    # update wall status and recursive call next cell
                    if dir[0] == -1: # going west
                        self.maze[y][x].w = True
                        self.maze[next_y][next_x].e = True
                    elif dir[0] == 1:
                        self.maze[y][x].e = True
                        self.maze[next_y][next_x].w = True
                    elif dir[1] == -1:
                        self.maze[y][x].n = True
                        self.maze[next_y][next_x].s = True
                    elif dir[1] == 1:
                        self.maze[y][x].s = True
                        self.maze[next_y][next_x].n = True
                    self.carve_passage_from(next_x, next_y)
        
        
    def update_theme(self):
        image_dir_abs = os.path.join(map_dir, self.options.image_directory)
        image_dir_abs = os.path.normpath(image_dir_abs)
        self.theme = []
        f = []
        for (dirpath, dirnames, filenames) in walk(image_dir_abs):
            f.extend(filenames)
            break
            
        f.sort()
            
        # running loop twice to get order correct
        for file in f:
            if file == '_bg.png':
                self.theme.append(file)
                
        for file in f:
            if file == '_bg.png'or file == '_food.png'or file == '_talk.png'or file == 'body6.png' or file == 'head6.png' or file == '_bg_replace.png':
                continue
            else:
                self.theme.append(file)    
        
    def generate_map(self):
        self.update_theme()
        self.map = Map(self.theme, self.options.image_directory)
        text = 'Maze generated by recursive backtrack. Maze size (' + str(self.x_size) + ',' + str(self.y_size) + '). Cell size (' + str(self.width) + ',' + str(self.height) + ')'
        self.map.set_description(text)
        
        x_size = self.x_size * (self.width + 1) -1
        y_size = self.y_size * (self.height + 1) -1
        ruleset = Ruleset()
        self.wall = Wall( (y_size,x_size), id = ruleset.board_id_base.board_object, generate = True )
        
        # first set walls around all cells
        for y in range(self.y_size-1):
            y_map = y*(self.height + 1) + self.height
            for x in range(x_size):
                self.wall.map[y_map,x] = 1
            
        for x in range(self.x_size-1):
            x_map = x*(self.width + 1) + self.width
            for y in range(y_size):
                self.wall.map[y,x_map] = 1
            
        # then clear the openings and count number of walls for shortcut routine
        numwalls = self.y_size*(self.x_size-1) + self.x_size*(self.y_size-1) # before carving        
        for y in range(self.y_size):
            y_map = y*(self.height + 1)
            for x in range(self.x_size):
                x_map = x*(self.width + 1)
                if self.maze[y][x].s:
                    if y < (self.y_size-1):
                        numwalls -= 1
                        for w in range(self.width):
                            self.wall.map[y_map + self.height, x_map + w] = 0
                if self.maze[y][x].e:
                    if x < (self.x_size -1):
                        numwalls -= 1
                        for w in range(self.height):
                            self.wall.map[y_map + w, x_map + self.width ] = 0
        
        # open shortcuts if enabled                    
        shortcuts = int(numwalls * self.options.shortcut_pct / 100)
        while (shortcuts > 0):            
            x   = random.randrange(self.x_size)
            y   = random.randrange(self.y_size)
            dir = random.randrange(2)
            if dir == 0:
                if not self.maze[y][x].s and not self.maze[y][x].shortcut_s and y < (self.y_size-1):
                    shortcuts -= 1
                    self.maze[y][x].shortcut_s = True
                    size = random.randrange(self.options.shortcut_size[0], self.options.shortcut_size[1] +1,1)
                    if size > self.width -1: size = self.width -1
                    y_map = y*(self.height + 1) + self.height
                    x_map = x*(self.width + 1) + 1 + int((self.width-1-size)/2)
                    for w in range(size):
                        self.wall.map[y_map, x_map + w] = 0
                    
            else:
                if not self.maze[y][x].e and not self.maze[y][x].shortcut_e and x < (self.x_size-1):
                    shortcuts -= 1
                    self.maze[y][x].shortcut_e = True
                    size = random.randrange(self.options.shortcut_size[0], self.options.shortcut_size[1] +1,1)
                    if size > self.height -1: size = self.height -1
                    y_map = y*(self.height + 1) + 1 + int((self.height-1-size)/2)
                    x_map = x*(self.width + 1) + self.width
                    for w in range(size):
                        self.wall.map[y_map + w, x_map] = 0
                
                            
        # then replace and rotate if multiple images to draw maze
        if (self.options.image_style == 2):
            print(y_size,x_size)
            for y in range(y_size):
                for x in range(x_size):
                    if (x>0 and x < (x_size -1) and y>0 and y<(y_size-1)): 
                        if self.wall.map[y,x] > 0:
                            if (self.wall.map[y-1,x] > 0 and self.wall.map[y+1,x] > 0 and self.wall.map[y,x-1] > 0 and self.wall.map[y,x+1]): # four walls
                                self.wall.map[y,x] = 5
                            elif (self.wall.map[y-1,x] > 0 and self.wall.map[y,x-1] > 0 and self.wall.map[y,x+1]): # three walls, open down
                                self.wall.map[y,x] = 4
                            elif (self.wall.map[y+1,x] > 0 and self.wall.map[y,x-1] > 0 and self.wall.map[y,x+1]): # three walls, open up
                                self.wall.map[y,x] = 4
                                self.wall.map_effects[y,x] = 2
                            elif (self.wall.map[y-1,x] > 0 and self.wall.map[y+1,x] > 0 and self.wall.map[y,x+1]): # three walls, open left
                                self.wall.map[y,x] = 4
                                self.wall.map_effects[y,x] = 3
                            elif (self.wall.map[y-1,x] > 0 and self.wall.map[y+1,x] > 0 and self.wall.map[y,x-1] > 0): # three walls, open right
                                self.wall.map[y,x] = 4
                                self.wall.map_effects[y,x] = 1
                            elif (self.wall.map[y-1,x] > 0 and self.wall.map[y,x+1] > 0): # two walls, open left and down
                                self.wall.map[y,x] = 3
                            elif (self.wall.map[y-1,x] > 0 and self.wall.map[y,x-1] > 0): # two walls, open right and down
                                self.wall.map[y,x] = 3
                                self.wall.map_effects[y,x] = 1
                            elif (self.wall.map[y+1,x] > 0 and self.wall.map[y,x-1] > 0): # two walls, open up and right
                                self.wall.map[y,x] = 3
                                self.wall.map_effects[y,x] = 2
                            elif (self.wall.map[y+1,x] > 0 and self.wall.map[y,x+1] > 0): # two walls, open left and up
                                self.wall.map[y,x] = 3
                                self.wall.map_effects[y,x] = 3
                            elif (self.wall.map[y,x-1] > 0 and self.wall.map[y,x+1] > 0): # two walls, open left and right 
                                self.wall.map[y,x] = 1
                                self.wall.map_effects[y,x] = 1
                            elif (self.wall.map[y-1,x] > 0 and self.wall.map[y+1,x] > 0): # two walls, open left and right 
                                self.wall.map[y,x] = 1
                            elif (self.wall.map[y+1,x] > 0): # one wall, up
                                self.wall.map[y,x] = 2
                            elif (self.wall.map[y-1,x] > 0): # one wall, down
                                self.wall.map[y,x] = 2
                                self.wall.map_effects[y,x] = 2
                            elif (self.wall.map[y,x-1] > 0): # one wall, left
                                self.wall.map[y,x] = 2
                                self.wall.map_effects[y,x] = 3
                            elif (self.wall.map[y,x+1] > 0): # one wall, right
                                self.wall.map[y,x] = 2
                                self.wall.map_effects[y,x] = 1
                            else:
                                self.wall.map[y,x] = 0 # no walls connected - remove also this (if large shortcuts on all connected walls for instance)
                    else: # edges
                        if self.wall.map[y,x] > 0:
                            if y == 0:
                                if self.wall.map[y+1,x] == 0:
                                    self.wall.map[y,x] = 0
                                else:
                                    self.wall.map[y,x]=2
                            elif y == (y_size -1):
                                if self.wall.map[y-1,x] == 0:
                                    self.wall.map[y,x] = 0
                                else:
                                    self.wall.map[y,x]=2
                                    self.wall.map_effects[y,x] = 2
                            elif x == 0:
                                if self.wall.map[y,x+1] == 0:
                                    self.wall.map[y,x] = 0
                                else:
                                    self.wall.map[y,x]=2
                                    self.wall.map_effects[y,x] = 1
                            elif x == (x_size -1):
                                if self.wall.map[y,x-1] == 0:
                                    self.wall.map[y,x] = 0
                                else:
                                    self.wall.map[y,x]=2
                                    self.wall.map_effects[y,x] = 3
                    
         
    def save_map(self):
        filename = os.path.join(map_dir, self.options.save_name)
        filename = os.path.normpath(filename)
        self.map.save_map(filename, self.wall)  
        
        print('')
        print('saved to file ', filename)
            
    def load_map(self):
        filename = os.path.join(map_dir, self.options.save_name)
        filename = os.path.normpath(filename)
        self.map.load_map(filename, self.wall)  
        
        print('')
        print('saved to file ', filename)
        
    def ascii(self):
        print('')
        print('Generated this amazing maze!')
        print('')
        topp = ' '
        for i in range(self.x_size):
            topp += '_ '
            
        print(topp)
        
        for y in range(self.y_size):
            line = '|'
            for x in range(self.x_size):
                if self.maze[y][x].s:
                    line += ' '
                else:
                    line += '_'
                if self.maze[y][x].e:
                    line += ' '
                else:
                    line += '|'
            print(line)
        
        
        
