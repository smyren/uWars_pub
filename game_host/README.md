# How to run the uWars game host on your computer

## Step 1: 
### Install python 3.5

You do not have to add this to path, but for ease of use install to `C:\python35`

## Step 2: 
### Install the needed python libraries

Run the following command in the `game_host` folder for pip to install the libraries needed

```
C:\python35\Scripts\pip.exe install -r requirements.txt
```

## Step 3: 
### Configure the game rules

Edit `\game_host\Ruleset.py` to configure the game as you wish.  

## Step 4:
### Connect device(s)

Snake controllers and directly connected snake nodes must be connected before starting the game.  
Disconnecting a directly connected snake node will retire it permanently until you restart the game.  
If you are using a snake controller you can connect and disconnect the snake nodes at any time while running the game.

### And/or add python snake(s)

Make a copy of `user_python_template.py` inside `\game_host\user\` and customize it as you wish.

{- NB! -} Combining hardware and python snakes can result in a bit unfair response times,  
however this can be adjusted in `ruleset.user_snake_response_time_offset`.

## Step 5: 
### Start the game

```
\game_host\run.bat
```

### Have fun!