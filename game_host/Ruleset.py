##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

import os


# Rulesets
class Board_id_base():
    def __init__(self):
        self.snake = 2
        self.food = 128
        self.board_object = 129
        self.no_board = 255

class Display():
    def __init__(self):
        self.fullscreen = False
        self.scale_to_map = False
        self.board_size = (30,30)   # Sprites (y,x)
        self.graphics = True         # Enable graphic update on screen
        self.enable_sound = False    #
        self.jingle_interval = 35    # In sec
        self.icon_scroll_speed = 10  # Every x ms
        self.enable_fps = False      # Enable fps


class RulesetGameScore():
    def __init__(self):
        # Specify the name of the class that should be used for calculating the game scoring/ranking
        # The specified name must be a class in the Game_Score.py file
        # self.game_score_class = 'GameScoreMaxAvgSnakeMassPerRound'
        # self.game_score_class = 'GameScoreMaxSnakeMass'
        #self.game_score_class = 'GameScoreMaxAge'
        self.game_score_class = 'GameScoreMaxLength'


class Ruleset_data_export():
    def __init__(self):
        # Enable/disable data export
        self.enable = True

        # File name for the database file, this file and folder will be created if they don't
        # already exist. The full path will be:
        #   <dyn_file_path>/<folder_name>/<file_name>
        # NB the database is shared with the game_monitor, so if the path or file name are changed
        # they must also be changed accordingly in the game_monitor config
        self.file_name = 'game_data.db'
        self.folder_name = 'uwars_live_data'        # The folder where 'file_name' is created

        # Path to where the folder for the live game data will be stored. For best performance a
        # RAM drive will be used. Windows doesn't have RAM drive built in so this will have to be
        # installed separately, eg from http://www.amd.com/en-us/products/memory/ramdisk. If
        # 'dyn_file_path' isn't available the static_file_path will be used instead.
        # NB - no check is made to verify if the path is a RAMDisk, if the path exists it will be
        # used regardless.
        self.dyn_file_path = {'linux': os.path.join(os.sep, 'dev', 'shm'),
                              'windows': os.path.join('U:', os.sep)}

        # Enable backup of game data
        self.enable_backup = True

        # Path for static/permanent data, eg for keeping files from previous games. Is also used
        # for the running game data if the 'dyn_file_path' doesn't exist. Using the users home
        # directory as this should be available on both Windows and Linux.
        self.static_file_path = {'linux': os.path.join('~'),
                                 'windows': os.path.join('~')}
        self.static_folder_name = 'uwars_game_data'

        # Set the minimum time (in seconds) between two writes to the external database. Mainly
        # used to speed up the game when graphics is disabled by not exporting data after each
        # round. Also helps reducing disk activity on Windows systems where the data is stored on
        # the hard drive (on Unix all data is stored in RAM)
        self.min_write_interval = 0.5

        # Path to the file containing the list of previous winners
        root_path = os.path.dirname(os.path.realpath(__file__))
        self.winners_file = os.path.join(root_path, 'game_data', 'game_winners.json')


class Ruleset():
    def __init__(self):

        # Debug options
        self.single_step = False      # One click per any-key-press for debug
        self.com_read_timeout   = 1.0 # Set to None to have infinite timeout. This will support single stepping in atmel studio
        self.get_selftiming = True	  # Enable readout of internal timing measurement, for use with uart devices
        self.snake_scan_interval = 30 # Number of ticks between each scan for snakes.

        # Visuals
        self.display = Display()     # Display settings
        self.snake_comment_time = 10 # Number of tics to display comments
        self.game_speed = 6         # Ticks per s
        self.tile_map = False        # Should a map be repeated over the whole screen
        self.save_local_screenshot = False   # Save screenshots in local folder
        self.map_file_name = 'maps/steinar1.pkl'
        #self.map_file_name = None
        self.generate_map = False     # Autogenerate map when starting game - overrules self.map_file_name if True!

        # Game mode parameters
        self.snake_length = 3        # Respawn length
        self.snake_speed = 1         # Blocks to move per tick
        self.snake_radar = {"top":2, "right":2, "bottom":2, "left":2} # Blocks to send back to snake for navigation

        self.grow_per_food = 1.0     # Blocks to grow per food
        self.grow_per_kill = 0.0     # Blocks to grow per kill
        self.grow_per_tick = 0.0     # Blocks to grow per tick

        self.add_food_per_round = 1  # Need to be whole number
        self.max_food_on_board = -1  # Maximum number of food on board
        self.max_random_food_on_board = 30 # Maximum number of food when adding random food per round
        self.food_age_range = (1500, 5000) # Age (in ticks) of food will be in this range before automatic removal

        self.selfkill = True             # Will snake be killed by colliding into itself
        self.eat_other_snakes = False    # Hitting another snake will case it to die
        self.cut_other_snakes = False      # Hitting another snake will cause a it to be cut. Require eat_other_snakes = True
        self.spawn_food_on_death = True  # Set to False to not spawn food after death

        # Alternative snake controll
        self.enable_keyboard_controlled_snake = True     # Allow anonymous user entering game by pressing the keyboard
        self.keyboard_snake_response_time_offset = 0.0   # Sec
        self.number_of_ai_snakes = 2                     # Number of AI snakes to spawn
        self.ai_snake_response_time_offset = 0.0        # Sec
        self.ai_snakes_eat_other_snakes = False          # Enable to make ai snakes kill on touching other snakes even if "eat_other_snakes" is not set
        self.enable_user_snakes = False
        self.user_snake_response_time_offset = 0.1       # Sec
        self.enable_snake_radar_image = False            # Display radar image for selected snake on screen
        self.hw_snake_response_time_offset = 0.0         # Sec

        # Board defines
        self.board_id_base = Board_id_base()

        # Import data export config
        self.data_export = Ruleset_data_export()

        # Game score config
        self.game_score = RulesetGameScore()
