##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Snake

#Import Modules
import pygame, random, copy
from src.Tools import *
from src.Stats import *
import numpy as np
from src.User_Snake import *

class AI_Snake(User_Snake):
    enable = True
    id = 0xbaba
    name = "KillU"
    def __init__(self, id=None, name=None):
        if id is not None:
            self.id = id
        if name is not None:
            self.name = name

    def update(self, game_packet ):
        movement = game_packet.movements["fast_forward"]
        # Get snake head position
        head_left = game_packet.head_x
        head_top = game_packet.head_y
        
        # Check for food
        food = 128
        if game_packet.radar_image[ head_top + 1, head_left ] == food:
            movement = game_packet.movements["forward"]
        elif game_packet.radar_image[ head_top, head_left + 1 ] == food:
            movement = game_packet.movements["right"]
        elif game_packet.radar_image[ head_top, head_left - 1] == food:
            movement = game_packet.movements["left"]
        else:
            if random.random() > 0.9:
                movement = random.choice([game_packet.movements["left"], game_packet.movements["right"]])

                # Check for collision
                if (movement == game_packet.movements["left"] and game_packet.radar_image[ head_top, head_left - 1 ] == 0 ) or \
                   (movement == game_packet.movements["right"] and game_packet.radar_image[ head_top, head_left + 1 ] == 0 ):
                   return movement
            
            if game_packet.radar_image[ head_top + 1, head_left ] == 0: # Assume radar image is always showing one space in front of snake
                if game_packet.radar_image[ head_top + 2, head_left ] == 0: # Two spaces in front of head
                    movement = game_packet.movements["fast_forward"]
                else:
                    movement = game_packet.movements["forward"]
            elif game_packet.radar_image[ head_top, head_left - 1 ] == 0: # Check left side of snake
                movement = game_packet.movements["left"]
            else: # No other option, then move right
                movement = game_packet.movements["right"]


        return movement
            