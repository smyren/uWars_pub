##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Snake

#Import Modules
import pygame, random, copy
from src.Tools import *
from src.Stats import *
import numpy as np
from src.Keyboard_Snake import *

class Keyboard_Snake1(Keyboard_Snake):
    colors = [(0,0,0),(255,255,255)]            
    icon = np.array( [
            [0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,1,1,0,0,0,0],
            [0,0,0,0,0,1,1,1,0,0,0,0],
            [0,0,0,0,1,1,1,1,0,0,0,0],
            [0,0,0,0,0,1,1,1,0,0,0,0],
            [0,0,0,0,0,1,1,1,0,0,0,0],
            [0,0,0,0,0,1,1,1,0,0,0,0],
            [0,0,0,0,0,1,1,1,0,0,0,0],
            [0,0,0,0,0,1,1,1,0,0,0,0],
            [0,0,0,0,0,1,1,1,0,0,0,0],
            [0,0,0,0,0,1,1,1,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0] ] )
    def event( self, events ):
        for event in events:
            if event.type == KEYDOWN:
                if event.key == K_LEFT:
                    self.move = "left"
                elif event.key == K_RIGHT:
                    self.move = "right"
                elif event.key == K_UP:
                    self.move = "up"
                elif event.key == K_DOWN:
                    self.move = "down"
                    
class Keyboard_Snake2(Keyboard_Snake):
    colors = [(255,255,255),(0,0,0)]
    icon = np.array( [
            [0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,1,1,1,1,0,0,0,0],
            [0,0,0,1,1,1,1,1,1,0,0,0],
            [0,0,0,1,1,0,0,1,1,0,0,0],
            [0,0,0,0,0,0,0,1,1,0,0,0],
            [0,0,0,0,0,0,1,1,1,0,0,0],
            [0,0,0,0,0,1,1,1,0,0,0,0],
            [0,0,0,0,1,1,1,0,0,0,0,0],
            [0,0,0,1,1,1,0,0,0,0,0,0],
            [0,0,0,1,1,1,1,1,1,0,0,0],
            [0,0,0,1,1,1,1,1,1,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0] ] )
    def event( self, events ):
        for event in events:
            if event.type == KEYDOWN:
                if event.key == K_a:
                    self.move = "left"
                elif event.key == K_d:
                    self.move = "right"
                elif event.key == K_w:
                    self.move = "up"
                elif event.key == K_s:
                    self.move = "down"
