##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

import serial, time, ctypes
import serial.tools.list_ports
import numpy as np
from src.User_Snake import *

D_CMD_PING          = 0
D_CMD_ID            = 1
D_CMD_STATUS        = 2
D_CMD_SN_COMMAND    = 3
D_CMD_ERROR         = 66

# Error types
D_ERROR_SNAKE_NODE_NOT_CONNCETED = 100

D_CMD_SN_ID		    = 100
D_CMD_SN_NAME	    = 101
D_CMD_SN_COMMENT    = 102
D_CMD_SN_ICON	    = 103
D_CMD_SN_COLORS	    = 104
D_CMD_SN_OWNER	    = 105
D_CMD_SN_INFO	    = 106
D_CMD_SN_DBG_TIMING = 107
D_CMD_SN_MOVE	    = 110

D_ERROR_SNAKE_NODE_NOT_CONNCETED = 100

BAUD_RATE = 115200
D_RESPONSE_TIME_FACTOR = 1.0 / (32 * 1000000)

D_CMD_SN_MOVE_STAUS_FLAG	    = 7
D_CMD_SN_MOVE_MOVE_SLOW_FLAG	= 3

class Snake_Node(User_Snake):
    response_time = -1
    def __init__( self, updateFunc, ctrl, node ):
        super( User_Snake, self ).__init__()
        self.updateFunc = updateFunc
        self.ctrl = ctrl
        self.node = node
        
    def update( self, game_packet ):
        response_time, movement, comment, slow_move = self.updateFunc( self.ctrl, self.node, game_packet )
        self.response_time = response_time
        self.comment = comment
        self.slow_move = slow_move
        
        return movement
    
    def activate( self ):
        self.enable = True

    def deactivate( self ):
        self.enable = False
        
    def respawn( self, force_enable ):
        return self.enable
    
    def kill( self ):
        pass        

class SC():
    def __init__(self, ser, type):
        self.enable = True
        self.ser = ser
        self.type = type
        self.nodes = [0,0,0,0,0]
    
class Snake_Controller():
    controllers = []
    scan_for_hw_change = True
    def __init__(self, read_timeout, debug_timing):
        self.debug_timing = debug_timing
        
        # coms = []
        types, coms = self.find()
        for i, com in enumerate(coms):
            print("Snake Controller Found type '{0}', found on '{1}'".format( types[i], com ) )
            try:
                ser = serial.Serial( com, baudrate=BAUD_RATE, timeout=read_timeout, write_timeout=0 )
            except:
                print("Failed to open: {0}".format(com) )
            else:
                if ser:
                    self.add_controller( ser, types[i] )
                else:
                    print("Init com returned None: {0}".format( com ) )
                    
    def __del__( self ):
        for ctrl in self.controllers:
            ctrl.ser.close()

    def add_controller( self, controller, controller_type ):
        self.controllers.append( SC(controller, controller_type ) )
                
    def find( self ):
        coms = []
        types = []
        avail_ports = serial.tools.list_ports.comports()
        for com in avail_ports:
            if (com.description.find("USB Serial Device") == 0) or (com.description.find("Communication Device Class ASF example") == 0) or (com.description.find("uWars") == 0) or (com.description.find("mEDBG") == 0):
                try:
                    ser = serial.Serial( com.device, baudrate=BAUD_RATE, timeout=1.0, write_timeout=0 )
                except:
                    pass
                else:
                    if self.is_snake_controller( ser ):
                        type = "SC"
                        types.append( type )
                        coms.append( com.device )
                        ser.close()
                    elif self.is_snake_node( ser ):
                        type = "SN"
                        types.append( type )
                        coms.append( com.device )
                        ser.close()
                    
        return types, coms

    def read(self, ser=None):
        return_data = []
        read_ok = False
        going = True
        if ser is None:
            print("Snake_Controller.read: Automatic reading all SC not implemented yet")
            # for ser in self.coms:
                # return_data.append( proc_data )
        else:
            try:
                read_data = np.frombuffer(ser.read(), dtype=np.uint8)
                # print("Snake_Controller.read data first:", read_data, type(read_data), read_data.size)
                if read_data.size == 0:
                    print("Snake_Controller.read: Timed out")
                    return_data.append( np.array([]) )
                    return return_data

                while going:
                    if read_data.size == read_data[0]: # Check if data is valid
                        read_ok = True
                        going = False
                    elif read_data.size > read_data[0]:
                        going = False
                        print("Snake_Controller.read: Too much data", read_data.size, read_data[0])
                    else:
                        temp_read_data = np.frombuffer(ser.read(), dtype=np.uint8)
                        if temp_read_data.size == 0: # Read timed out. No more data
                            print("Snake_Controller.read: Timed out2", read_data.size, read_data[0])
                            going = False
                        else:
                            read_data = np.concatenate( (read_data, temp_read_data) )
                            nr_bytes = ser.in_waiting
                            read_data = np.concatenate( (read_data, np.frombuffer(ser.read(nr_bytes), dtype=np.uint8)) )
                            # print("Snake_Controller.read data next:", read_data)
            except serial.serialutil.SerialException as iik:
                Snake_Controller.scan_for_hw_change = True
                return_data = []
                return_data.append( np.array([]) )
                return return_data

            if read_ok:
                return_data.append(read_data)
            else:
                print("Snake_Controller.read:", read_data)
                return_data.append( np.array([]) )
                
        # print("Snake_Controller.read data:", return_data)

        return return_data

    def write(self, in_data, ser=None):
        write_data = None
        if type(in_data) == list:
            write_data = np.array([len(in_data)+1] + in_data, dtype=np.uint8)
        elif type(in_data)==np.ndarray:
            write_data = np.concatenate( ([in_data.size + 1], in_data) )
            
        if write_data is not None:
            # print("Snake_Controller.write:", write_data)
            if ser is None:
                for ctrl in self.controllers:
                    ctrl.ser.write(write_data)
            else:
                # print(write_data)
                try:
                    ser.write(write_data)
                except:
                    print("Snake_Controller.write: FAILED")
        else:
            print('Snake_Controller.write: Expecting list as input')
    
    def is_snake_controller( self, ser ):
        expString = 'Snake Controller'
        writeList = [D_CMD_ID]
        self.write( writeList, ser )
        readStr = self.read(ser)
        # print("Snake_Controller.is_snake_controller:", readStr)
        if readStr[0].size > 2 and readStr[0][1] == D_CMD_ID:
            readStr = self.npchar_to_string(readStr[0][2:])
            # print("is_snake_controller:", readStr)
            if readStr == expString:
                return True
        else:
            print("Snake_Controller.is_snake_controller: Timed Out")

        return False

    def is_snake_node( self, ser ):
        expString = 'Snake Node'
        writeList = [D_CMD_ID]
        self.write( writeList, ser )
        readStr = self.read(ser)

        try:
            if readStr[0].size > 2 and readStr[0][1] == D_CMD_ID:
                readStr = self.npchar_to_string(readStr[0][2:])
                # print("is_snake_node:", readStr)
                if readStr == expString:
                    return True
        except IndexError:
            pass

        return False

    def get_snake_controller_status( self ):
        writeStr = chr(0) + 'S' + chr(0) + chr(0) + chr(0) + chr(1) + chr(0)
        for c, ctrl in enumerate(self.controllers):
            if ctrl.type == 'SC':
                ctrl.ser.write( writeStr.encode() )
                print( ctrl.ser.read(14) )
            elif ctrl.type == 'SN':
                print('Get controller status of snake node. What to do....?')

    def get_new_snake_nodes( self ):
        new_nodes = []
        nodes_list = []
        removed_nodes = []
        
        # Add/remove nodes
        for ctrl in self.controllers:
            if ctrl.enable:
                nodes_list = self.get_nodes( ctrl )
                if nodes_list is None:
                    # print("Snake_Controller.get_new_snake_nodes: Controller disconnected", ctrl)
                    ctrl.enable = False
                    for node in range(len(ctrl.nodes)):
                        removed_nodes.append( (ctrl.ser, node) )
                    
                else:
                    for node in range(len(ctrl.nodes)):
                        if node < len(nodes_list):
                            if nodes_list[node] == 1 and ctrl.nodes[node] == 0: # Add node
                                print("Found new snake node:", node, "at", ctrl.ser.name )
                                ctrl.nodes[node] = 1
                                sif = Snake_Node( self.update_snake_node, ctrl, node ) 
                                if not self.get_snake_node_info( sif ):
                                    sif.deactivate()
                                    ctrl.nodes[node] = 0
                                    # removed_nodes.append( (ctrl.ser, node) ) # Do not need to remove since it's not been added
                                else:
                                    sif.activate()
                                    new_nodes.append( sif )
                                
                            elif nodes_list[node] == 0 and ctrl.nodes[node] == 1: # Remove node
                                print("Lost snake node:", node, "at", ctrl.ser.name )
                                ctrl.nodes[node] = 0
                                removed_nodes.append( (ctrl.ser, node) )
                        else:
                            # Remove nodes not in the returned list of active nodes
                            ctrl.nodes[node] = 0
                            removed_nodes.append( (ctrl.ser, node) )
                        
                                
        return new_nodes, removed_nodes
    
    def get_nodes( self, ctrl ):
        connected = None
        if ctrl.type == 'SC':
            # print("Ask Snake Controller about what nodes are connected")
            writeList = [D_CMD_STATUS]
            self.write( writeList, ctrl.ser )
            
            read_data = self.read(ctrl.ser)
            # print("Snake_Controller.get_nodes:", read_data)
            if read_data[0].size > 2:
                connected = read_data[0][2:]
            else:
                print("Snake_Controller.get_nodes: Timed Out")
                
        elif ctrl.type == 'SN':
            # print("Check if Snake Node is still connected")
            if self.is_snake_node( ctrl.ser ):
                connected = [1]
            else:
                connected = [0]
                ctrl.type = 'NA'
        elif ctrl.type == 'NA':
            pass
        else:
            print("Snake_Controller.sn_write: Unknown controller type", ctrl.type)
                
        return connected

    def sn_write( self, sn_data, ctrl, node ):
        if ctrl.type == 'SC':
            timingL = 0
            timingH = 0
            bytes = 0
            sn_data_size = len(sn_data)+1
            # data = np.concatenate((np.array([ D_CMD_SN_COMMAND, node, timingL, timingH, bytes, sn_data_size ], dtype=np.uint8), np.array(sn_data, dtype=np.uint8) ) )
            data = [ D_CMD_SN_COMMAND, node, timingL, timingH, bytes, sn_data_size ] + list(sn_data)
        elif ctrl.type == 'SN':
            data = list(sn_data)
        else:
            print("Snake_Controller.sn_write: Unnknown controller type", ctrl.type)
            
        # print( "Snake_Controller.sn_write:", data)
        self.write( data, ctrl.ser )

    def sn_read( self, ctrl, node ):
        info_packet = self.read(ctrl.ser)
        sn_data = None
        sc_data = None

        try:
            if len(info_packet[0]) >= 2:
                if ctrl.type == 'SC':
                    # print( "Snake_Controller.sn_read:", info_packet)
                    size    = info_packet[0][0]
                    command = info_packet[0][1]
                    if command == D_CMD_SN_COMMAND and len(info_packet[0]) > 8:
                        target  = info_packet[0][2]
                        timingL = info_packet[0][3]
                        timingH = info_packet[0][4]
                        powerL  = info_packet[0][5]
                        powerH  = info_packet[0][6]
                        sn_data = info_packet[0][7:]
                        if sn_data.size < 2:
                            # Invalid package
                            sn_data = None
                        else:
                            sc_data = [ctypes.c_int32(timingL + (timingH << 8)).value, ctypes.c_int32(powerL + (powerH << 8)).value, target]

                elif ctrl.type == 'SN':
                    sn_data = info_packet[0]
                    sc_data = [0, 0, node]
                else:
                    print("Snake_Controller.sn_write: Unknown controller type", ctrl.type)
            else:
                print("Too small packet, got {0} bytes, at least 2 required".format(info_packet[0]))
        except IndexError:
            print("Too small packet, got {0} bytes, at least 2 required".format(len(info_packet)))

        return [sn_data], [sc_data]

    def get_snake_node_info( self, sif ):
        # Get id, name, icon, comment +++
        # print("Ask Snake Node for updated info")
        # try:
        self.sn_write( [D_CMD_SN_ID], sif.ctrl, sif.node )
        info_packet, sc_data = self.sn_read(sif.ctrl, sif.node)
        if (info_packet[0] is not None) and (info_packet[0].size > 2 ):
            sif.id = self.npnr_to_string( info_packet[0][2:] )
            if sif.id == "":
                return False
        else:
            print("Snake_Controller.get_snake_node_info: id - wrong size on info_packet")
            return False
        # print( "sif.id:", sif.id )

        self.sn_write( [D_CMD_SN_NAME], sif.ctrl, sif.node )
        info_packet, sc_data = self.sn_read(sif.ctrl, sif.node)
        if (info_packet[0] is not None) and (info_packet[0].size > 2 ):
            sif.name = self.npchar_to_string( info_packet[0][2:] )
        else:
            print("Snake_Controller.get_snake_node_info: name - wrong size on info_packet")
            return False
        # print( "sif.name:", sif.name )

        self.sn_write( [D_CMD_SN_COMMENT], sif.ctrl, sif.node )
        info_packet, sc_data = self.sn_read(sif.ctrl, sif.node)
        if (info_packet[0] is not None) and (info_packet[0].size > 1 ):
            sif.comment = self.npchar_to_string( info_packet[0][2:] )
        else:
            print("Snake_Controller.get_snake_node_info: comment - wrong size on info_packet")
            return False
        # print( "sif.comment:", sif.comment )

        self.sn_write( [D_CMD_SN_ICON], sif.ctrl, sif.node )
        info_packet, sc_data = self.sn_read(sif.ctrl, sif.node)
        if (info_packet[0] is not None) and (info_packet[0].size == 2+(12*12) ):
            sif.icon = np.reshape(info_packet[0][2:], (12,12))
        else:
            print("Snake_Controller.get_snake_node_info: icon - wrong size on info_packet")
            return False
        # print( "sif.icon:", sif.icon )
        
        self.sn_write( [D_CMD_SN_COLORS], sif.ctrl, sif.node )
        info_packet, sc_data = self.sn_read(sif.ctrl, sif.node)
        if (info_packet[0] is not None) and (info_packet[0].size > 6 ):
            sif.colors = [(info_packet[0][2],info_packet[0][3],info_packet[0][4]),(info_packet[0][5],info_packet[0][6],info_packet[0][7])]
        else:
            print("Snake_Controller.get_snake_node_info: colors - wrong size on info_packet")
            return False
        # print( "sif.colors:", sif.colors )

        self.sn_write( [D_CMD_SN_OWNER], sif.ctrl, sif.node )
        info_packet, sc_data = self.sn_read(sif.ctrl, sif.node)
        if (info_packet[0] is not None) and (info_packet[0].size > 1 ):
            sif.owner = self.npchar_to_string( info_packet[0][2:] )
        else:
            print("Snake_Controller.get_snake_node_info: owner - wrong size on info_packet")
            return False
        # print( "sif.owner:", sif.owner )
        
        self.sn_write( [D_CMD_SN_INFO], sif.ctrl, sif.node )
        info_packet, sc_data = self.sn_read(sif.ctrl, sif.node)
        if (info_packet[0] is not None) and (info_packet[0].size > 1 ):
            sif.info = self.npchar_to_string( info_packet[0][2:] )
        else:
            print("Snake_Controller.get_snake_node_info: info - wrong size on info_packet")
            return False
        # print( "sif.info:", sif.info )
        # except:
            # print("Snake_Controller.sn_get_snake_node_info: Failed")
            # return False
            
        return True
        
    def update_snake_node( self, ctrl, node, game_packet ):
        # print("Send Game packet to Snake Node", node, "at", ctrl.ser.name ," and wait for feedback")
        comment = ""
        writeList = self.game_packet_to_snake_node_data( game_packet )
        
        self.sn_write( writeList, ctrl, node )
        read_data, sc_data = self.sn_read(ctrl, node)
        response_time = -1
        movement = 0
        try:
            if sc_data[0][2] == node:
                if read_data[0][1] == D_CMD_SN_MOVE:
                    movement = read_data[0][2]
                    response_time = sc_data[0][0]
                    #print(response_time)
                    if response_time > 0:
                        response_time = response_time * D_RESPONSE_TIME_FACTOR
                    #print("response_time:", response_time)
                else:
                    print("Snake_Controller.sn_update_snake_node: Unexpected response from Snake Node:", read_data)
            else:
                print("Snake_Controller.sn_update_snake_node: Got response from wrong node. Expected", node, "got", sc_data[0][2])
        except:
            print("Snake_Controller.update_snake_node: Com failure", read_data)
        
        # Check for comment
        if movement & (1 << D_CMD_SN_MOVE_STAUS_FLAG):
            # print("Snake_Controller.sn_update_snake_node: Snake status bit set", movement)
            self.sn_write( [D_CMD_SN_COMMENT], ctrl, node )
            info_packet, sc_data = self.sn_read(ctrl, node)
            try:
                comment = self.npchar_to_string( info_packet[0][2:] )
            except:
                pass

        # Check move slow flag
        slow_move = False
        if movement & (1 << D_CMD_SN_MOVE_MOVE_SLOW_FLAG):
            slow_move = True
        
        movement &= 0x03 # Only use 2 first bits for movements
        
        if ctrl.type == 'SN' and self.debug_timing == True:
            self.sn_write( [D_CMD_SN_DBG_TIMING], ctrl, node)
            info_packet, sc_data = self.sn_read(ctrl, node) #??
            try:
                response_time = info_packet[0][2] | (info_packet[0][3] << 8)
                if response_time > 0:
                    response_time = response_time * D_RESPONSE_TIME_FACTOR
            except:
                pass
        
        
        return response_time, movement, comment, slow_move
    
    
    def game_packet_to_snake_node_data( self, game_packet ):
        rotation_map = {0:0, 90:1, 180:2, 270:3}
        data = np.array([D_CMD_SN_MOVE, game_packet.respawn, rotation_map[game_packet.rotation], int(game_packet.length) & 0x00ff, int(game_packet.length) >> 8, \
                        game_packet.active_snakes, game_packet.ranking, \
                        game_packet.radar_image.shape[1], game_packet.radar_image.shape[0] ], dtype=np.uint8 )
        # data = np.concatenate( (data, game_packet.radar_image.flatten()))
        data = np.concatenate( (data, self.normalize_radar_image( game_packet.radar_image, game_packet.head_y, game_packet.head_x).flatten()))
        
        # data[0] = data.size
        # print(data)
        return data

    def normalize_radar_image( self, radar_image, center_y, center_x):
        radar_image_norm = np.zeros((11,11), dtype=np.uint8 )
        top = int(radar_image_norm.shape[0]/2) - center_y
        left = int(radar_image_norm.shape[1]/2) - center_x
    
        radar_image_norm[top:top+radar_image.shape[0],left:left+radar_image.shape[1]] = radar_image[:]
        # print(radar_image_norm, center_y, center_x)
        
        return radar_image_norm
        
    def npchar_to_string( self, data ):
        return (''.join(chr(e) for e in data)).rstrip(chr(0))

    def npnr_to_string( self, data ):
        return (''.join(str(e) for e in data)).rstrip(chr(0))
        
        
# sc = Snake_Controller()
# sc.get_snake_controller_status()
            
