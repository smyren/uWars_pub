##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Board

#Import Modules
from src.Tools import *
from src.Board_Render import *

from src.Snake import *
from src.Food import *
from src.Wall import *

snake_images = [["body6.png"], ["head6.png"]]

class Board_objects():
    def __init__( self, board_id_base, number_of_objects ):
        self._ids = ["Outer Wall"]
        self._board_ids = [board_id_base.no_board]
        self.add_object( 0, 0 ) # Open space image
        self.add_object( board_id_base.food, board_id_base.food ) # Add food
        for i in range(number_of_objects):
            self.add_object( board_id_base.board_object+i, board_id_base.board_object+i )

    def add_object( self, id, board_id ):
        # print("Board_objects.add_object: Adding", id, board_id, image_name)
        self._ids.append(id)
        self._board_ids.append(board_id)

    def get_id_by_board_id(self, board_id):
        if board_id in self._board_ids:
            return self._ids[ self._board_ids.index( board_id ) ]
        else:
            return None

    def get_board_id_by_id(self, id):
        if id in self._ids:
            return self._board_ids[ self._ids.index( id ) ]
        else:
            return None

class Board():
    _board = None
    _board_object_rotation = None
    _board_object_sub_id = None
    _board_objects = None
    _next_snake_id = 0

    def __init__( self, board_id_base, board_size, number_of_objects ):
        self._board = np.zeros( (board_size[0], board_size[1]), dtype=np.uint8 ) # Support 256 different element.
        self._pre_board = None
        self._board_object_rotation = np.zeros( self._board.shape, dtype=np.int )
        self._pre_board_object_rotation = None
        self._board_object_sub_id = np.zeros( self._board.shape, dtype=np.int )
        self._pre_board_object_sub_id = None
        self._board_objects = Board_objects( board_id_base, number_of_objects )

    def get_board(self):
        return self._board.copy()

    def get_board_size(self):
        return self._board.shape

    def get_radar_image( self, part, snake_radar ):
        if part.rotation == 90:
            slice_x = part.x - snake_radar["top"]
            slice_x_end = part.x + snake_radar["bottom"] + 1
            slice_y = part.y - snake_radar["left"]
            slice_y_end = part.y + snake_radar["right"] + 1
            sized_image = np.zeros( (snake_radar["left"]+snake_radar["right"] + 1, snake_radar["bottom"]+snake_radar["top"]+1), dtype=np.uint8 ) + self._board_objects.get_board_id_by_id( "Outer Wall" )
            # board_objects = np.rot90( board_objects )
        elif part.rotation == 270:
            slice_x = part.x - snake_radar["bottom"]
            slice_x_end = part.x + snake_radar["top"] + 1
            slice_y = part.y - snake_radar["right"]
            slice_y_end = part.y + snake_radar["left"] + 1
            sized_image = np.zeros( (snake_radar["left"]+snake_radar["right"] + 1, snake_radar["bottom"]+snake_radar["top"]+1), dtype=np.uint8 ) + self._board_objects.get_board_id_by_id( "Outer Wall" )
            # board_objects = np.rot90( board_objects, 3 )
        elif part.rotation == 180:
            slice_x = part.x - snake_radar["right"]
            slice_x_end = part.x + snake_radar["left"] + 1
            slice_y = part.y - snake_radar["top"]
            slice_y_end = part.y + snake_radar["bottom"] + 1
            sized_image = np.zeros( (snake_radar["bottom"]+snake_radar["top"]+1, snake_radar["left"]+snake_radar["right"] + 1), dtype=np.uint8 ) + self._board_objects.get_board_id_by_id( "Outer Wall" )
            # board_objects = np.rot90( board_objects, 2 )
        else:
            slice_x = part.x - snake_radar["left"]
            slice_x_end = part.x + snake_radar["right"] + 1
            slice_y = part.y - snake_radar["bottom"]
            slice_y_end = part.y + snake_radar["top"] + 1
            sized_image = np.zeros( (snake_radar["bottom"]+snake_radar["top"]+1, snake_radar["left"]+snake_radar["right"] + 1), dtype=np.uint8 ) + self._board_objects.get_board_id_by_id( "Outer Wall" )

        radar_image = self._board[ max(0,slice_y):min(self._board.shape[0],slice_y_end), max(0,slice_x):min(self._board.shape[1],slice_x_end)  ].copy()
        
        # Check if radar image has the correct size. If not resize
        #sized_image = np.zeros( (snake_radar["bottom"]+snake_radar["top"]+1, snake_radar["left"]+snake_radar["right"] + 1), dtype=np.uint8 ) + self._board_objects.get_board_id_by_id( "Outer Wall" )
        if radar_image.shape != sized_image.shape:
            radar_posx = 0
            radar_posy = 0
            if slice_x < 0:
                radar_posx = -slice_x
            if slice_y < 0:
                radar_posy = -slice_y

            sized_image[radar_posy:radar_posy+radar_image.shape[0],radar_posx:radar_posx+radar_image.shape[1]] = radar_image[:]
        else:
            sized_image = radar_image

        if part.rotation == 90:
            sized_image = np.rot90( sized_image )
        elif part.rotation == 270:
            sized_image = np.rot90( sized_image, 3 )
        elif part.rotation == 180:
            sized_image = np.rot90( sized_image, 2 )


        head_position = (snake_radar["bottom"], snake_radar["left"])
        return sized_image, head_position

    def get_change_array(self ):

        # Need to check for change on rotation also!

        if self._pre_board is None:
            # First update
            changes = (self._board == self._board)
        else:
            changes = ((self._board != self._pre_board) | (self._board_object_sub_id != self._pre_board_object_sub_id) | (self._board_object_rotation != self._pre_board_object_rotation))

        # print(self._board.shape, changes.shape)
        self._pre_board = self.get_board()
        self._pre_board_object_sub_id = self._board_object_sub_id.copy()
        self._pre_board_object_rotation = self._board_object_rotation.copy()
        return changes

    def update_object_radar(self, object, object_id_base=None, board_render=None, overwrite = True ):
        is_snake = False
        if type(object) == Snake:
            is_snake = True
            # Assign object id to snake
            if self._board_objects.get_board_id_by_id( object.id ) is None:
                self._board_objects.add_object( object.id, object_id_base.snake + self._next_snake_id, )
                self._board_objects.add_object( object.id, object_id_base.snake + self._next_snake_id + 1 )
                board_render._board_render_objects.add_object( object_id_base.snake + self._next_snake_id, snake_images[0], object.color, None )
                board_render._board_render_objects.add_object( object_id_base.snake + self._next_snake_id + 1, snake_images[1], object.color, object.icon, True )
                self._next_snake_id += 2

        object_id = self._board_objects.get_board_id_by_id( object.id )
        # Delete current object from board radar
        current_object_positions = (self._board==object_id)
        np.place( self._board, current_object_positions, 0 ) # Remove current object from board radar
        np.place( self._board_object_rotation, current_object_positions, 0 )
        np.place( self._board_object_sub_id, current_object_positions, 0 )
        if is_snake:
            current_object_positions = (self._board==object_id+1)
            np.place( self._board, current_object_positions, 0 ) # Remove head from board radar
            np.place( self._board_object_rotation, current_object_positions, 0 )
            np.place( self._board_object_sub_id, current_object_positions, 0 )

        # Add objects to radar
        # print(object.parts[0].position, self.block_to_position( object.parts[0].position ))
        for i in range(len(object.parts)-1,-1,-1):
            if is_snake and i == 0:
                # +1 for snake head
                id = object_id + 1
            else:
                id = object_id

            if overwrite:
                self._board[ object.parts[i].position ] = id  # Add element to board radar
                self._board_object_rotation[ object.parts[i].position ] = object.parts[i].rotation
                self._board_object_sub_id[ object.parts[i].position ] = object.parts[i].subtype
            else:
                if not self._board[ object.parts[i].position ]:
                    self._board[ object.parts[i].position ] = id # Add element to board radar
                    self._board_object_rotation[ object.parts[i].position ] = object.parts[i].rotation
                    self._board_object_sub_id[ object.parts[i].position ] = object.parts[i].subtype
