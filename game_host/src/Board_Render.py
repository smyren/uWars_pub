##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##


import os, pygame
from pygame.compat import geterror
from pygame.locals import *
import numpy as np
import datetime

from src.Tools import *

import sys
import ctypes
import random, time

# Image locations
# Should be changed to board dir when level editor is ready
data_dir = os.path.join(main_dir, '../data')
screenshot_dir = os.path.join(main_dir, '../screenshots')
export_images_dir = 'game_host_images'

g_help_image_name = os.path.join(data_dir, 'help.png')
g_snake_info_image_name = os.path.join(data_dir, 'info.png')
sound_effects = ["Speedy_dues2.wav"]
game_font = 'novem___.ttf'

class Board_Render_Objects():
    _mode = "Normal" # "Normal", "Terminal", "Matrix"
    _replace_color = [(0, 0, 0),(255, 0, 0),(0,255, 0)] # Image colors to replace (snake base, snake icon, background)
    _gameregion_color = (0, 0, 50)
    _gameregion_color_matrix = (0,0,30)
    _board_ids = []
    _sub_ids = []
    _images = []
    _icon_images = []
    _images_matrix = []
    _image_size = 0
    _icon_image_size = 0
    _user_graphics_size = (12,12)
    _user_graphics_position = (9,9)
    _effect_images = []
    _jingle = None

    def __init__( self, board_id_base, theme, image_size, icon_size ):
        self.path = theme.path
        try:
            bg_replace_image, rect = self.load_image('_bg_replace.png')
        except:
            self.img_replace = False
            self.img_replace_size = (0,0)
        else:
            self.img_replace = True
            self.replace_array = pygame.PixelArray(bg_replace_image)
            self.img_replace_size = self.replace_array.shape
            
        self._jingle = self.load_sound(sound_effects[0])
        self._image_size = image_size
        self._icon_image_size = icon_size
        self.add_object( 0, theme.board_images[0] ) # Empty space image
        self.add_object( board_id_base.food, theme.board_images[1] ) # Food
        for i, images in enumerate( theme.board_images[2:] ):
            self.add_object( board_id_base.board_object+i, images, use_path = False )

        self.load_effect_images( theme.effect_images )

    def cleanup(self):
        self._board_ids = []
        del self._sub_ids[:]
        del self._images[:]   # This is the important one for board generator resize... so hard to find :S
        self._icon_images = []
        self._images_matrix = []
        self._effect_images = []

    def load_effect_images( self, effect_images ):
        for image_name in effect_images:
            image, rect = self.load_image( image_name, -1, (60, 60))
            image.convert()
            self._effect_images.append( image )

    def get_effect_image( self, effect ):
        if effect == "talk":
            return self._effect_images[0]

    def get_active_objects( self ):
        return len( self._board_ids )

    def get_color(self, id, bg_color):
        r = (id >> 6)&3
        g = (id >> 4)&3
        b = (id >> 2)&3
        a = id&3
        red   = int( ( r * a/3 *85) + ( bg_color[0]*(3-a) /3 ) )
        green = int( ( g * a/3 *85) + ( bg_color[1]*(3-a) /3 ) )
        blue  = int( ( b * a/3 *85) + ( bg_color[2]*(3-a) /3 ) )

        return((red, green, blue))

    def add_object( self, board_id, image_names, colors=None, icon=None, load_icon_image=False, use_path = True ):
        # print("Board_Render_Objects.add_object: Adding", board_id, image_names)
        self._board_ids.append(board_id)
        self._images.append([]) # Create new images list
        self._icon_images.append([]) # Create new images list
        self._images_matrix.append([]) # Create new images list
        self._sub_ids.append([])
        for sub_id, image_name in enumerate(image_names):
            image, rect = self.load_image( image_name, None, build_path = use_path)
            pxarray = pygame.PixelArray(image)
            if self.img_replace and pxarray.shape == self.img_replace_size:
                bg_color = (self._replace_color[2][0] << 16) + (self._replace_color[2][1] << 8) + self._replace_color[2][2]
                for x in range(0, pxarray.shape[0]):
                        for y in range (0, pxarray.shape[1]):
                            if pxarray[x,y] == bg_color:
                                pxarray[x,y] = self.replace_array[x,y]
            else:
                pxarray.replace( self._replace_color[2], self._gameregion_color ) # Set background color
            if (colors is not None) and (icon is not None):
                # check if icon is using new 8 bit system
                new_icon_style = False
                for x in range(min(self._user_graphics_size[0],icon.shape[1])):
                    for y in range(min(self._user_graphics_size[1],icon.shape[0])):
                        if icon[y,x] > 1:
                            new_icon_style = True
                if colors[0] == colors[1]: new_icon_style = True
                pxarray.replace( self._replace_color[0], colors[0] )
                pxarray.replace( self._replace_color[1], colors[0] )
                x_offset = self._user_graphics_position[0]
                y_offset = self._user_graphics_position[1]
                for x in range(min(self._user_graphics_size[0],icon.shape[1])):
                    for y in range(min(self._user_graphics_size[1],icon.shape[0])):
                        if new_icon_style:
                            color = self.get_color(icon[y,x], colors[1])
                            image.set_at((x_offset+x, y_offset+y), color)
                        else:
                            if icon[y,x]:
                                image.set_at((x_offset+x, y_offset+y), colors[1])


            # Change color
            elif colors is not None:
                pxarray.replace( self._replace_color[0], colors[0] )
                pxarray.replace( self._replace_color[1], colors[1] )

            del pxarray # Required to prevent locking surface
            self._images[-1].append( pygame.transform.smoothscale( image, (self._image_size, self._image_size) ).convert() )
            if load_icon_image:
                image.set_colorkey( image.get_at((0,0)), RLEACCEL )
                self._icon_images[-1].append( pygame.transform.scale( image, (self._icon_image_size, self._icon_image_size) ).convert() )

            text_surface = pygame.Surface( (30, 30) )
            text_surface.fill( self._gameregion_color_matrix )
            font = pygame.font.Font(None, 24 )
            line = "{:^3}".format(str(board_id))
            # colors = None # Override for now since the display is nicer
            if colors is None:
                font_image = font.render(line, 1, (0,150,0) )
            else:
                font_image = font.render(line, 1, colors[0] )

            text_surface.blit( font_image, ( int((self._image_size - font_image.get_width())/2) , int(( self._image_size - font_image.get_height() )/2) ) )
            self._images_matrix[-1].append( pygame.transform.smoothscale( text_surface, (self._image_size, self._image_size) ).convert() )

            self._sub_ids[-1].append( sub_id )


    def get_image_by_board_id(self, board_id, sub_id, rotation):
        if board_id in self._board_ids:
            index = self._board_ids.index( board_id )
            if self._mode == "Normal":
                image = self._images[ index ][ self._sub_ids[index][sub_id] ]
                image = pygame.transform.rotate(image, rotation)
            elif self._mode == "Matrix":
                image = self._images_matrix[ index ][ self._sub_ids[index][sub_id] ]
                image = pygame.transform.rotate(image, random.choice([0,90,180,270]) )
            return image

        else:
            return None

    def get_icon_image_by_board_id(self, board_id, sub_id, rotation):
        if board_id in self._board_ids:
            index = self._board_ids.index( board_id )
            if self._mode == "Normal":
                image = self._icon_images[ index ][ self._sub_ids[index][sub_id] ]
            elif self._mode == "Matrix":
                image = self._images_matrix[ index ][ self._sub_ids[index][sub_id] ]
            return image

        else:
            return None


    def matrix_mode( self ):
        self._mode = "Matrix"

    def normal_mode( self ):
        self._mode = "Normal"

    def play_jingle( self ):
        self._jingle.play()
    def stop_jingle( self ):
        self._jingle.stop()

    #functions to create our resources
    def load_image(self, name, colorkey=None, scale=None, build_path = True):
        if build_path:
            if self.path:
                fullname = os.path.join(self.path, name)
            else:
                fullname = os.path.join(data_dir, name)
        else:
            fullname = name
        try:
            image = pygame.image.load(fullname)
        except pygame.error:
            print ('Cannot load image:', fullname)
            raise SystemExit(str(geterror()))
        if scale is not None:
            image = pygame.transform.scale( image, scale )
        image = image.convert()
        if colorkey is not None:
            if colorkey is -1:
                colorkey = image.get_at((0,0))
            image.set_colorkey(colorkey, RLEACCEL)
        return image, image.get_rect()

    def load_sound(self, name):
        class NoneSound:
            def play(self): pass
        if not pygame.mixer or not pygame.mixer.get_init():
            return NoneSound()
        fullname = os.path.join(data_dir, name)
        try:
            sound = pygame.mixer.Sound(fullname)
        except pygame.error:
            print ('Cannot load sound: %s' % fullname)
            raise SystemExit(str(geterror()))
        return sound



# Screen has (x,y) = (0,0)  in top left corner
# Blocks has (y,x) = (0,0)  in bottom left corner
class Board_Render():
    _board_size = (0, 0)
    _radar_rect = None
    class _statusregion:
        width = 0
        border_width = 12
        color = (0, 50, 0)
        text_size = 24
        text_color = (220, 220, 220)
    class _user_icon_section:
        height = 50
        color = (0,0,0)
        offset = 0
        text_offset_y = 5
        spacing = 10
        icon_size = 30
        text_width = 250
        text_color = (200,200,200)
        text_font = game_font
        text_size = 24
    class _effect:
        size = 60
        data = None
        pre_data = None
    class _border:
        width = 12
        height = 12
        color = (50, 50, 50)
    class _wall:
        width = 4
        color = (0, 0, 0)
    _block_size = 30
    background = None
    screen = None
    screen_hw = None
    _board_render_objects = None
    _redraw_rects = []
    _show_help_screen = False

    _snake_colors = [ (0, 0, 0), (128, 128, 128), (255, 255, 255), (200, 200, 0), (0, 200, 200), (200, 50, 200), (200, 50, 0), (0, 200, 0), (0, 50, 200) ] # 3 first are normally: debug, user1 and user2

    def __init__( self, board_id_base, display, board_images, snake_radar=None, set_block_size = 0 ):
        if display.fullscreen:
            # Windows pixel stretching fix
            if sys.platform == "win32":
                ctypes.windll.user32.SetProcessDPIAware()
                true_res = (ctypes.windll.user32.GetSystemMetrics(0),ctypes.windll.user32.GetSystemMetrics(1))

            display_info = pygame.display.Info()
            self.screen_hw = pygame.display.set_mode([display_info.current_w, display_info.current_h], FULLSCREEN | DOUBLEBUF | HWSURFACE )
            # self.
            # Calculate board size

            if display.board_size == (0,0): # Autoscale
                x_size = int(np.floor((display_info.current_w-self._border.width*2-self._statusregion.width)/self._block_size))
                y_size = int(np.floor((display_info.current_h-self._border.height*2-self._user_icon_section.height)/self._block_size))

                self._board_size = (y_size, x_size)
            else:
                # Stretch to width and adjust height accordingly
                block_x_size = int( np.floor((display_info.current_w-self._border.width*2-self._statusregion.width)/display.board_size[1]) )
                block_y_size = int( np.floor((display_info.current_h-self._border.height*2-self._user_icon_section.height)/display.board_size[0]) )
                self._block_size = min(block_x_size, block_y_size)
                self._board_size = (display.board_size[0], display.board_size[1])
                # Adjust boarder to center the board on the screen
                self._border.width = int( (display_info.current_w - self._board_size[1]*self._block_size - self._statusregion.width)/2 )
                self._border.height = int( (display_info.current_h - self._board_size[0]*self._block_size - self._user_icon_section.height)/2 )
        else:
            if (set_block_size != 0): #allowing rescale in board generator
                self._block_size = set_block_size
            self._board_size = (display.board_size[0], display.board_size[1])
            self.screen_hw = pygame.display.set_mode( self.get_screen_size(), DOUBLEBUF  | HWSURFACE ) #, pygame.NOFRAME )

        self.screen = pygame.Surface( self.screen_hw.get_size() )
        self._board_render_objects = Board_Render_Objects( board_id_base, board_images, self._block_size, self._user_icon_section.icon_size )

        # Initialise radar
        if snake_radar is not None:
            radar_image_width = (snake_radar["left"] + 1 + snake_radar["right"]) * self._block_size
            radar_image_height = (snake_radar["bottom"] + 1 + snake_radar["top"] + 1) * self._block_size
            self._radar_rect = pygame.Rect(self._border.width, self._border.height, radar_image_width, radar_image_height)



        pygame.display.set_caption('uWars')
        # Create The Backgound
        self.create_board_background()
        self._effect.data = np.ones( self._board_size, dtype=np.bool )
        self._effect.pre_data = np.ones( self._board_size, dtype=np.bool )

        # Display The Background
        self.draw_background()
        self.game_board_ready_to_draw = False
        # self.flip()

    def cleanup(self):
        self._board_render_objects.cleanup()
        self._redraw_rects = []
        self.background = None
        self.screen = None
        self.screen_hw = None
        self._board_render_objects = None
        self._redraw_rects = []
        self._show_help_screen = False

    def export_icon_image(self, board_id, id, path):
        full_path = os.path.join( path, export_images_dir )
        # filename =  id + '__' + time.asctime().replace(':','_') + '.png'
        timestamp = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")
        filename = id + '__' + timestamp + '.png'
        fullname = os.path.join( full_path, filename )
        image = self._board_render_objects.get_icon_image_by_board_id( board_id, 0, 0 )
        image = image.convert_alpha()

        if not os.path.isdir(full_path):
            os.makedirs(full_path, exist_ok=True)

        dirlist = os.listdir(full_path)
        for fn in dirlist:
            if fn.find(id) != -1:
                os.remove( os.path.join( full_path, fn ) )

        pygame.image.save( image, fullname )
        # filename_ui =  id + '_ui' + '__' + time.asctime().replace(':','_') + '.png'
        filename_ui =  id + '_ui' + '__' + timestamp + '.png'
        fullname_ui = os.path.join( full_path, filename_ui )

        cropped = pygame.Surface((self._board_render_objects._user_graphics_size[0], self._board_render_objects._user_graphics_size[1]))
        cropped.blit(image, (0, 0), (self._board_render_objects._user_graphics_position[0], self._board_render_objects._user_graphics_position[1], self._board_render_objects._user_graphics_size[0], self._board_render_objects._user_graphics_size[0]))
        pygame.image.save( cropped, fullname_ui )

        return os.path.abspath(fullname), os.path.abspath(fullname_ui)


    def get_number_of_active_objects( self ):
        return self._board_render_objects.get_active_objects()

    def get_board_size( self ):
        return self._board_size

    def get_block_size( self ):
        return self._block_size

    def block_to_position( self, block_pos ):
        # Input format is (y,x) where (0,0) is bottom left
        # Output format is (x,y) where (0,0) is top left
        x = block_pos[1]*self._block_size + self.get_gameregion_rect().left
        y = (self.get_gameregion_blocks().height-1-block_pos[0])*self._block_size + self.get_gameregion_rect().top
        return (x, y)

    def position_to_block( self, pos ):
        x = np.floor( float(pos[0] - self.get_gameregion_rect().left) / self._block_size )
        y = np.ceil( self.get_gameregion_blocks().height - float(pos[1]-self.get_gameregion_rect().top)/self._block_size )
        return ( int(y), int(x) )


    def get_gameregion_blocks(self):
        return pygame.Rect(0, 0, self._board_size[1], self._board_size[0] )

    def get_screen_size(self):
        return ( (self._board_size[1] * self._block_size) + self._border.width * 2 + self._statusregion.width, (self._board_size[0] * self._block_size) + self._border.height * 2 + self._user_icon_section.height )

    def get_gameregion_rect(self):
        return pygame.Rect( self._border.width, self._border.height, self._board_size[1] * self._block_size, self._board_size[0] * self._block_size )

    def get_gameregion_size(self):
        return ( self._board_size[1] * self._block_size, self._board_size[0] * self._block_size )

    def get_statusregion_rect(self):
        return pygame.Rect( self._border.width + (self._board_size[1] * self._block_size) + self._statusregion.border_width, self._border.height, self._statusregion.width-self._statusregion.border_width*2, self._board_size[0] * self._block_size )

    def get_snake_color(self, index):
        if index < (len(self._snake_colors)-1):
            return [self._snake_colors[index],self._snake_colors[index + 1]]
        elif index < (len(self._snake_colors)-2)*2:
            index -= len(self._snake_colors)
            return [self._snake_colors[index],self._snake_colors[index + 2]]
        elif index < (len(self._snake_colors)-3)*2:
            index -= len(self._snake_colors)*2
            return [self._snake_colors[index],self._snake_colors[index + 3]]
        else:
            return [(0,0,0), (255,255,255)]

    def draw_board( self, board, board_changes, board_object_rotation, board_object_sub_id, draw_on_background=False ):
        # print(board)
        self._redraw_rects = []
        for change_array in [board_changes, self._effect.data, self._effect.pre_data]:
            change_indexes = np.where(change_array)
            for i in range(change_indexes[0].size):
                y = change_indexes[0][i]
                x = change_indexes[1][i]
                image = self._board_render_objects.get_image_by_board_id( board[y,x], board_object_sub_id[y,x], board_object_rotation[y,x] )
                if image is None:
                    print("This will fail!!!", board[y,x], board_object_sub_id[y,x], board_object_rotation[y,x])
                screen_pos = self.block_to_position( (y,x) )
                if draw_on_background:
                    self.background.blit(image, screen_pos )
                else:
                    self.screen.blit(image, screen_pos )

                self._redraw_rects.append( pygame.Rect( screen_pos[0], screen_pos[1], image.get_width(), image.get_height() ) )

        self._effect.pre_data = self._effect.data[:]
        self._effect.data = np.zeros( self._effect.data.shape, dtype=np.bool )

    def draw_effect( self, position, text ):
        image = self._board_render_objects.get_effect_image( "talk" ).copy()
        # image.set_alpha( 180 )
        screen_pos = self.block_to_position( position )
        effect_size = self._effect.size
        if position[1] < np.ceil(effect_size/self._block_size):
            x = screen_pos[0] + self._block_size
            flip_x = True
        else:
            x = screen_pos[0] - effect_size
            flip_x = False
        if ( position[0] + np.ceil(effect_size/self._block_size) ) > ( self._board_size[0] - 1 ):
            y = screen_pos[1] + self._block_size
            flip_y = True
        else:
            y = screen_pos[1] - effect_size
            flip_y = False

        image = pygame.transform.flip( image, flip_x, flip_y )

        # Add text
        font = pygame.font.Font( os.path.join(data_dir, game_font), 13 )
        image.blit( font.render(text[:7], 1, (0,0,0)), (4,8) )
        image.blit( font.render(text[7:14], 1, (0,0,0)), (4,22) )
        image.blit( font.render(text[14:21], 1, (0,0,0)), (4,36) )

        screen_pos = ( x, y )
        self.screen.blit(image, screen_pos )
        self._redraw_rects.append( pygame.Rect( screen_pos[0], screen_pos[1], image.get_width(), image.get_height() ) )
        # print(position)
        position = self.position_to_block(screen_pos)
        # print(position, position[0] - int(np.ceil(effect_size/self._block_size)), position[1] + int(np.ceil(effect_size/self._block_size)) )
        for x in range(position[1], position[1] + int(np.ceil(effect_size/self._block_size))):
            for y in range( position[0] - int(np.ceil(effect_size/self._block_size)), position[0] ):
                self._effect.data[y,x] = True


        # self.snake_chats[] = image

    def draw_info_text( self, text ):
        font = pygame.font.Font(None, 24 )
        text_surface = font.render(text, 1, (250,0,0), (200,200,200) )
        text_surface.set_alpha(180)
        text_surface = text_surface.convert()

        screen_pos = self.block_to_position( [0, self._board_size[1]-1] )
        screen_pos = ( screen_pos[0] - text_surface.get_width(), screen_pos[1] - text_surface.get_height() )

        self.screen.blit(text_surface, screen_pos )
        self._redraw_rects.append( pygame.Rect( screen_pos[0], screen_pos[1], text_surface.get_width(), text_surface.get_height() ) )
        position = self.position_to_block(screen_pos)
        for x in range(position[1], position[1] + int(np.ceil(text_surface.get_width()/self._block_size))):
            for y in range( position[0] - int(np.ceil(text_surface.get_height()/self._block_size)), position[0] ):
                self._effect.data[y,x] = True


    def splash_text( self, text, to_hw=False ):
        font = pygame.font.Font( os.path.join(data_dir, game_font), 50 )
        image = font.render(text, 1, (0,0,0))
        splash_surface = pygame.Surface( (image.get_width()+10, image.get_height()+10) )
        splash_surface.set_alpha( 180 )
        splash_surface.fill( (200,200,200) )
        splash_surface.blit( image, (5,5) )
        x = int( self.screen.get_width()/2 - splash_surface.get_width()/2 )
        y = int( self.screen.get_height()/2 - splash_surface.get_height()/2 )
        if to_hw:
            self.screen_hw.blit( splash_surface, (x,y) )
        else:
            self.screen.blit( splash_surface, (x,y) )

    def toggle_help_screen( self ):
        self._show_help_screen = not self._show_help_screen
        self.force_redraw()

    def draw_help_screen( self, file_name = g_help_image_name ):
        if self._show_help_screen:
            # self.splash_text( "Help!" )
            fullname = os.path.join(data_dir, file_name)
            image, rect = self._board_render_objects.load_image( fullname, -1, build_path = False)
            x = int( self.screen.get_width()/2 - image.get_width()/2 )
            y = int( self.screen.get_height()/2 - image.get_height()/2 )
            self.screen.blit( image, (x,y) )

    def draw_snake_info( self, active_snake, board_objects ):
        bg, rect = self._board_render_objects.load_image( g_snake_info_image_name, -1, build_path = False)
        pygame.draw.rect(bg, self._user_icon_section.color, (18,15,self._user_icon_section.icon_size*2,self._user_icon_section.icon_size*2), 0)
        image = self._board_render_objects.get_icon_image_by_board_id( board_objects.get_board_id_by_id( active_snake.get_id() )+1, 0, 0 )
        image = pygame.transform.scale(image,(self._user_icon_section.icon_size*2,self._user_icon_section.icon_size*2))
        # bg.set_alpha( 180 )
        bg.blit(image, [18,15])
        
        # Snake name and owner
        text = active_snake.get_name()
        font = pygame.font.Font(os.path.join(data_dir, self._user_icon_section.text_font), self._user_icon_section.text_size)
        text_img = font.render(text[:24], 1, [50,50,50] )
        bg.blit(text_img, (15 + self._user_icon_section.icon_size*2 + 15, self._user_icon_section.icon_size) )
        # Snake name and owner
        text = active_snake.get_snake_owner()
        font_size = int(self._user_icon_section.text_size*2.0/3)
        font = pygame.font.Font(os.path.join(data_dir, self._user_icon_section.text_font), font_size)
        text_img = font.render(text[:24], 1, [50,50,50] )
        bg.blit(text_img, (15 + self._user_icon_section.icon_size*2 + 15, self._user_icon_section.icon_size + self._user_icon_section.text_size + 5) )

        # Snake info
        font_size = int(self._user_icon_section.text_size*2.0/3)
        font = pygame.font.Font(os.path.join(data_dir, self._user_icon_section.text_font), font_size)
        text = active_snake.get_snake_info()
        
        text_split_index_end = -1
        for i in range(3):
            text_split_index_start = text_split_index_end + 1
            if text_split_index_start + 45 < len(text):
                text_split_index_end = text_split_index_start + text[text_split_index_start:text_split_index_start+45].rfind(" ")
                if text_split_index_end == -1:
                    text_split_index_end = text_split_index_start+45
            else:
                text_split_index_end = text_split_index_start + 45
                
            text_img = font.render(text[text_split_index_start:text_split_index_end], 1, [50,50,50] )
            bg.blit(text_img, (18, 15 + self._user_icon_section.icon_size*2 + 10 + (font_size+10)*i) )
        
        x = self.get_gameregion_rect()[0] + 10
        y = self.get_gameregion_rect()[1] + 15
        self.screen.blit( bg, (x,y) )

            

    def force_redraw( self ):
        self._effect.data.fill(True) # Trigger redraw of whole screen


    def draw_background( self ):
        self.screen.blit( self.background, (0, 0) )

    def draw_wall_images( self ):
        self.screen.blit( self.scrollbar_surface, (self._user_icon_section.offset, self.screen_hw.get_height() - self._user_icon_section.height) )

    def push_to_screen( self ):
        self.game_board_ready_to_draw = True

    def flip( self ):
        # Update the full display Surface to the screen
        pygame.display.flip()

    def copy_screen_buf_to_screen_hw(self):
        self.screen_hw.blit( self.screen, (0, 0) )

    def update_scrollbar( self, scroll_speed ):
        # Draw directly on screen_hw
        if self.game_board_ready_to_draw:
            self.game_board_ready_to_draw = False
            self.copy_screen_buf_to_screen_hw()
        self.screen_hw.blit( self.scrollbar_surface, (self._user_icon_section.offset, self.screen_hw.get_height() - self._user_icon_section.height) )
        self._user_icon_section.offset -= scroll_speed
        if (self.scrollbar_surface.get_width()/2) + self._user_icon_section.offset <= 0:
            self._user_icon_section.offset = (self.scrollbar_surface.get_width()/2) + self._user_icon_section.offset

        # scroll_rect = pygame.Rect(0, self.screen_hw.get_height() - self._user_icon_section.height, self.screen_hw.get_width(), self._user_icon_section.height)
        # pygame.display.update( scroll_rect )
        self.flip()
        # print("Scroll")

    def update_gamearea(self):
        # game_rect = pygame.Rect(0, 0, self.screen.get_width(), self.screen.get_height() - self._user_icon_section.height)
        # pygame.display.update( game_rect )
        pass
        # pygame.display.update( self._redraw_rects )

        # print("Game")

    def draw_radar_image(self, radar_image, head_position, name):
        if (self._board_render_objects._mode == "Normal") and (self._radar_rect is not None):
            radar_surface = pygame.Surface( (self._radar_rect.width, self._radar_rect.height) )
            radar_surface.set_alpha(180)
            radar_surface = radar_surface.convert()
            pygame.draw.rect(radar_surface, (200, 200, 200), (0,0,self._radar_rect.width,self._radar_rect.height), 0)
            pygame.draw.rect(radar_surface, self._wall.color, (0,0,self._radar_rect.width,self._radar_rect.height), self._wall.width)

            font = pygame.font.Font(None, int(self._block_size*0.5) ) # Using block size for font size seems to work fine
            text_pos_top = self._block_size + 8

            if radar_image is not None:
                radar_image = np.flipud(radar_image)
                for y in range(radar_image.shape[0]):
                    text_pos_left = 4
                    for x in range(radar_image.shape[1]):
                        line = "{:^3}".format(str(radar_image[y,x]))
                        if (y,x) == head_position: # Mark head position
                            text = font.render(line, 1, (200,50,50) )
                        else:
                            text = font.render(line, 1, (50,50,50) )
                        textpos = text.get_rect(left=text_pos_left, top=text_pos_top)
                        radar_surface.blit(text, textpos)
                        text_pos_left += self._block_size
                    text_pos_top += self._block_size

            font = pygame.font.Font(None, int(self._block_size*0.8) ) # Using block size for font size seems to work fine
            radar_surface.blit( font.render(name, 1, (50,50,50) ), (4, 8) )
            self.screen.blit( radar_surface, self._radar_rect.topleft )

            # Hardcoded to start in top right corner...
            for y in range(self._board_size[0]-int(np.ceil(self._radar_rect.height/self._block_size)), self._board_size[0]):
                for x in range( int(np.ceil(self._radar_rect.width/self._block_size)) ):
                    self._effect.data[y,x] = True


    def update_status_text(self, stats_list):
        if self._statusregion.width:
            # Put Text On status field
            if pygame.font:
                font = pygame.font.Font(None, self._statusregion.text_size)
                text_pos_top = self.get_statusregion_rect()[1]+ (self._statusregion.text_size*2)
                text_pos_left = self.get_statusregion_rect()[0]+10
                for i, stats in enumerate(stats_list):
                    if stats.lengths.size:
                        line = "{:<15.15} {:3.2f} /{:3.0f}".format(stats.get_name(), stats.get_mean_length(), stats.get_max_length())
                    else:
                        line = "{:<15.15} {:3.2f} /{:3.0f}".format(stats.get_name(), 0.0, 0.0 )
                    text = font.render(line, 1, stats.get_color()[0] )
                    textpos = text.get_rect(left=text_pos_left, top=text_pos_top)
                    self.screen.blit(text, textpos)
                    text_pos_top += self._statusregion.text_size

    def draw_user_icon_section(self, snakes):
        # snakes = [[board_id, name],[]]
        needed_width = self._user_icon_section.spacing + len(snakes) * (self._user_icon_section.icon_size + self._user_icon_section.spacing + self._user_icon_section.text_width) + self._user_icon_section.spacing
        icon_surface = pygame.Surface( (max(self.screen.get_width(), needed_width), self._user_icon_section.height ) )
        icon_surface.fill( self._user_icon_section.color )

        image_pos = [self._user_icon_section.spacing, int((self._user_icon_section.height-self._user_icon_section.icon_size)/2) ]
        for snake in snakes.values():
            image = self._board_render_objects.get_icon_image_by_board_id( snake[0], 0, 0 )
            image = pygame.transform.scale(image,(self._user_icon_section.icon_size,self._user_icon_section.icon_size))
            icon_surface.blit(image, image_pos)
            image_pos[0] += self._user_icon_section.icon_size+self._user_icon_section.spacing
            font = pygame.font.Font(os.path.join(data_dir, self._user_icon_section.text_font), self._user_icon_section.text_size)
            text = font.render(snake[1][:20], 1, self._user_icon_section.text_color )
            icon_surface.blit(text, (image_pos[0], image_pos[1]+self._user_icon_section.text_offset_y, self._user_icon_section.text_width, self._user_icon_section.icon_size ) )
            image_pos[0] += self._user_icon_section.text_width

            # Should maybe add copy of chat bubble here?

        self.scrollbar_surface = pygame.Surface( ( icon_surface.get_width()*2, self._user_icon_section.height ) )
        self.scrollbar_surface.blit( icon_surface, (0,0) )
        self.scrollbar_surface.blit( icon_surface, (icon_surface.get_width(),0) )

    def draw_images_below_board(self, images, highlight = 0, size = 30, hl_color = (191, 191, 191), hl_width = 4, fill_color = None ):
        # TODO: Should add check/handling for too many wall elements to fit surface!
        icon_surface = pygame.Surface( (self.screen.get_width(), self._user_icon_section.height ) )
        if fill_color:
            icon_surface.fill( fill_color )
        else:
            icon_surface.fill( self._user_icon_section.color )
        

        image_pos = [self._user_icon_section.spacing, int((self._user_icon_section.height-self._user_icon_section.icon_size)/2) ]
        for image in images:

            icon_surface.blit(image, image_pos)
            image_pos[0] += size + self._user_icon_section.spacing

        x = (self._user_icon_section.spacing) - hl_width + highlight * (size + self._user_icon_section.spacing)
        y = int((self._user_icon_section.height-self._user_icon_section.icon_size)/2) - hl_width
        mark_rect = pygame.Rect( x, y, self._user_icon_section.icon_size + 2 * hl_width, self._user_icon_section.icon_size + 2 * hl_width  )
        pygame.draw.rect(icon_surface, hl_color, mark_rect, hl_width )

        self.scrollbar_surface = pygame.Surface( ( icon_surface.get_width()*2, self._user_icon_section.height ) )
        self.scrollbar_surface.blit( icon_surface, (0,0) )
        
    def draw_copy_paste_box(self, x, y, d_x, d_y, width = 4, color = (191, 191, 191)):
        mark_surface_w = pygame.Surface( (d_x * self._block_size + 2 * width, width) )
        mark_surface_h = pygame.Surface( (width, d_y * self._block_size + 2* width) )
        mark_surface_w.fill(color)
        mark_surface_h.fill(color)
        self.screen.blit( mark_surface_w, (x * self._block_size + self._statusregion.border_width - width, y * self._block_size + self._statusregion.border_width - width) )
        self.screen.blit( mark_surface_w, (x * self._block_size + self._statusregion.border_width - width, y * self._block_size + self._statusregion.border_width  + d_y * self._block_size) )
        self.screen.blit( mark_surface_h, (x * self._block_size + self._statusregion.border_width - width, y * self._block_size + self._statusregion.border_width -width) )
        self.screen.blit( mark_surface_h, (x * self._block_size + self._statusregion.border_width + d_x * self._block_size, y * self._block_size + self._statusregion.border_width- width) )

    def create_board_background(self):
        self.background = pygame.Surface( self.screen.get_size() )
        self.background = self.background.convert()
        self.background.fill( self._border.color )

        gameregion      = pygame.draw.rect(self.background, self._board_render_objects._gameregion_color  , self.get_gameregion_rect()    , 0 )

        gameborder      = pygame.draw.rect(self.background, self._wall.color        , self.get_gameregion_rect().inflate(self._wall.width, self._wall.width)    , self._wall.width )
        if self._statusregion.width:
            statusregion    = pygame.draw.rect(self.background, self._statusregion.color, self.get_statusregion_rect()  , 0 )
            statusboarder   = pygame.draw.rect(self.background, self._wall.color        , self.get_statusregion_rect().inflate(self._wall.width, self._wall.width)  , self._wall.width )

            # Put Text On status field
            if pygame.font:
                font = pygame.font.Font(None, self._statusregion.text_size*2)
                text = font.render("Score", 1, self._statusregion.text_color )
                textpos = text.get_rect(left=self.get_statusregion_rect()[0]+10, top=self.get_statusregion_rect()[1]+10)
                self.background.blit(text, textpos)

    def toggle_matrix_mode( self ):
        if self._board_render_objects._mode == "Normal":
            # self._board_render_objects._mode = "Matrix"
            self._board_render_objects.matrix_mode()

            # Create black bacground
            # self.background.fill( (0,30,0) )
        else:
            # self._board_render_objects._mode = "Normal"
            self._board_render_objects.normal_mode()
            # Create black bacground
            # self.create_board_background()

        self.force_redraw()

    def play_jingle( self ):
        self._board_render_objects.play_jingle()
    def stop_jingle( self ):
        self._board_render_objects.stop_jingle()

    def save_screenshot( self, save_local, path, board, mark_object, text ):
        timestamp = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")

        if mark_object is not None:
            self.splash_text( text, True )
            mark = np.where( board == mark_object )
            screen_pos = self.block_to_position( (mark[0][0], mark[1][0]) )
            mark_rect = pygame.Rect( screen_pos[0], screen_pos[1], self._block_size, self._block_size )
            pygame.draw.rect(self.screen_hw, (255,0,0), mark_rect, 4 )
            time.sleep( 1 ) # Wait for next screen update and show screenshot for min 1 sec

        if save_local: # Turn of local save for now
            filename = timestamp + '.png'
            fullname = os.path.join( screenshot_dir, filename )
            pygame.image.save( self.screen_hw, fullname )
        # Save copy to keep track of latest change
        full_path = os.path.join( path, export_images_dir )
        export_file_name = 'MaxSnake'
        dirlist = os.listdir(full_path)
        for fn in dirlist:
            if fn.find(export_file_name) != -1:
                os.remove( os.path.join( full_path, fn ) )

        # fullname = os.path.join( full_path, export_file_name + '__' + time.asctime().replace(':','_') + '.png' )
        fullname = os.path.join( full_path, export_file_name + '__' + timestamp + '.png' )
        pygame.image.save( self.screen_hw, fullname )

        return os.path.abspath(fullname)
