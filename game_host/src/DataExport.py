##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

import os
import sys
import datetime
import shutil
import glob
from sqlalchemy import create_engine
from sqlalchemy import text

from sqlalchemy.orm import sessionmaker

from .Models import Base
from .Models import Player, Score, Game, Chat, Death, BoardMap, LifeStat

# Version number for the database, if the models are changed this nuumber should be increased
# The game monitor can use it to select the correct model definitions when reading data.
DATABASE_VERSION = 2


class DataExport():
    """Manage the export of game data to external resources"""
    def __init__(self,
                 db_name,
                 main_folder,
                 base_path,
                 static_folder,
                 static_path,
                 db_min_wrt_interval=0,
                 num_temp_backups=5):
        """Initialize the parameters for the data export. To actually set up the database run
        initialize_database().

        Input parameters:
            db_name -- file name of the database file
            main_folder -- folder name for where the database file is created
            base_path -- the path to where the main_folder will be created
                expected format is {'linux': linux_path, 'windows': windows_path)
            static_folder -- folder name for where the database copies will be created
            static_path -- path to where static_folder will be created
                expected format is {'linux': linux_path, 'windows': windows_path)
            db_min_wrt_interval -- minimum time between each write to the database, in seconds
            num_temp_backups -- Number of copies of old games that will be kept in the temp backup

        The full path to the database file will be:
            <base_path>/<main_folder>/<db_name>
        If <base_path> is not a valid path, data will instead be stored in (if present)
            <static_path>/<main_folder>/<db_name>

        Copies of old databases will be stored in
            <static_path>/<static_folder>/

        The paths specified by base_path and static path are expected to exist, if not no data will
        be stored. The main_folder and static_folder will be created as necessary.

        """
        # Dict for keeping the status of the last run db commands
        self.last_command_status = {}
        self.set_error_status('Not initialized')

        # Set the minimum time (in seconds) between writes to the db. Allows reducing disk activity
        # when running the game at high speeds. The game monitor only reads data 1-2 times per
        # second so it is usually not necessary to write much faster than this
        self.db_min_wrt_interval = db_min_wrt_interval

        # Number of temp backups to keep
        self.num_temp_backups = num_temp_backups

        # Set up the database name
        self.db_name = db_name

        # Setup the dict key for accessing the paths for the current system
        if sys.platform.startswith('win'):
            system_key = 'windows'
        else:
            system_key = 'linux'

        # Get the path for the game data copies (expanding path in case it includes '~')
        self.static_path = os.path.expanduser(static_path.get(system_key, ''))
        self.static_main_folder = static_folder

        if not os.path.isdir(self.static_path):
            self.static_path = None

        # Get the path for the live game data (expanding path in case it includes '~')
        self.base_path = os.path.expanduser(base_path.get(system_key, ''))
        self.main_folder = main_folder

        if not os.path.isdir(self.base_path):
            # The base folder doesn't exist, trying the alternate folder instead
            self.base_path = self.static_path

        if self.base_path is None or self.static_path is None:
            self.set_error_status('Data export paths not found, no data will be exported')
            return

        self.engine = None
        self.session = None

    def initialize_database(self):
        """Setup the database for a new game"""

        # Set to True to ouput all SQL queries (for debug only)
        debug_output = False

        try:
            # Create the data folder if it doesn't already exist
            if not os.path.isdir(self.get_database_folder_path()):
                os.makedirs(self.get_database_folder_path(), exist_ok=True)

            # Specify the database engine
            self.engine = create_engine('sqlite:///' + self.get_path_to_db_file(),
                                        echo=debug_output)

            # Clean up database if already exist
            Base.metadata.drop_all(self.engine)
            self.engine.execute('VACUUM')           # For SQLite only

            # Create all the tables
            Base.metadata.create_all(self.engine)

            # Bind the engine to the metadata of the Base class so that the declaratives can be
            # accessed through a DBSession instance
            Base.metadata.bind = self.engine

            # A DBSession() instance establishes all conversations with the database and represents
            # a "staging zone" for all the objects loaded into the database session object. Any
            # change made against the objects in the session won't be persisted into the database
            # until you call session.commit(). If you're not happy about the changes, you can revert
            # all of them back to the last commit by calling session.rollback()
            DBSession = sessionmaker(bind=self.engine)
            self.session = DBSession()

            # Write the database version to the game table
            self.set_game_data({'db_version': DATABASE_VERSION})

            self.set_error_status_all_ok()
        except Exception as e:
            self.set_error_status('Error initializing the database: {}'.format(str(e)))
            self.engine = None
            self.session = None

    def close_database(self):
        """Release the connection to the database"""
        self.session.close()

    def get_db_file_name(self):
        """Get database file name"""
        return self.db_name

    def get_path_to_backup_folder(self):
        """Get the path to the folder where database backups are stored"""
        return os.path.join(self.static_path, self.static_main_folder)

    def get_path_to_temp_backup_folder(self):
        """Get the path to the folder where temporary game backups are stored"""
        return os.path.join(self.get_path_to_backup_folder(), 'temp')

    def get_database_folder_path(self):
        """Get the path to the folder where the database is stored"""
        return os.path.join(self.base_path, self.main_folder)

    def get_path_to_db_file(self):
        """ Get the full path of the database file, ie directory + file name"""
        return os.path.join(self.get_database_folder_path(), self.db_name)

    def get_minimum_db_write_interval(self):
        return self.db_min_wrt_interval

    def run_temp_backup(self):
        """Make a temp backup of the game files. A copy of the game folder for the last N games
        are kept in case we need to recover data after a crash (the main backup is run after the
        game is closed).

        NB - This function must be called before the database is initialized for the current game.
        """
        if self.engine is not None or self.session is not None:
            self.set_error_status("Can't do a temp backup while connected to the database")
            return False

        if not os.path.isdir(self.get_database_folder_path()):
            # No existing database, nothing to back up
            self.set_error_status_all_ok()
            return True

        # Get the current timestamp
        timestamp = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")
        try:
            # Create the backup folder if necessary
            if not os.path.isdir(self.get_path_to_backup_folder()):
                os.makedirs(self.get_path_to_backup_folder(), exist_ok=True)

            # Create the temp folder if necessary
            if not os.path.isdir(self.get_path_to_temp_backup_folder()):
                os.makedirs(self.get_path_to_temp_backup_folder(), exist_ok=True)

            # The name of the temp data folder
            temp_data = 'temp_game_data_{}'.format(timestamp)
            dest_path = os.path.join(self.get_path_to_temp_backup_folder(), temp_data)

            # Copy the current game data to temp
            shutil.copytree(self.get_database_folder_path(), dest_path)

            # Get a list of existing temp bakcups
            temp_folders = glob.glob(os.path.join(self.get_path_to_temp_backup_folder(),
                                                  'temp_game_data_*'))

            # Sort the folders (bacause of the timestamp the newest ones should now be first)
            temp_folders.sort(reverse=True)

            # Remove the oldest folders
            if len(temp_folders) > self.num_temp_backups:
                for folder in temp_folders[self.num_temp_backups:]:
                    shutil.rmtree(folder)

        except Exception as e:
            self.set_error_status("Error in temp backup: {}".format(str(e)))
            print('Failed temp backup. ' + str(e))
            return False

        return True

    def close_and_backup_database(self):
        """Make a backup of the current game data"""

        # Check if there is anything to back up
        if not os.path.isfile(self.get_path_to_db_file()):
            print("No database found, nothing to back up")
            return False

        if not os.path.isdir(self.static_path):
            print('No valid backup path was found, no files are copied')
            return False

        # Create the backup folder for the database if it doesn't already exist
        if not os.path.isdir(self.get_path_to_backup_folder()):
            os.makedirs(self.get_path_to_backup_folder(), exist_ok=True)

        # Get the game start time and image paths from the database
        start_time = self.get_game_start_time()
        img_files = self.get_image_file_paths()

        if start_time:
            # Using the game start time as a unique file id
            file_id = start_time.split('.')[0]      # Strip of the fractional seconds
        else:
            # No start time was found in the database, use the current time instead
            file_id = 'ERR_' + datetime.datetime.now().strftime("%Y%m%dT%H%M%S")

        backup_name = self.get_db_file_name()
        (base, ext) = os.path.splitext(backup_name)
        backup_name = base + '_' + file_id

        src = self.get_path_to_db_file()
        dest_folder = os.path.join(self.get_path_to_backup_folder(), backup_name)

        # Close the database connection before we start copying files
        self.close_database()

        try:
            # Create the backup folder
            os.makedirs(dest_folder, exist_ok=True)

            # Copy the database file
            shutil.copy(src, os.path.join(dest_folder, backup_name + ext))

            # Copy image files
            for img_path in img_files:
                img_file_name = os.path.basename(img_path)
                shutil.copy(img_path, os.path.join(dest_folder, img_file_name))

        except Exception as e:
            print('Failed database backup. ' + str(e))
            return False

        return True

    def get_image_file_paths(self):
        """Create list of the path to all image files referenced by the database"""

        img_paths = []

        max_length_img_path = self.get_max_length_image_path()
        if max_length_img_path:
            img_paths.append(max_length_img_path)

        img_paths.extend(self.get_player_image_paths())

        return img_paths

    def set_error_status(self, msg):
        self.last_command_status['error'] = True
        self.last_command_status['message'] = msg

    def set_error_status_all_ok(self):
        self.last_command_status['error'] = False
        self.last_command_status['message'] = ''

    def error_in_last_cmd(self):
        return self.last_command_status['error']

    def get_last_cmd_status_msg(self):
        return self.last_command_status['message']

    def set_game_data(self, param_dict):
        """Set game data parameters

        Each parameter is given as a key/value pair in the param_dict, and these are inserted as
        new rows in the game table. If a row with the key value aready exist the row is overwritten
        """
        # Create a list of tuples form the input dict
        try:
            for key, value in param_dict.items():
                rows = self.session.query(Game).filter_by(key=key).update({'value': value})
                if rows == 0:
                    gameParam = Game(key=key, value=value)
                    self.session.add(gameParam)

            self.session.commit()
        except Exception as e:
            print(str(e))

    def get_game_param(self, key, default=None):
        value = default
        try:
            game_param = self.session.query(Game).filter_by(key=key).one()
            value = game_param.value
        except Exception as e:
            print("Error reading '{}' from game table".format(key))
            print(str(e))
            value = default

        return value

    def get_game_start_time(self):
        """Get the start time for the current game"""
        return self.get_game_param('game_start_time', '')

    def get_max_length_image_path(self):
        """Get the start time for the current game"""
        return self.get_game_param('max_length_img')

    def get_player_image_paths(self):
        """Get a list of paths to all player images"""
        img_list = []
        try:
            players = self.session.query(Player.snake_head_img, Player.snake_icon_img).all()
            for head_img_path, icon_img_path in players:
                img_list.append(head_img_path)
                img_list.append(icon_img_path)
        except Exception as e:
            print("Error reading player image paths from database")
            print(str(e))
            img_list = []

        return img_list

        sql_query = 'SELECT snake_head_img, snake_icon_img FROM players;'

        img_list = []
        try:
            with self.db_conn:
                # Run the query
                cursor = self.db_conn.cursor()
                cursor.execute(sql_query)

                # Fetch the data, only one row is expected
                raw_data = cursor.fetchall()

                # Add all the image paths to the list
                for row in raw_data:
                    img_list.append(row[0])
                    img_list.append(row[1])

        except Exception as e:
            print("Error reading image paths from the player table. " + str(e))

        return img_list

    def add_player(self, player_info):
        """Add a single player to the table of static player info, ie name, images, etc"""
        self.add_players([player_info])

    def add_players(self, player_info_list):
        """Add players to the table of static player info, ie name, images, etc"""
        try:
            # TBD check if node id already exists...
            for player in player_info_list:
                playerRow = Player(node_id=player['id'],
                                   name=player['name'],
                                   snake_head_img=player['head_img'],
                                   snake_icon_img=player['icon_img'],
                                   color1=player['color1'],
                                   color2=player['color2'],
                                   owner=player['owner'],
                                   info=player['info'],
                                   score=Score())
                self.session.add(playerRow)

                # scoreRow = Score(node_id=player['id'])
                # self.session.add(scoreRow)

        except KeyError as e:
            self.set_error_status("Error setting up database data, missing data value. " + str(e))
            return
        else:
            self.session.commit()

    def export_data(self, round_num, data_dict):

        board_pos_list = []

        try:
            for player in self.session.query(Player).all():
                node_id = player.node_id

                score_data = data_dict[node_id]['snake_scores']

                # Update the current data values (change for each tick/round)
                player.score.status = score_data['status']
                player.score.length_current = score_data['length_current']
                player.score.max_length_current = score_data['max_length_current']
                player.score.alive_time_current = score_data['alive_time_current']
                player.score.rounds = score_data['rounds']
                player.score.rank = score_data['rank']
                player.score.rank_score = score_data['rank_score']
                player.score.last_err_msg = data_dict[node_id]['errors']['msg']

                player.score.move_forwards = score_data['moves']['forward']
                player.score.move_lefts = score_data['moves']['left']
                player.score.move_rights = score_data['moves']['right']
                player.score.move_fast_forwards = score_data['moves']['fast_forward']

                player.score.kills = score_data['kills']
                player.score.food = score_data['food']

                # (updated when the snake dies)
                player.score.eol_length_max = score_data['eol_length']['max']
                player.score.eol_length_quartile3 = score_data['eol_length']['q3']
                player.score.eol_length_median = score_data['eol_length']['median']
                player.score.eol_length_quartile1 = score_data['eol_length']['q1']
                player.score.eol_length_min = score_data['eol_length']['min']
                player.score.eol_length_sum = score_data['eol_length']['sum']

                player.score.eol_length_lastn_max = score_data['eol_length_lastn']['max']
                player.score.eol_length_lastn_quartile3 = score_data['eol_length_lastn']['q3']
                player.score.eol_length_lastn_median = score_data['eol_length_lastn']['median']
                player.score.eol_length_lastn_quartile1 = score_data['eol_length_lastn']['q1']
                player.score.eol_length_lastn_min = score_data['eol_length_lastn']['min']
                player.score.eol_length_lastn_sum = score_data['eol_length_lastn']['sum']
                player.score.acc_snake_mass = score_data['acc_snake_mass']

                player.score.max_length_max = score_data['max_length']['max']
                player.score.max_length_quartile3 = score_data['max_length']['q3']
                player.score.max_length_median = score_data['max_length']['median']
                player.score.max_length_quartile1 = score_data['max_length']['q1']
                player.score.max_length_min = score_data['max_length']['min']
                player.score.max_length_sum = score_data['max_length']['sum']

                player.score.alive_time_max = score_data['alive_time']['max']
                player.score.alive_time_quartile3 = score_data['alive_time']['q3']
                player.score.alive_time_median = score_data['alive_time']['median']
                player.score.alive_time_quartile1 = score_data['alive_time']['q1']
                player.score.alive_time_min = score_data['alive_time']['min']
                player.score.alive_time_sum = score_data['alive_time']['sum']
                player.score.alive_time_lastn_max = score_data['alive_time_lastn']['max']
                player.score.alive_time_lastn_quartile3 = score_data['alive_time_lastn']['q3']
                player.score.alive_time_lastn_median = score_data['alive_time_lastn']['median']
                player.score.alive_time_lastn_quartile1 = score_data['alive_time_lastn']['q1']
                player.score.alive_time_lastn_min = score_data['alive_time_lastn']['min']
                player.score.alive_time_lastn_sum = score_data['alive_time_lastn']['sum']

                player.score.response_time_max = score_data['response_time']['max']
                player.score.response_time_min = score_data['response_time']['min']
                player.score.response_time_avg = score_data['response_time']['average']
                player.score.response_time_sum_valid = score_data['response_time']['sum_valid']
                player.score.response_time_num_valid = score_data['response_time']['num_valid']
                player.score.response_time_num_missed = score_data['response_time']['num_missed']

                player.score.deaths = score_data['deaths']

                for message in data_dict[node_id]['chat_messages']:
                    chatMsg = Chat(round=message['round'], player_id=player.id, msg=message['msg'])
                    self.session.add(chatMsg)

                for stats in data_dict[node_id]['life_stats']:
                    chatMsg = LifeStat(player_id=player.id,
                                       start_round=stats['start_round'],
                                       age=stats['age'],
                                       eol_length=stats['eol_length'],
                                       max_length=stats['max_length'])
                    self.session.add(chatMsg)

                # Create a list of all updated board positions, these will be written to the
                # database in a separate bulk update
                for coord, counts in data_dict[node_id]['board_pos'].items():
                    board_pos_list.append({'player_id': player.id,
                                           'x': coord[1],
                                           'y': coord[0],
                                           'death_count': counts['death_count'],
                                           'visit_count': counts['visit_count']})

#                if data_dict['errors']['msg']:
#                    err_msg_list.append({'node_id': data_dict['id'],
#                                         'msg': data_dict['errors']['msg']})

            # TBD - replace node ids with link to player.id
            for node_id, node_data in data_dict.items():
                for killer_id, count in node_data['deaths'].items():
                    rows = self.session.query(Death).filter_by(node_id=node_id) \
                               .filter_by(killer_id=killer_id).update({'count': count})
                    if rows == 0:
                        death = Death(node_id=node_id,
                                      killer_id=killer_id,
                                      count=count)
                        self.session.add(death)

        except KeyError as e:
            self.set_error_status("Error setting up database data, missing data value. " + str(e))
            return

        else:
            self.session.commit()

        if board_pos_list:
            try:
                # Run a bulk update of all changed board positions (SQLite specific query)
                query_text = text('INSERT OR REPLACE INTO board_map (player_id, x, y, visit_count, '
                                  'death_count) '
                                  'VALUES (:player_id, :x, :y, :visit_count, :death_count)')
                self.session.execute(query_text, board_pos_list)
                self.session.commit()
            except Exception as e:
                print(str(e))

        # Update the round number
        self.set_game_data({'rounds_played': round_num})

        self.remove_old_chat_messages(round_num)

    def remove_old_chat_messages(self, current_round):
        """The game monitor only care about new messages, so old messages are deleted to prevent
        the table from growing too big
        """
        try:
            self.session.query(Chat).filter(Chat.round < current_round - 100).delete()
            self.session.commit()
        except Exception as e:
            print(str(e))
