##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Food

#Import Modules
import pygame, random
import numpy as np
from src.Tools import *

class Food_part(Part):
    def __init__(self, position, rotation, subtype=0, max_age=None):
        self.set_position(position)
        self.set_rotation(rotation)
        self.set_subtype(subtype)
        self.age = 0
        if type(max_age) is tuple:
            if None not in max_age:
                self.max_age = random.randrange(max_age[0], max_age[1])
            else:
                self.max_age = None
        else:
            self.max_age = max_age

    def cure(self):
        if self.max_age is not None:
            self.age = self.age + 1

    def is_stale(self):
        if self.max_age is not None:
            return self.age > self.max_age
        else:
            return False


class Food( Game_object ):
    def __init__(self, board_size, color, id):
        self.board_size = board_size
        self.color = color
        self.parts = []
        if id is None:
            self.id = 0
        else:
            self.id = id #(1 << 7) + 0 # Get wall id from some dict here

    def add_random_food( self, number_of_food, board_objects, max_food=-1, max_age=None ):
        food_pos = []

        avail_index = np.where(board_objects == 0)
        # if len(self.parts) > len(np.where(board_objects == self.id)[0]):
            # print( "avail: {0}, parts: {1}, food: {2}".format(len(avail_index[0]), len(self.parts), len(np.where(board_objects == self.id)[0])) )
            # print(board_objects)
            # self.debug_check_parts()
        
        if len(avail_index[0]):
            for i in range(number_of_food):
                index = random.choice(range(len(avail_index[0])))
                y = avail_index[0][index]
                x = avail_index[1][index]
                food_pos.append( (y, x) )
            self.add_food( food_pos, max_food, max_age=max_age )
    
    def add_food( self, positions, max_food=-1, max_age=None ):
        number_of_food = len(positions)
        if max_food >= 0:
            # Limit spawning of new food to the max number
            number_of_food = min(max_food - len(self.parts), number_of_food )
            if number_of_food < 0:
                number_of_food = 0

        for i in range(number_of_food):
            position = positions[i]
            # Avoid creating food duplicates
            food_exist = False
            for part in self.parts:
                if position == part.position:
                    food_exist = True
            if not food_exist:
                self.parts.append( Food_part(position, 0, max_age=max_age) )

    def remove_food( self, position ):
        for i, parts in enumerate(self.parts):
            if parts.position == position:
                del self.parts[i]
                return

    def remove_stale_food(self):
        self.parts[:] = [part for part in self.parts if not part.is_stale()]

    def cure_food(self):
        for part in self.parts:
            part.cure()

    def is_food( self, position ):
        for part in self.parts:
            if part.position == position:
                return True
        return False
