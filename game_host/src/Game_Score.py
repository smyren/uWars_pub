##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

import numpy as np


class GameScore:
    """Base class with common functions for the game scoring. To specify the scoring for a new game
    make a child class and override the _calculate_scores method.
    """
    def __init__(self):
        # Override and set to False to make the lowest score win
        self.highest_score_wins = True

        # Description of the game scoring rules (show in the game monitor)
        self.description = "Insert a description of the game rules here"

        # Name for selecting the preferred game_monitor dashboard. The name must match one of the
        # defined names in game_monitor/dashboards.py. It the name is unknown or invalid a
        # default dashboard will be selected
        self.dashboard = 'default'

    def update_game_ranking(self, snakes):
        """The public function called after each game round to trigger an update of the ranking
        based on the current stats.
        """
        # Calculate the score values
        try:
            scores = self._calculate_scores(snakes)
        except:
            # Something failed during the score calculation, set empty data
            scores = np.array([])

        # Find the rank numbers based on the new score values
        ranks = self._calculate_ranks(scores, self.highest_score_wins)

        # Update the snake stats
        self._update_stats(snakes, scores, ranks)

    def _calculate_scores(self, snakes):
        """Calculate the score values used for ranking the players.
        Override this in the child class to define the scoring for the game.

        Arguments:
        snakes -- The list of snake objects, including the stats for each at snake[i].stats

        Returns a numpy array of the same size (and in the same order) as the snakes list with the
        calculated score for each snake.
        """
        # Set default values
        return np.zeros(len(snakes))

    def _calculate_ranks(self, scores, sort_highest_first=True):
        """Find the rank for each snake based on the calculated score values. The ranks will be
        from 1 (for the leader) to the total number of snakes in the game. Snakes with the
        same score will get the same rank. The default is to rank the snakes so that the highest
        score is ranked #1, but this can be reversed by setting sort_highest_first to False.

        Arguments:
        scores -- numpy array with the calculated score for each snake in the game
        sort_highest_first -- if True the scores are sorted from high to low, ie the highest value
            is ranked as #1, if False they are sorted from low to high, ie the lowest value as #1

        Returns a numpy array (of the same size and order as the input array) with the rank for
        each snake
        """
        if sort_highest_first:     # 1st place for the highest score
            ranks = np.searchsorted(np.sort(scores), scores, side='right')
            ranks = scores.size - ranks + 1
        else:                           # 1st place for the lowest score
            ranks = np.searchsorted(np.sort(scores), scores) + 1

        return ranks

    def _update_stats(self, snakes, scores, ranks):
        """Write updated values to ranks and ranks_score

        Arguments:
        snakes -- The list of snake objects, including the stats for each at snake[i].stats
        scores -- The calculated score for each snake in the snakes list
        rank -- The calculated rank for each snake in the snakes list

        NB - The scores and rank lists must be of the same size as the snakes list
        """
        if len(snakes) != len(ranks) or len(snakes) != len(scores):
            # Mismatch in the array lengths, override the invalid data with default values
            ranks = np.zeros(len(snakes))
            scores = np.zeros(len(snakes))

        # Update the stats with the new values
        for si, snake in enumerate(snakes):
            snake.stats.set_rank(int(ranks[si]))
            snake.stats.set_rank_score(scores[si])


class GameScoreMaxLength(GameScore):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.description = "Longest snake wins"
        self.dashboard = 'max_length'
        self.highest_score_wins = True

    def _calculate_scores(self, snakes):
        """Longest snake wins, so setting the score to the same as the snake max length"""

        # Clear and update the array
        scores = np.zeros(len(snakes))
        for si, snake in enumerate(snakes):
            scores[si] = snake.stats.get_max_length()

        return scores


class GameScoreMaxSnakeMass(GameScore):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.description = "Maximum accumulated snake mass wins"
        self.dashboard = 'max_acc_snake_mass'
        self.highest_score_wins = True

    def _calculate_scores(self, snakes):
        """The highest accumulated snake mass wins, so setting the score to the same as this"""

        # Clear and update the array
        scores = np.zeros(len(snakes))
        for si, snake in enumerate(snakes):
            scores[si] = snake.stats.get_acc_snake_mass()

        return scores


class GameScoreMaxAvgSnakeMassPerRound(GameScore):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.description = "The highest average snake mass per round wins"
        self.dashboard = 'max_acc_snake_mass'
        self.highest_score_wins = True

    def _calculate_scores(self, snakes):
        """The highest average snake mass pr round wins, calculating this from the total
        accumulated snake mass divided by the number of rounds the snake has played"""

        # Clear and update the array
        scores = np.zeros(len(snakes))
        for si, snake in enumerate(snakes):
            scores[si] = snake.stats.get_acc_snake_mass() / snake.stats.get_num_rounds_played()

        return scores


class GameScoreMaxAge(GameScore):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.description = "Oldest snake wins"
        self.dashboard = 'max_age'
        self.highest_score_wins = True

    def _calculate_scores(self, snakes):
        """Oldest snake wins, so setting the score to the same as the max snake age"""

        # Clear and update the array
        scores = np.zeros(len(snakes))
        for si, snake in enumerate(snakes):
            scores[si] = snake.stats.get_max_age()

        return scores
