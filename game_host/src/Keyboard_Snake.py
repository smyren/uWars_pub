##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Snake

#Import Modules
import pygame, random, copy
from pygame.locals import *
from src.Tools import *
from src.Stats import *
import numpy as np
from src.User_Snake import *

class Keyboard_Snake(User_Snake):
    enable = False
    id = 0xbaba
    name = "KillU"
    move = None
    shifted_movements = ["up","left","down","right"]
    def __init__(self, id=None, name=None):
        if id is not None:
            self.id = id
        if name is not None:
            self.name = name
    
    def event( self, events ):
        print("Keyboard_Snake.event: No events defined")
        
    def update(self, game_packet ):
        if game_packet.respawn:
            # print("respawn")
            self.comment = "Hey  I'm  you"
        else:
            self.comment = ""
            
        # Add rotation ...
        # game_packet.rotation
        if self.move is None:
            movement = game_packet.movements["forward"]
        else:
            add_rotation = game_packet.rotation - 360
            rotate = (self.shifted_movements.index(self.move)*90 - add_rotation) % 360
            
            movement = game_packet.movements["forward"]
            if rotate == 180:
                movement = game_packet.movements["forward"]
            elif rotate == 0:
                movement = game_packet.movements["fast_forward"]
            elif rotate == 270:
                movement = game_packet.movements["right"]
            elif rotate == 90:
                movement = game_packet.movements["left"]

        self.move = None
        return movement
    
    def respawn( self, force_enable ):
        if force_enable:
            self.enable = True
        
        return self.enable
    
    def kill( self ):
        self.enable = False