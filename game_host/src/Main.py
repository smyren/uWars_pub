##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Main

#Import Modules
import pygame, random
from pygame.locals import *
import time, datetime, operator, threading, queue, traceback
import sys

if not pygame.font: print ('Warning, fonts disabled')
if not pygame.mixer: print ('Warning, sound disabled')

# from src.Snake import *
# from src.Food import *
from src.Board import *
from src.Board_Render import *
import maps.theme as theme
from Ruleset import *

from interface.AI_Snake import *
from interface.Keyboard_Snakes import *
from interface.Snake_Controller import *
import src.DataExport as data_export

import src.Game_Score as game_score

sys.path.append("../board_generator")
from generator.MapGenerator import *


class Benchmark():
    quiet = True
    def __init__( self ):
        self.start_time = time.time()
        self.interval_time = self.start_time
        self.total = 0
        self.totals = np.zeros(10)
        self.totals_index = 0

    def get_avg_fps( self ):
        avg_time = self.totals.mean()
        if avg_time:
            return 1/avg_time
        else:
            return 0

    def get_fps( self ):
        if self.total:
            return 1/self.total
        else:
            return 0

    def restart( self ):
        now = time.time()
        self.total = now - self.start_time
        self.totals[self.totals_index] = self.total
        self.totals_index += 1
        self.totals_index = self.totals_index % self.totals.size
        if not self.quiet:
            print( "BM {:s}:{:.0f} ms".format( "Total", self.total*1000) )
        self.start_time = time.time()
        self.interval_time = self.start_time

    def measure( self, text ):
        now = time.time()
        if not self.quiet:
            print( "BM {:s}:{:.0f} ms".format( text, (now - self.interval_time)*1000) )
        self.interval_time = now

class Game_flow:
    # class game_score:
    #     active_snakes = 0
    #     ranking = -1

    def __init__(self):
        self.round_count = 0
        self.pre_jingle_start = 0
        self.show_radar_for_snake = None
        self.max_snake_id = None
        self.max_snake_name = ""
        self.max_snake_lenght = 0

        self.bm = Benchmark()
        # Initialize Everything
        pygame.init()

        self.ruleset = Ruleset()

        self.map = None
        self.wall = Wall( id = self.ruleset.board_id_base.board_object ) # Create walls
        map_from_file = self.ruleset.map_file_name #self.map.map_images
        generate_map = self.ruleset.generate_map
        if generate_map:
            map_generator = MapGenerator()
            self.map, self.wall = map_generator.generate_runtime()
        elif map_from_file:
            self.map = Map()
            self.map.load_map(map_from_file, self.wall)
        if self.ruleset.display.scale_to_map:
            self.wall.generate_map( None, self.ruleset.tile_map )
            self.ruleset.display.board_size = self.wall.board_size   # Update rulset with new board size
            if map_from_file or generate_map:
                self.board_render = Board_Render( self.ruleset.board_id_base, self.ruleset.display, self.map.map_images, self.ruleset.snake_radar )
            else:
                theme.path = os.path.join(main_dir, theme.path)
                self.board_render = Board_Render( self.ruleset.board_id_base, self.ruleset.display, theme, self.ruleset.snake_radar )
            self.board = Board( self.ruleset.board_id_base, self.board_render.get_board_size(), self.board_render.get_number_of_active_objects()  )
        else:
            if map_from_file or generate_map:
                self.board_render = Board_Render( self.ruleset.board_id_base, self.ruleset.display, self.map.map_images, self.ruleset.snake_radar )
            else:
                theme.path = os.path.join(main_dir, theme.path)
                self.board_render = Board_Render( self.ruleset.board_id_base, self.ruleset.display, theme, self.ruleset.snake_radar )
            self.board = Board( self.ruleset.board_id_base, self.board_render.get_board_size(), self.board_render.get_number_of_active_objects()  )
            self.wall.generate_map( self.board.get_board_size(), self.ruleset.tile_map )

        if self.ruleset.display.board_size == (0,0): # If map size is still 0 at this point generate empty wall maps with same size as board
            self.wall.generate_map( self.board_render.get_board_size(), self.ruleset.tile_map )
            
        self.board.update_object_radar( self.wall ) # Add walls to radar
        # Save screenshot of clean board....
        # self.board_render.save_screenshot(  self.ruleset.save_local_screenshot,
                                            # self.data_exporter.get_database_folder_path(),
                                            # None, None, None )


        # pygame.mouse.set_visible(0)

        # Prepare Game Objects
        self.clock = pygame.time.Clock()

        # Create food
        self.food = Food( self.board.get_board_size(), (0,0,0), self.ruleset.board_id_base.food )
        self.food.add_random_food(self.ruleset.add_food_per_round*10, self.board.get_board(), self.ruleset.max_random_food_on_board, max_age = self.ruleset.food_age_range)
        self.board.update_object_radar( self.food ) # Add food to radar

        # Create snakes
        self.snakes = []
        self.snake_index = 0

        # Keyboard controlled snakes
        if self.ruleset.enable_keyboard_controlled_snake:
            self.snakes.append( Snake( Keyboard_Snake1(0xbabe1, "user 1"),
                                       self.board.get_board(),
                                       self.board_render.get_snake_color(self.snake_index),
                                       self.ruleset.snake_length,
                                       self.ruleset.keyboard_snake_response_time_offset,
                                       self.round_count,
                                       False ) )
            self.snakes[-1].stats.disable()
            self.snake_index += 1
            self.snakes.append( Snake( Keyboard_Snake2(0xbabe2, "user 2"),
                                       self.board.get_board(),
                                       self.board_render.get_snake_color(self.snake_index),
                                       self.ruleset.snake_length,
                                       self.ruleset.keyboard_snake_response_time_offset,
                                       self.round_count,
                                       False ) )
            self.snakes[-1].stats.disable()
            self.snake_index += 1

        # HW snake support
        self.snake_controller = Snake_Controller( self.ruleset.com_read_timeout, self.ruleset.get_selftiming )

        # User snakes
        if self.ruleset.enable_user_snakes:
            user_dir = os.path.abspath(os.path.join(main_dir, '../user'))
            class_name = "Snake"
            for filename in os.listdir( user_dir ):
                if os.path.splitext( filename )[1] == '.py':
                    try:
                        module = __import__('user.' + os.path.splitext( filename )[0], fromlist=[class_name])
                        User_Snake = getattr(module, class_name)
                        user_snake = User_Snake()
                        if user_snake.enable:
                            self.snakes.append( Snake( user_snake,
                                                       self.board.get_board(),
                                                       self.board_render.get_snake_color(self.snake_index),
                                                       self.ruleset.snake_length,
                                                       self.ruleset.user_snake_response_time_offset,
                                                       self.round_count ) )
                            self.snake_index += 1
                    except:
                        print("Loading ", filename, " FAILED")
                        print( traceback.format_exc() )

        # AI snakes
        for i in range(self.ruleset.number_of_ai_snakes):
            self.snakes.append( Snake( AI_Snake(0xa100+i, "ai " + str(i)),
                                       self.board.get_board(),
                                       self.board_render.get_snake_color(self.snake_index),
                                       self.ruleset.snake_length,
                                       self.ruleset.ai_snake_response_time_offset,
                                       self.round_count ) )
            self.snake_index += 1


        # Update board
        # self.pre_comments = {}
        for snake in self.snakes:
            self.board.update_object_radar( snake, self.ruleset.board_id_base, self.board_render ) # Add snake to radar
            # self.pre_comments[snake.id] = ["", self.ruleset.snake_comment_time]


        self.icon_info = {}
        self.update_scrollbar_icons( True ) # Generate scroll bar

        # Create a instance of the specified game score class
        try:
            self.game_score_calc = getattr(game_score, self.ruleset.game_score.game_score_class)()
        except AttributeError:
            print("Error - The game score class '{}' was not found.".format(self.ruleset.game_score.game_score_class))
            print("Please check that the name has been typed correctly in the ruleset file.")
            sys.exit(1)

        # Start data threads
        self.enable_threading = True
        self.export_ready = False
        self.add_snake_queue = queue.Queue()
        self.stats_queue = queue.Queue(maxsize=1)
        self.game_data_queue = queue.Queue(maxsize=1)
        self.export_thread = threading.Thread( target=self.export_thread )
        self.export_thread.start()
        self.update_scrollbar_thread()

        while(self.export_ready == False): # Wait for exporter setup to complete
            pass

    def update_scrollbar_icons( self, force_update = False ):
        active_snakes = 0
        update_icons = force_update
        icon_info = {}
        for snake in self.snakes:
            if snake.is_alive():
                head_board_id = self.board._board_objects.get_board_id_by_id( snake.id )+1
                icon_info[snake.id] = [ head_board_id , snake.stats.get_name() ]
                if not snake.id in self.icon_info: # New snake added
                    update_icons = True

                active_snakes += 1

        if update_icons or ( len(icon_info) < len(self.icon_info) ):
            # print("Updating scrollbar icons")
            self.icon_info = icon_info
            self.board_render.draw_user_icon_section( self.icon_info )

        # self.game_score.active_snakes = len(icon_info)

    def add_snake_nodes( self ):
        new_snake_nodes, del_snake_nodes = self.snake_controller.get_new_snake_nodes()
        for snake_node in new_snake_nodes:
            found_existing_node = False
            for snake in self.snakes:
                if (snake.snake_if.ctrl is not None) and (snake.snake_if.enable == False):
                    if cryptate_id(snake_node.id) == snake.id:
                        print("Main.add_snake_nodes: Reactivating existing node. Previous position:", snake.snake_if.ctrl.ser.name, snake.snake_if.node)
                        found_existing_node = True
                        snake.snake_if = snake_node
                        snake.stats.enable()
                        break

            if found_existing_node == False:
                print("Main.add_snake_nodes: New node")
                self.snakes.append( Snake( snake_node,
                                           self.board.get_board(),
                                           self.board_render.get_snake_color(self.snake_index),
                                           self.ruleset.snake_length,
                                           self.ruleset.hw_snake_response_time_offset,
                                           self.round_count ) )
                self.snake_index += 1
                self.board.update_object_radar( self.snakes[-1], self.ruleset.board_id_base, self.board_render ) # Add snake to radar
                self.export_icons(self.snakes[-1])

        # Deactivate missing snake nodes
        for snake_node in del_snake_nodes:
            for snake in self.snakes:
                if (snake.snake_if.ctrl is not None) and (snake.snake_if.ctrl.ser == snake_node[0]) and (snake.snake_if.node == snake_node[1]):
                    print("Snake node removed!")
                    snake.snake_if.deactivate()
                    snake.kill( snake.stats.get_id() )

    def update_scrollbar_thread( self ):
        self.draw_scrollbar()
        if self.enable_threading:
            self.thread = threading.Timer( self.ruleset.display.icon_scroll_speed/1000, self.update_scrollbar_thread )
            self.thread.start()

    def play_jingle( self ):
        if self.ruleset.display.graphics and self.ruleset.display.enable_sound:
            now_s = time.time()
            if ( self.pre_jingle_start == 0 ) or ( (now_s - self.pre_jingle_start) > self.ruleset.display.jingle_interval ):
                self.board_render.play_jingle()
                self.pre_jingle_start = now_s
        else:
            self.board_render.stop_jingle()

    def wait_next_tick( self ):
        if self.ruleset.display.graphics:
            self.clock.tick( self.ruleset.game_speed )
            # self.clock.tick( self.ruleset.display.icon_scroll_speed )

    def event_handler( self, halt ):
        going = True
        save_screenshot = False

        events = pygame.event.get()
        if halt:
            self.board_render.splash_text( "Press any key to start" )
            for event in events:
                if event.type == KEYDOWN:
                    halt = False
                    self.board_render.force_redraw()
        else:
            if self.ruleset.single_step:
                # Wait for key press per round
                wait = True
                while wait:
                    for event in events:
                        if event.type == KEYDOWN:
                            wait = False
                        if event.type == QUIT:
                            wait = False
                            going = False
                    if wait:
                        events = pygame.event.get()


            #Handle Input Events
            for event in events:
                if event.type == QUIT:
                    going = False
                    for snake in self.snakes:
                        if snake.is_alive():
                            snake.kill( "Board" )
                            # snake.snake_if.deactivate()

                elif event.type == KEYDOWN:
                    # Check for stop event
                    # if pygame.key.get_mods() & KMOD_LCTRL and event.key == K_s:
                        # print("Stop!")
                        # going = False
                        # for snake in self.snakes:
                            # snake.kill( "Board" )
                            # snake.snake_if.deactivate()

                    if self.ruleset.enable_keyboard_controlled_snake:
                        # User snake 1
                        if event.key == K_1:
                            for snake in self.snakes:
                                if type(snake.snake_if) == Keyboard_Snake1:
                                    if not snake.is_alive():
                                        snake.respawn( self.ruleset.snake_length,
                                                       self.round_count,
                                                       self.board.get_board(),
                                                       True )
                                        self.board.update_object_radar( snake, self.ruleset.board_id_base, self.board_render )

                        # User snake 2
                        if event.key == K_2:
                            for snake in self.snakes:
                                if type(snake.snake_if) == Keyboard_Snake2:
                                    if not snake.is_alive():
                                        snake.respawn( self.ruleset.snake_length,
                                                       self.round_count,
                                                       self.board.get_board(),
                                                       True )
                                        self.board.update_object_radar( snake, self.ruleset.board_id_base, self.board_render )

                    if event.key == K_ESCAPE:
                        going = False

                    # Help screen
                    elif event.key == K_h:
                        self.board_render.toggle_help_screen()

                    # Test mode: Single step
                    elif event.key == K_t:
                        self.ruleset.single_step = not self.ruleset.single_step

                    elif event.key == K_f:
                        self.ruleset.display.enable_fps = not self.ruleset.display.enable_fps

                    # Matrix mode
                    elif event.key == K_m:
                        self.board_render.toggle_matrix_mode()

                    # Graphics mode
                    elif event.key == K_g:
                        self.ruleset.display.graphics = not self.ruleset.display.graphics
                        if self.ruleset.display.graphics:
                            self.board_render.force_redraw()
                        else:
                            self.board_render.splash_text( "Graphics off" )
                            self.board_render.flip()

                    # Export data mode
                    elif event.key == K_e:
                        self.ruleset.data_export.enable = not self.ruleset.data_export.enable

                    elif event.key == K_n:
                        self.ruleset.display.enable_sound = not self.ruleset.display.enable_sound

                    elif event.key == K_KP_PLUS:
                        self.ruleset.game_speed *= 2
                        if self.ruleset.game_speed > 128:
                            self.ruleset.game_speed = 128

                    elif event.key == K_KP_MINUS:
                        self.ruleset.game_speed = int(self.ruleset.game_speed/2)
                        if self.ruleset.game_speed < 1:
                            self.ruleset.game_speed = 1

                    # Toggle radar image
                    elif event.key == K_r:
                        if self.show_radar_for_snake is None:
                            self.show_radar_for_snake = 0
                        elif self.show_radar_for_snake >= len(self.snakes)-1:
                            self.show_radar_for_snake = None
                        else:
                            self.show_radar_for_snake += 1

                    elif event.key == K_p:
                        save_screenshot = True

            for snake in self.snakes:
                if type(snake.snake_if) == Keyboard_Snake1:
                    snake.snake_if.event(events)
                if type(snake.snake_if) == Keyboard_Snake2:
                    snake.snake_if.event(events)

        return halt, going, save_screenshot

    def is_keyboard_snake( self, snake ):
        return ( (type(snake.snake_if) == Keyboard_Snake1) or (type(snake.snake_if) == Keyboard_Snake2) )

    def is_hw_snake( self, snake ):
        return (type(snake.snake_if) == Snake_Node) 
        
    def snake_handler(self, look_for_snakes = True):
        # Respawn snakes
        for snake in self.snakes:
            if not snake.is_alive():
                snake.respawn( self.ruleset.snake_length, self.round_count, self.board.get_board() ) #board_copy )
                self.board.update_object_radar( snake, self.ruleset.board_id_base, self.board_render )
                if not snake.is_alive() and self.is_hw_snake(snake): # Scan for new nodes on next run 
                    look_for_snakes = True
                    print("Main.snake_handler: HW snake lost. Forcing scan for HW change.")

        # print(self.game_score.active_snakes)
        if look_for_snakes:
            self.add_snake_nodes() # Check for new connections
            Snake_Controller.scan_for_hw_change = False

        # active_snake_count = 0
        active_snakes = []
        snake_return = {}
        response_time = np.zeros(len(self.snakes)) -1
        # board_copy = self.board.get_board()
        # self.bm.measure( "snake_handler1" )

        for si, snake in enumerate(self.snakes):
            # Get snake movement
            if snake.is_alive():
                radar_image, head_position = self.board.get_radar_image( snake.parts[0], self.ruleset.snake_radar )
                game_packet = Game_packet(radar_image,
                                          head_position,
                                          snake.parts[0].rotation,
                                          False,
                                          snake.length,
                                          len(self.snakes),     # ...or only the currently active snakes?
                                          snake.stats.get_rank())
                snake_return[snake.id] = snake.update( game_packet, self.ruleset, self.round_count )
                response_time[si] = snake.current_response_time
            else:
                response_time[si] = -1
                snake_return[snake.id] = []

            # if snake.is_alive(): # Dead snakes does not show on list. For user snakes to not appear when not active
            # active_snake_count += 1
            active_snakes.append(snake.stats)
            # snakeNames.append(snake.stats.get_name() + "            " + "{:.2f}".format(snake.stats.get_mean_length()) + " / " + "{:.0f}".format(snake.stats.get_max_length()) )

        # self.game_score.active_snakes = active_snake_count
        self.update_scrollbar_icons()

        # self.bm.measure( "snake_handler2" )
        # Now order snakes according to response time
        snake_order = []
        if len(response_time):
            np.place( response_time, response_time==-1, response_time.max()+1 )
            snake_order = response_time.argsort()
            # Randomize equal response times
            pre_time = -1
            randomize_indexes = []
            found_equal_timing = False
            for soi, si in enumerate(snake_order):
                if response_time[si] == pre_time:
                    if found_equal_timing == False:
                        found_equal_timing = True
                        randomize_indexes.append([soi-1])
                    randomize_indexes[-1].append(soi)
                else:
                    found_equal_timing = False

                pre_time = response_time[si]

            # print("Before:", snake_order)
            for ril in randomize_indexes:
                random.shuffle(snake_order[ril[0]:ril[-1]+1])
            # print("After", snake_order)
            # print(response_time)

        # print("snake order:  ", snake_order)
        # Reorder based on slow_move flag
        slow_move_indexes = []
        fast_move_indexes = []
        for si in snake_order:
            snake = self.snakes[si]
            if snake.slow_move:
                # print("Slow move on:", snake.stats.get_name())
                slow_move_indexes.append(si)
            else:
                fast_move_indexes.append(si)

        snake_order = fast_move_indexes + slow_move_indexes[-1::-1]
        # print("snake reorder:", snake_order)

        # for soi, si in enumerate(snake_order):
        for si in snake_order:
            snake = self.snakes[si]
            #print ("Snake ",snake.id, ":", snake.current_response_time)
            remains = snake_return[snake.id]
            if not snake.is_alive():
                if self.ruleset.spawn_food_on_death and remains is not None: # If snake killed itself by running out of the board
                    # Should only kick in when snake node is disconnected physically
                    self.food.add_food( remains, self.ruleset.max_food_on_board, max_age = self.ruleset.food_age_range )

            # Check for collisions
            if snake.is_alive():
                snake.execute_move() # Do the actual move

            # self.bm.measure( "snake_handler snake collision" )
            if snake.is_alive():
                # Check for wall
                if self.wall.is_wall( snake.parts[0].position ) or \
                   (len(snake.parts) > 1 and self.wall.is_wall( snake.parts[1].position )):
                    # remains = snake.kill( self.wall.id )
                    # print("Main.snake_handler: Killed by Wall!!")
                    remains = snake.kill( "Board", selfkill = True )
                    if self.ruleset.spawn_food_on_death and remains is not None:
                        self.food.add_food( remains, self.ruleset.max_food_on_board, max_age = self.ruleset.food_age_range )


            if snake.is_alive():
                # Only check for collision
                killed_other_snakes = []
                for other_snake in self.snakes:
                    # Check for collision to other snake head and body next to head in case of move 2
                    got_killed = False
                    if other_snake.is_alive():
                        if other_snake is snake:
                            if self.ruleset.selfkill: # Check for selfkill
                                skip = 2
                                if other_snake.is_snake( snake.parts[0].position, skip ):
                                    # print(snake.stats.get_name(), "self kill")
                                    got_killed = True
                                if snake.last_move == Game_packet.movements["fast_forward"] and len(snake.parts) > 1 and other_snake.is_snake( snake.parts[1].position, skip ):
                                    # print(snake.stats.get_name(), "self kill")
                                    got_killed = True
                        else:
                            if other_snake.is_snake( snake.parts[0].position ):
                                if self.ruleset.eat_other_snakes:
                                    # print(snake.stats.get_name(), "killed", other_snake.stats.get_name())
                                    # Kill other snake
                                    if self.ruleset.cut_other_snakes:
                                        remains = other_snake.kill( snake.id, position = snake.parts[0].position )
                                    else:
                                        remains = other_snake.kill( snake.id )

                                    if self.ruleset.spawn_food_on_death and remains is not None:
                                        self.food.add_food( remains, self.ruleset.max_food_on_board, max_age = self.ruleset.food_age_range )
                                    # Update radar with kill info
                                    #self.board.update_object_radar( other_snake, self.ruleset.board_id_base, self.board_render ) # If dead this will remove snake from radar

                                    if not other_snake.is_alive(): # Got killed
                                        killed_other_snakes.append(other_snake.id)

                                else:
                                    got_killed = True

                            if snake.last_move == Game_packet.movements["fast_forward"] and len(snake.parts) > 1 and other_snake.is_snake( snake.parts[1].position ):
                                if self.ruleset.eat_other_snakes:
                                    # print(snake.stats.get_name(), "killed", other_snake.stats.get_name())
                                    # Kill other snake
                                    if self.ruleset.cut_other_snakes:
                                        remains = other_snake.kill( snake.id, position = snake.parts[1].position )
                                    else:
                                        remains = other_snake.kill( snake.id )

                                    if self.ruleset.spawn_food_on_death and remains is not None:
                                        self.food.add_food( remains, self.ruleset.max_food_on_board, max_age = self.ruleset.food_age_range )
                                    # Update radar with kill info
                                    #self.board.update_object_radar( other_snake, self.ruleset.board_id_base, self.board_render ) # If dead this will remove snake from radar

                                    if not other_snake.is_alive(): # Got killed
                                        killed_other_snakes.append(other_snake.id)

                                else:
                                    got_killed = True

                        if got_killed:
                            if other_snake is snake:
                                other_snake.caused_kill( snake.id, False ) # Do not add lenght when selfkill
                            else:
                                other_snake.caused_kill( snake.id, self.ruleset.grow_per_kill )

                            remains = snake.kill( other_snake.id, selfkill = True )
                            if self.ruleset.spawn_food_on_death and remains is not None:
                                self.food.add_food( remains, self.ruleset.max_food_on_board, max_age = self.ruleset.food_age_range )
                            break

                # Register kills in the end to prevent selfkill after growing on kill...
                for other_snake_id in killed_other_snakes:
                    snake.caused_kill( other_snake_id, self.ruleset.grow_per_kill )


            # self.bm.measure( "snake_handler wall collision" )
            if snake.is_alive():
                # Check for food
                if self.food.is_food( snake.parts[0].position ):
                    # Increase snake length on next move
                    snake.grow( self.ruleset.grow_per_food )
                    self.food.remove_food( snake.parts[0].position )
                    snake.stats.add_food()
                # Grow per tick
                snake.grow( self.ruleset.grow_per_tick )

            # self.bm.measure( "snake_handler food collision" )

            # Register move in stats if snake is alive
            if snake.is_alive():
                snake.stats.add_visit( snake.parts[0].position[0], snake.parts[0].position[1] )



            # if self.ruleset.single_step:
                # pygame.event.wait()
                # self.draw(active_snakes)

        # Update radar
        # print(snake_order)
        for snake in self.snakes:
            self.board.update_object_radar( snake, self.ruleset.board_id_base, self.board_render ) # If dead this will remove snake from radar
            # self.bm.measure( "snake_handler board update" )
            # print(snake.stats.get_name(), int(snake.length), len(snake.parts), snake.last_move, snake.current_response_time, snake.is_alive() )
        # print(self.board._board)
        # print(np.flipud(self.board._board))

        self.board.update_object_radar( self.food, overwrite = False )

        # Calculate and update the score and ranking
        self.game_score_calc.update_game_ranking(self.snakes)

        # self.bm.measure( "snake_handler3" )
        if self.export_ready:
            self.export_ready = False
            self.create_export_data()
            # threading.Thread( target=self.create_export_data ).start()

        return active_snakes

    def create_export_data( self ):
        # Collect the data from all snakes
        # new_data = []
        # for snake in self.snakes:
        #     new_data.append(snake.stats.get_export_stats())
        new_data = {}
        for snake in self.snakes:
            stats = snake.stats.get_export_stats()
            new_data[stats['id']] = stats

        self.stats_queue.put( new_data )

    def draw(self, active_snakes, save_previous_screen=False):
        if save_previous_screen:
            image_path = self.board_render.save_screenshot(
                                self.ruleset.save_local_screenshot,
                                self.data_exporter.get_database_folder_path(),
                                self.board._pre_board,
                                self.board._board_objects.get_board_id_by_id( self.max_snake_id )+1,
                                self.max_snake_name + ': ' + str(self.max_snake_lenght)
                                )
            if not self.game_data_queue.empty():
                self.game_data_queue.queue.clear()

            self.game_data_queue.put({'max_length_img': image_path,
                                      'max_length_round_num': self.round_count,
                                      'max_length_time': datetime.datetime.now().strftime("%Y%m%dT%H%M%S.%f"),
                                     })

        # Draw
        if self.ruleset.display.graphics:
            # self.board_render.draw_background()
            # self.bm.measure( "Draw" )
            board_change_array = self.board.get_change_array()
            # self.bm.measure( "Draw1" )
            self.board_render.draw_board( self.board.get_board(), board_change_array, self.board._board_object_rotation, self.board._board_object_sub_id )
            # self.bm.measure( "Draw2" )

            for snake in self.snakes:
                if snake.is_alive():
                    comment = snake.stats.get_comment()
                    if comment != "":
                        self.board_render.draw_effect( snake.parts[0].position, comment )
            # self.bm.measure( "Draw3" )

            self.board_render.update_status_text(active_snakes)
            # self.bm.measure( "Draw" )
            if self.show_radar_for_snake is not None:
                if len( self.snakes[self.show_radar_for_snake].parts):
                    radar_image, head_position = self.board.get_radar_image( self.snakes[self.show_radar_for_snake].parts[0], self.ruleset.snake_radar )
                    self.board_render.draw_radar_image( radar_image, head_position, self.snakes[self.show_radar_for_snake].stats.get_name() )
                else:
                    self.board_render.draw_radar_image( None, None, self.snakes[self.show_radar_for_snake].stats.get_name() )
            # self.bm.measure( "Draw4" )

            # self.board_render.update_gamearea( ) # Draw on screen

        if self.ruleset.display.enable_fps:
            self.board_render.draw_info_text( "{:0.0f}/{:d} fps".format(self.bm.get_avg_fps(), self.ruleset.game_speed) )

        self.board_render.draw_help_screen()
        self.board_render.push_to_screen()

    def draw_scrollbar( self ):
        self.board_render.update_scrollbar(2)

    def init_data_exporter( self ):
        """Setup game data export, run in the export thread"""
        self.data_exporter = None
        if self.ruleset.data_export.enable:
            # Set up the data exporter
            self.data_exporter = data_export.DataExport(
                        self.ruleset.data_export.file_name,
                        self.ruleset.data_export.folder_name,
                        self.ruleset.data_export.dyn_file_path,
                        self.ruleset.data_export.static_folder_name,
                        self.ruleset.data_export.static_file_path,
                        self.ruleset.data_export.min_write_interval)

            # Create a temporary backup of the previous game data (for the cases where the ordinary
            # backup hasn't been run, eg after a crash
            if self.ruleset.data_export.enable_backup:
                if not self.data_exporter.run_temp_backup():
                    print("Failed backup of the previous database, check the folder permissions")

            self.data_exporter.initialize_database()
            if self.data_exporter.error_in_last_cmd():
                print("Error setting up data export, no game data will be exported")
                print("  -> " + self.data_exporter.get_last_cmd_status_msg())
                # No valid DataExport could be made, clearing the variable
                self.data_exporter = None
                # TBD - exit or give warning the user through the GUI?
        else:
            print("Data export has been disabled")

    def export_thread( self ):
        self.init_data_exporter()

        # Set the initial game parameters
        game_start_time = datetime.datetime.now().strftime("%Y%m%dT%H%M%S.%f")
        if self.map is not None:
            # Load the board description
            board_desc = self.map.get_description()
        else:
            # No board loaded, set default description
            board_desc = 'Basic board with no walls'

        self.data_exporter.set_game_data({'game_name': 'uWars',
                                          'game_start_time': game_start_time,
                                          'rounds_played': 0,
                                          'board_size_y': self.board.get_board_size()[0],
                                          'board_size_x': self.board.get_board_size()[1],
                                          'max_length_img': '',
                                          'max_length_round_num': 0,
                                          'max_length_time': '',
                                          'winners_file': self.ruleset.data_export.winners_file,
                                          'dashboard': self.game_score_calc.dashboard,
                                          'game_score_desc': self.game_score_calc.description,
                                          'board_desc': board_desc
                                          })

        # Update the player info for all snakes that are currently connected. The add_player
        # function should be run again for any snake that is added or reconnected later in the game
        for snake in self.snakes:
            self.export_icons(snake)

        while self.enable_threading:
            time.sleep( self.data_exporter.get_minimum_db_write_interval() )
            # Export stats
            if self.ruleset.data_export.enable:

                # First make sure the data_exporter is still alive
                if self.data_exporter:
                    while( not self.add_snake_queue.empty() ):
                        self.data_exporter.add_player(self.add_snake_queue.get())

                    if not self.game_data_queue.empty():
                        self.data_exporter.set_game_data(self.game_data_queue.get())

                    self.export_ready = True
                    try:
                        new_data = self.stats_queue.get( timeout = 1)
                    except:
                        new_data = None
                        # print("export_thread: No data received")
                    else:
                        # Export the data
                        # print("Export data to database")
                        round_count = self.round_count
                        self.data_exporter.export_data(round_count, new_data)
                        if self.data_exporter.error_in_last_cmd():
                            print("Error exporting stats")
                            print("  -> " + self.data_exporter.get_last_cmd_status_msg())
                            # TBD - error exporting data, ignore or try to reconnect?
                            pass

                else:
                    # TBD - data export has failed, try to reconnect
                    pass

        if self.ruleset.data_export.enable_backup:
            # Close the database and make a backup of the files
            self.data_exporter.close_and_backup_database()
        else:
            self.data_exporter.close_database()

        del self.data_exporter

    def export_icons(self, snake):
        head_board_id = self.board._board_objects.get_board_id_by_id( snake.id )+1
        full_file_name, full_file_name_ui = self.board_render.export_icon_image(
                                                head_board_id,
                                                snake.id,
                                                self.data_exporter.get_database_folder_path())
        snake.stats.set_head_img_file_name( full_file_name )
        snake.stats.set_icon_img_file_name( full_file_name_ui )
        # print( snake.stats.get_head_img_file_name() )
        # print( snake.stats.get_icon_img_file_name() )
        self.add_snake_queue.put(snake.stats.get_static_player_info()) # Make snake available to game monitor


    def get_max_snake_length( self ):
        lenght = -1
        id = None
        name = None
        for snake in self.snakes:
            if np.floor(snake.length) > lenght:
                lenght = int(np.floor(snake.length))
                id = snake.id
                name = snake.stats.get_name()

        return id, name, lenght

    def run( self ):

        if self.ruleset.enable_snake_radar_image:
            self.show_radar_for_snake = 0

        # Main Loop
        halt = True
        present_snake_index = 0
        present_snake_tics = 0
        going = True
        pre_max_snake = 0
        game_max_length = 0
        max_snake_id = None
        max_snake_name = ""
        max_snake_lenght = 0
        while going:
            save_screenshot = False
            self.bm.restart()

            self.play_jingle()

            self.wait_next_tick()

            halt, going, save_screenshot = self.event_handler(halt)
            self.bm.measure( "Events" )

            if halt:
                self.add_snake_nodes() # Check for new connections
                Snake_Controller.scan_for_hw_change = False
                active_snakes = []
                for snake in self.snakes:
                    active_snakes.append(snake.stats)
                    self.board.update_object_radar( snake, self.ruleset.board_id_base, self.board_render ) # Update radar incase dock/undock

                if len(active_snakes):
                    if present_snake_index >= len(active_snakes): # If currently presented snake is the last one and a snake get un-docked select a new one
                        present_snake_index = 0
                        present_snake_tics = 0

                    self.board_render.draw_snake_info(active_snakes[present_snake_index], self.board._board_objects)

                    present_snake_tics += 1
                    if present_snake_tics % (self.ruleset.game_speed * 5) == 0:
                        present_snake_index += 1
                        if present_snake_index >= len(active_snakes):
                            present_snake_index = 0

                        # self.board_render.force_redraw()

            else:
                # Add and remove food
                self.food.cure_food()
                self.food.remove_stale_food()
                self.food.add_random_food(self.ruleset.add_food_per_round, self.board.get_board(), self.ruleset.max_random_food_on_board, max_age = self.ruleset.food_age_range)
                self.board.update_object_radar( self.food, False )

                self.bm.measure( "Add food" )

                active_snakes = self.snake_handler(look_for_snakes = (self.round_count % self.ruleset.snake_scan_interval == 0) or Snake_Controller.scan_for_hw_change)

                # Update for previous round
                self.max_snake_id = max_snake_id
                self.max_snake_name = max_snake_name
                self.max_snake_lenght = max_snake_lenght

                max_snake_id, max_snake_name, max_snake_lenght = self.get_max_snake_length()
                # print(max_snake, pre_max_snake)
                if max_snake_lenght < pre_max_snake or going == False: # Max snake died or lost length or it's the last round
                    if pre_max_snake > game_max_length:
                        # print("Max:", max_snake_lenght, pre_max_snake, game_max_length, )
                        # print(self.max_snake_name, self.max_snake_lenght)
                        game_max_length = pre_max_snake
                        # The max snake died. Save screen shot
                        save_screenshot = True
                        # print("Save")
                pre_max_snake = max_snake_lenght

                self.bm.measure( "Movement handling" )

                # self.export_stats( i )

                # self.bm.measure( "Export" )

                self.round_count += 1


            self.draw( active_snakes, save_screenshot )

            self.bm.measure( "Draw" )
            # print(np.flipud(board._board_objects))
            # print(board._board_objects)




        # Wait for previous export to complete
        while self.export_ready == False:
            pass

        # Do final export
        self.export_ready = False
        self.create_export_data()

        # Wait for final export to complete
        while self.export_ready == False:
            pass

        # Force the export thread to run a backup and close down
        self.enable_threading = False

        self.thread.cancel()
        while self.thread.isAlive() or self.export_thread.is_alive():
            pygame.time.wait(100)

        pygame.quit()

        # Game Over
