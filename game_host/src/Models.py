##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, ForeignKey, Integer, String, Float
from sqlalchemy.orm import relationship

Base = declarative_base()


class Player(Base):
    """Table of all registered snake nodes in the game

    Contains all static data for each node.
    """
    __tablename__ = 'players'

    id = Column(Integer, primary_key=True)
    node_id = Column(String(), unique=True)
    name = Column(String(), default='NONAME')
    snake_head_img = Column(String())
    snake_icon_img = Column(String())
    color1 = Column(String())
    color2 = Column(String())
    owner = Column(String(), default='')
    info = Column(String(), default='')
    score = relationship("Score", uselist=False, back_populates="players")
    chat = relationship("Chat", back_populates="players")
    # deaths = relationship("Chat", back_populates="players")
    board_map = relationship("BoardMap", back_populates="players")
    life_stats = relationship("LifeStat", back_populates="players")


class Score(Base):
    """Table for the game score."""
    __tablename__ = 'scores'
    id = Column(Integer, primary_key=True)

    #
    status = Column(String())
    length_current = Column(Integer, default=0)
    max_length_current = Column(Integer, default=0)
    alive_time_current = Column(Integer, default=0)
    rounds = Column(Integer, default=0)
    rank = Column(Integer, default=0)
    rank_score = Column(Integer, default=0)
    last_error_msg = Column(String(), default='')

    move_forwards = Column(Integer, default=0)
    move_lefts = Column(Integer, default=0)
    move_rights = Column(Integer, default=0)
    move_fast_forwards = Column(Integer, default=0)

    kills = Column(Integer, default=0)
    food = Column(Integer, default=0)

    #
    eol_length_max = Column(Integer, default=0)
    eol_length_quartile3 = Column(Float(), default=0.0)
    eol_length_median = Column(Float(), default=0.0)
    eol_length_quartile1 = Column(Float(), default=0.0)
    eol_length_min = Column(Integer, default=0)
    eol_length_sum = Column(Integer, default=0)
    eol_length_lastn_max = Column(Integer, default=0)
    eol_length_lastn_quartile3 = Column(Float(), default=0.0)
    eol_length_lastn_median = Column(Float(), default=0.0)
    eol_length_lastn_quartile1 = Column(Float(), default=0.0)
    eol_length_lastn_min = Column(Integer, default=0)
    eol_length_lastn_sum = Column(Integer, default=0)
    acc_snake_mass = Column(Integer, default=0)
    max_length_max = Column(Integer, default=0)
    max_length_quartile3 = Column(Float(), default=0.0)
    max_length_median = Column(Float(), default=0.0)
    max_length_quartile1 = Column(Float(), default=0.0)
    max_length_min = Column(Integer, default=0)
    max_length_sum = Column(Integer, default=0)
    alive_time_max = Column(Integer, default=0)
    alive_time_quartile3 = Column(Float(), default=0.0)
    alive_time_median = Column(Float(), default=0.0)
    alive_time_quartile1 = Column(Float(), default=0.0)
    alive_time_min = Column(Integer, default=0)
    alive_time_sum = Column(Integer, default=0)
    alive_time_lastn_max = Column(Integer, default=0)
    alive_time_lastn_quartile3 = Column(Float(), default=0.0)
    alive_time_lastn_median = Column(Float(), default=0.0)
    alive_time_lastn_quartile1 = Column(Float(), default=0.0)
    alive_time_lastn_min = Column(Integer, default=0)
    alive_time_lastn_sum = Column(Integer, default=0)
    response_time_max = Column(Float(), default=0.0)
    response_time_min = Column(Float(), default=0.0)
    response_time_avg = Column(Float(), default=0.0)
    response_time_sum_valid = Column(Integer, default=0)
    response_time_num_valid = Column(Integer, default=0)
    response_time_num_missed = Column(Integer, default=0)
    deaths = Column(Integer, default=0)

    #? board_x = Column(Integer)
    #? board_y = Column(Integer)
    #? total_rounds = Column(Integer)

    player_id = Column(Integer, ForeignKey('players.id'))
    players = relationship("Player", back_populates="score")


class Game(Base):
    """Table for common game parameters"""
    __tablename__ = 'game'

    # id = Column(Integer, primary_key=True)
    key = Column(String(), primary_key=True)
    value = Column(String())


class Chat(Base):
    """Table for chat messages.

    This table is updated for every move/tick in the game.
    """
    __tablename__ = 'chat'

    id = Column(Integer, primary_key=True)
    round = Column(Integer)
    msg = Column(String())

    player_id = Column(Integer, ForeignKey('players.id'))
    players = relationship("Player", back_populates="chat")


class Death(Base):
    __tablename__ = 'deaths'

    # id = Column(Integer, primary_key=True)
    node_id = Column(String(), primary_key=True)
    killer_id = Column(String(), primary_key=True)
    count = Column(Integer)

    # Self referencing, many to many?
    # player_id = Column(Integer, ForeignKey('players.id'))
    # players = relationship("Player", back_populates="deaths")
    # killer_id = Column(Integer, ForeignKey('players.id'))
    # killer_snake = relationship("Player", back_populates="deaths")


class BoardMap(Base):
    """Table of all coordinates each snake has visited or died on"""
    __tablename__ = 'board_map'

    x = Column(Integer, primary_key=True)
    y = Column(Integer, primary_key=True)
    death_count = Column(Integer, default=0)
    visit_count = Column(Integer, default=0)

    player_id = Column(Integer, ForeignKey('players.id'), primary_key=True)
    players = relationship("Player", back_populates="board_map")


class LifeStat(Base):
    """Table for logging stats pr snake life"""
    __tablename__ = 'life_stats'

    id = Column(Integer, primary_key=True)
    start_round = Column(Integer, default=0)
    eol_length = Column(Integer, default=0)
    max_length = Column(Integer, default=0)
    age = Column(Integer, default=0)

    player_id = Column(Integer, ForeignKey('players.id'))
    players = relationship("Player", back_populates="life_stats")
