##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Snake

#Import Modules
import pygame, random, copy
import numpy as np
from src.Tools import *
from src.Stats import *
import copy, datetime, traceback
from interface.Snake_Controller import Snake_Node

class Snake( Game_object ):
    def __init__(self, snake_if, board_objects, color, start_length, response_time_offset,
                 round_num, spawn=True):
        self.snake_if = snake_if
        self.icon = snake_if.icon
        self.board_size = (board_objects.shape[0], board_objects.shape[1])
        self.id = cryptate_id(self.snake_if.id) # Cryptate ID
        if self.snake_if.colors == None:
            self.color = color
        else:
            self.color = self.snake_if.colors

        # Create object for keeping snake stats
        self.stats = Stats( self.board_size, self.id, self.snake_if.name, self.color,
                            snake_owner=self.snake_if.owner,
                            snake_info=self.snake_if.info )

        self.length = 0
        self.alive = False
        self.parts = []
        self.pre_parts = []
        self.current_response_time = -1
        self.response_time_offset = response_time_offset
        self.slow_move = False
        self.last_move = None

        self.respawned = True
        if spawn:
            self.respawn( start_length, round_num, board_objects )

    def update(self, game_packet, ruleset, round_num ):
        game_packet.respawn = self.respawned

        if type(self.snake_if) == Snake_Node:
            # HW snake
            if not self.snake_if.enable:
                # Snake not enabled anymore
                return self.kill( self.stats.get_id(), selfkill = True ) # Exit here

            snake_movement = self.snake_if.update( game_packet )
            if not self.snake_if.enable:
                # Snake not enabled anymore
                return self.kill( self.stats.get_id(), selfkill = True ) # Exit here

            if self.snake_if.response_time == -1:
                self.current_response_time = self.snake_if.response_time # Timeout
            else:
                self.current_response_time = self.snake_if.response_time + self.response_time_offset

            self.stats.update_response_time( self.snake_if.response_time )
            #self.stats.set_name( self.snake_if.name )  # Update name if it was changed
            self.stats.set_comment( self.snake_if.comment, ruleset.snake_comment_time, round_num )

        else:
            # SW snake
            try:
                start_time = datetime.datetime.now()
                snake_movement = self.snake_if.update( game_packet )
                self.current_response_time = (datetime.datetime.now() - start_time).total_seconds() + self.response_time_offset
                self.stats.update_response_time( self.current_response_time )
                self.stats.set_name( self.snake_if.name )  # Update name if it was changed
                self.stats.set_comment( self.snake_if.comment, ruleset.snake_comment_time, round_num )
            except:
                self.stats.set_exception( traceback.format_exc() )
                # print("Exception string:", self.stats.get_exception())
                snake_movement = game_packet.movements["forward"] # If snake interface is not responding
                self.stats.update_response_time( -1 )

        if snake_movement > 3: # Not a legal move
            snake_movement = game_packet.movements["forward"]

        self.slow_move = self.snake_if.slow_move
        self.last_move = snake_movement
        self.stats.add_movement(snake_movement)
        self.respawned = False

        self.rotate = 0
        # if snake_movement == game_packet.movements["forward"]: # Nothing to do, continue in same direction
        if snake_movement == game_packet.movements["fast_forward"]:
            if self.move[0]:
                self.move[0] = 2 * int(self.move[0]/abs(self.move[0]))
            elif self.move[1]:
                self.move[1] = 2 * int(self.move[1]/abs(self.move[1]))
            else:
                print("Snake: unknown snake.move state")
        else:
            # Make sure move is set to defaul speed '1'
            if self.move[0]:
                self.move[0] = int(self.move[0]/abs(self.move[0]))
            if self.move[1]:
                self.move[1] = int(self.move[1]/abs(self.move[1]))

        if snake_movement == game_packet.movements["left"]:
            self.rotate = 90
        elif snake_movement == game_packet.movements["right"]:
            self.rotate = 270

        return None

    def respawn(self, length, round_num, board_objects, force_enable = False):
        if self.snake_if.respawn( force_enable ):
            # Create new snake
            start_position, direction = self.get_random_position(board_objects)
            if start_position is not None:
                self.respawned = True
                self.stats.respawn(1, round_num)
                self.stats.enable()

                self.length = 1 # Float length (not real int length)
                self.alive = True

                self.move = direction[0]
                self.start_angle = direction[1]

                # Create head
                self.parts = [ Part( start_position, self.start_angle ) ]

                self.grow(length-1)

            else:
                print ("Snake.respawn: No place to respawn")

    def grow(self, add_length):
        self.length += add_length
        if np.floor(self.length) > self.stats.get_current_length(): # Length increased to new sprite
            self.add_sprite( self.parts[-1].position, self.parts[-1].rotation, int(np.floor(self.length) - self.stats.get_current_length()) ) # Appends to the end
        elif np.floor(self.length) < self.stats.get_current_length(): # Length decreased to new sprite
            self.remove_sprite( int(self.stats.get_current_length() - np.floor(self.length)) )
            if len(self.parts) == 0: # Self kill
                self.kill( self.stats.get_id(), selfkill = True ) # Exit here
                return

    def add_sprite(self, position, rotation, add_length ):
        self.stats.change_length( add_length )
        for i in range(add_length):
            self.parts.append( Part( position, rotation ) )

    def remove_sprite(self, sub_length ):
        sub_length = min(sub_length, self.stats.get_current_length())
        self.stats.change_length( -sub_length )
        del self.parts[-sub_length:]

    def get_random_position( self, board_objects ):
        # Generate random position, but keep away from wall to make room for tale
        # x = int(random.random() * (self.game_area.width-3)) + 1
        # y = int(random.random() * (self.game_area.height-3)) + 1
        avail_index = np.where(board_objects == 0)
        if len(avail_index[0]):
            index = random.choice(range(len(avail_index[0])))
            y = avail_index[0][index]
            x = avail_index[1][index]
            direction = random.choice( [ ([0, 1], 270), ([0, -1], 90), ([-1, 0], 180), ([1, 0], 0) ]  )
            return (y, x), direction
        else:
            return None, None

    def execute_move(self):
        # Save backup of previous parts. Used to spawn food and record death location
        if self.length > 2:
            self.pre_parts = copy.deepcopy(self.parts[-2:])
        else:
            self.pre_parts = copy.deepcopy(self.parts)

        # print(self.pre_parts, self.pre_parts[0].position)

        # Rotate snake head
        newpos = [0,0]
        if self.rotate == 90:
            newpos[0] = self.move[1]
            newpos[1] = -self.move[0]
            self.move = newpos
        elif self.rotate == 270:
            newpos[0] = -self.move[1]
            newpos[1] = self.move[0]
            self.move = newpos

        if len(self.parts) > 1:
            # Take last sprite and move it to head position, then move head to new position
            self.parts[-1] = copy.copy(self.parts[0]) # First copy head information to tail
            self.parts = [self.parts[0]] + [self.parts[-1]] + self.parts[1:-1] # Move parts around

            # If fast forward then move last part close to head
            if abs(self.move[0]) == 2 or abs(self.move[1]) == 2:
                self.parts[-1] = copy.copy(self.parts[0])
                if self.move[0] == 2:
                    move_part = [1,0]
                elif self.move[0] == -2:
                    move_part = [-1,0]
                elif self.move[1] == 2:
                    move_part = [0,1]
                else:
                    move_part = [0,-1]
                self.parts[-1].set_position( (self.parts[-1].y + move_part[0], self.parts[-1].x + move_part[1]) )
                self.parts = [self.parts[0]] + [self.parts[-1]] + self.parts[1:-1]


        # Update head position
        self.parts[0].set_position( ( self.parts[0].position[0] + self.move[0], self.parts[0].position[1] + self.move[1] ) )
        self.parts[0].set_rotation( (self.parts[0].rotation + self.rotate) % 360 )

        # Return new head position
        head = self.parts[0].position
        try: # Will fail if snake is out of board, but will handle that later
            if head[0] < 0 or head[0] >= self.board_size[0]:
                # Snake is out of the board, either above or below
                pass
            elif head[1] < 0 or head[1] >= self.board_size[1]:
                # Snake is out of the board, either to the left or the right
                pass
            else:
                # Snake is still within the board, updating the position
                # self.stats.add_visit( head[0], head[1] )
                pass
        except:
            print(traceback.format_exc())
            pass

    def get_head_position(self):
        return self.parts[0].position

    def is_snake( self, position, skip = 0 ):
        for i in range(skip, len(self.parts)):
            if self.parts[i].position == position:
                return True
        return False

    def caused_kill( self, killed_snake_id, grow_per_kill ):
        self.stats.add_kill( killed_snake_id )
        self.grow( grow_per_kill )

    def kill(self, killer_id, position = None, selfkill = False):
        # print(self.parts[0].position, self.pre_parts[0].position)
        remains = []
        if len(self.parts) == 0:
            # Kill snake
            self.snake_if.kill()
            self.stats.enabled = self.snake_if.enable

            self.alive = False
            self.length = 0
            
            if len(self.pre_parts) > 0:
                self.stats.add_death( killer_id, self.pre_parts[0].position )
            
        elif (position is None) or (position == self.parts[0].position):
            # Kill snake
            self.snake_if.kill()
            self.stats.enabled = self.snake_if.enable

            self.alive = False
            self.length = 0

            if selfkill:
                self.stats.add_death( killer_id, self.pre_parts[0].position )
            else:
                self.stats.add_death( killer_id, self.parts[0].position )

            if len(self.parts): # If any parts left
                # if (killer_id == self.stats.get_id()) or (killer_id == "Board"): # Self kill
                if selfkill:
                    # print("Selfkill ...", len(self.parts))
                    try:
                        if self.last_move == Game_packet.movements["fast_forward"] and len(self.parts) > 1:
                            old_parts = self.parts[2:]
                            old_parts.append(self.pre_parts[0])
                            old_parts.append(self.pre_parts[1])
                        else:
                            old_parts = self.parts[1:]
                            old_parts.append(self.pre_parts[1])
                    except IndexError:
                        old_parts = self.pre_parts

                    for parts in old_parts:
                        remains.append( parts.position[:] )
                else:
                    for parts in self.parts:
                        remains.append( parts.position[:] )

            self.parts = []
        else:
            # Cut snake
            kill_part_index = -1
            for i in range(len(self.parts)):
                if self.parts[i].position == position:
                    kill_part_index = i
                    break

            if kill_part_index != -1:
                for part in self.parts[kill_part_index:]:
                    remains.append( part.position[:] )

                self.grow( -len( self.parts[kill_part_index:] ) )
                # print("Snake length ", self.stats.get_name(), ":", self.length, self.stats.get_current_length() )

        return remains[::2]

    def is_alive(self):
        return self.alive
