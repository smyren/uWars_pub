##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Stats
import numpy as np


class Stats():
    def __init__(self, board_size, snake_id, name="NA", color=[(0, 0, 0), (0, 0, 0)],
                 head_img_file='', icon_img_file='', snake_owner='', snake_info=''):
        self.enabled = True                             # Tell if snake is active (false on disconnect)
        self.id = snake_id
        self.name = name
        self.color = color
        self.color_hex = [self.get_color_hex_string_from_rgb_tuple(color[0]),
                          self.get_color_hex_string_from_rgb_tuple(color[1])]
        self.board_size = board_size
        self.head_img_file_name = head_img_file         # The name of the image file for the snake head
        self.icon_img_file_name = icon_img_file         # The name of the image file for the snake icon
        self.snake_owner = snake_owner                  # The name of the person who made the snake
        self.snake_info = snake_info                    # Some inforamtion about the snake

        self.start_rounds = np.array([])                # Each item represent start round number per respawn
        self.eol_lengths = np.array([])                 # Each item represent length when the snake died for each respawn
                                                        # For an active snake the last element is also the current length
        self.max_lengths = np.array([])                 # Each item represent the maximum length during each snake life (ie for each respawn)
        self.acc_snake_mass = 0                         # The accumulated snake mass
        self.alive_times = np.array([])                 # Each item represent alive "clicks" per respawn
        self.kills = {}                                 # dict of ID of snake killed and count
        self.deaths = {}                                # dict of ID of snake causing the death and count. {id:count}
        self.temp_deaths = {}                           # Dict for keeping track of death and count to update on the next data export
        self.temp_lives = []                            # Dict for keeping track of stats on completed lives to update on the next data export
        # Growrate can be calculated: Length/alive/time
        self.visit_rate = np.zeros( (board_size[0], board_size[1]) )  # Moveabillity. Store visit rate per block
        self.death_rate = np.zeros( (board_size[0], board_size[1]) )  # Death rate per block
        self.temp_visit_pos = {}                        # Dict for keeping track of visit positions to update on the next data export
        self.temp_death_pos = {}                        # Dict for keeping track of death positions to update on the next data export
        self.temp_board_pos = {}                        # Dict for keeping track of visit and death positions to update on the next data export
        self.self_touch = np.array([])                  # Count number of time snake touch itself per respawn

        self.response_times = {'max': 0,                # Longest valid response time
                               'min': 0,                # Shortest response time
                               'average': 0,            # Average of the valid response times
                               'sum_valid': 0,          # The sum of all valid response times
                               'num_valid': 0,          # Count of valid response times
                               'num_missed': 0,         # Count of missed responses (ie was too long)
                               'status': ''}            # Status of the last response ('valid' or 'missed')
        self.movements = np.zeros(4)                    # Count of avaialble movements (4 for now..)
        self.food = 0                                   # Count the number of food the snake has eaten
        self.rank = 0                                   # Snakes overall rank in the game, ie 1st, 2nd, etc
        self.rank_score = 0                             # Individually calculated score value for each snake
                                                        # that the overall rank is decided from
        self.comment = ""
        self.export_comments = []                       # List of comments since last export
        self.comment_timer = 0
        self.exception_string = ""

    def set_head_img_file_name(self, fn):
        self.head_img_file_name = fn

    def get_head_img_file_name(self):
        return self.head_img_file_name

    def set_icon_img_file_name(self, fn):
        self.icon_img_file_name = fn

    def get_icon_img_file_name(self):
        return self.icon_img_file_name

    def set_exception(self, e):
        self.exception_string = repr(e)

    def get_exception(self):
        return self.exception_string

    def set_comment(self, text, snake_comment_time, round_num):
        if ( self.comment_timer < snake_comment_time ):
            if (text == self.comment) or (text == ""):
                self.comment_timer += 1
            else:
                self.comment = text
                self.comment_timer = 1
        elif ( text != self.comment ) and ( text != "" ):
            self.comment = text
            self.comment_timer = 1
        else:
            self.comment_timer = snake_comment_time
            self.comment = ""

        # Add comment to the list for export
        if text:
            self.export_comments.append({'round': round_num, 'msg': text})

    def get_comment(self):
        return self.comment

    def get_id(self):
        """Get teh snake id"""
        return self.id

    def get_name(self):
        """Get the snake name"""
        return self.name

    def set_name(self, name):
        """Get the snake name"""
        self.name = name

    def get_snake_owner(self):
        """Get the snake owner"""
        return self.snake_owner

    def get_snake_info(self):
        """Get the snake owner"""
        return self.snake_info

    def get_color(self):
        """Get the snake color"""
        return self.color

    def set_rank(self, rank):
        self.rank = rank

    def get_rank(self):
        return self.rank

    def set_rank_score(self, rank_score):
        self.rank_score = rank_score

    def get_current_length(self):
        """Get the current snake length"""
        try:
            if self.enabled:
                length = self.eol_lengths[-1]
            else:
                length = 0
        except IndexError:
            # lengths array is empty
            length = 0

        return length

    def get_current_max_length(self):
        """Get the max length for the current snake livfe"""
        try:
            if self.enabled:
                length = self.max_lengths[-1]
            else:
                length = 0
        except IndexError:
            # lengths array is empty
            length = 0

        return length

    def get_mean_length(self):
        """Get the mean snake length, not including the current life"""
        try:
            if self.enabled:
                mean_length = self.eol_lengths[:-1].mean()
            else:
                # Snake not active, use all logged lengths
                mean_length = self.eol_lengths.mean()
        except ValueError:
            # Emtpy array, or no completed lives
            mean_length = 0

        return mean_length

    def get_max_length(self):
        """Get the maximum snake length, not including the current life"""
        try:
            if self.enabled:
                max_length = self.max_lengths[:-1].max()
            else:
                # Snake not active, use all logged lengths
                max_length = self.max_lengths.max()
        except ValueError:
            # Emtpy array, or no completed lives
            max_length = 0

        return max_length

    def get_current_alive_time(self):
        """Get the current snake alive time"""
        try:
            if self.enabled:
                alive_time = self.alive_times[-1]
            else:
                alive_time = 0
        except IndexError:
            # alive_times array is empty
            alive_time = 0

        return alive_time

    def get_max_age(self):
        """Get the maximum snake age, not including the current life"""
        try:
            if self.enabled:
                max_age = self.alive_times[:-1].max()
            else:
                # Snake not active, use all logged alive_times
                max_age = self.alive_times.max()
        except ValueError:
            # Emtpy array, or no completed lives
            max_age = 0

        return max_age

    def get_acc_snake_mass(self):
        """Get the accumulated snake mass"""
        return int(self.acc_snake_mass)

    def get_num_rounds_played(self):
        """Get the total number of rounds this snake has played"""
        return int(np.sum(self.alive_times))

    def add_food(self, nr_of_food=1):
        """Add number of food eaten her"""
        self.food += nr_of_food

    def add_kill(self, killed_snake_id):
        """Add one kill of the given snake id"""
        if killed_snake_id in self.kills:
            self.kills[killed_snake_id] += 1
        else:
            self.kills[killed_snake_id] = 1

    def add_death(self, killer_id, position):
        """Add one death caused by the given snake and at the given position"""
        self.death_rate[position] += 1

        if killer_id in self.deaths:
            self.deaths[killer_id] += 1
        else:
            self.deaths[killer_id] = 1

        coord = (int(position[0]), int(position[1]))

        if coord in self.temp_death_pos:
            self.temp_death_pos[coord] += 1
        else:
            self.temp_death_pos[coord] = int(self.death_rate[position])

        if coord in self.temp_board_pos:
            self.temp_board_pos[coord]['death_count'] += 1
        else:
            #
            self.temp_board_pos[coord] = {'visit_count': int(self.visit_rate[position]),
                                          'death_count': int(self.death_rate[position])}

        # Update counts for next data export
        if killer_id in self.temp_deaths:
            self.temp_deaths[killer_id] += 1
        else:
            self.temp_deaths[killer_id] = self.deaths[killer_id]

        self.temp_lives.append({'start_round': self.start_rounds[-1],
                                'age': self.alive_times[-1],
                                'eol_length': self.eol_lengths[-1],
                                'max_length': self.max_lengths[-1]})

        # Hack to make sure that the status for the user snakes are disabled when the snake dies
        # This should be removed when the setting the enabled/disabled status has been finished in
        # the game host
        if self.name.startswith('user'):
            self.response_times['status'] = 'missed'

    def add_visit(self, x, y):
        """Add visit at the given position"""
        self.visit_rate[x, y] += 1

        coord = (int(x), int(y))

        if coord in self.temp_visit_pos:
            self.temp_visit_pos[coord] += 1
        else:
            self.temp_visit_pos[coord] = int(self.visit_rate[x, y])

        if coord in self.temp_board_pos:
            self.temp_board_pos[coord]['visit_count'] += 1
        else:
            #
            self.temp_board_pos[coord] = {'visit_count': int(self.visit_rate[x, y]),
                                          'death_count': int(self.death_rate[x, y])}

        # Update the accumulated snake mass
        self.acc_snake_mass += self.eol_lengths[-1]

    def increase_alive_time(self, added_alive_time=1):
        """Increase the current alive time by one tick"""
        self.alive_times[-1] += added_alive_time

    def change_length(self, added_length=1):
        """Increase/decrease the current snake length"""
        self.eol_lengths[-1] += added_length

        # Update the max length if if the current length is longer
        if self.max_lengths[-1] < self.eol_lengths[-1]:
            self.max_lengths[-1] = self.eol_lengths[-1]

    def respawn(self, respawn_length, round_num):
        """Snake has respawned, update the necessary data"""
        self.start_rounds = np.append(self.start_rounds, round_num)
        self.eol_lengths = np.append(self.eol_lengths, np.floor(respawn_length))
        self.max_lengths = np.append(self.max_lengths, np.floor(respawn_length))
        self.alive_times = np.append(self.alive_times, 0)
        self.self_touch = np.append(self.self_touch, 0)
        self.comment = ""
        self.pre_comment = ""

    def update_response_time(self, response_time):
        self.increase_alive_time()

        if response_time == -1:
            # Too long response time, snake missed the time limit
            self.response_times['num_missed'] += 1
            self.response_times['status'] = 'missed'
        else:
            # A valid response time was received
            if self.response_times['num_valid'] == 0:
                # This is the first valid time, forcing the minimum time so that we have a valid
                # starting point
                self.response_times['min'] = response_time

            # Increase the valid count, update the accumulated sum and average
            self.response_times['num_valid'] += 1
            self.response_times['sum_valid'] += response_time
            self.response_times['average'] = self.response_times['sum_valid'] / self.response_times['num_valid']

            # Update the min/max values if necessary
            self.response_times['max'] = max(self.response_times['max'], response_time)
            self.response_times['min'] = min(self.response_times['min'], response_time)

            self.response_times['status'] = 'valid'

    def enable(self):
        self.enabled = True

    def disable(self):
        self.enabled = False

    def add_movement(self, move):
        self.movements[move] += 1

    def get_color_hex_string_from_rgb_tuple(self, color):
        """Convert a set of rgb value to hex string"""
        # Make sure the rgb values are within the valid range, else clamp to 0/255
        r = max(0, min(color[0], 255))
        g = max(0, min(color[1], 255))
        b = max(0, min(color[2], 255))

        return "#{0:02x}{1:02x}{2:02x}".format(r, g, b)

    def get_status(self):
        if not self.enabled:
            # Snake node disconnected
            return 'Stopped'
        else:
            # Snake node connected, make sure it is  actually responding
            if self.response_times['status'] == 'valid':
                return 'Active'
            else:
                return 'Paused'

    def get_array_params(self, array):
        """Calculate a set of parameters from a array with one value pr round of the game

        The following values are found
            Percentiles
                0 - minimum value
                25 - 1st quartile
                50 - median
                75 - 3rd quartile
                100 - max value
            Sum of all values
        """

        # Calculate the percentiles
        # - only completed rounds/lives are included in the calculations, ie the active/current
        # life (the last value in the array) is excluded from the results
        if array.size == 0:
            # The snake hasn't played any rounds yet, forcing 0 values
            percentiles = np.percentile(np.array([0]), [0, 25, 50, 75, 100])
            total_sum = 0
        else:
            # Game is running, check if the node is enabled
            if self.enabled:
                if array.size == 1:
                    # Only one round has been played, no previosu round to calculate data from so
                    # forcing 0 values except for the current value
                    percentiles = np.percentile(np.array([0]), [0, 25, 50, 75, 100])
                    total_sum = 0
                else:
                    # Calculate the percentiles, excluding the result for the current life
                    percentiles = np.percentile(array[:-1], [0, 25, 50, 75, 100])
                    total_sum = np.sum(array[:-1])
            else:
                # At least one round has been played, but the node is disconnected, calculating
                # percentiles based on all previous rounds
                percentiles = np.percentile(array[:], [0, 25, 50, 75, 100])
                total_sum = np.sum(array[:])

        # Collect the parameters, converting form numpy types to standard python types
        params = {}
        params['sum'] = int(total_sum)
        params['min'] = float(percentiles[0])
        params['q1'] = float(percentiles[1])
        params['median'] = float(percentiles[2])
        params['q3'] = float(percentiles[3])
        params['max'] = int(percentiles[4])

        return params

    def get_export_stats(self):
        """Collect data for export"""

        # Build data for the snake score
        snake_scores = {}
        snake_scores['id'] = self.id
        snake_scores['name'] = self.name
        snake_scores['color1'] = self.color_hex[0]
        snake_scores['color2'] = self.color_hex[1]
        snake_scores['status'] = self.get_status()

        snake_scores['rank'] = self.rank
        snake_scores['rank_score'] = self.rank_score

        # Find the number of rounds the snake has played
        snake_scores['rounds'] = int(np.sum(self.alive_times))

        # Calculate default box plot parameters for the arrays with a single value per round
        snake_scores['eol_length'] = self.get_array_params(self.eol_lengths)
        snake_scores['max_length'] = self.get_array_params(self.max_lengths)
        snake_scores['alive_time'] = self.get_array_params(self.alive_times)

        # Get the lengths and age for the current life
        snake_scores['length_current'] = self.get_current_length()
        snake_scores['max_length_current'] = self.get_current_max_length()
        snake_scores['alive_time_current'] = self.get_current_alive_time()

        # Calculate the box plot parameters for the last N rounds
        num_rounds = 10      # TBD move to Ruleset.py?
        snake_scores['eol_length_lastn'] = self.get_array_params(self.eol_lengths[-num_rounds:])
        snake_scores['alive_time_lastn'] = self.get_array_params(self.alive_times[-num_rounds:])

        snake_scores['acc_snake_mass'] = self.get_acc_snake_mass()

        # Number of kills on other snakes
        if self.id in self.kills:
            snake_scores['kills'] = sum(self.kills.values()) - self.kills[self.id]
        else:
            snake_scores['kills'] = sum(self.kills.values())

        # The total number of times the snake has died
        snake_scores['deaths'] = sum(self.deaths.values())

        # The number of foods the snake has eaten
        snake_scores['food'] = self.food

        # TBD - move board size to a separate table with static game data
        snake_scores['board'] = {'x': self.board_size[1], 'y': self.board_size[0]}

        # Add the movement count
        # TBD - hardcoded mapping for now should be updated later
        # movements = {'forward':0,'left':1,'right':2,'fast_forward':3}
        snake_scores['moves'] = {}
        snake_scores['moves']['forward'] = int(self.movements[0])
        snake_scores['moves']['left'] = int(self.movements[1])
        snake_scores['moves']['right'] = int(self.movements[2])
        snake_scores['moves']['fast_forward'] = int(self.movements[3])

        # Add response times
        snake_scores['response_time'] = {}
        snake_scores['response_time']['min'] = self.response_times['min']
        snake_scores['response_time']['max'] = self.response_times['max']
        snake_scores['response_time']['average'] = self.response_times['average']
        snake_scores['response_time']['sum_valid'] = self.response_times['sum_valid']
        snake_scores['response_time']['num_valid'] = self.response_times['num_valid']
        snake_scores['response_time']['num_missed'] = self.response_times['num_missed']

        # Combine all the results into one dict
        data = {'id': self.id,
                'snake_scores': snake_scores,
                'deaths': self.temp_deaths,
                'death_pos': self.temp_death_pos,
                'visit_pos': self.temp_visit_pos,
                'board_pos': self.temp_board_pos,
                'life_stats': self.temp_lives,
                'chat_messages': self.export_comments,
                'errors': {'msg': self.get_exception()}}

        # Clear the dicts/arrays keeping changes since last data export
        self.temp_death_pos = {}
        self.temp_visit_pos = {}
        self.temp_board_pos = {}
        self.temp_deaths = {}
        self.temp_lives = []
        self.export_comments = []

        # Clear the last error message
        self.exception_string = ''

        return data

    def get_static_player_info(self):
        """Return the static player info"""
        static_data = {}
        static_data['id'] = self.id
        static_data['name'] = self.name
        static_data['head_img'] = self.head_img_file_name
        static_data['icon_img'] = self.icon_img_file_name
        static_data['color1'] = self.color_hex[0]
        static_data['color2'] = self.color_hex[1]
        static_data['owner'] = self.snake_owner
        static_data['info'] = self.snake_info
        static_data['last_error_msg'] = self.get_exception()

        return static_data
