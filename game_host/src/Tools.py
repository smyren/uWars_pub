##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Tools
import hashlib, os

main_dir = os.path.split(os.path.abspath(__file__))[0]

# Prototype for any sprite postion object    
class Part():
    def __init__(self, position, rotation, subtype=0):
        self.set_position(position)
        self.set_rotation(rotation)
        self.set_subtype(subtype)
    def set_position(self, pos):
        self.position = pos
        self.x  = pos[1]
        self.y = pos[0]
    def set_rotation(self, rotation):
        self.rotation = rotation
    def set_subtype(self, subtype):
        self.subtype = subtype

# Prototype class for any game object
class Game_object():
    board_size = None
    color = None
    id = 0

    def update(self):
        print("Update not defined for:", type(self) )

# Send to controller per node movement
class Game_packet():
    movements = {'forward':0,'left':1,'right':2,'fast_forward':3}
    def __init__(self, radar_image, head_pos, rotation, respawn, lenght, active_snakes, ranking, socket=0): # Set socket to '0' until implemented
        self.respawn = respawn # Tell if this is first move after respawn
        self.radar_image = radar_image

        self.head_x = head_pos[1]
        self.head_y = head_pos[0]
        
        self.rotation = rotation
        self.length = lenght

        self.active_snakes = active_snakes
        self.ranking = ranking
        
        self.socket = socket

def cryptate_id( id ):
    return hashlib.md5( str(id).encode() ).hexdigest()
        
        
node_response_movements = {'forward':0,'left':1,'right':2,'fast_forward':3}
# Received from node per movement
class Node_response():
    decode_package = {'movement':0}
    def __init__(self, packet):
        self.movement = packet[ self.decode_package['movement'] ]

# Received from controller per node movement        
class Controller_response():
    decode_package = {'response_time':0, 'node_response':1}
    def __init__(self, packet ):
        self.response_time = packet[ self.decode_package['response_time'] ]
        self.node_response = Node_response( packet[ self.decode_package['node_response'] ] )

# Send to controller board to find connected nodes
class Get_connected_nodes_package():
    def __init__(self, socket):
        self.socket = socket

# Received from connected node
class Node_connected_response_package():
    decode_package = {'id':0, 'name':1}
    def __init__( self, packet ):
        self.id = packet[ self.decode_package['id'] ] # id = 0 means no response
        self.name = packet[ self.decode_package['name'] ]
