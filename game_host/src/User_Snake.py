##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Snake

#Import Modules
import pygame, random, copy
from src.Tools import *
from src.Stats import *
import numpy as np
import hashlib

class User_Snake():
    enable = True
    ctrl = None
    node = None
    id = 0xdbdb
    name = "My snake"
    comment = ""
    slow_move = False
    icon = None
    colors = None
    owner = "What is my name?"
    info = "Some personal information I would like to share.\nAnd some more information on the next line :)"
    def __init__(self, id=None, name=None):
        if id is not None:
            self.id = id
        if name is not None:
            self.name = name

        # self.id = hashlib.md5( str(self.id).encode() ).hexdigest() # Cryptate ID
        
    def update(self, game_packet ):
        print("Debug_snake.update: function not implemented")

        movement = game_packet.movements["forward"]
        return movement

    def activate( self ):
        self.enable = True

    def deactivate( self ):
        self.enable = False
        
    def respawn( self, force_enable ):
        return self.enable
    
    def kill( self ):
        pass