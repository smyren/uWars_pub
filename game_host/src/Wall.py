##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Food

#Import Modules
import pygame, random
import numpy as np
import pickle
from src.Tools import *
from src.WallMaps import *

map_dir = os.path.join(main_dir, '../maps')
map_dir = os.path.normpath(map_dir)

class Images():
    def __init__(self):
        self.background = []
        self.food = []
        self.walls = []
        self.board_images = []
        self.effect_images = []
        self.path = None

class Map_Object():
    def __init__(self, id, name = None, path = None, blocking = True, rotation = 0):
        self.id = id
        self.name = name
        self.path = path
        self.blocking = blocking
        self.rotation = rotation

    def rotate(self, rotation = 1):
        self.rotation += rotation
        self.rotation = self.rotation%4

    def print_ascii(self):
        print(self.id, self.name, self.path, self.blocking, self.rotation)

class Map():
    replace_with_background = (255 << 8)  #(0,255,0)
    rel_dir_main    = []      # for background and such
    rel_dir_present = []      # present folder for images
    replace_image   = []      # background for all images (i.e. replace (0,255,0) in images with pixel from this

    def __init__(self, images = None, rel_dir = None, no_walls = False):
        if (rel_dir):
            self.rel_dir_main = self.split_path(rel_dir)

        if (images):
            self.map_objects = []
            id = 0
            for icon in images:
                if id == 0:
                    mo = Map_Object(id, icon, self.rel_dir_main, False)
                    self.map_objects.append(mo)
                else:
                    if not no_walls:
                        mo = Map_Object(id, icon, self.rel_dir_main)
                        self.map_objects.append(mo)

                id += 1

            self.update_theme()

        else:
            self.map_images = []
            self.map_objects = []

        self.description = ''

    def load_image(self, path, name, colorkey=None, scale=None):
        fullname = os.path.join(path, name)
        try:
            image = pygame.image.load(fullname)
        except pygame.error:
            print ('Cannot load image:', fullname)
            raise SystemExit(str(geterror()))
        if scale is not None:
            image = pygame.transform.scale( image, scale )
        image = image.convert()

        return image

    def update_wall_images(self, gameregion_color, wall_display_size, replace_bg = True):
        self.replace_image = self.load_image(self.build_path(map_dir, self.rel_dir_main),'_bg_replace.png')
        replace_array = pygame.PixelArray(self.replace_image)
        replace_size = replace_array.shape

        images = []
        for obj in self.map_objects:
            path = self.build_path(map_dir, obj.path)
            image = self.load_image(path, obj.name, None)
            pxarray = pygame.PixelArray(image)
            if replace_bg:
                pxarray_size = pxarray.shape
                if pxarray_size == replace_size:
                    for x in range(0, pxarray_size[0]):
                        for y in range (0, pxarray_size[1]):
                            if pxarray[x,y] == self.replace_with_background:
                                pxarray[x,y] = replace_array[x,y]
                else:
                    pxarray.replace( self.replace_with_background, gameregion_color ) # Set background color
            rotation = (obj.rotation  % 4 )*90
            image = pygame.transform.rotate(image, rotation)

            images.append( pygame.transform.smoothscale( image, (wall_display_size, wall_display_size) ).convert() )

        return images


    def rotate_map_object(self, id):
        for obj in self.map_objects:
            if obj.id == id:
                obj.rotate()

    def get_rotation(self, id):
        for obj in self.map_objects:
            if obj.id == id:
                return obj.rotation
        return 0

    def save_map(self, filename, wall):
        if not self.description:
            self.description = filename.rsplit("/")[-1]
        output = open(filename, 'wb')
        pickle.dump(self.rel_dir_main, output)
        pickle.dump(self.map_objects, output)
        pickle.dump(wall.map, output)
        pickle.dump(wall.map_effects, output)
        pickle.dump(self.description, output)
        output.close()

    def load_map(self, filename, wall):
        wall.map = []
        wall.map_effects = []
        input = open(filename, 'rb' )
        self.rel_dir_main = pickle.load(input)
        self.map_objects = pickle.load(input)
        wall.map = pickle.load(input)
        wall.map_effects = pickle.load(input)
        self.description = pickle.load(input)
        input.close()

        self.update_theme()
        self.map_images.path = self.build_path(map_dir, self.rel_dir_main)

    def is_image_in_map(self, path, file):
        in_map = False
        id = -1
        for mo in self.map_objects:
            if path == os.path.join(self.build_path(map_dir, mo.path)) and file == mo.name:
                in_map = True
                id = mo.id
                break

        return in_map, id

    def add_image_to_map(self, path, file):
        id = 0
        path = self.split_path(path)
        for mo in self.map_objects:
            if mo.id >= id: id = (mo.id + 1)

        mo = Map_Object(id, file, path)
        self.map_objects.append(mo)
        self.update_theme()

        return id

    def update_dir(self, path):
        new_path = self.split_path(path)
        if new_path != self.rel_dir_main:
            self.rel_dir_main = new_path
            self.map_objects[0].path = new_path
            self.update_theme()

    def update_theme(self):
        self.map_images = Images()
        self.map_images.background.append('_bg.png')

        self.map_images.food.append('_food.png')
        self.map_images.effect_images.append('_talk.png')

        for mo in self.map_objects:
            if mo.id == 0: continue
            else:
                fullname = os.path.join(self.build_path(map_dir, mo.path), mo.name)
                self.map_images.walls.append(fullname)

        self.map_images.board_images = [self.map_images.background, self.map_images.food, self.map_images.walls]
        self.map_images.path = self.build_path(map_dir, self.rel_dir_main)

    def split_path(self, path):
        done = False
        split_path = path
        path_list = []
        while not done:
            head,tail = os.path.split(split_path)
            path_list.append(tail)
            if head == '':
                done = True
            split_path = head
        return path_list

    def build_path(self, path, path_list):
        for i in reversed(path_list):
            path = os.path.join(path, i)

        return path

    def set_description(self, text):
        self.description = text

    def get_description(self):
        return self.description

    def print_ascii(self):
        print('Self directory: ', self.rel_dir_main)
        print('Description: ', self.description)
        for mo in self.map_objects:
            mo.print_ascii()


class Wall( Game_object ):
    map = []          # object IDs
    map_effects = []  # object properties: low two bits rotation, upper six bits not used atm

    def __init__(self, board_size = None, color = (0,0,0), filename = None, generate = False, id = None):
        self.color = color
        self.parts = []
        if id is None:
            self.id = 0
        else:
            self.id = id #(1 << 7) + 1 # Get wall id from some dict here

        if (generate == True) and (board_size is not None):
            self.map = np.zeros( (board_size[0], board_size[1]), dtype=np.uint8 )
            self.map_effects = np.zeros( (board_size[0], board_size[1]), dtype=np.uint8 )
        elif (filename != None):
            self.load(filename)
        else:
            self.map = np.zeros((0,0))
            self.map_effects = np.zeros((0,0))

        if board_size is None:
            self.board_size = self.map.shape
        else:
            self.board_size = board_size

    def generate_map( self, board_size, tile ):
        if board_size is None:
            self.board_size = self.map.shape
        else:
            self.board_size = board_size

        self.map_to_wall(tile)

    def is_wall( self, position ):
        # Check for out of game zone
        if ( position[0] >= self.board_size[0] or \
           position[1] >= self.board_size[1] or \
           position[0] < 0 or \
           position[1] < 0 ):
            return True

        for part in self.parts:
            if part.position == position:
                return True

        return False

    def map_to_wall(self, tile = False):
        self.parts = []
        if tile:
            for x in range(self.board_size[1]):
                for y in range(self.board_size[0]):
                    object_x = x%self.map.shape[1]
                    object_y = (self.board_size[0] - 1 - y)%self.map.shape[0]
                    if self.map[object_y,object_x]:
                        new_part = Part( (y, x), 0, self.map[object_y,object_x] - 1)
                        new_part.rotation = (self.map_effects[object_y,object_x] % 4 )*90
                        self.parts.append( new_part )

        else:
            x_offset = 0
            y_offset = 0
            if self.map.shape[0] < self.board_size[0]:
                y_offset = int( (self.board_size[0] - self.map.shape[0])/2 )
            if self.map.shape[1] < self.board_size[1]:
                x_offset = int( (self.board_size[1] - self.map.shape[1])/2 )

            for x in range( min(self.map.shape[1],self.board_size[1]) ):
                for y in range( min(self.map.shape[0], self.board_size[0]) ):
                    if self.map[y,x]:
                        new_part = Part( (self.board_size[0] - 1 - y - y_offset, x_offset + x), 0, self.map[y,x] - 1 )
                        new_part.rotation = (self.map_effects[y,x] % 4 )*90
                        self.parts.append( new_part )

    def rotate(self, x_obj, y_obj):
        if ((self.map_effects[y_obj, x_obj] %4)  == 3):
            self.map_effects[y_obj, x_obj] -= 3
        else:
            self.map_effects[y_obj, x_obj] += 1

    def set_wall(self, x, y, id, rotation = 0):
        self.map[y, x] = id
        self.map_effects[y, x] -= (self.map_effects[y, x] % 4) # TODO replace with bit manipulation, using modulo for the time beeing (syntax error with &= 0x3 or 0x11?)
        self.map_effects[y, x] += (rotation % 4)

    def copy_wall_segment(self, from_x, from_y, to_x, to_y):
        self.map[to_y, to_x] = self.map[from_y, from_x]
        self.map_effects[to_y, to_x] = self.map_effects[from_y, from_x]

    def copy(self, start_x, start_y, x_delta, y_delta, cut = False):
        self.copy_map = []
        self.copy_effect_map = []
        self.copy_map = np.zeros( (y_delta, x_delta), dtype=np.uint8 )
        self.copy_effect_map = np.zeros( (y_delta, x_delta), dtype=np.uint8 )
        for x in range(0, x_delta):
            for y in range(0, y_delta):
                map_x = start_x + x
                map_y = start_y + y
                if (map_x < self.map.shape[1]) and (map_y < self.map.shape[0]):
                    self.copy_map[y,x] = self.map[map_y,map_x]
                    self.copy_effect_map[y,x] = self.map_effects[map_y,map_x]
                    if cut:
                        self.map[map_y,map_x] = 0
                        self.map_effects[map_y,map_x] = 0

    def paste(self, start_x, start_y):
        for x in range(0, self.copy_map.shape[1]):
            for y in range(0, self.copy_map.shape[0]):
                map_x = start_x + x
                map_y = start_y + y
                if (map_x < self.map.shape[1]) and (map_y < self.map.shape[0]):
                     self.map[map_y,map_x] = self.copy_map[y,x]
                     self.map_effects[map_y,map_x] = self.copy_effect_map[y,x]

    def save(self, filename):
        np.save(filename, self.map)

    def load(self, filename):
        map = []
        self.map = np.load(filename)
        map_effects = []
        self.map_effects = np.zeros( (self.map.shape[0], self.map.shape[1]), dtype=np.uint8 )

    def save_map(self, filename):
        np.savez(filename, map = self.map, effect = self.map_effects)

    def load_map(self, filename):
        map = []
        map_effects = []
        with np.load(filename) as data:
            self.map = data['map']
            self.map_effects = data['effect']

    def print_ascii(self):
        print('Object ID')
        for y in range(self.map.shape[0]):
            line = ''
            for x in range(self.map.shape[1]):
                line += str(self.map[y,x])
            print(line)
        print('Object effects')
        for y in range(self.map_effects.shape[0]):
            line = ''
            for x in range(self.map_effects.shape[1]):
                line += str(self.map_effects[y,x])
            print(line)
