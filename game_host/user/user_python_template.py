##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Snake node template file
#
# Goal: The users should just have to make a copy of this file and rename it
#       to have a (stupid) snake up and running, they can then add the missing
#       smartness, and rule the board!

#Import Modules
import pygame, random, copy
from src.Tools import *
from src.Stats import *
import numpy as np
from src.User_Snake import *

class Snake(User_Snake):
    # Set to false to disable only this python snake (ruleset.enable_user_snakes enables/disables all)
    enable = True

    # Set a unique (random) id for your snake
    id = 0xfeedbeef

    # Name of your snake.
    name = "YOUR SNAKE-NAME HERE"

    # Name of snake owner.
    owner = "YOUR NAME HERE"

    # Background story (flavor text) for your snake.
    info = "Nobody has taken the time to update my story... No one loves ME!"

    # Colors of your snake, [format documentation goes here].
    # Colors from the icon editor can be pasted directly into this file.
    colors = [(200,50,80),(80,50,200)]            

    # Icon of your snake, [format documentation goes here].
    # We recommend pasting from the icon editor (like the colors above).
    icon = np.array( [
            [0,0,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,1,0],
            [0,0,0,0,0,0,0,0,0,1,0,0],
            [0,0,0,0,0,0,0,0,1,0,0,0],
            [0,0,0,0,0,0,0,1,0,0,0,0],
            [0,0,0,0,0,0,1,0,0,0,0,0],
            [0,0,0,0,0,1,0,0,0,0,0,0],
            [0,0,0,0,1,0,0,0,0,0,0,0],
            [0,0,0,1,0,0,0,0,0,0,0,0],
            [0,0,1,0,0,0,0,0,0,0,0,0],
            [0,1,0,0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0,0,0,0] ] )
    
    # Example code
    def update(self, game_packet ):
        self.comment = ""
        movement = game_packet.movements["forward"]
        # Get snake head position
        head_left = game_packet.head_x
        head_top = game_packet.head_y
        
        # Check for food
        food = 128
        food_not_reachable = False
        if food in game_packet.radar_image:
            if game_packet.radar_image[ head_top + 1, head_left ] == food:
                movement = game_packet.movements["forward"]
            elif game_packet.radar_image[ head_top + 2, head_left ] == food and game_packet.radar_image[ head_top + 1, head_left ] == 0:
                movement = game_packet.movements["fast_forward"]
            elif food in game_packet.radar_image[ :, 0:head_left ] and \
                 (game_packet.radar_image[ head_top, head_left - 1 ] == 0 or game_packet.radar_image[ head_top, head_left - 1 ] == food):
                movement = game_packet.movements["left"]
            elif food in game_packet.radar_image[ :, head_left: ] and \
                 (game_packet.radar_image[ head_top, head_left + 1 ] == 0 or game_packet.radar_image[ head_top, head_left + 1 ] == food):
                movement = game_packet.movements["right"]
            else:
                food_not_reachable = True
        else:
            food_not_reachable = True

        if food_not_reachable:
            if random.random() > 0.7:
                movement = random.choice([game_packet.movements["left"], game_packet.movements["right"]])

                # Check for collision
                if (movement == game_packet.movements["left"] and game_packet.radar_image[ head_top, head_left - 1 ] == 0 ) or \
                   (movement == game_packet.movements["right"] and game_packet.radar_image[ head_top, head_left + 1 ] == 0 ):
                   return movement
            
            if game_packet.radar_image[ head_top + 1, head_left ] == 0: # Assume radar image is always showing one space in front of snake
                if game_packet.radar_image[ head_top + 2, head_left ] == 0: # Two spaces in front of head
                    movement = game_packet.movements["fast_forward"]
                else:
                    movement = game_packet.movements["forward"]
            elif game_packet.radar_image[ head_top, head_left - 1 ] == 0: # Check left side of snake
                movement = game_packet.movements["left"]
            else: # No other option, then move right
                movement = game_packet.movements["right"]
        else:
            self.comment = "mmm... Taco!!!Taco!!!"

        return movement
            