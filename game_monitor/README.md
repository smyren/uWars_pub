# How to run the uWars game monitor on your computer

## Step 1: 
### Install python 3.5

You do not have to add this to path, but for ease of use install to `C:\python35`

## Step 2: 
### Install the needed python libraries

If you do not want to mess up your Python installation, see **Appendix I** below.  
Run the following command in the `game_monitor` folder for pip to install the libraries needed

```
Windows: C:\python35\Scripts\pip.exe install -r requirements.txt
Linux: pip install -r requirements.txt
```

### scipy/numpy for Windows
scipy/numpy **will not install automatically** with pip on Windows systems, but require a manual installation. The web server will run as normal also without these packages, except that some charts will be unavailable.

The packages are included in some Python distributions like Anaconda and PythonXY, see [scipy.org](www.scipy.org/install.html). 

To install the packages manually download the appropriate whl files for your system (eg Python 3.5, 64bit) from here: [numpy](http://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy) and [scipy](http://www.lfd.uci.edu/~gohlke/pythonlibs/#scipy) and install each of them with pip:
```
C:\python35\Scripts\pip.exe install <the downloaded whl file>
```

## Step 3: 
### Start the monitor

If you only want to access the monitor from your own computer, use the following.
```
\game_monitor\run.bat
```

If you want to allow public access to the monitor, use this.
```
\game_monitor\run_public.bat
```


## Step 4: 
### Start the game

```
\game_host\run.bat
```
See the [game host readme](game_host/README.md) for more info on how to configure the game host.

## Step 5: 
### View the data

The game stats can now be viewed by opening `localhost:5000` in a browser.  
If you have started the public monitor it will also be accessible from any other computer on the same network on `<your ip>:5000`.

---

# Appendix I: Virtual Environment 
If you don`t want to mess up your Python installation it is also possible to 
create a virtual environment and install the necessary packages there,  
no changes will be made to the Python installed on the system.

From the folder where you want to create the virtual envirnment (eg from the
folder where you`ve cloned the Git repo, NB not inside the repo folder):  
```
Windows: c:\Python35\python.exe c:\Python35\Tools\scripts\pyvenv.py venv35
Linux: pyvenv venv35
```
{- NB! -} make sure these commands are run using the Python 3.5 executable


#### To activate the virtualenv:  
```
Windows: venv35\Scripts\activate.bat
Linux: venv35/bin/activate
```

All python commands run in the terminal will now use the Python binaries and
packages from the selected virtual environment

#### To install packages from a requirements file in the virtual environment:  

After creating the virtual environment make sure you are running the latest version of pip:

```
Windows: python -m pip install --upgrade pip
Linux: pip install --upgrade pip
```

Then run pip to install everything:
```
pip install -r requirements.txt
```

To save the currently loaded python packages:  
```
pip freeze > requirements.txt
```

To deactivate the virtualenv:  
```
deactivate
```
