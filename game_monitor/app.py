#!/usr/bin/env python
##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##


# NB don't move these two lines, they should come before the other imports
import eventlet
eventlet.monkey_patch()

from flask import Flask, render_template, request, url_for, send_from_directory
from flask_socketio import SocketIO, emit, join_room, leave_room, rooms

import os
import argparse
import collections
import json
import datetime

import models
import lib_db
import charts as my_charts
import tables as my_tables
import info_boxes as my_info_boxes
import sidebar_menu
import dashboards as my_dashboards


app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)
thread_main = None

# Set up database config
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(lib_db.get_database_path())
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_ECHO'] = False       # Set to True to output SQL during debugging

# Register the database in the app
models.db.init_app(app)


def background_thread_main(app_ref=None):
    #
    # TBD disable data transfer when game is paused/stopped
    #
    print("Starting main background thread")
    sleep_time = 1

    with app_ref.app_context():
        # List of data extraction functions that should be run (ie funcions in the db object)
        db_data_funcs = []

        # Add data extraction function needed for getting the number of rounds played so far
        db_data_funcs.append('get_game_run_time_info')

        # Prepare the tables that will be updated
        score_tables = my_tables.ScoreTables()
        db_data_funcs += score_tables.get_db_data_funcs()

        # Prepare the charts that will be updated
        charts = my_charts.Charts()
        db_data_funcs += charts.get_db_data_funcs()

        # Prepare the info boxes that will be updated
        info_boxes = my_info_boxes.InfoBoxes()
        db_data_funcs += info_boxes.get_db_data_funcs()

        # The id for the DOM is not known and not used so just passing in a dummy value
        table_id = 'dummy_table_id'
        chart_id = 'dummy_chart_id'

        # Convert the list to a set to remove duplicates
        db_data_funcs = set(db_data_funcs)

        if lib_db.slow_data_getter_funcs:
            print("Skipping any charts/tables using the following database getter functions:")
            for func in db_data_funcs & lib_db.slow_data_getter_funcs:
                print("  - {}".format(func))
        # Remove any slow data getter functions to reduce the load on the database.
        db_data_funcs = db_data_funcs.difference(lib_db.slow_data_getter_funcs)
        # Convert back to a list
        db_data_funcs = list(db_data_funcs)

        counter = 0
        chat_last_round = 0
        last_round = {'round_num': 0, 'count': 0}
        missed_data_count = 0
        while True:
            # Reset data parameters
            data = {}
            send_data_to_clients = True

            # Get the required data from the database, reading all data first before sending
            # anything to the clients to keep data (almost) in sync
            missing_data = False
            for func in db_data_funcs:
                data[func] = getattr(lib_db, func)()
                if not data[func]:
                    missing_data = True

            if missing_data:
                # No data was found, database is either missing or empty
                send_data_to_clients = False

                # Increase the missed data counter
                missed_data_count += 1

                # TBD - the current status should be send when the client is loading the page, the
                # emit here should only fire the first time no data isn't found, not every second
                # (same for the 'paused' and 'active' status)
                # Allowing two missed data sets before updating the status on the clients (with only
                # one we might get some false updates, eg if the database is locked for writing)
                if missed_data_count > 1:
                    socketio.emit('status',
                                  {'status': 'stopped'},
                                  namespace='/snake',
                                  room='status')
            else:
                # Found data, reset the missing data counter
                missed_data_count = 0

                # Check that the data from the database is actually changing, no need to push the
                # same data to the clients in case the game host has been paused/stopped
                # Checking is done by checking the current game round with the one from the last
                # round
                this_round = data['get_game_run_time_info']['rounds_played']
                if this_round != last_round['round_num']:
                    # New round number, update the number and reset the counter
                    last_round['round_num'] = this_round
                    last_round['count'] = 1
                else:
                    # Same round number as last time, increase the counter
                    last_round['count'] += 1

                    if last_round['count'] > 5:
                        # The database hasn't changed for the last n seconds, send the pause signal
                        send_data_to_clients = False

                        socketio.emit('status',
                                      {'status': 'paused'},
                                      namespace='/snake',
                                      room='status')

            # Send data
            # - skipping data transfer if there were no valid data, or if the same data has been
            # read from the database more then the max allowed number of rounds
            if send_data_to_clients:

                # Valid data was found, update the status
                socketio.emit('status',
                              {'status': 'active'},
                              namespace='/snake',
                              room='status')

                # Loop through all tables, generate updated data and send to the respective
                # socket room
                for table_name, table_param in score_tables.tables.items():
                    if table_param['db_data_func'] in db_data_funcs:
                        table = None
                        # Using 'with app...' to be able to use url_for outside the main app thread
                        with app.test_request_context():
                            # Create an instance of the table with the new data
                            table = table_param['class'](data[table_param['db_data_func']],
                                                         table_id=table_id)

                            if table and table.room_id:
                                # print("sending data to room '{}'".format(table.room_id))
                                # Send new data to all clients connected to the room for this table
                                socketio.emit('table_data',
                                              {'table_room_id': table.room_id,
                                               'table_data': table.table.get_data_json()},
                                              namespace='/snake',
                                              room=table.room_id)

                # Loop through all charts, generate updated data and send to the respective
                # socket room
                for chart_name, chart_param in charts.charts.items():
                    # if 'live_data_enabled' not in chart_param or chart_param['live_data_enabled']:
                    if chart_param['db_data_func'] in db_data_funcs:
                        # Create an instance of the chart with the new data
                        chart = chart_param['class'](chart_id, data[chart_param['db_data_func']])

                        if chart and chart.live_data_supported():
                            # Send new data to all clients connected to the room for this chart
                            socketio.emit('chart_data',
                                          {'chart_room_id': chart.room_id,
                                           'chart_data': chart.get_data_json()},
                                          namespace='/snake',
                                          room=chart.room_id)

                # Send snake chat messages
                chat_data = lib_db.get_last_messages(chat_last_round)
                if chat_data:
                    socketio.emit('chat_message',
                                  {'room_id': 'chat_room',
                                   'messages': chat_data},
                                  namespace='/snake',
                                  room='chat_room')

                # Update the last round number so we know which messages to get the next time
                chat_last_round = data['get_game_run_time_info']['rounds_played']

                for info_box_name, info_box_param in info_boxes.info_boxes.items():
                    if info_box_param['db_data_func'] in db_data_funcs:
                        # Create an instance of the info box with the new data
                        info_box = info_box_param['class'](data[info_box_param['db_data_func']])

                        if info_box and info_box.live_data_supported():
                            # Send new data to all clients connected to the room for this info box
                            socketio.emit('info_box_data',
                                          {'room_id': info_box.room_id,
                                           'data': info_box.get_dynamic_data()},
                                          namespace='/snake',
                                          room=info_box.room_id)

            # Delay until the next update should be pushed to the clients
            socketio.sleep(sleep_time)
            counter += 1


def background_thread_slow():
    """Background thread for pushing data to clients, this thread is used for data that require
    slower update rates. The update rate for this thread may also be adjusted and slowed down as
    the game progresses. For instance heatmaps may need less updates after the game has run for a
    while as a new point won't be as noticable any more"""

    pass


@app.route('/')
def dashboard():
    # Prepare the list of available dashboards
    dashboards = my_dashboards.Dashboards()

    # Get the name of the dashboard selected by the game host
    dash_select = lib_db.get_dashboard_selected_by_game_host()

    if not dashboards.is_valid_selector(dash_select):
        # The selected dashboard was not a valid one, using the default dashboard instead
        dash_select = dashboards.default_dashboard

    # Create the dashboard
    dash = dashboards.dashboards[dash_select]['class']()

    return render_template(dash.html_template,
                           menu=sidebar_menu.SidebarMenu(request.path).get_menu(),
                           info_box_list=dash.info_box_list,
                           chart_list=dash.chart_list,
                           table_list=dash.table_list,
                           chat_list=dash.chat_list,
                           content_title=dash.title)


@app.route('/dashboards/<dash_select>')
def dashboards(dash_select=''):
    # Prepare the list of available dashboards
    dashboards = my_dashboards.Dashboards()

    if dash_select not in dashboards.dashboards.keys():
        # 404
        pass
    else:
        dash = dashboards.dashboards[dash_select]['class']()

        return render_template(dash.html_template,
                               menu=sidebar_menu.SidebarMenu(request.path).get_menu(),
                               info_box_list=dash.info_box_list,
                               chart_list=dash.chart_list,
                               table_list=dash.table_list,
                               chat_list=dash.chat_list,
                               content_title=dash.title)


@app.route('/charts')
@app.route('/charts/<chart_select>')
def charts(chart_select=''):
    charts = my_charts.Charts()
    chart_dict = collections.OrderedDict()

    # Create the list of title and urls for the dropdown menu
    for chart_id, chart in charts.charts.items():
        chart_dict[chart_id] = {
            'url': url_for('charts', chart_select=chart_id),
            'text': chart['title']}

    # Set up the list of dropdown elements
    dropdown_list = []
    dropdown_list.append({'btn_text': 'Select a chart',
                          'attr': 'autofocus',
                          'title_attr': 'Select a chart',
                          'element_dict': chart_dict})

    content_title_small = ''
    chart_selected = False
    enable_live_data_checkbox = False
    chart_list = []

    warning_message = ''
    if chart_select not in chart_dict.keys():
        if chart_select != '':
            warning_message = "Unknown chart, please select one from the menu."
    else:
        # Update dropdown attributes
        dropdown_list[0]['attr'] = ''
        dropdown_list[0]['btn_text'] = chart_dict[chart_select]['text']

        # Read data from the database
        # data = getattr(db, charts.charts[chart_select]['db_data_func'])()
        data = getattr(lib_db, charts.charts[chart_select]['db_data_func'])()

        # Create the selected table
        chart_id = 'chart_id'
        chart = charts.charts[chart_select]['class'](chart_id, data)

        chart_selected = True
        if chart.live_data_supported():
            enable_live_data_checkbox = True
        else:
            content_title_small = """Press F5 or click refresh in your browser to load the latest
                                  data"""

        chart_list.append({'id': chart_id,
                           'room_id': chart.room_id,
                           'chart_lib': chart.chart_lib,
                           'chart_json': chart.get_chart_json(),
                           'chart_html': chart.get_chart_html()
                           })

    return render_template('charts.html',
                           menu=sidebar_menu.SidebarMenu(request.path).get_menu(),
                           chart_list=chart_list,
                           content_title="µWars charts",
                           content_title_small=content_title_small,
                           dropdown_list=dropdown_list,
                           chart_selected=chart_selected,
                           enable_live_data_checkbox=enable_live_data_checkbox,
                           warning_message=warning_message)


@app.route('/board_maps')
@app.route('/board_maps/<chart_select>')
@app.route('/board_maps/<chart_select>/<node_id>')
def board_maps(chart_select='', node_id='All'):

    charts = my_charts.BoardMapCharts()
    chart_dict = collections.OrderedDict()

    # Create the list of title and urls for the dropdown menu
    for chart_id, chart in charts.charts.items():
        chart_dict[chart_id] = {
            'url': url_for('board_maps', chart_select=chart_id),
            'text': chart['title']}

    # Set up the list of dropdown elements
    dropdown_list = []
    dropdown_list.append({'btn_text': 'Select a map',
                          'attr': 'autofocus',
                          'title_attr': 'Select a map',
                          'element_dict': chart_dict})

    dropdown_list.append({'btn_text': 'All',
                          'attr': 'disabled="disabled"',
                          'title_attr': 'Select a snake',
                          'element_dict': {}})

    content_title_small = ''
    chart_selected = False
    chart_lib = 'not_selected'
    chart_id = 'chart_id'
    chart_room = ''
    chart_json = {}
    chart_html = ''
    enable_live_data_checkbox = False

    warning_message = ''
    if chart_select not in chart_dict.keys():
        if chart_select != '':
            warning_message = "Unknown chart, please select one from the menu."
    else:
        # Update dropdown attributes
        dropdown_list[0]['attr'] = ''
        dropdown_list[0]['btn_text'] = chart_dict[chart_select]['text']
        dropdown_list[1]['attr'] = 'autofocus'

        # Get a list of players
        players = lib_db.get_player_id_and_names()

        # Create the options for the dropdown, manually adding the 'All' element then looping
        # through all the players/snakes
        player_dict = collections.OrderedDict()
        player_dict['All'] = {'url': url_for('board_maps', chart_select=chart_select),
                              'text': 'All'}

        for player in players:
            player_dict[player['node_id']] = {
                'url': url_for('board_maps', chart_select=chart_select, node_id=player['node_id']),
                'text': player['name']}

        dropdown_list[1]['element_dict'] = player_dict

        if node_id not in player_dict.keys():
            warning_message = "Unknown snake, please select one from the menu."
            node_select = ''
            chart_html = ''
        else:
            if node_id == 'All':
                node_select = ''
            else:
                node_select = node_id

            # Update the drop down button text
            dropdown_list[1]['btn_text'] = player_dict[node_id]['text']

            # Create the selected chart (without data)
            chart = charts.charts[chart_select]['class'](chart_id, [])

            # Get the map data from the database
            data = lib_db.get_board_map(chart.get_data_select(), node_select)

            # Update the chart with the data
            chart.set_data(data)

            chart_lib = chart.chart_lib
            chart_room = chart.room_id
            chart_json = chart.get_chart_json()
            chart_html = chart.get_chart_html()

            chart_selected = True
            if chart.live_data_supported():
                enable_live_data_checkbox = True
            else:
                content_title_small = """Press F5 or click refresh in your browser to load the
                                      latest data"""

    return render_template('board_maps.html',
                           menu=sidebar_menu.SidebarMenu(request.path).get_menu(),
                           chart_id=chart_id,
                           chart_room=chart_room,
                           content_title="µWars board maps",
                           content_title_small=content_title_small,
                           dropdown_list=dropdown_list,
                           chart_selected=chart_selected,
                           chart_lib=chart_lib,
                           enable_live_data_checkbox=enable_live_data_checkbox,
                           chart_json=chart_json,
                           chart_html=chart_html,
                           warning_message=warning_message)


@app.route('/tables')
@app.route('/tables/<table_select>')
def tables(table_select=''):

    tables = my_tables.ScoreTables()
    table_dict = collections.OrderedDict()

    # Create the list of title and urls for the dropdown menu
    for table_id, table in tables.tables.items():
        table_dict[table_id] = {
            'url': url_for('tables', table_select=table_id),
            'text': table['title']}

    # Set up the list of dropdown elements
    dropdown_list = []
    dropdown_list.append({'btn_text': 'Select a table',
                          'attr': 'autofocus',
                          'title_attr': 'Select a table',
                          'element_dict': table_dict})

    table_list = []
    table_selected = False
    enable_live_data_checkbox = False

    warning_message = ''
    if table_select not in table_dict.keys():
        if table_select != '':
            warning_message = "Unknown table, please select one from the menu."
    else:
        # Update dropdown attributes
        dropdown_list[0]['attr'] = ''
        dropdown_list[0]['btn_text'] = table_dict[table_select]['text']

        data = lib_db.get_score_totals()

        # Create the selected table
        table_id = 'datatable_id'
        table_list.append(tables.tables[table_select]['class'](data, table_id=table_id))

        table_selected = True
        enable_live_data_checkbox = True

    return render_template('tables.html',
                           menu=sidebar_menu.SidebarMenu(request.path).get_menu(),
                           content_title="µWars tables",
                           content_title_small="",
                           dropdown_list=dropdown_list,
                           table_selected=table_selected,
                           enable_live_data_checkbox=enable_live_data_checkbox,
                           table_list=table_list,
                           warning_message=warning_message)


@app.route('/players')
def show_player_table():
    """Show table of all users with icons"""
    warning_message = ''

    data = lib_db.get_players_info()

    table_id = 'datatable_id'
    table_list = [my_tables.PlayerInfoTable(data, table_id=table_id)]
    return render_template('tables.html',
                           menu=sidebar_menu.SidebarMenu(request.path).get_menu(),
                           content_title="µWars players",
                           content_title_small="",
                           dropdown_list=[],
                           table_selected=True,
                           enable_live_data_checkbox=False,
                           table_list=table_list,
                           warning_message=warning_message)


@app.route('/chat')
def snake_chat():
    """Route for showing the snake chat messages"""
    return render_template('chat.html',
                           menu=sidebar_menu.SidebarMenu(request.path).get_menu(),
                           content_title='Snake chat')


@app.route('/winners')
def winners_table():
    """Route for showing a table of game winners

    TBD - log errors on reading the json file to the system log
    """
    warning_message = ''

    # Get the path to the winners json file
    winners_file = lib_db.get_winners_file_path()

    game_data = {}
    try:
        with open(winners_file) as json_file:
            game_data = json.load(json_file)
    except IOError:
        # Error opening the file, eg file doesn't exist or missing access rights
        warning_message = "No winners found..."
    except ValueError:
        # Error parsing the json, eg the file is on a invalid format
        warning_message = "Error parsing the list of winners..."

    data = []
    if 'games' in game_data:
        for game in game_data['games']:
            row = {}
            try:
                row['title'] = game['title']
                row['started'] = game['started']
                names = []
                owners = []
                for winner in game['winner']:
                    names.append(winner['snake_name'])
                    owners.append(winner['player_name'])
                row['name'] = ' / '.join(names)
                row['owner'] = ' / '.join(owners)

            except KeyError:
                row['title'] = '''Error reading winner data for this game, please check the raw
                               data for errors'''
                row['started'] = ''
                row['name'] = ''
                row['owner'] = ''

            data.append(row)

    table_id = 'datatable_id'
    table_list = [my_tables.GameWinnersTable(data, table_id=table_id)]
    return render_template('tables.html',
                           menu=sidebar_menu.SidebarMenu(request.path).get_menu(),
                           content_title="µWars winners",
                           content_title_small="",
                           dropdown_list=[],
                           table_selected=True,
                           enable_live_data_checkbox=False,
                           table_list=table_list,
                           warning_message=warning_message)


@app.route('/game_info')
def show_game_info_table():
    """Show table of static game info parameters"""
    warning_message = ''

    game_name = lib_db.get_game_name()
    run_time = lib_db.get_game_run_time_info()
    dash = lib_db.get_dashboard_selected_by_game_host()
    board_size = lib_db.get_board_size()
    score_desc = lib_db.get_game_score_description()
    board_desc = lib_db.get_board_description()

    data = []
    data.append({'key': 'Game name:', 'value': game_name})
    if run_time['game_start_time']:
        start_time_obj = datetime.datetime.strptime(run_time['game_start_time'], "%Y%m%dT%H%M%S.%f")
        start_time_str = start_time_obj.strftime("%H:%M, %d. %b")
    else:
        start_time_str = 'NA'
    data.append({'key': 'Start time:', 'value': start_time_str})
    data.append({'key': 'Board size (X x Y):', 'value': "{} x {}".format(board_size['x'],
                                                                         board_size['y'])})
    data.append({'key': 'Game score description:', 'value': score_desc})
    data.append({'key': 'Board description:', 'value': board_desc})
    data.append({'key': 'Default dashboard:', 'value': dash})

    table_id = 'datatable_id'
    table_list = [my_tables.GameInfoTable(data, table_id=table_id)]
    return render_template('tables.html',
                           menu=sidebar_menu.SidebarMenu(request.path).get_menu(),
                           content_title="µWars game info",
                           content_title_small="",
                           dropdown_list=[],
                           table_selected=True,
                           enable_live_data_checkbox=False,
                           table_list=table_list,
                           warning_message=warning_message)


@app.route('/screenshots/max_length')
def show_screenshot():
    """Page for showing a screenshot image"""

    # Check if the image exists, if not show a default image
    local_img_path = lib_db.get_snake_max_length_img_path()
    if local_img_path:
        img_url = url_for('get_screenshot_file')
    else:
        img_url = url_for('static', filename='images/favicon-16x16.png')

    return render_template('screenshot.html',
                           menu=sidebar_menu.SidebarMenu(request.path).get_menu(),
                           content_title='µWars screenshots',
                           img_url=img_url,
                           content_title_small="""Click on the image to open a zoomable version in
                                               full screen""")


@app.route('/game_images/screenshots/max_length')
def get_screenshot_file():
    """Route for getting a screenshot image from dis"""
    file_path = lib_db.get_snake_max_length_img_path()

    return send_from_directory(os.path.dirname(file_path),
                               os.path.basename(file_path),
                               mimetype='image/png',
                               cache_timeout=0)


@app.route('/game_images/snake/<img_type>/<node_id>')
def get_snake_img(img_type, node_id):
    """Route for getting a snake image file"""
    if img_type == 'head':
        img_file_path = lib_db.get_snake_head_img_file_path(node_id)
    elif img_type == 'icon':
        img_file_path = lib_db.get_snake_icon_img_file_path(node_id)
    else:
        img_file_path = ''

    return send_from_directory(os.path.dirname(img_file_path),
                               os.path.basename(img_file_path),
                               mimetype='image/png',
                               cache_timeout=1)


@socketio.on('connect', namespace='/snake')
def snake_socket_connect():
    """Run when a client connects to the websocket, start the background thread for pushing
    data"""
    print("Connected (snake): ", request.sid)
    global thread_main
    if thread_main is None:
        thread_main = socketio.start_background_task(target=background_thread_main, app_ref=app)


@socketio.on('join_room', namespace='/snake')
def snake_socket_connect_add_to_room(data):
    """Add the client to the room"""
    print("joining room {}".format(data['room']))
    join_room(data['room'])
    # TBD - send start data for this room (or assume this has been sent with the initial html?)
    emit('message', "joined room {}".format(data['room']))


@socketio.on('leave_room', namespace='/snake')
def snake_socket_connect_remove_from_room(data):
    """Add the client to the room"""
    print("leaving room {}".format(data['room']))
    leave_room(data['room'])
    # TBD - send start data for this room (or assume this has been sent with the initial html?)
    emit('message', "left room {}".format(data['room']))


@socketio.on('disconnect', namespace='/snake')
def snake_socket_disconnect():
    """Run when a client disconnects"""
    print('Client disconnected from snake', request.sid)


if __name__ == '__main__':
    # Command line parser
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', dest='debug_enabled', action='store_true', help='''Set the debug
                        switch when starting the dev server''')
    parser.add_argument('-p', dest='allow_public', action='store_true', help='''Make server publicly
                        available on local network''')
    args = parser.parse_args()

    if args.allow_public:
        # Allow network connections
        host = "0.0.0.0"
    else:
        # Only allow connections from the same computer (localhost)
        host = "127.0.0.1"

    msg = "Starting flask server at " + host + ". Debug is "
    if args.debug_enabled:
        msg += "enabled"
    else:
        msg += "disabled"

    print(msg)
    # Start the app with websockets enabled
    socketio.run(app, debug=args.debug_enabled, host=host)
