##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

import os
import sys
import re
import collections
import plotly
import cgi
import plotly.graph_objs as go
from plotly.graph_objs import Layout

"""
NB - some of the plots in plotly.figure_factory requires scipy. On Linux These should be installed
automatically when installing the requirements with pip, but on Windows a manual install is
required. Since this only concerns a few plots the webserver should continue running in both cases,
just disabling the specific plots if the packages are missing. This can for instance be done by
putting all calls using ff (ie plotly.figure_factory) inside a try/except block catching any
ImportError exceptions and outputting an error message instead of the plot.
"""
import plotly.figure_factory as ff
try:
    # Check if scipy is available
    import scipy
    if sys.platform == 'win32':
        # Check if numpy+mkl is available
        import numpy as np
        if not (np.__config__.blas_mkl_info and np.__config__.lapack_mkl_info):
            raise ImportError
except ImportError as e:
    is_valid = False
    print("\n***** WARNING *****")
    print("** Could not find a valid numpy/scipy installation, some charts will not be " +
          "available.")
    print("** On Windows Scipy will not install properly with pip but has to be installed")
    print("** manually, please check the README.md file for instructions on how to do this.")
    print("** If you are running on Linux please make sure all packages have been installed")
    print("** according to the requirements.txt file.")
    print("*******************\n")


def get_missing_numpy_scipy_for_plotly_ff_err_msg():
    """Error message that can be displayed instead of the chart if scipy is missing"""
    return ('This chart could not be created on this system. Please check the README.md file for '
            'more information on how to fix this by installing scipy.')


class ChartCollection:
    def get_db_data_funcs(self):
        func_list = []
        for chart_id, chart_data in self.charts.items():
            func_list.append(chart_data['db_data_func'])

        return func_list


class Charts(ChartCollection):
    """Class for grouping charts"""
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.charts = collections.OrderedDict()
        self.charts['snake_ages_box'] = {'class': SnakeAgeBoxChart,
                                         'title': 'Snake age',
                                         'db_data_func': 'get_score_totals'}
        self.charts['snake_ages_lastn_box'] = {'class': SnakeAgeLastnBoxChart,
                                               'title': 'Snake age, last N lives',
                                               'db_data_func': 'get_score_totals'}
        self.charts['max_snake_lengths_box'] = {'class': MaxSnakeLengthBoxChart,
                                                'title': 'Max snake length',
                                                'db_data_func': 'get_score_totals'}
        self.charts['eol_snake_lengths_box'] = {'class': EolSnakeLengthBoxChart,
                                                'title': 'Snake length at EOL',
                                                'db_data_func': 'get_score_totals'}
        self.charts['eol_snake_lengths_lastn_box'] = {'class': EolSnakeLengthLastnBoxChart,
                                                      'title': 'Snake length at EOL, last N rounds',
                                                      'db_data_func': 'get_score_totals'}
        self.charts['snake_moves'] = {'class': SnakeMoveChart,
                                      'title': 'Snake moves',
                                      'db_data_func': 'get_score_totals'}
        self.charts['snake_mass'] = {'class': SnakeMassChart,
                                     'title': 'Snake mass',
                                     'db_data_func': 'get_score_totals'}
        self.charts['snake_kills'] = {'class': SnakeKillsChart,
                                      'title': 'Snake kills',
                                      'db_data_func': 'get_death_list_by_killed_snake'}
        self.charts['eol_snake_length_per_life'] = {'class': EolSnakeLengthPerLifeChart,
                                                    'title': 'Snake length at the end of each life',
                                                    'db_data_func': 'get_eol_lengths_per_life'}
        self.charts['max_snake_length_per_life'] = {'class': MaxSnakeLengthPerLifeChart,
                                                    'title': 'Max snake length per life',
                                                    'db_data_func': 'get_max_lengths_per_life'}
        self.charts['snake_age_per_life'] = {'class': SnakeAgePerLifeChart,
                                             'title': 'Snake age per life',
                                             'db_data_func': 'get_ages_per_life'}
        self.charts['eol_snake_length_distribution'] = {'class': EolSnakeLengthDistributionChart,
                                                        'title': 'Snake length distribution (EOL)',
                                                        'db_data_func': 'get_eol_lengths_per_life'}
        self.charts['max_snake_length_distribution'] = {'class': MaxSnakeLengthDistributionChart,
                                                        'title': 'Max snake length distribution',
                                                        'db_data_func': 'get_max_lengths_per_life'}
        self.charts['snake_age_distribution'] = {'class': SnakeAgeDistributionChart,
                                                 'title': 'Snake age distribution',
                                                 'db_data_func': 'get_ages_per_life'}


class BoardMapCharts(ChartCollection):
    """Class for grouping board map charts"""
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.charts = collections.OrderedDict()
        self.charts['visit_map'] = {'class': VisitHeatmapChart,
                                    'title': 'Visit map',
                                    'db_data_func': 'TBD'}
        self.charts['death_map'] = {'class': DeathHeatmapChart,
                                    'title': 'Death map',
                                    'db_data_func': 'TBD'}
        self.charts['visit_map_3d'] = {'class': VisitHeatmap3DChart,
                                       'title': 'Visit map 3D',
                                       'db_data_func': 'TBD'}
        self.charts['death_map_3d'] = {'class': DeathHeatmap3DChart,
                                       'title': 'Death map 3D',
                                       'db_data_func': 'TBD'}


def get_color_lum(hex_color):
    """Get an approximation for the color luminance"""
    if len(hex_color) == 7 and hex_color.startswith('#'):
        try:
            r = int(hex_color[1:3], 16)
            g = int(hex_color[3:5], 16)
            b = int(hex_color[5:7], 16)

            lum = 0.299 * r + 0.587 * g + 0.114 * b
            return lum
        except Exception as e:
            # Error converting the hex color to separate rgb values
            return -1


def select_series_color(main_color, top_color):
    """Select a color for a chart series based on the actual snake colors. The color is check to
    make sure it will give sufficient contrast against the chart background.

    The returned color will be the first valid one from this list:
        - the main color
        - the top mark color
        - a default color
    """

    # Checking which color can be used
    if get_color_lum(main_color) < 240:
        color = main_color
    elif get_color_lum(top_color) < 240:
        color = top_color
    else:
        # Both snake colors are to bright, using the default color
        color = '#0073b7'

    return color


class GeneralChart:
    """General chart class for defining the interface and internal variables"""
    def __init__(self, chart_id, data):
        self.chart_id = chart_id
        self.chart_title = "Insert chart title here"
        self.data = data
        self.chart_lib = ''     # Eg plotly or highcharts
        self.room_id = ''

    def live_data_supported(self):
        """Returns True if the table supports live data updates, else False"""
        if self.room_id:
            return True
        else:
            return False

    def get_formatted_chart_title(self):
        return self.chart_title

    def set_data(self, data):
        self.data = data

    def get_data_json(self):
        """Get all the necessary data for updating the chart over sockets"""
        return {}

    def get_chart_json(self):
        """Create the complete json for generating the chart"""
        return {}

    def get_chart_html(self):
        """Create the complete html for generating the chart"""
        return ''


class GeneralPlotlyChart(GeneralChart):
    """Class with common functions for Plotly chart

    NB - Plotly won't do any escaping of labels or titles so this must be done manually when using
    values that might not be safe (ie strings from the database). Escape this using
        cgi.escape(value)

    NB - Plotly seem to select the y axis data type based on the first value, so if the y axis
    are string labels but the first value can be converted to a number (eg '42') it will expect
    all other values to be numbers too. In these cases the label must be changed to something that
    can't be convered to a number before adding to the axis.
    """
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)
        self.chart_lib = 'plotly'

    def chart_html_postproc(self, plot_html):
        """Post processing of the generated html.

        - replaces the div id assigned by plotly with the one specified by the user
        - inserts class for enabling responsive charts (automatic resizing on window change)

        NB - this function should always be run on the html generated by plotly before returning
        it to the html templates
        """

        # Find the div id and replace it with the value set for this chart class
        m = re.search('id="(?P<id>[\w-]+)"', plot_html)
        if m:
            div_id = m.group('id')

            # Replace all occurences of the id
            plot_html = plot_html.replace(div_id, self.chart_id)

        # Find the class attribute and insert the class for enabling responsive charts
        m = re.search('class="(?P<classes>[\w-]+)"', plot_html)
        if m:
            div_classes_org = m.group('classes')
            div_classes_new = div_classes_org + ' responsive-plotly'

            # Replace all occurences of the id
            plot_html = plot_html.replace(div_classes_org, div_classes_new)

        return plot_html


class GeneralHighchartsChart(GeneralChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)
        self.chart_lib = 'highcharts'


class SnakeMassChart(GeneralPlotlyChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.room_id = 'snake_mass_chart_room'

    def get_data_json(self):
        """Get all the necessary data for updating the chart over sockets"""
        new_data = {'values': [], 'labels': [], 'colors': []}
        for row in self.data:
            new_data['values'].append(row['acc_snake_mass'])
            new_data['labels'].append(cgi.escape(row['name']))
            new_data['colors'].append(select_series_color(row['color1'], row['color2']))

        return new_data

    def get_chart_json(self):
        """Create the complete json for generating the chart"""
        return {}

    def get_chart_html(self):
        """Create the complete html for generating the chart"""

        raw_data = self.get_data_json()

        new_data = [{
            "values": raw_data['values'],
            "labels": raw_data['labels'],
            'marker': {'colors': raw_data['colors']},
            # "domain": {"x": [0, .48]},
            "name": "",
            "hoverinfo": "label+percent+name+value",
            "hole": .4,
            # "rotation": 90 - 360 * max(raw_data['values']) / sum(raw_data['values']),
            "direction": 'clockwise',
            "type": "pie"
        }]

        layout = {
            "title": "Accumulated snake mass",
        }

        plot_html = plotly.offline.plot({"data": new_data,
                                         "layout": layout},
                                        output_type='div',
                                        include_plotlyjs=False,
                                        show_link=False)

        plot_html = self.chart_html_postproc(plot_html)

        return plot_html


class SnakeMoveChart(GeneralPlotlyChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

    def get_data_json(self):
        """Get all the necessary data for updating the chart over sockets"""
        return {}

    def get_chart_html(self):
        """Create the complete html for generating the chart"""

        # Define the y axis labels and the keys for getting the corresponding data
        # (defined here to make it easier to change the order if necessary)
        y_def = []
        y_def.append({'key': 'move_rights', 'label': 'R'})
        y_def.append({'key': 'move_lefts', 'label': 'L'})
        y_def.append({'key': 'move_forwards', 'label': 'F'})
        y_def.append({'key': 'move_fast_forwards', 'label': 'FF'})

        # Create the list for the y labels
        y = []
        for y_val in y_def:
            y.append(y_val['label'])

        # Find the max count value so that we can set the scaling factor, also calculating the sum
        # for the title
        max_count = 0
        total_count = 0
        for y_val in y_def:
            for snake in self.data:
                total_count += snake[y_val['key']]
                if max_count < snake[y_val['key']]:
                    max_count = snake[y_val['key']]

        # TBD - how should the scaling factor be selected?
        # current values are found by trial and error on a single screen
        scale = max_count / 2000        # sizemode='area'
        # scale = max_count / 60          # sizemode='diameter'

        # Create the title
        total_string = '{0:,}'.format(total_count).replace(',', ' ')
        # max_string = '{0:,}'.format(max_count).replace(',', ' ')
        title = 'Distribution of snake moves<br><sub>{0} moves in total </sub>'.format(total_string)

        # Iterate through all the moves and create a trace for each
        new_data = []
        for y_val in y_def:
            # Create the y axis array, using the same value to get all bubbles on the same line
            y = [y_val['label']] * len(self.data)

            # Loop through all the snakes, creating the x axis and size data arrays
            x = []
            size = []
            color = []
            line_color = []
            text = []
            for snake in self.data:
                # Add the snake to the x axis
                x.append(cgi.escape(snake['name']))

                # Add the move count
                size.append(snake[y_val['key']])

                # Set the bubble color to the snake base color
                color.append(snake['color1'])

                # If the color is very bright, add a border around the circle to make it stand out
                # from the background, else keep it the same as the main color
                set_line_color = snake['color1']
                if get_color_lum(snake['color1']) > 240:
                    # Use the top marking color if it is dark enough, else use black
                    if get_color_lum(snake['color2']) > 240:
                        set_line_color = '#000000'
                    else:
                        set_line_color = snake['color2']
                line_color.append(set_line_color)

                # Set the hover text to show the count value
                text.append('{0:,}'.format(snake[y_val['key']]).replace(',', ' '))

            # Create the trace
            trace = go.Scatter(
                x=x,
                y=y,
                hoverinfo='y+x+z+text',     # Disabling the trace name from the hover text
                text=text,
                mode='markers',
                marker=dict(
                    size=size,
                    sizeref=scale,
                    sizemode='area',
                    opacity=0.75,
                    color=color,
                    line=dict(color=line_color)
                )
            )

            # Add the trace to the data
            new_data.append(trace)

        layout = {'title': title,
                  'xaxis': {'tickangle': -60},
                  'showlegend': False}

        plot_html = plotly.offline.plot({"data": new_data,
                                         "layout": layout},
                                        output_type='div',
                                        include_plotlyjs=False,
                                        show_link=False)

        plot_html = self.chart_html_postproc(plot_html)

        return plot_html


class SnakeKillsChart(GeneralPlotlyChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

    def get_data_json(self):
        """Get all the necessary data for updating the chart over sockets"""
        return {}

    def get_chart_html(self):
        """Create the complete html for generating the chart"""

        # Find the max count value so that we can set the scaling factor, also calculating the sum
        # for the title
        max_count = 0
        total_count = 0
        for snake_id, death_data in self.data.items():
            for killer_id, killer_data in death_data['deaths'].items():
                total_count += killer_data['count']
                if max_count < killer_data['count']:
                    max_count = killer_data['count']

        # TBD - how should the scaling factor be selected?
        # current values are found by trial and error on a single screen
        scale = max_count / 1750        # sizemode='area'
        # scale = max_count / 60          # sizemode='diameter'

        # Create the title
        total_string = '{0:,}'.format(total_count).replace(',', ' ')
        title = 'Snake kills<br><sub>Killer snakes along the x axis, dead snakes on the y axis.'
        title += ' {0} kills so far</sub>'''.format(total_string)

        # Iterate through all the killer snakes, creating a series for each
        new_data = []
        for snake_id, death_data in self.data.items():
            x = []
            size = []
            color = []
            line_color = []
            line_widths = []
            text = []
            symbols = []

            try:
                # If the first y axis label is a string that could be converted to a number Plotly
                # seem to expect all other labels to be valid numbers too, causing plotting to fail
                # they aren't. As a temporary workaround such labels are changed to a string that
                # can't be converted to a number (making it italic for now...)
                test_number = float(death_data['name']) # Will throw an exception if not a number
                # Change the value into something that definitely won't convert to a number
                death_data['name'] = "<i>" + death_data['name'] + "</i>"
            except Exception as e:
                pass

            # Create the y axis array, using the same value to get all bubbles on the same line
            y = [cgi.escape(death_data['name'])] * len(death_data['deaths'])

            # Set the bubble color to the dead snakes base color
            color = [death_data['color1']] * len(death_data['deaths'])

            # If the color is very bright, add a border around the circle to make it stand out
            # from the background, else keep it the same as the main color
            set_line_color = death_data['color1']
            if get_color_lum(death_data['color1']) > 240:
                # Use the top marking color if it is dark enough, else use black
                if get_color_lum(death_data['color2']) > 240:
                    set_line_color = '#000000'
                else:
                    set_line_color = death_data['color2']
            line_color = [set_line_color] * len(death_data['deaths'])

            # Iterate through all the deaths for this snake and build the values in the series
            index = 0
            for killer_id, kill_data in death_data['deaths'].items():
                # Add the snake to the x axis
                x.append(cgi.escape(kill_data['name']))

                # Add the kill count
                size.append(kill_data['count'])

                # Set the hover text to show the count value
                text.append('{0:,}'.format(kill_data['count']).replace(',', ' '))

                # Set the symbol
                if snake_id == killer_id or killer_id == 'Board':
                    # Self kills or board kills - using open circle with wider line
                    # Also need to override the color to get it drawn correctly
                    symbol = 'circle-open'
                    line_width = 3
                    color[index] = line_color[index]
                else:
                    # Default symbol is a filled circle
                    symbol = 'circle'
                    line_width = 1

                symbols.append(symbol)
                line_widths.append(line_width)

                index += 1

            # Create the trace
            trace = go.Scatter(
                x=x,
                y=y,
                hoverinfo='y+x+z+text',     # Disabling the trace name from the hover text
                text=text,
                mode='markers',
                marker=dict(
                    symbol=symbols,
                    size=size,
                    sizeref=scale,
                    sizemode='area',
                    opacity=0.65,
                    color=color,
                    line=dict(color=line_color, width=line_widths)
                )
            )

            # Add the trace to the data
            new_data.append(trace)

        if not new_data:
            new_data.append(go.Scatter())

        layout = {'title': title,
                  'hovermode': 'closest',
                  'xaxis': {'tickangle': -60},
                  'showlegend': False}

        plot_html = plotly.offline.plot({"data": new_data,
                                         "layout": layout},
                                        output_type='div',
                                        include_plotlyjs=False,
                                        show_link=False)

        plot_html = self.chart_html_postproc(plot_html)

        return plot_html


class SnakeParamPerLifeChart(GeneralPlotlyChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

    def get_data_json(self):
        """Get all the necessary data for updating the chart over sockets"""
        # Live update not supported for this plot
        return {}

    def get_chart_html(self):
        """Create the complete html for generating the chart"""

        # Create the traces
        new_data = []
        title = self.title
        title += '<br><sub>Click/double click on the snake names to filter the data</sub>'
        for node_id, node_data in self.data.items():
            trace = go.Scattergl(
                x=node_data['start_rounds'],
                y=node_data[self.y_data_key],
                mode='markers',
                name=node_data['name'],
                marker={
                    'color': select_series_color(node_data['color1'], node_data['color2'])
                }
            )

            # Add the trace to the data
            new_data.append(trace)

        if not new_data:
            # Create a empty plot in case no data was found in the database
            new_data.append(go.Scattergl())

        layout = {'title': title,
                  'xaxis': {'title': 'Round number'},
                  'yaxis': {'title': self.y_title},
                  }

        plot_html = plotly.offline.plot({"data": new_data,
                                         "layout": layout},
                                        output_type='div',
                                        include_plotlyjs=False,
                                        show_link=False)

        plot_html = self.chart_html_postproc(plot_html)

        return plot_html


class EolSnakeLengthPerLifeChart(SnakeParamPerLifeChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.title = "Final length per snake life"
        self.y_data_key = 'eol_lengths'
        self.y_title = 'Length'


class MaxSnakeLengthPerLifeChart(SnakeParamPerLifeChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.title = "Max length per snake life"
        self.y_data_key = 'max_lengths'
        self.y_title = 'Length'


class SnakeAgePerLifeChart(SnakeParamPerLifeChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.title = "Age per snake life"
        self.y_data_key = 'ages'
        self.y_title = 'Age'


class SnakeLifeParamDistributionChart(GeneralPlotlyChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

    def get_data_json(self):
        """Get all the necessary data for updating the chart over sockets"""
        # Live update not supported for this plot
        return {}

    def get_chart_html(self):
        """Create the complete html for generating the chart"""

        try:
            # Create the traces
            title = self.title
            title += '<br><sub>Click/double click on the snake names to filter the data</sub>'
            hist_data = []
            group_labels = []
            colors = []
            for node_id, node_data in self.data.items():
                # Calculation of histogram/distribution will fail if there is too few data points
                if len(node_data[self.y_data_key]) > 2:
                    hist_data.append(node_data[self.y_data_key])
                    group_labels.append(node_data['name'])
                    colors.append(select_series_color(node_data['color1'], node_data['color2']))

            # Create distplot
            fig = ff.create_distplot(hist_data,
                                     group_labels,
                                     colors=colors,
                                     show_rug=False,
                                     show_hist=False)

            fig['layout'].update(title=title)
            fig['layout']['xaxis'].update(title=self.x_title)
            fig['layout']['yaxis'].update(title='Frequency')

            plot_html = plotly.offline.plot(fig,
                                            output_type='div',
                                            include_plotlyjs=False,
                                            show_link=False)

            plot_html = self.chart_html_postproc(plot_html)
        except ImportError as e:
            # Missing the required numpy/scipy packages, the plot can't be created returning
            # an error message instead
            # print("*********")
            # print(str(e))
            plot_html = get_missing_numpy_scipy_for_plotly_ff_err_msg()
        except IndexError as e:
            # No valid traces found in the data, output error message instead
            plot_html = 'No data found'

        return plot_html


class EolSnakeLengthDistributionChart(SnakeLifeParamDistributionChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.title = "Snake length distribution (EOL length)"
        self.y_data_key = 'eol_lengths'
        self.x_title = 'Length'


class MaxSnakeLengthDistributionChart(SnakeLifeParamDistributionChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.title = "Snake length distribution (max length)"
        self.y_data_key = 'max_lengths'
        self.x_title = 'Length'


class SnakeAgeDistributionChart(SnakeLifeParamDistributionChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.title = "Snake age distribution"
        self.y_data_key = 'ages'
        self.x_title = 'Age'


class BoxChart(GeneralHighchartsChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.yaxis_title = "Insert yaxis title here"
        self.box_series_name = 'Insert series name here'

        self.box_data_source_keys = None    # The dict keys for accessing the box chart data params
        self.scatter_series = []            # List of configs for optional scatter plots

    def get_data_json(self):
        """Get all the necessary data for updating the chart over sockets"""
        # Get the processed data arrays
        new_data = self.process_data()

        update_data = {
            'common': {
                'xAxis': {
                    'categories': new_data['categories'],
                },
                # 'yAxis': {
                #     'min': 0,
                #     'max': max_val * 1.05
                #     'endOnTick': False,
                #     'plotLines': [{
                #         'value': new_data['max_median'],
                #         'color': '#444444',
                #         'width': 1,
                #         'dashStyle': 'dash',
                #         'label': {
                #             'text': 'Best<br>median',
                #             'align': 'right'
                #         }
                #     }]
                # },
                # TBD - can the chart be updated without setting the plotOptions?
                # Commenting it out makes the chart updates look smoother (animated)
                'plotOptions': {
                    'boxplot': {
                        'colors': new_data['box_colors'],
                    }
                }
            },
            'data': [new_data['box_data']] + new_data['scatter_data']
        }

        return update_data

    def process_data(self):
        """Process the raw data and create the arrays that can be inserted in the chart json"""
        new_data = {}
        new_data['categories'] = []
        new_data['box_colors'] = []
        new_data['box_data'] = []
        new_data['scatter_data'] = []
        for series in self.scatter_series:
            new_data['scatter_data'].append([])
        # new_data['max_median'] = 0

        for row in self.data:
            try:
                new_data['categories'].append(row['name'])

                new_box = [row[self.box_data_source_keys['min']],
                           row[self.box_data_source_keys['q1']],
                           row[self.box_data_source_keys['median']],
                           row[self.box_data_source_keys['q3']],
                           row[self.box_data_source_keys['max']]]
                new_data['box_data'].append(new_box)

                # if new_data['max_median'] < row[self.box_data_source_key]['median']:
                #     new_data['max_median'] = row[self.box_data_source_key]['median']

                # Select the color for the box
                color = select_series_color(row['color1'], row['color2'])

                new_data['box_colors'].append(color)

                # Add data for the optional scatter plots
                for index, series in enumerate(self.scatter_series):
                    keys = series['data_source_key'].split('.')
                    if len(keys) == 1:
                        value = row[keys[0]]
                    elif len(keys) == 2:
                        value = row[keys[0]][keys[1]]
                    else:
                        # Error
                        print("Unsupported key in charts: {}".format(series['data_source_key']))
                        value = 0

                    new_data['scatter_data'][index].append({'y': value,
                                                            'color': color})
            except Exception as e:
                # Could not extract the box plot parameters from the data (eg missing dict keys
                # (for data source or individual parameters), nothing is added to the new_data array
                print("Error - extracting box plot data")
                print(str(e))

        return new_data

    def get_chart_json(self):
        """Create the complete json for generating the chart"""
        # Get the processed data arrays
        new_data = self.process_data()

        data_series = [{
            'name': self.box_series_name,
            'type': 'boxplot',
            'lineWidth': 2,
            'medianWidth': 3,
            'colorByPoint': True,                   # To be able to specify box colors
            'fillColor': 'rgba(255, 255, 255, 0)',  # Make the boxes transparent
            'data': new_data['box_data'],
        }]

        for index, series in enumerate(self.scatter_series):
            data_series.append({
                'name': series['name'],
                'type': 'scatter',
                'marker': series['marker'],
                'data': new_data['scatter_data'][index],
            })

        # Create the chart json
        self.chart = {
            'chart': {
                'renderTo': self.chart_id,
                'animation': False,
            },
            'title': {'text': self.get_formatted_chart_title()},
            'legend': {'enabled': False},
            'xAxis': {
                'categories': new_data['categories'],
            },
            'yAxis': {
                'title': {
                    'text': self.yaxis_title
                },
                'min': 0,
                # 'max': max_val * 1.05
                'endOnTick': False,
                # 'plotLines': [{
                #     'value': new_data['max_median'],
                #     'color': '#444444',
                #     'width': 1,
                #     'dashStyle': 'dash',
                #     'label': {
                #         'text': 'Best<br>median',
                #         'align': 'right'
                #     }
                # }]
            },
            'series': data_series,
            'plotOptions': {
                'boxplot': {
                    'colors': new_data['box_colors'],
                }
            }
        }

        return self.chart


class SnakeAgeBoxChart(BoxChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.chart_title = 'Snake age'
        self.yaxis_title = 'Number of rounds'
        self.box_series_name = 'Age'

        self.box_data_source_keys = {'max': 'alive_time_max',
                                     'q3': 'alive_time_quartile3',
                                     'median': 'alive_time_median',
                                     'q1': 'alive_time_quartile1',
                                     'min': 'alive_time_min'}
        self.scatter_series = [{'data_source_key': 'alive_time_current',
                                'name': 'Current',
                                'marker': {
                                    'lineWidth': 1,
                                    'symbol': 'triangle',
                                    'radius': 6
                                }},{'data_source_key': 'alive_time_lastn_median',
                                'name': 'Median last N lives',
                                'marker': {
                                    'lineWidth': 1,
                                    'symbol': 'circle',
                                    'radius': 6
                                }}]

        self.room_id = 'snake_age_box_chart_room'


class SnakeAgeLastnBoxChart(BoxChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.chart_title = 'Snake age for last N lives'
        self.yaxis_title = 'Number of rounds'
        self.box_series_name = 'Age'

        self.box_data_source_key = 'alive_time_lastn'
        self.box_data_source_keys = {'max': 'alive_time_lastn_max',
                                     'q3': 'alive_time_lastn_quartile3',
                                     'median': 'alive_time_lastn_median',
                                     'q1': 'alive_time_lastn_quartile1',
                                     'min': 'alive_time_lastn_min'}
        self.scatter_series = [{'data_source_key': 'alive_time_current',
                                'name': 'Current',
                                'marker': {
                                    'lineWidth': 1,
                                    'symbol': 'triangle',
                                    'radius': 6
                                }}]

        self.room_id = 'snake_age_lastn_box_chart_room'


class EolSnakeLengthBoxChart(BoxChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.chart_title = 'Snake length at EOL'
        self.yaxis_title = 'Length'
        self.box_series_name = 'Length'

        self.box_data_source_keys = {'max': 'eol_length_max',
                                     'q3': 'eol_length_quartile3',
                                     'median': 'eol_length_median',
                                     'q1': 'eol_length_quartile1',
                                     'min': 'eol_length_min'}
        self.scatter_series = [{'data_source_key': 'length_current',
                                'name': 'Current',
                                'marker': {
                                    'lineWidth': 1,
                                    'symbol': 'triangle',
                                    'radius': 6
                                }}, {'data_source_key': 'max_length_max',
                                'name': 'Max length',
                                'marker': {
                                    'lineWidth': 1,
                                    'symbol': 'triangle-down',
                                    'radius': 6
                                }}, {'data_source_key': 'eol_length_lastn_median',
                                'name': 'Median last N lives',
                                'marker': {
                                    'lineWidth': 1,
                                    'symbol': 'circle',
                                    'radius': 5
                                }}]

        self.room_id = 'snake_eol_length_box_chart_room'


class EolSnakeLengthLastnBoxChart(BoxChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.chart_title = 'Snake length at EOL for last N lives'
        self.yaxis_title = 'Length'
        self.box_series_name = 'Length'

        self.box_data_source_keys = {'max': 'eol_length_lastn_max',
                                     'q3': 'eol_length_lastn_quartile3',
                                     'median': 'eol_length_lastn_median',
                                     'q1': 'eol_length_lastn_quartile1',
                                     'min': 'eol_length_lastn_min'}
        self.scatter_series = [{'data_source_key': 'length_current',
                                'name': 'Current',
                                'marker': {
                                    'lineWidth': 1,
                                    'symbol': 'triangle',
                                    'radius': 6
                                }}]

        self.room_id = 'snake_eol_length_lastn_box_chart_room'


class MaxSnakeLengthBoxChart(BoxChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.chart_title = 'Max snake length'
        self.yaxis_title = 'Length'
        self.box_series_name = 'Length'

        self.box_data_source_keys = {'max': 'max_length_max',
                                     'q3': 'max_length_quartile3',
                                     'median': 'max_length_median',
                                     'q1': 'max_length_quartile1',
                                     'min': 'max_length_min'}
        self.scatter_series = [{'data_source_key': 'length_current',
                                'name': 'Current',
                                'marker': {
                                    'lineWidth': 1,
                                    'symbol': 'triangle',
                                    'radius': 6
                                }}, {'data_source_key': 'max_length_current',
                                'name': 'Current max',
                                'marker': {
                                    'lineWidth': 1,
                                    'symbol': 'triangle-down',
                                    'radius': 6
                                }}]

        self.room_id = 'max_snake_length_box_chart_room'


class BoardMapChart(GeneralPlotlyChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.colorscale = 'Portland'
        self.data_select = ''

        self.map_type = '2d'          # '2d' or '3d'

    def get_data_select(self):
        """Get the selector that will be passed to the database for selecting which data to get"""
        return self.data_select

    def get_data_json(self):
        """Get all the necessary data for updating the chart over sockets"""
        return {}

    def get_chart_json(self):
        """Create the complete json for generating the chart"""
        return {}

    def get_chart_html(self):
        """Create the complete html for generating the chart"""

        try:
            z_data = self.data['z']

            # Update the max value to make sure the color scale is drawn correctly, for charts with
            # little data the max value is forced to make sure the chart is drawn properly
            if self.data['max'] < 5:
                z_max = 4
            else:
                z_max = self.data['max']
        except Exception as e:
            # No data was found in self.data, setting empty values
            z_data = []
            z_max = 4

        if self.map_type == '3d':
            new_data = [
                go.Surface(
                    z=self.data['z'],
                    colorscale=self.colorscale,
                    cmax=z_max,
                    cmin=0,
                )
            ]
        else:   # '2d'
            new_data = [
                go.Heatmap(
                    z=z_data,
                    colorscale=self.colorscale,
                    zmax=z_max,
                    zmin=0,
                )
            ]

        layout = Layout(title=self.get_formatted_chart_title(),
                        xaxis={'showticklabels': False},
                        yaxis={'showticklabels': False},
                        )

        plot_html = plotly.offline.plot({"data": new_data,
                                         "layout": layout},
                                        output_type='div',
                                        include_plotlyjs=False,
                                        show_link=False)

        plot_html = self.chart_html_postproc(plot_html)

        return plot_html


class VisitHeatmapChart(BoardMapChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        # Identifier to decide which data is read from the database
        self.data_select = 'visits'

        # self.room_id = 'visit_map_chart_room'

    def get_formatted_chart_title(self):
        """Return a formatted title for the chart"""

        if 'sum' in self.data:
            total_string = '{0:,}'.format(self.data['sum']).replace(',', ' ')
            chart_title = 'Board visits<br><sub>{0} moves in total</sub>'.format(total_string)
        else:
            chart_title = 'Board visits'

        return chart_title


class DeathHeatmapChart(BoardMapChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        # Identifier to decide which data is read from the database
        self.data_select = 'deaths'

        # self.room_id = 'death_map_chart_room'

    def get_formatted_chart_title(self):
        """Return a formatted title for the chart"""

        if 'sum' in self.data:
            total_string = '{0:,}'.format(self.data['sum']).replace(',', ' ')
            # if node_id:
            #     chart_title = 'Map of Death<br><sub>{0} deaths</sub>'.format(total_string)
            # else:
            chart_title = 'Map of Death<br><sub>{0} dead snakes</sub>'.format(total_string)
        else:
            chart_title = 'Map of Death'

        return chart_title


class VisitHeatmap3DChart(VisitHeatmapChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.map_type = '3d'


class DeathHeatmap3DChart(DeathHeatmapChart):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        self.map_type = '3d'
