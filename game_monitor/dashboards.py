##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

import collections

import lib_db
import charts as my_charts
import tables as my_tables
import info_boxes as my_info_boxes


class Dashboards:
    """Class for grouping dashboards"""
    def __init__(self):
        # Specify the key for the the default dashboard, this will be used as the main page
        # The keys are also used when selecting a dashboard from the game host.
        self.default_dashboard = 'max_length'

        self.dashboards = collections.OrderedDict()
        self.dashboards['max_length'] = {'class': DashboardMaxLength,
                                         'menu_title': 'Max length'}
        self.dashboards['max_acc_snake_mass'] = {'class': DashboardMaxAccSnakeMass,
                                                 'menu_title': 'Max accumulated snake mass'}
        self.dashboards['max_age'] = {'class': DashboardMaxAge,
                                      'menu_title': 'Max age'}

    def is_valid_selector(self, name):
        if name in self.dashboards.keys():
            return True

        return False


class BaseDashboard:
    """Base class for creating a dashboard"""
    def __init__(self):

            self.title = 'µWars dashboard'
            self.html_template = ''

            # List of ids for the elements that should be included in the dashboard. The ids
            # must match the keys in the dictionaries for the tables/charts/info_boxes groups
            self.table_ids = []
            self.chart_ids = []
            self.info_box_ids = []

            # List of chats to show on the dashboard
            self.chats = []

            # Lists of elements used by the html template, these will be created based on the
            # id lists
            self.info_box_list = []
            self.chart_list = []
            self.table_list = []
            self.chat_list = []

    def create_dashboard(self):
        """Create the dashboard based on the selected chart/table/info_box ids"""

        # Get a list of all functions needed for reading data from the database. The set ensures
        # that each function is listed (and called) only once even if it is used by several
        # charts/tables/etc
        data_funcs = set()

        if self.table_ids:
            tables = my_tables.ScoreTables()
            for _id in self.table_ids:
                data_funcs.update([tables.tables[_id]['db_data_func']])

        if self.chart_ids:
            charts = my_charts.Charts()
            for _id in self.chart_ids:
                data_funcs.update([charts.charts[_id]['db_data_func']])

        if self.info_box_ids:
            info_boxes = my_info_boxes.InfoBoxes()
            for _id in self.info_box_ids:
                data_funcs.update([info_boxes.info_boxes[_id]['db_data_func']])

        # Get all necessary data from the database
        dash_data = {}
        for func in data_funcs:
            dash_data[func] = getattr(lib_db, func)()

        # Create all the tables
        for index, _id in enumerate(self.table_ids):
            table_id = 'table_id_{}'.format(index)
            table_data_func = tables.tables[_id]['db_data_func']

            self.table_list.append(tables.tables[_id]['class'](dash_data[table_data_func],
                                                               table_id=table_id))

        # Create all the charts
        for index, _id in enumerate(self.chart_ids):
            chart_id = 'chart_id_{}'.format(index)
            chart_data_func = charts.charts[_id]['db_data_func']

            chart = charts.charts[_id]['class'](chart_id, dash_data[chart_data_func])
            self.chart_list.append({'id': chart.chart_id,
                                    'room_id': chart.room_id,
                                    'chart_lib': chart.chart_lib,
                                    'chart_json': chart.get_chart_json(),
                                    'chart_html': chart.get_chart_html()
                                    })

        # Create all the info boxes
        for index, _id in enumerate(self.info_box_ids):
            info_box_data_func = info_boxes.info_boxes[_id]['db_data_func']
            box_class = info_boxes.info_boxes[_id]['class'](dash_data[info_box_data_func])
            self.info_box_list.append(box_class.get_complete_config())

        # Creat the chat
        for index, chat in enumerate(self.chats):
            chat_id = 'chat_id_{}'.format(index)
            self.chat_list.append({'id': chat_id,
                                   'room_id': chat['room_id'],
                                   'title': chat['title'],
                                   'max_messages': chat['max_msg']})


class DashboardMaxLength(BaseDashboard):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        # Select the html template
        self.html_template = 'dashboard.html'

        # Select the elements on the dashboard
        self.table_ids = ['dashboard_max_length']
        self.chart_ids = ['max_snake_lengths_box', 'snake_ages_box']
        self.info_box_ids = ['game_runtime_info_box',
                             'longest_snake_info_box',
                             'shortest_snake_info_box',
                             'most_deaths_info_box']
        self.chats = [{'title': 'Snake chat', 'room_id': 'chat_room', 'max_msg': 50}]

        # Create the dashboard elements
        self.create_dashboard()


class DashboardMaxAccSnakeMass(BaseDashboard):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        # Select the html template
        self.html_template = 'dashboard.html'

        # Select the elements on the dashboard
        self.table_ids = ['dashboard_max_acc_mass']
        self.chart_ids = ['snake_mass', 'max_snake_lengths_box']
        self.info_box_ids = ['game_runtime_info_box',
                             'max_acc_snake_mass_info_box',
                             'longest_snake_info_box',
                             'most_deaths_info_box']
        self.chats = [{'title': 'Snake chat', 'room_id': 'chat_room', 'max_msg': 50}]

        # Create the dashboard elements
        self.create_dashboard()


class DashboardMaxAge(BaseDashboard):
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        # Select the html template
        self.html_template = 'dashboard.html'

        # Select the elements on the dashboard
        self.table_ids = ['dashboard_max_age']
        self.chart_ids = ['snake_ages_box', 'max_snake_lengths_box']
        self.info_box_ids = ['game_runtime_info_box',
                             'oldest_snake_info_box',
                             'longest_snake_info_box',
                             'most_deaths_info_box']
        self.chats = [{'title': 'Snake chat', 'room_id': 'chat_room', 'max_msg': 50}]

        # Create the dashboard elements
        self.create_dashboard()
