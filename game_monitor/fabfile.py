##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##


from fabric.api import cd, env, lcd, put, prompt, local, sudo, run, task
from fabric.contrib.files import exists


local_app_dir = './'
local_config_dir = './server_config'

remote_app_name = 'uwars_game_monitor'

remote_app_dir = '/home/microchip/sites'
remote_flask_dir = remote_app_dir + '/' + remote_app_name
remote_temp_dir = remote_app_dir + '/temp'
remote_logs_dir = remote_app_dir + '/uwars_logs'

remote_nginx_dir = '/etc/nginx/sites-available'
remote_supervisor_dir = '/etc/supervisor/conf.d'

nginx_conf_file = 'uwars_nginx_conf'
supervisor_conf_file = 'uwars_supervisord.conf'

env.use_ssh_config = True               # Tell fab to use the ~/.ssh/config file


@task
def localhost():
    """Set the uwars server as remote"""
    env.use_ssh_config = False
    env.user = 'microchip'
    env.hosts = ['localhost']


@task
def remote():
    """Set the uwars server as remote"""
    env.user = 'microchip'
    env.hosts = ['10.191.255.175']


@task
def install_server_packages():
    """ Install required packages."""
    sudo('apt-get update')
    sudo('apt-get install -y python3-venv')         # For virtualenv
    sudo('apt-get install -y python3-dev')          # Required for Fabric3
    sudo('apt-get install -y libssl-dev')           # Required for Fabric3
    sudo('apt-get install -y nginx')
    sudo('apt-get install -y supervisor')

    # sudo('service supervisor restart')
    sudo('systemctl enable supervisor')             # To make sure it is started on boot
    sudo('systemctl start supervisor')


@task
def install_webapp():
    """
    1. Create project directories
    2. Create and activate a virtualenv
    3. Copy Flask files to remote host
    """
    if not exists(remote_app_dir):
        run('mkdir -p ' + remote_app_dir)
    if not exists(remote_logs_dir):
        run('mkdir -p ' + remote_logs_dir)
    if not exists(remote_flask_dir):
        run('mkdir -p ' + remote_flask_dir)
    if not exists(remote_temp_dir):
        run('mkdir -p ' + remote_temp_dir)
    with lcd(local_app_dir):
        # Copy the files for the web site
        with cd(remote_flask_dir):
            put('*', './')

    update_venv()


@task
def update_venv():
    """Update the virutalevn, or create it if necessary
    The requirements file must already have been copied to the remote.
    """
    with cd(remote_app_dir):
        # Create the virtual environment and install the required packages
        if not exists('venv_35/bin/pip'):
            # No venv found, create it and upgrade pip
            run('python3 -m venv venv_35')
            run('venv_35/bin/pip install --upgrade pip -q')

        # Install the required packages
        # TBD - only run --no-cache-dir if the install fails to speed up the install?
        run('venv_35/bin/pip install -r ' + remote_app_name + '/requirements.txt -q --no-cache-dir')


@task
def configure_nginx():
    """Upload a new config file, enable the site and restart nginx

    1. Remove default nginx config file
    2. Create new config file
    3. Setup new symbolic link
    4. Copy local config to remote config
    5. Restart nginx
    """
    if not exists(remote_logs_dir):
        run('mkdir -p ' + remote_logs_dir)

    # Remove the default config
    if exists('/etc/nginx/sites-enabled/default'):
        sudo('rm /etc/nginx/sites-enabled/default')

    # Copy the new config file
    with lcd(local_config_dir):
        with cd(remote_nginx_dir):
            put(nginx_conf_file, './', use_sudo=True)

    # Enable the new config
    if not exists('/etc/nginx/sites-enabled/' + nginx_conf_file):
        sudo('touch /etc/nginx/sites-available/' + nginx_conf_file)
        sudo('ln -s /etc/nginx/sites-available/' + nginx_conf_file +
             ' /etc/nginx/sites-enabled/' + nginx_conf_file)

    sudo('systemctl restart nginx')


@task
def disable_nginx_config():
    """
    1. Remove the nginx config file from sites-enabled
    2. Restart nginx (in case there are other sites running)
    """
    sudo('rm -f /etc/nginx/sites-enabled/' + nginx_conf_file)

    sudo('systemctl restart nginx')


@task
def configure_supervisor():
    """
    1. Create new supervisor config file
    2. Copy local config to remote config
    3. Register new command

    On remote run 'supervisorctl' to monitor running processes
    """

    # if the 'service supervisor restart' wasn't enough, try these
    # sudo systemctl start supervisor
    # sudo systemctl enable supervisor
    # sudo supervisorctl reload

    if not exists(remote_logs_dir):
        run('mkdir -p ' + remote_logs_dir)

    with lcd(local_config_dir):
        with cd(remote_supervisor_dir):
            put(supervisor_conf_file, './', use_sudo=True)
            sudo('systemctl start supervisor')  # Make sure supervisor is started
            sudo('supervisorctl reread')        # Reread the config files
            sudo('supervisorctl update')        # Apply the updates


@task
def disable_supervisor_config():
    """
    1. Remove the supervisor config file
    2. Reread/update supervisor
    """
    with cd(remote_supervisor_dir):
        sudo('rm -f ' + supervisor_conf_file)
        sudo('supervisorctl reread')        # Reread the config files
        sudo('supervisorctl update')        # Apply the updates


@task
def deploy():
    """
    1. Copy new Flask files
    2. Restart gunicorn via supervisor
    """

    if not exists(remote_logs_dir):
        run('mkdir -p ' + remote_logs_dir)

    sudo('supervisorctl stop')

    with lcd(local_app_dir):

        # Delete the web site files and replace with the updated files
        run('rm -rf ' + remote_flask_dir + '/*')
        if not exists(remote_flask_dir):
            run('mkdir -p ' + remote_flask_dir)
        # TBD zip files before copying to remote (setuptools?)
        with cd(remote_flask_dir):
            put('*', './')

    # Install any new requirements
    update_venv()

    # sudo('supervisorctl restart flask_project')
    sudo('supervisorctl restart uwars_game_monitor')


@task
def status():
    """ Is our app live? """
    sudo('supervisorctl status')


@task
def create():
    # install_server_packages()
    install_webapp()
    configure_nginx()
    configure_supervisor()


@task
def disable_webserver():
    """Disable the web site, removing the nginx and supervisor setup"""
    disable_supervisor_config()
    disable_nginx_config()


@task
def restart_server():
    """Enable the web site, copying and enabling the nginx and supervisor setup"""
    configure_nginx()
    configure_supervisor()
    sudo('supervisorctl restart uwars_game_monitor')
