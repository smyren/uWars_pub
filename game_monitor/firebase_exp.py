##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

import os
import sys
import pyrebase
from requests.exceptions import HTTPError
from flask import Flask

import models
import lib_db
import dashboards as my_dashboards

# Creating a temporary Flask app so that we can use the defined models
app = Flask(__name__)
app.config['SECRET_KEY'] = 'app_secret!'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(lib_db.get_database_path())
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_ECHO'] = False       # Set to True to output SQL during debugging

# Register the database in the app
models.db.init_app(app)

# Config created on firebase.google.com
# The serviceAccount json file was generated and downloaded from Firebase Admin SDK. This file must
# be copied to the web server manually. This also reduces the risk of other users than the server
# uploading data to the game database by accident.
# The rest of the values are copied from Web setup on the Authentication page (same as the js setup)
config = {
    "apiKey": "AIzaSyDzpWkEeeJALBkthSlgSrxz1SK5G1JLeD8",
    "authDomain": "uwars-b2508.firebaseapp.com",
    "databaseURL": "https://uwars-b2508.firebaseio.com",
    "storageBucket": "uwars-b2508.appspot.com",
    "serviceAccount": "~/uwars_fb_auth/uwars-b2508-firebase-adminsdk-eazc4-f473e570e4.json"
}

# Expand the home directory path (~) if any and verify that the serviceAccount file actually exists
config['serviceAccount'] = os.path.expanduser(config['serviceAccount'])
if not os.path.isfile(config['serviceAccount']):
    print("Error - No service account file found, can't connect to the remote server")
    sys.exit(1)

firebase = pyrebase.initialize_app(config)

# Get a reference to the auth service (for using password login instead of the serviceAccount file)
# auth = firebase.auth()
# Authenticate the user
# email = "uwars_fb@gmail.com"
# password = "insert_password_here"
# user = auth.sign_in_with_email_and_password(email, password)
# Refresh token if necessary (expires after 1 hour)
# user = auth.refresh(user['refreshToken'])

# Get a reference to the database service
fb_db = firebase.database()

# Prepare the data containers
scores = {}
table_cfg = {}
fb_data = {}

general = {}
general['timestamp'] = {'.sv': 'timestamp'}     # Automatically create a timestamp in firebase
general['interval_s'] = 30 * 60

# Get data from the database
# NB - all database access must be done within the app.app_context() block
with app.app_context():
    # Get the name of the dashboard selected by the game host
    dash_select = lib_db.get_dashboard_selected_by_game_host()

    # Get the list of available dashboards, and check if we have a valid selection
    dashboards = my_dashboards.Dashboards()
    if not dashboards.is_valid_selector(dash_select):
        # The selected dashboard was not a valid one, using the default dashboard instead
        dash_select = dashboards.default_dashboard

    # Create a instance of the dashboard (only using the main table for now)
    # The data for the tables/charts is also read from the database here
    dash = dashboards.dashboards[dash_select]['class']()

    # Get general game data (running this right after the table data is retrieved to keep the data
    # as in-sync as possible)
    game_data = lib_db.get_game_run_time_info()

    # Get general game data
    try:
        general['game_start_time'] = game_data['game_start_time']
        general['rounds_played'] = game_data['rounds_played']
    except Exception as e:
        general['game_start_time'] = 0
        general['rounds_played'] = -1

    # Delete the snake icon column from the table (icons are not uploaded to firebase, and the
    # url_for() function in the column formatter will fail when run outside of the main app)
    del dash.table_list[0].table._cols['snake_icon']

    # Create the data structure for the column names (col index as the key and the column name
    # as the value, the col index must match the indexes for the data table)
    table_cfg['columns'] = {col_index: col_val.name for col_index, col_val in
                            enumerate(dash.table_list[0].table._cols.values())}

    # Get the table data, assuming that the first table in the list is the main table
    data = dash.table_list[0].table.get_data_json()

    for player_id, player_data in enumerate(data['data']):
        fb_data[player_id] = {}
        for col_index, col_data in enumerate(player_data):
            fb_data[player_id][col_index] = col_data

    scores = {'general': general, 'table_cfg': table_cfg, 'players': fb_data}

# Upload data to Firebase
try:
    # Add data (using serviceAccount)
    test = fb_db.child("scores").set(scores)

    # Add data using user/password authentication
    # fb_db.child("scores").set(scores, token=user['idToken'])
except HTTPError as e:
    print("Error uploading data")
    print(e.strerror)
    print(e.errno)
