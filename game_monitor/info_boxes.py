##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

import collections
import datetime
import cgi


class InfoBoxes:
    """Class for grouping info boxes"""
    def __init__(self):
        self.info_boxes = collections.OrderedDict()
        self.info_boxes['game_runtime_info_box'] = {'class': GameRunTimeBox,
                                                    'db_data_func': 'get_game_run_time_info'}
        self.info_boxes['longest_snake_info_box'] = {'class': LongestSnakeBox,
                                                     'db_data_func': 'get_score_totals'}
        self.info_boxes['shortest_snake_info_box'] = {'class': ShortestSnakeBox,
                                                      'db_data_func': 'get_score_totals'}
        self.info_boxes['most_deaths_info_box'] = {'class': MostDeathsBox,
                                                   'db_data_func': 'get_score_totals'}
        self.info_boxes['oldest_snake_info_box'] = {'class': OldestSnakeBox,
                                                    'db_data_func': 'get_score_totals'}
        self.info_boxes['max_acc_snake_mass_info_box'] = {'class': MaxAccSnakeMassBox,
                                                          'db_data_func': 'get_score_totals'}

    def get_db_data_funcs(self):
        func_list = []
        for box_id, box_data in self.info_boxes.items():
            func_list.append(box_data['db_data_func'])

        return func_list


class TemplateBox:
    """Base class for info boxes

    NB - all data strings from the database must be escaped before inserted in the box fields,
    ie use cgi.escape(value) whenever parsing and storing the data values in update_box_data

    Info box parameters
    title -- the title of the info box

    main_text -- the main text/main parameter to display

    sub_text -- smaller text shown below main_text for extra description of the parameter

    icon_class -- by default selected from http://ionicons.com/, use the complete class needed for
    the icon library, eg 'ion ion-trophy'

    color -- selected from the main colors of AdminLTE, see 'colors' in static/dist/js/app.js.
    Use 'bg' + color name, eg 'bg-green'.
    Available colors:
    eg lightBlue, red, green, aqua, yellow, blue, navy, teal, olive, lime, orange, fuchsia, purple,
    maroon, black, gray,

    room_id -- the name used by the sockets when transmitting data to the web clients. To disable
    live data set to None, else leave at the default value

    """
    def __init__(self, raw_data=None):
        # Store the data
        self.raw_data = raw_data

        # Create a room name from the class name
        # To disable live data in a child class set room_id = None
        self.room_id = self.__class__.__name__ + '_room'

        # Set default values, override these in the child classes as needed
        self.title = ''
        self.main_text = ''
        self.sub_text = ''
        self.icon_class = 'io ion-help'
        self.color = 'bg_aqua'
        self.prog_bar_width = None

    def live_data_supported(self):
        """Returns True if the table supports live data updates, else False

        Live data is enabled by default, to disable it in a child class just set room_id = None
        """
        if self.room_id:
            return True
        else:
            return False

    def update_box_data(self):
        """Update the box parameters based on the raw data

        By default this method does nothing, ie only static data will be displayed. Override as
        necessary to write new values to the box parameters, including any parsing/formatting.

        eg:
            self.main_text = cgi.escape('{0}'.format(self.raw_data['rounds'}))
        """
        pass

    def get_dynamic_data(self):
        """Return the dynamic data for the box, this can return anything from nothing to the
        complete box configuration. Any parameter not included will remain at the previous
        value.

        Returning nothing by default, this method is usually overridden in the child class
        by a method returning the parameters that should be updated on the web page

        eg:
            new_data = {'main_text': self.main_text}
            return new_data
        """
        return {}

    def get_complete_config(self):
        """Return the complete box configuration, including the data"""
        box_config = {}
        box_config['room_id'] = self.room_id
        box_config['title'] = self.title
        box_config['icon_class'] = self.icon_class
        box_config['color'] = self.color
        box_config['main_text'] = self.main_text
        box_config['sub_text'] = self.sub_text
        box_config['prog_bar_width'] = self.prog_bar_width

        return box_config


class GameRunTimeBox(TemplateBox):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.title = 'Number of rounds played'
        self.icon_class = 'io ion-load-c'
        self.color = 'bg-aqua'
        self.main_text = ''         # Number of played rounds from the database
        self.sub_text = ''          # Game start time from database

        # Update the box data parameters with the new input data
        self.update_box_data()

    def update_box_data(self):
        """Extract and format the updated data for the info box"""
        try:
            # Get the data value
            total_rounds = int(self.raw_data['rounds_played'])
            raw_start_time = self.raw_data['game_start_time']
            # start_time = start_time.split('.')[0]
            start_time_obj = datetime.datetime.strptime(raw_start_time, "%Y%m%dT%H%M%S.%f")

            start_time_str = start_time_obj.strftime("%H:%M, %d. %b")

            # Format the data
            self.main_text = '{0:,}'.format(total_rounds).replace(',', ' ')
            self.sub_text = 'Game started at {0}'.format(start_time_str)
        except Exception as e:
            # Error extracting the data, set default values
            print(str(e))
            self.main_text = ''
            self.sub_text = ''

    def get_dynamic_data(self):
        """Return the fields that is updated dynamically"""
        return {'main_text': self.main_text,
                'sub_text': self.sub_text}


class LongestSnakeBox(TemplateBox):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.title = 'Longest snake'
        self.icon_class = 'io ion-trophy'
        self.color = 'bg-green'
        self.main_text = ''     # Max snake length from the database
        self.sub_text = ''      # Name of longest snake, from the database

        # Update the box data parameters with the new input data
        self.update_box_data()

    def update_box_data(self):
        """Extract and format the updated data for the info box"""
        try:
            # Group the snakes by their max length
            max_lengths = {}
            for row in self.raw_data:
                max_len = row['max_length_max']

                if max_len in max_lengths.keys():
                    max_lengths[max_len].append(row['name'])
                else:
                    max_lengths[max_len] = [row['name']]

            # Find the max length
            max_len = max(max_lengths.keys())

            # Format the data
            self.main_text = '{0:,}'.format(max_len).replace(',', ' ')
            self.sub_text = cgi.escape(' / '.join(max_lengths[max_len]))
        except Exception as e:
            # Error extracting the data, set default values
            print(str(e))
            self.main_text = ''
            self.sub_text = ''

    def get_dynamic_data(self):
        """Return the fields that is updated dynamically"""
        return {'main_text': self.main_text,
                'sub_text': self.sub_text}


class ShortestSnakeBox(TemplateBox):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.title = 'Shortest snake'
        self.icon_class = 'io ion-arrow-graph-down-right'
        self.color = 'bg-orange'
        self.main_text = ''     # Lowest max length from the database
        self.sub_text = ''      # Name of the snake, from the database

        # Update the box data parameters with the new input data
        self.update_box_data()

    def update_box_data(self):
        """Extract and format the updated data for the info box"""
        try:
            # Group the snakes by their max length
            max_lengths = {}
            for row in self.raw_data:
                max_len = row['max_length_max']

                if max_len != 0:
                    if max_len in max_lengths.keys():
                        max_lengths[max_len].append(row['name'])
                    else:
                        max_lengths[max_len] = [row['name']]

            # Find the lowest max length
            min_len = min(max_lengths.keys())

            # Format the data
            self.main_text = '{0:,}'.format(min_len).replace(',', ' ')
            self.sub_text = cgi.escape(' / '.join(max_lengths[min_len]))
        except Exception as e:
            # Error extracting the data, set default values
            print(str(e))
            self.main_text = ''
            self.sub_text = ''

    def get_dynamic_data(self):
        """Return the fields that is updated dynamically"""
        return {'main_text': self.main_text,
                'sub_text': self.sub_text}


class MostDeathsBox(TemplateBox):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.title = 'Most deaths'
        self.icon_class = 'io ion-medkit'
        self.color = 'bg-orange'
        self.main_text = ''     # Max number of deaths from the database
        self.sub_text = ''      # Name of the snake, from the database

        # Update the box data parameters with the new input data
        self.update_box_data()

    def update_box_data(self):
        """Extract and format the updated data for the info box"""
        try:
            # Group the snakes by the number of deaths
            deaths = {}
            for row in self.raw_data:
                snake_deaths = row['deaths']

                if snake_deaths in deaths.keys():
                    deaths[snake_deaths].append(row['name'])
                else:
                    deaths[snake_deaths] = [row['name']]

            # Find the max number of deaths
            max_deaths = max(deaths.keys())

            # Format the data
            self.main_text = '{0:,}'.format(max_deaths).replace(',', ' ')
            self.sub_text = cgi.escape(' / '.join(deaths[max_deaths]))
        except Exception as e:
            # Error extracting the data, set default values
            print(str(e))
            self.main_text = ''
            self.sub_text = ''

    def get_dynamic_data(self):
        """Return the fields that is updated dynamically"""
        return {'main_text': self.main_text,
                'sub_text': self.sub_text}


class OldestSnakeBox(TemplateBox):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.title = 'Oldest snake'
        self.icon_class = 'io ion-trophy'
        self.color = 'bg-green'
        self.main_text = ''     # Max snake age from the database
        self.sub_text = ''      # Name of the snake, from the database

        # Update the box data parameters with the new input data
        self.update_box_data()

    def update_box_data(self):
        """Extract and format the updated data for the info box"""
        try:
            # Group the snakes by their max age
            ages = {}
            for row in self.raw_data:
                max_age = row['alive_time_max']

                if max_age in ages.keys():
                    ages[max_age].append(row['name'])
                else:
                    ages[max_age] = [row['name']]

            # Find the max age
            max_age = max(ages.keys())

            # Format the data
            self.main_text = '{0:,}'.format(max_age).replace(',', ' ')
            self.sub_text = cgi.escape(' / '.join(ages[max_age]))
        except Exception as e:
            # Error extracting the data, set default values
            print(str(e))
            self.main_text = ''
            self.sub_text = ''

    def get_dynamic_data(self):
        """Return the fields that is updated dynamically"""
        return {'main_text': self.main_text,
                'sub_text': self.sub_text}


class MaxAccSnakeMassBox(TemplateBox):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.title = 'Highest accumulated snake mass'
        self.icon_class = 'io ion-trophy'
        self.color = 'bg-green'
        self.main_text = ''     # Max snake length from the database
        self.sub_text = ''      # Name of longest snake, from the database

        # Update the box data parameters with the new input data
        self.update_box_data()

    def update_box_data(self):
        """Extract and format the updated data for the info box"""
        try:
            # Group the snakes by their max length
            max_acc_vals = {}
            for row in self.raw_data:
                max_acc = row['acc_snake_mass']

                if max_acc in max_acc_vals.keys():
                    max_acc_vals[max_acc].append(row['name'])
                else:
                    max_acc_vals[max_acc] = [row['name']]

            # Find the max length
            max_acc = max(max_acc_vals.keys())

            # Format the data
            self.main_text = '{0:,}'.format(max_acc).replace(',', ' ')
            self.sub_text = cgi.escape(' / '.join(max_acc_vals[max_acc]))
        except Exception as e:
            # Error extracting the data, set default values
            print(str(e))
            self.main_text = ''
            self.sub_text = ''

    def get_dynamic_data(self):
        """Return the fields that is updated dynamically"""
        return {'main_text': self.main_text,
                'sub_text': self.sub_text}
