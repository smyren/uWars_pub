##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

import os
import sys
import collections

import models
from sqlalchemy.sql import func


# Set with data getter functions that are database heavy and may be slow. This set can be used
# to exclude these functions from the ones run in the main background thread.
# Using a set to make it easier to check for difference and intersections
slow_data_getter_funcs = {'get_death_pos_map',
                          'get_visit_pos_map',
                          'get_eol_lengths_per_life',
                          'get_max_lengths_per_life',
                          'get_ages_per_life'}


def get_database_path():
    """Get the path for the database file, code and setttings copied from the game_host"""

    # Settings from game_monitor/Ruleset.py
    file_name = 'game_data.db'
    main_folder = 'uwars_live_data'        # The folder where 'file_name' is created
    base_path = {'linux': os.path.join(os.sep, 'dev', 'shm'),
                 'windows': os.path.join('U:', os.sep)}
    static_path = {'linux': os.path.join('~'),
                   'windows': os.path.join('~')}

    # From Data_Export.py
    # Setup the dict key for accessing the paths for the current system
    if sys.platform.startswith('win'):
        system_key = 'windows'
    else:
        system_key = 'linux'

    # Get the path for the live game data (expanding path in case it includes '~')
    base_path = os.path.expanduser(base_path.get(system_key, ''))
    main_folder = main_folder

    # Get the path for the game data copies (expanding path in case it includes '~')
    static_path = os.path.expanduser(static_path.get(system_key, ''))

    if not os.path.isdir(base_path):
        # The base_folder doesn't exist, trying the static folder instead
        if not os.path.isdir(static_path):
            print('No valid path found, no data will be exported')
            return ''
        else:
            # Live game data will be stored in the static data folder
            base_path = static_path

    path = os.path.join(base_path, main_folder, file_name)

    return path


def get_snake_head_img_file_path(node_id):
    """Get the path to the snake head image file"""
    try:
        player = models.db.session.query(models.Player.snake_head_img).\
            filter_by(node_id=node_id).one()
        img_file_path = player.snake_head_img
    except Exception as e:
        # No file path found in the database
        # print(str(e))
        img_file_path = ''

    return img_file_path


def get_snake_icon_img_file_path(node_id):
    """Get the path to the snake icon image file"""
    try:
        player = models.db.session.query(models.Player.snake_icon_img).\
            filter_by(node_id=node_id).one()
        img_file_path = player.snake_icon_img
    except Exception as e:
        # No file path found in the database
        # print(str(e))
        img_file_path = ''

    return img_file_path


def get_snake_max_length_img_path():
    """Get path to max length image"""
    try:
        file = models.Game.query.filter_by(key='max_length_img').one()
        file_path = file.value
    except Exception as e:
        # No file path found in the database
        # print(str(e))
        file_path = ''

    return file_path


def get_winners_file_path():
    """Get path to the winners file"""
    try:
        file = models.Game.query.filter_by(key='winners_file').one()
        file_path = file.value
    except Exception as e:
        # No file path found in the database
        # print(str(e))
        file_path = ''

    return file_path


def get_board_size():
    """Get the size of the current board"""
    board_size = {'x': 0, 'y': 0}
    try:
        board_params = models.Game.query.\
            filter(models.Game.key.in_(['board_size_x', 'board_size_y'])).\
            order_by(models.Game.key.asc()).all()

        if len(board_params) != 2:
            raise

        #
        board_size['x'] = int(board_params[0].value)
        board_size['y'] = int(board_params[1].value)
    except Exception as e:
        # No file path found in the database
        print(str(e))
        board_size = {'x': 0, 'y': 0}

    return board_size


def get_game_run_time_info():
    """Get the info on when the game was started and how long it has been running"""
    game_data = {}
    try:
        game_params = models.Game.query.\
            filter(models.Game.key.in_(['game_start_time', 'rounds_played'])).\
            order_by(models.Game.key.asc()).all()

        if len(game_params) != 2:
            raise

        #
        game_data['game_start_time'] = game_params[0].value
        game_data['rounds_played'] = game_params[1].value
    except Exception as e:
        # No file path found in the database
        print(str(e))
        game_data = {'game_start_time': '', 'rounds_played': 0}

    return game_data


def get_dashboard_selected_by_game_host():
    """Get the name of the preferred dashboard selected by the host"""
    return get_game_param('dashboard')


def get_game_name():
    """Get the game name"""
    return get_game_param('game_name')


def get_game_score_description():
    """Get the descroption for the game score rules"""
    return get_game_param('game_score_desc')


def get_board_description():
    """Get the description for the board"""
    return get_game_param('board_desc')


def get_game_param(param_name, default=''):
    """Get game parameter"""
    try:
        param = models.Game.query.filter_by(key=param_name).one()
        return param.value
    except Exception as e:
        # No file path found in the database
        # print(str(e))
        return default


def get_players_info():
    """Get static info all players"""
    data = []

    try:
        players = models.Player.query.order_by(models.Player.name.asc()).all()
        for player in players:
            data.append(player.__dict__)
    except Exception as e:
        print(str(e))

    return data


def get_player_id_and_names():
    """Get a list of players names and node ids"""
    data = []

    try:
        players = models.Player.query.order_by(models.Player.name.asc()).\
                values(models.Player.node_id, models.Player.name, models.Player.color1,
                       models.Player.color2)
        for node_id, name, color1, color2 in players:
            data.append({'node_id': node_id, 'name': name, 'color1': color1, 'color2': color2})
    except Exception as e:
        print(str(e))

    return data


def get_score_totals():
    """Get the score data from the database"""
    data = []
    try:
        players = models.db.session.query(models.Player, models.Score).\
            join(models.Score).order_by(models.Player.name.asc())
        for player, score in players:
            score_data = score.__dict__
            score_data['node_id'] = player.node_id
            score_data['name'] = player.name
            score_data['color1'] = player.color1
            score_data['color2'] = player.color2
            data.append(score_data)
    except Exception as e:
        print(str(e))

    return data


def get_death_list_by_killed_snake():
    """Get a list of how many times each snake has killed each of the other snakes"""

    deaths = []
    data = collections.OrderedDict()
    try:
        # Get complete list of players (ids, names and colors)
        players = get_player_id_and_names()

        # Create dict will all killer/death combinations and initialize the kill count to 0
        for snake in players:
            data[snake['node_id']] = collections.OrderedDict()
            data[snake['node_id']]['name'] = snake['name']
            data[snake['node_id']]['color1'] = snake['color1']
            data[snake['node_id']]['color2'] = snake['color2']
            data[snake['node_id']]['deaths'] = collections.OrderedDict()

            # Add the board as killer
            data[snake['node_id']]['deaths']['Board'] = {
                'name': 'Board',
                'color1': '#000000',
                'color2': '#FFFFFF',
                'count': 0
            }

            # Add each snake
            for killer_snake in players:
                data[snake['node_id']]['deaths'][killer_snake['node_id']] = {
                    'name': killer_snake['name'],
                    'color1': killer_snake['color1'],
                    'color2': killer_snake['color2'],
                    'count': 0
                }

        deaths = models.Death.query.all()
    except Exception as e:
        deaths = []

    # Add the count values for each snake combination
    for death in deaths:
        try:
            data[death.node_id]['deaths'][death.killer_id]['count'] = death.count
        except KeyError as e:
            # Possible missing node id in the data dict (eg node added after calling
            # get_players, just skipping this node for now, it will be included the next
            # time the data is extracted)
            print("Warning - Key error on death/kill table: {}".format(str(e)))

    return data


def get_board_pos_map(map_type, node_id=''):
    """Get the data for a board map"""
    if map_type == 'visits':
        return get_board_map('visits', node_id, 0)
    elif map_type == 'deaths':
        return get_board_map('deaths', node_id, 0)
    else:
        return []


def get_death_pos_map(node_id=''):
    """Get the map of the death count on each coordinate on the board"""
    return get_board_map('deaths', node_id, 0)


def get_visit_pos_map(node_id=''):
    """Get the map of the visit count on each coordinate on the board"""
    return get_board_map('visits', node_id, 0)


def get_board_map(board_type, node_id='', fill_value=0):
    """Get the map of count values for each coordinate of the board

    board_type -- selects the column to use as the source for the count values
    node_id -- If specified only fetch data for this snake node
    fill_value -- the database may not contain data for all coordinates, these coordinates will
        be set to fill_value
    """

    # Prepare the data output
    data = {'z': [], 'sum': 0, 'max': 0, 'size_x': 0, 'size_y': 0}

    try:
        # Build the query
        board_data = models.BoardMap.query.join(models.Player)
        if node_id:
            # Add filtering on node id, else the sum of all will be used
            board_data = board_data.filter_by(node_id=node_id)
        board_data = board_data.group_by(models.BoardMap.x, models.BoardMap.y)

        # Set up the count parameter to sum up
        if board_type == 'visits':
            sum_param = func.sum(models.BoardMap.visit_count)
        elif board_type == 'deaths':
            sum_param = func.sum(models.BoardMap.death_count)
        else:
            print("Unknown board type: {}".format(board_type))
            raise
        board_data = board_data.values(models.BoardMap.x, models.BoardMap.y, sum_param)


        # Get the board size
        board = get_board_size()

        if board['x'] != 0 and board['y'] != 0:
            data['size_x'] = board['x']
            data['size_y'] = board['y']

            # Create a array with the same size as the board and initialize to the fill value
            data['z'] = [[fill_value for col in range(board['x'])] for row in range(board['y'])]

            # Process the database data adding the count values at the correct coordinates, also
            # calculating a few key parameters to avoid having to loop the entire list again
            # for row in raw_data:
            for x, y, count in board_data:
                # print(x, y, count)
                data['z'][y][x] = count
                data['sum'] += count
                if data['max'] < count:
                    data['max'] = count
    except Exception as e:
        print(str(e))
        data['z'] = []

    return data


def get_last_messages(min_round_num):
    """Return the chat messages since the given round"""

    data = []
    try:
        messages = models.db.session.query(models.Chat, models.Player).join(models.Player).\
            filter(models.Chat.round >= min_round_num).order_by(models.Chat.round.asc()).\
            values(models.Player.node_id, models.Player.name, models.Chat.round, models.Chat.msg)

        # Filtering out repeated messages to limit the output to screen a little
        current_node = ''
        current_msg = ''
        for node_id, name, round_num, msg in messages:
            # Don't add the message if it was already sent by the same node
            if not (current_node == node_id and current_msg == msg):
                data.append({'node_id': node_id,
                             'name': name,
                             'round': round_num,
                             'msg': msg})

            # Update the current values
            current_node = node_id
            current_msg = msg
    except Exception as e:
        print(str(e))
        data = []

    return data


def get_eol_lengths_per_life():
    data = collections.OrderedDict()
    try:
        eol_lengths = models.db.session.query(models.LifeStat.start_round,
                                              models.LifeStat.eol_length,
                                              models.Player.node_id,
                                              models.Player.name,
                                              models.Player.color1,
                                              models.Player.color2).join(models.Player).\
            order_by(models.LifeStat.start_round.asc()).all()
            # filter(models.Player.node_id == node_id)

        for start_round, eol_length, node_id, name, color1, color2 in eol_lengths:
            if node_id in data:
                data[node_id]['start_rounds'].append(start_round)
                data[node_id]['eol_lengths'].append(eol_length)
            else:
                data[node_id] = {'node_id': node_id,
                                 'name': name,
                                 'color1': color1,
                                 'color2': color2,
                                 'start_rounds': [],
                                 'eol_lengths': []}

    except Exception as e:
        print(str(e))
        data = {}

    return data


def get_max_lengths_per_life():
    data = collections.OrderedDict()
    try:
        max_lengths = models.db.session.query(models.LifeStat.start_round,
                                              models.LifeStat.max_length,
                                              models.Player.node_id,
                                              models.Player.name,
                                              models.Player.color1,
                                              models.Player.color2).join(models.Player).\
            order_by(models.LifeStat.start_round.asc()).all()
            # filter(models.Player.node_id == node_id)

        for start_round, max_length, node_id, name, color1, color2 in max_lengths:
            if node_id in data:
                data[node_id]['start_rounds'].append(start_round)
                data[node_id]['max_lengths'].append(max_length)
            else:
                data[node_id] = {'node_id': node_id,
                                 'name': name,
                                 'color1': color1,
                                 'color2': color2,
                                 'start_rounds': [],
                                 'max_lengths': []}

    except Exception as e:
        print(str(e))
        data = {}

    return data


def get_ages_per_life():
    data = collections.OrderedDict()
    try:
        ages = models.db.session.query(models.LifeStat.start_round,
                                       models.LifeStat.age,
                                       models.Player.node_id,
                                       models.Player.name,
                                       models.Player.color1,
                                       models.Player.color2).join(models.Player).\
            order_by(models.LifeStat.start_round.asc()).all()
            # filter(models.Player.node_id == node_id)

        for start_round, age, node_id, name, color1, color2 in ages:
            if node_id in data:
                data[node_id]['start_rounds'].append(start_round)
                data[node_id]['ages'].append(age)
            else:
                data[node_id] = {'node_id': node_id,
                                 'name': name,
                                 'color1': color1,
                                 'color2': color2,
                                 'start_rounds': [],
                                 'ages': []}

    except Exception as e:
        print(str(e))
        data = {}

    return data
