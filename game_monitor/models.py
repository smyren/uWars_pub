##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##


from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Player(db.Model):
    """Table of all registered snake nodes in the game

    Contains all static data for each node.
    """
    __tablename__ = 'players'

    id = db.Column(db.Integer, primary_key=True)
    node_id = db.Column(db.String(), unique=True)
    name = db.Column(db.String(), default='NONAME')
    snake_head_img = db.Column(db.String())
    snake_icon_img = db.Column(db.String())
    color1 = db.Column(db.String())
    color2 = db.Column(db.String())
    owner = db.Column(db.String(), default='')
    info = db.Column(db.String(), default='')
    score = db.relationship("Score", uselist=False, back_populates="players")
    chat = db.relationship("Chat", back_populates="players")
    # deaths = db.relationship("Chat", back_populates="players")
    board_map = db.relationship("BoardMap", back_populates="players")
    life_stats = db.relationship("LifeStat", back_populates="players")


class Score(db.Model):
    """Table for the game score."""
    __tablename__ = 'scores'
    id = db.Column(db.Integer, primary_key=True)

    #
    status = db.Column(db.String())
    length_current = db.Column(db.Integer, default=0)
    max_length_current = db.Column(db.Integer, default=0)
    alive_time_current = db.Column(db.Integer, default=0)
    rounds = db.Column(db.Integer, default=0)
    rank = db.Column(db.Integer, default=0)
    rank_score = db.Column(db.Integer, default=0)
    last_error_msg = db.Column(db.String(), default='')

    move_forwards = db.Column(db.Integer, default=0)
    move_lefts = db.Column(db.Integer, default=0)
    move_rights = db.Column(db.Integer, default=0)
    move_fast_forwards = db.Column(db.Integer, default=0)

    kills = db.Column(db.Integer, default=0)
    food = db.Column(db.Integer, default=0)

    #
    eol_length_max = db.Column(db.Integer, default=0)
    eol_length_quartile3 = db.Column(db.Float(), default=0.0)
    eol_length_median = db.Column(db.Float(), default=0.0)
    eol_length_quartile1 = db.Column(db.Float(), default=0.0)
    eol_length_min = db.Column(db.Integer, default=0)
    eol_length_sum = db.Column(db.Integer, default=0)
    eol_length_lastn_max = db.Column(db.Integer, default=0)
    eol_length_lastn_quartile3 = db.Column(db.Float(), default=0.0)
    eol_length_lastn_median = db.Column(db.Float(), default=0.0)
    eol_length_lastn_quartile1 = db.Column(db.Float(), default=0.0)
    eol_length_lastn_min = db.Column(db.Integer, default=0)
    eol_length_lastn_sum = db.Column(db.Integer, default=0)
    acc_snake_mass = db.Column(db.Integer, default=0)
    max_length_max = db.Column(db.Integer, default=0)
    max_length_quartile3 = db.Column(db.Float(), default=0.0)
    max_length_median = db.Column(db.Float(), default=0.0)
    max_length_quartile1 = db.Column(db.Float(), default=0.0)
    max_length_min = db.Column(db.Integer, default=0)
    max_length_sum = db.Column(db.Integer, default=0)
    alive_time_max = db.Column(db.Integer, default=0)
    alive_time_quartile3 = db.Column(db.Float(), default=0.0)
    alive_time_median = db.Column(db.Float(), default=0.0)
    alive_time_quartile1 = db.Column(db.Float(), default=0.0)
    alive_time_min = db.Column(db.Integer, default=0)
    alive_time_sum = db.Column(db.Integer, default=0)
    alive_time_lastn_max = db.Column(db.Integer, default=0)
    alive_time_lastn_quartile3 = db.Column(db.Float(), default=0.0)
    alive_time_lastn_median = db.Column(db.Float(), default=0.0)
    alive_time_lastn_quartile1 = db.Column(db.Float(), default=0.0)
    alive_time_lastn_min = db.Column(db.Integer, default=0)
    alive_time_lastn_sum = db.Column(db.Integer, default=0)
    response_time_max = db.Column(db.Float(), default=0.0)
    response_time_min = db.Column(db.Float(), default=0.0)
    response_time_avg = db.Column(db.Float(), default=0.0)
    response_time_sum_valid = db.Column(db.Integer, default=0)
    response_time_num_valid = db.Column(db.Integer, default=0)
    response_time_num_missed = db.Column(db.Integer, default=0)
    deaths = db.Column(db.Integer, default=0)

    #? board_x = db.Column(db.Integer)
    #? board_y = db.Column(db.Integer)
    #? total_rounds = db.Column(db.Integer)

    player_id = db.Column(db.Integer, db.ForeignKey('players.id'))
    players = db.relationship("Player", back_populates="score")


class Game(db.Model):
    """Table for common game parameters"""
    __tablename__ = 'game'

    # id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.String(), primary_key=True)
    value = db.Column(db.String())


class Chat(db.Model):
    """Table for chat messages.

    This table is updated for every move/tick in the game.
    """
    __tablename__ = 'chat'

    id = db.Column(db.Integer, primary_key=True)
    round = db.Column(db.Integer)
    msg = db.Column(db.String())

    player_id = db.Column(db.Integer, db.ForeignKey('players.id'))
    players = db.relationship("Player", back_populates="chat")


class Death(db.Model):
    __tablename__ = 'deaths'

    # id = db.Column(db.Integer, primary_key=True)
    node_id = db.Column(db.String(), primary_key=True)
    killer_id = db.Column(db.String(), primary_key=True)
    count = db.Column(db.Integer)

    # Self referencing, many to many?
    # player_id = db.Column(db.Integer, db.ForeignKey('players.id'))
    # players = db.relationship("Player", back_populates="deaths")
    # killer_id = db.Column(db.Integer, db.ForeignKey('players.id'))
    # killer_snake = db.relationship("Player", back_populates="deaths")


class BoardMap(db.Model):
    """Table of all coordinates each snake has visited or died on"""
    __tablename__ = 'board_map'

    x = db.Column(db.Integer, primary_key=True)
    y = db.Column(db.Integer, primary_key=True)
    death_count = db.Column(db.Integer, default=0)
    visit_count = db.Column(db.Integer, default=0)

    player_id = db.Column(db.Integer, db.ForeignKey('players.id'), primary_key=True)
    players = db.relationship("Player", back_populates="board_map")


class LifeStat(db.Model):
    """Table for logging stats pr snake life"""
    __tablename__ = 'life_stats'

    id = db.Column(db.Integer, primary_key=True)
    start_round = db.Column(db.Integer, default=0)
    eol_length = db.Column(db.Integer, default=0)
    max_length = db.Column(db.Integer, default=0)
    age = db.Column(db.Integer, default=0)

    player_id = db.Column(db.Integer, db.ForeignKey('players.id'))
    players = db.relationship("Player", back_populates="life_stats")
