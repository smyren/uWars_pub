##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

from flask import url_for

import dashboards as my_dashboards
import charts as my_charts
import tables as my_tables


class SidebarMenu:
    def __init__(self, active_page=None):
        """Create the Sidebar menu for the web page

        The menu supports two levels, a main menu level and a optional sub level for each main
        item, eg
            item0
            item1
                subitem1.0
                subitem1.1
            etc

        NB - the function names used inside the url_for() calls must match the ones defined for
        respective routes in app.py. So if the names in app.py is updated they must also be
        updated here.

        active_page -- The url for the currently selected page. This is used for highlighting
        this page in the generated menu
        """
        self.active_page = active_page
        self.menu = []
        self.default_sub_menu_icon = 'fa fa-circle-o'

        # Prepare the data for poplating the sub menus
        dashboards = my_dashboards.Dashboards()
        charts = my_charts.Charts()
        board_maps = my_charts.BoardMapCharts()
        tables = my_tables.ScoreTables()

        # Prepare the main sidebar menu, the menu items will appear in the same order as they
        # are added here.

        self.add_menu_item('Home', 'fa fa-home', url=url_for('dashboard'))

        self.add_menu_item('Dashboards', 'fa fa-dashboard')
        for dash_id, dash in dashboards.dashboards.items():
            self.add_sub_menu_item(dash['menu_title'],
                                   url=url_for('dashboards', dash_select=dash_id))

        self.add_menu_item('Game info', 'fa fa-info-circle', url=url_for('show_game_info_table'))

        self.add_menu_item('Charts', 'fa fa-line-chart')
        for chart_id, chart in charts.charts.items():
            self.add_sub_menu_item(chart['title'], url=url_for('charts', chart_select=chart_id))

        self.add_menu_item('Board maps', 'fa fa-map')
        for chart_id, chart in board_maps.charts.items():
            self.add_sub_menu_item(chart['title'], url=url_for('board_maps', chart_select=chart_id))

        self.add_menu_item('Tables', 'fa fa-table')
        for table_id, table in tables.tables.items():
            self.add_sub_menu_item(table['title'], url=url_for('tables', table_select=table_id))

        self.add_menu_item('Screenshots', 'fa fa-camera')
        self.add_sub_menu_item('Max snake length', url=url_for('show_screenshot'))

        self.add_menu_item('Snake chat', 'fa fa-comments', url=url_for('snake_chat'))
        self.add_menu_item('Players', 'fa fa-users', url=url_for('show_player_table'))
        self.add_menu_item('Game winners', 'fa fa-trophy', url=url_for('winners_table'))

    def add_menu_item(self, title, icon, url='#'):
        """Append a menu item to the list

        title -- The name shown for this menu item
        icon -- The icon for the menu item. For a collapsed sidebar only this icon is shown
        url -- The url for the page this menu item links to. This value is ignored if any sub menu
        items are added.
        """
        is_active = True if self.active_page == url else False
        self.menu.append({'active': is_active,
                          'title': title,
                          'icon': icon,
                          'url': url,
                          'sub_menu': []})

    def add_sub_menu_item(self, title, icon=None, url='#'):
        """Append a sub menu item to the last main menu item in the list

        title -- The name shown for this sub menu item
        icon -- The icon for the sub menu item. The default icon is used if not specified
        url -- The url for the page this sub menu item links to
        """
        if icon is None:
            # Use the default sub menu icon
            icon = self.default_sub_menu_icon

        # Check if this is the currently selected page
        is_active = True if self.active_page == url else False

        if self.menu[-1]['sub_menu'] is None:
            # No sub menu defined yet, create a empty sub menu list for the last menu item
            self.menu[-1]['sub_menu'] = []

        # Add the cub menu item at the end of the menu list
        self.menu[-1]['sub_menu'].append({'active': is_active,
                                          'title': title,
                                          'icon': icon,
                                          'url': url})

        # If this was the currently selected page, override the active status for the main item
        # to make sure both the main menu item and the sub menu item are highlighted
        if is_active:
            self.menu[-1]['active'] = True

    def get_menu(self):
        """Return the generated menu"""
        return self.menu
