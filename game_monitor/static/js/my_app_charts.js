"use strict";

$(document).ready(function() {
    // Draw the chart
    var room_ids = [];
    for (var room_id in hs_charts) {
        if (hs_charts.hasOwnProperty(room_id)) {
            // Add the created chart to the hs_chart object
            hs_charts[room_id]['chart'] = new Highcharts.Chart(hs_charts[room_id].data);

            // Add the current room id to the list
            room_ids.push(room_id);
        }
    }

    for (var room_id in plotly_charts) {
        if (plotly_charts.hasOwnProperty(room_id)) {
            plotly_charts[room_id]['chart_div'] = document.getElementById(plotly_charts[room_id].id);

            // Add the current room id to the list
            room_ids.push(room_id);
        }
    }

    var socket_base_url = 'http://' + document.domain + ':' + location.port;
    var main_socket = io.connect(socket_base_url + '/snake');

    console.log("starting");

    // Event handler for new connections.
    main_socket.on('connect', function() {
        console.log("Connected /snake");
    });

    main_socket.on('disconnect', function() {
        console.log("Disconnected /snake");
    });

    main_socket.on('chart_data', function (data) {
        // console.log(data);
        if (data.chart_room_id in hs_charts) {
            // Update the data for the selected Highcharts chart
            // (redraw is disabled for each update, will do one common redraw at the end)

            // Update all the received series for this chart
            for (var i = 0; i < data.chart_data.data.length; i++ ) {
                hs_charts[data.chart_room_id].chart.series[i].setData(data.chart_data.data[i],
                                                                      false);
            }

            // Update the common settings
            hs_charts[data.chart_room_id].chart.update(data.chart_data.common, false);

            // Apply all the changes to the chart
            hs_charts[data.chart_room_id].chart.redraw();
        } else if (data.chart_room_id in plotly_charts) {
            // Update the data for the selected Plotly chart
            plotly_charts[data.chart_room_id].chart_div.data[0].values = data.chart_data.values;
            plotly_charts[data.chart_room_id].chart_div.data[0].labels = data.chart_data.labels;
            plotly_charts[data.chart_room_id].chart_div.data[0].marker.colors = data.chart_data.colors;

            // Trigger redraw
            Plotly.redraw(plotly_charts[room_id].id);
        }
    });

    // For debug
    main_socket.on('message', function (data) {
        console.log('msg: ', data);
    });

    $('#isLiveDataEnabled').change(function() {
        // console.log(room_ids.length);
        for (var i = 0; i < room_ids.length; i++) {
            if (this.checked) {
                console.log("Box is checked, trying to join room '" +
                            room_ids[i] + "'");
                main_socket.emit('join_room', {room: room_ids[i]});
            } else {
                console.log("unchecked");
                main_socket.emit('leave_room', {room: room_ids[i]});
            }
        }
    });
});
