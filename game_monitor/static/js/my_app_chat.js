"use strict";

$(document).ready(function() {

    // Get the deference to the div where the new messages will be inserted
    var $chat = $('#chat_area');

    // Make the chat box fill most of the screen vertically
    // TBD - scale according to available space (instead of guessing based on the header height)
    var window_height = $(window).height();
    var header_height = $(".main-header").height();
    $("#chat_area").css("height", window_height - header_height * 4.5);

    // Connect the socket
    var socket_base_url = 'http://' + document.domain + ':' + location.port;
    var main_socket = io.connect(socket_base_url + '/snake');

    console.log("starting");

    // Event handler for new connections.
    main_socket.on('connect', function() {
        console.log("Connected /snake");
        main_socket.emit('join_room',{room: "chat_room"});
    });

    main_socket.on('disconnect', function() {
        console.log("Disconnected /snake");
        main_socket.emit('leave_room',{room: "chat_room"});
    });

    main_socket.on('chat_message', function (data) {
        // console.log(data);

        // Loop through all received messages
        for (var i = 0; i < data.messages.length; i++) {
            // Alternate the orientation of the message labels left and right
            var msg_orientation = '';
            if (i % 2 === 0) {
                msg_orientation = 'right';
            }
            var msg = '<div class="direct-chat-msg ' + msg_orientation + '">' +
                      //'<div class="direct-chat-info clearfix">' +
                      //'<span class="direct-chat-name pull-right">' + data.messages[i].round + '</span>' +
                      //'</div>' +
                      '<img class="direct-chat-img" ' +
                      'src="/game_images/snake/icon/' + data.messages[i].node_id + '">' +
                      '<div class="direct-chat-text">' + data.messages[i].msg + '</div>' +
                      '</div>';
            // Add the message at the top of the chat area
            $chat.prepend(msg);

            // Limit the number of messages stored in the browser by deleting from the end
            $chat.children().slice(100).remove();
        }
    });

    // For debug
    main_socket.on('message', function (data) {
        console.log('msg: ', data);
    });

    var chat_running = true;
    $('#chat_pause').click(function() {
        if (chat_running) {
            chat_running = false;
            console.log("Pausing chat, leaving room 'chat_room'");
            main_socket.emit('leave_room',{room: 'chat_room'});
            $("#chat_pause i").removeClass("fa-pause" ).addClass("fa-play");
        } else {
            chat_running = true;
            console.log("Re-starting chat, joining room 'chat_room'");
            main_socket.emit('join_room',{room: 'chat_room'});
            $("#chat_pause i").removeClass("fa-play").addClass("fa-pause");
        }
    });
});
