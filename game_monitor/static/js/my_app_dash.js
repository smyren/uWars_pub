"use strict";

function clear_status_info() {
    set_status_alert('', '', '', '');
}

function set_status_info(heading, message) {
    set_status_alert('info', 'fa fa-info', heading, message);
}

function set_status_warning(heading, message) {
    set_status_alert('warning', 'fa fa-warning', heading, message);
}

function set_status_danger(heading, message) {
    set_status_alert('danger', 'fa fa-exclamation', heading, message);
}

function set_status_alert(type, icon, heading, message) {
    var alert_id = '#status_alert';

    if (type === '') {
        // Remove the alert
        $(alert_id).html('');
    } else {
        var alert_html = '<div class="alert alert-' + type + '">';
        alert_html += '<h4><i class="icon ' + icon + '"></i> ' +
                      heading + '</h4>' +
                      message +
                      '</div>';
        $(alert_id).html(alert_html);
    }
}

$(document).ready(function() {
    // Register the Highchart charts
    var room_ids = [];
    for (var room_id in hs_charts) {
        if (hs_charts.hasOwnProperty(room_id)) {
            // Add the created chart to the hs_chart object
            hs_charts[room_id]['chart'] = new Highcharts.Chart(hs_charts[room_id].data);

            // Add the current room id to the list
            room_ids.push(room_id);
        }
    }

    // Register the Plotly charts
    for (var room_id in plotly_charts) {
        if (plotly_charts.hasOwnProperty(room_id)) {
            plotly_charts[room_id]['chart_div'] = document.getElementById(plotly_charts[room_id].id);

            // Add the current room id to the list
            room_ids.push(room_id);
        }
    }

    // Register the Datatables tables
    for (var room_id in tables) {
        if (tables.hasOwnProperty(room_id)) {
            tables[room_id]['table'] = $('#' + tables[room_id].id).DataTable(tables[room_id].attr);

            // Add the current room id to the list
            room_ids.push(room_id);
        }
    }

    // Register chats
    for (var room_id in chats) {
        if (chats.hasOwnProperty(room_id)) {

            chats[room_id]['chat'] = $('#' + chats[room_id].id);

            // Add the current room id to the list
            room_ids.push(room_id);

            // Make the chat box fill the row vertically
            // TBD - update on size on window resize
            var row_height = chats[room_id]['chat'].closest('div.row').height();
            var box_height = chats[room_id]['chat'].closest('div.box').height();
            var box_body_height = chats[room_id]['chat'].height();
            var chat_height = box_body_height + row_height - box_height;
            chats[room_id]['chat'].css("height", chat_height);
        }
    }

    // Register info boxes
    for (var room_id in info_boxes) {
        if (info_boxes.hasOwnProperty(room_id)) {
            room_ids.push(room_id);

            var base_id = '#' + room_id + "_box";
            info_boxes[room_id]['fields'] = {'main_text': $(base_id + "_main_text"),
                                             'sub_text': $(base_id + "_sub_text"),
                                             'prog_bar_width': $(base_id + "_prog_bar_width")};
        }
    }

    var socket_base_url = 'http://' + document.domain + ':' + location.port;
    var main_socket = io.connect(socket_base_url + '/snake');

    console.log("starting");

    // Event handler for new connections.
    main_socket.on('connect', function() {
        console.log("Connected /snake");

        // Connect to the room for general status messages
        main_socket.emit('join_room', {room: 'status'});

        // Connect to the rooms for all charts in the page
        for (var i = 0; i < room_ids.length; i++) {
            main_socket.emit('join_room', {room: room_ids[i]});
        }

        clear_status_info();
    });

    main_socket.on('disconnect', function() {
        console.log("Disconnected /snake");

        // Quick fix - delaying the status message a little to reduce the chance of it showing when
        // the client is refreshing the page or navigating to another page.
        setTimeout(function() {set_status_danger('Lost contact with the web server...', '');},
                   2000);

        // // Disconnect form all the connected rooms
        // for (var i = 0; i < room_ids.length; i++) {
        //     main_socket.emit('leave_room', {room: room_ids[i]});
        // }
    });

    main_socket.on('chart_data', function (data) {
        // console.log(data);
        if (data.chart_room_id in hs_charts) {
            // Update the data for the selected Highcharts chart
            // (redraw is disabled for each update, will do one common redraw at the end)

            // Update all the received series for this chart
            for (var i = 0; i < data.chart_data.data.length; i++ ) {
                hs_charts[data.chart_room_id].chart.series[i].setData(data.chart_data.data[i],
                                                                      false);
            }

            // Update the common settings
            hs_charts[data.chart_room_id].chart.update(data.chart_data.common, false);

            // Apply all the changes to the chart
            hs_charts[data.chart_room_id].chart.redraw();
        } else if (data.chart_room_id in plotly_charts) {
            // Update the data for the selected Plotly chart
            plotly_charts[data.chart_room_id].chart_div.data[0].values = data.chart_data.values;
            plotly_charts[data.chart_room_id].chart_div.data[0].labels = data.chart_data.labels;
            plotly_charts[data.chart_room_id].chart_div.data[0].marker.colors = data.chart_data.colors;

            // Trigger redraw
            Plotly.redraw(plotly_charts[data.chart_room_id].id);
        }
    });

    main_socket.on('table_data', function (data) {
        if (data.table_room_id in tables) {
            tables[data.table_room_id]['table'].clear();
            tables[data.table_room_id]['table'].rows.add(data.table_data.data);
            tables[data.table_room_id]['table'].draw();
        }
    });

    main_socket.on('chat_message', function (data) {
        // console.log(data);

        if (data.room_id in chats) {
            // Loop through all received messages
            for (var i = 0; i < data.messages.length; i++) {
                // Alternate the orientation of the message labels left and right
                var msg_orientation = '';
                if (i % 2 === 0) {
                    msg_orientation = 'right';
                }
                var msg = '<div class="direct-chat-msg ' + msg_orientation + '">' +
                          //'<div class="direct-chat-info clearfix">' +
                          //'<span class="direct-chat-name pull-right">' + data.messages[i].round + '</span>' +
                          //'</div>' +
                          '<img class="direct-chat-img" ' +
                          'src="/game_images/snake/icon/' + data.messages[i].node_id + '">' +
                          '<div class="direct-chat-text">' + data.messages[i].msg + '</div>' +
                          '</div>';

                // Add the message at the top of the chat area
                chats[data.room_id]['chat'].prepend(msg);

                // Limit the number of messages stored in the browser by deleting from the end
                chats[data.room_id]['chat'].children().slice(chats[data.room_id].max_messages).remove();
            }
        }
    });

    main_socket.on('info_box_data', function (data) {
        // console.log(data);
        if (data.room_id in info_boxes) {
            if ('main_text' in data.data) {
                info_boxes[data.room_id]['fields'].main_text.html(data.data.main_text);
            }
            if ('sub_text' in data.data) {
                info_boxes[data.room_id]['fields'].sub_text.html(data.data.sub_text);
            }
            if ('prog_bar_width' in data.data) {
                var style = 'width: ' + data.data.prog_bar_width;
                info_boxes[data.room_id]['fields'].prog_bar_width.attr('style', style);
                console.log(data.data.prog_bar_width);
            }
        }
    });

    main_socket.on('status', function (data) {
        // console.log(data);
        if (data.status == 'active') {
            clear_status_info();
        } else if (data.status == 'paused') {
            set_status_warning('The game has been paused, waiting for it to restart...', '');
        } else if (data.status == 'stopped') {
            set_status_danger('No data was found on the server, waiting for a game to start...', '');
        }
    });

    // For debug
    main_socket.on('message', function (data) {
        console.log('msg: ', data);
    });

    //$('#isLiveDataEnabled').change(function() {
    //    // console.log(room_ids.length);
    //    for (var i = 0; i < room_ids.length; i++) {
    //        if (this.checked) {
    //            console.log("Box is checked, trying to join room '" +
    //                        room_ids[i] + "'");
    //            main_socket.emit('join_room', {room: room_ids[i]});
    //        } else {
    //            console.log("unchecked");
    //            main_socket.emit('leave_room', {room: room_ids[i]});
    //        }
    //    }
    //});

});
