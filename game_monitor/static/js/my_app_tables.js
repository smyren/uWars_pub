"use strict";

$(document).ready(function() {
    // Add the Datatables plugin to all the tables
    var room_ids = [];
    for (var room_id in tables) {
        if (tables.hasOwnProperty(room_id)) {
            tables[room_id]['table'] = $(tables[room_id].id).DataTable(tables[room_id].attr);

            // Add the current room id to the list
            room_ids.push(room_id);
        }
    }

    var socket_base_url = 'http://' + document.domain + ':' + location.port;
    var main_socket = io.connect(socket_base_url + '/snake');

    console.log("starting");

    // Event handler for new connections.
    main_socket.on('connect', function() {
        console.log("Connected /snake");
    });

    main_socket.on('disconnect', function() {
        console.log("Disconnected /snake");
    });

    main_socket.on('table_data', function (data) {
        // console.log(data);
        // Update the data for the selected table
        if (data.table_room_id in tables) {
            tables[data.table_room_id]['table'].clear();
            tables[data.table_room_id]['table'].rows.add(data.table_data.data);
            tables[data.table_room_id]['table'].draw();
        }
    });

    // For debug
    main_socket.on('message', function (data) {
        console.log('msg: ', data);
    });

    $('#isLiveDataEnabled').change(function() {
        for (var i = 0; i < room_ids.length; i++) {
            if (this.checked) {
                console.log("Box is checked, trying to join room '" +
                            room_ids[i] + "'");
                main_socket.emit('join_room',{room: room_ids[i]});
            } else {
                console.log("unchecked");
                main_socket.emit('leave_room',{room: room_ids[i]});
            }
        }
    });
});
