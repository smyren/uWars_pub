##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

from flask import url_for, Markup
import flask_table as ft
# from operator import itemgetter
import collections
import datetime


class ScoreTables:
    """Class for grouping tables presenting data from the scores database table. All these
    currently expect a unfiltered dataset from the scores table when updating the data
    """
    def __init__(self):
        self.tables = collections.OrderedDict()
        self.tables['snake_lengths'] = {'class': SnakeLengthTable,
                                        'title': 'Snake lengths',
                                        'db_data_func': 'get_score_totals'}
        self.tables['max_snake_lengths'] = {'class': MaxSnakeLengthTable,
                                            'title': 'Max snake lengths',
                                            'db_data_func': 'get_score_totals'}
        self.tables['snake_alive_times'] = {'class': SnakeAliveTimeTable,
                                            'title': 'Snake alive times',
                                            'db_data_func': 'get_score_totals'}
        self.tables['snake_response_times'] = {'class': SnakeResponseTimeTable,
                                               'title': 'Snake response times',
                                               'db_data_func': 'get_score_totals'}
        self.tables['snake_moves'] = {'class': SnakeMovesTable,
                                      'title': 'Snake moves',
                                      'db_data_func': 'get_score_totals'}
        self.tables['snake_kills_deaths'] = {'class': SnakeKillsDeathsTable,
                                             'title': 'Snake kills and deaths',
                                             'db_data_func': 'get_score_totals'}
        self.tables['score_table'] = {'class': SnakeScoreTable,
                                      'title': 'Score table',
                                      'db_data_func': 'get_score_totals'}
        self.tables['dashboard_max_length'] = {'class': DashboardMaxLengthTable,
                                               'title': 'Max length - dashboard',
                                               'db_data_func': 'get_score_totals'}
        self.tables['dashboard_max_acc_mass'] = {'class': DashboardMaxAccMassTable,
                                                 'title': 'Max accumulated mass - dashboard',
                                                 'db_data_func': 'get_score_totals'}
        self.tables['dashboard_max_age'] = {'class': DashboardMaxAgeTable,
                                            'title': 'Max age - dashboard',
                                            'db_data_func': 'get_score_totals'}

    def get_db_data_funcs(self):
        func_list = []
        for table_id, table_data in self.tables.items():
            func_list.append(table_data['db_data_func'])

        return func_list


def float_to_string(value, decimals=3, scale=1, exp=False):
    """Format a floting point number to a string, if not a number it is returned unchanged

        value -- The number to format
        decimals -- number of decimals in the formatted number
        scale -- scaling factor multiplied with the value before formatting
    """
    try:
        # Make sure the number is a float
        val = float(value)
        # Return as ms
        if exp:
            return "{val:.{decimals}e}".format(val=(val * scale), decimals=decimals)
        else:
            return "{val:.{decimals}f}".format(val=(val * scale), decimals=decimals)
    except Exception as e:
        # The value was not a number, returning it unchanged
        return value


def number_to_thousand_sep_string(value, sep=' '):
    """Format a number with thousand separators, if not a number it is returned unchanged

        value -- the number to format
        sep -- optional thousand separator
    """
    try:
        return '{0:,}'.format(value).replace(',', sep)
    except Exception as e:
        # The value was not a number, returning it unchanged
        return value


class FloatCol(ft.Col):
    """Special column class for formatting float values"""
    def __init__(self, name, decimals=3, **kwargs):
        super().__init__(name, **kwargs)
        self.decimals = decimals

    """Special column class for formatting response times in ms"""
    def td_format(self, content):
        """Format the float value

        If the value isn't recognized as a number it is returned unchanged
        """
        try:
            # Make sure the number is a float
            val = float(content)
            exp = False
            if val < 0.01:
                exp = True
            return float_to_string(val, decimals=self.decimals, exp=exp)
        except Exception as e:
            # The value was not a number, returning it unchanged
            return content


class ThousandSepCol(ft.Col):
    """Special column class for adding thousand separators to numbers"""
    def td_format(self, content):
        # TBD - formatting has been disabled until the the column sort order has been fixed
        # Will be sorted as strings after inserting the thousand separator
        # return number_to_thousand_sep_string(content, ' ')
        return content


class ScaledValueCol(ft.Col):
    """Special column class for scaling a numeric value"""
    def __init__(self, name, scale_factor=1, decimals=3, neg_val=None, **kwargs):
        """
            name -- Column name
            scale_factor -- factor multiplied with the cell value
            decimals -- numbers of decimals in the value
            neg_val -- If not None, replace all negative values with this
        """
        super().__init__(name, **kwargs)
        self.scale_factor = scale_factor
        self.decimals = decimals
        self.neg_val = neg_val

    def td_format(self, content):
        """Scale the value and limit the number of decimals

        If the value isn't recognized as a number it is returned unchanged
        """
        try:
            # Make sure the number is a float
            val = float(content)
            if self.neg_val and val < 0:
                return self.neg_val
            else:
                # Scale value and set number of decimals
                return float_to_string(val, self.decimals, self.scale_factor)
        except Exception as e:
            # The value was not a number, returning it unchanged
            return content


class IconCol(ft.Col):
    """Special column class for showing the snake icon"""
    def __init__(self, name, height=0, width=0, **kwargs):
        super().__init__(name, **kwargs)
        self.height = height
        self.width = width
        self.classes = ['icon_zoom_on_hover']

    def td_format(self, content):
        size = ''
        class_str = ''
        if self.height != 0 and self.width != 0:
            # Both the height and width must be set for the settings to be valid
            size = 'height="{0}" width="{1}"'.format(self.height, self.width)

        if self.classes:
            class_str = 'class="' + ' '.join(self.classes) + '"'

        img_html = '<img {2} src="{0}" {1}>'.format(url_for('get_snake_img',
                                                            img_type='icon',
                                                            node_id=content),
                                                    size,
                                                    class_str)

        return img_html


class SnakeImgCol(ft.Col):
    """Special column class for showing the snake head image"""
    def __init__(self, name, height=0, width=0, **kwargs):
        super().__init__(name, **kwargs)
        self.height = height
        self.width = width
        self.classes = ['snake_img_zoom_on_hover']

    def td_format(self, content):
        size = ''
        class_str = ''
        if self.height != 0 and self.width != 0:
            # Both the height and width must be set for the settings to be valid
            size = 'height="{0}" width="{1}"'.format(self.height, self.width)

        if self.classes:
            class_str = 'class="' + ' '.join(self.classes) + '"'

        img_html = '<img {2} src="{0}" {1}>'.format(url_for('get_snake_img',
                                                            img_type='head',
                                                            node_id=content),
                                                    size,
                                                    class_str)

        return img_html


class StatusCol(ft.Col):
    """Special column class for showing Snake status"""
    def __init__(self, name, **kwargs):
        super().__init__(name, **kwargs)

    def td_format(self, content):
        icon = 'glyphicon glyphicon-stop'
        color = 'text-danger'
        if content.lower() == 'active':
            icon = 'glyphicon glyphicon-play'
            color = 'text-success'
        elif content.lower() == 'paused':
            icon = 'glyphicon glyphicon-pause'
            color = 'text-warning'

        return '<span class="' + icon + ' ' + color + '"></span>'


class StringToDateCol(ft.Col):
    """Format the content as a date"""
    def __init__(self, name, **kwargs):
        super().__init__(name, **kwargs)

    def td_format(self, content):
        if content:
            try:
                date = datetime.datetime.strptime(content, "%Y%m%dT%H%M%S")
                return date.strftime("%Y-%m-%d")
            except Exception as e:
                return ''
        else:
            return ''

class EmptyTable():
    """Dummy class for empty table, used to make sure the templates don't crash if a table hasn't
    been selected yet
    """
    table = ''
    datatables_attr = {}


def get_default_classes():
    return ['table', 'table-bordered', 'table-hover']


def get_default_datatables_attr():
    return {'paging': False,
            'searching': False,
            'ordering': True,
            'responsive': True}


class DefaultFlaskTable(ft.Table):
    """Base class for Flask tables, used for keeping common default settings"""
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        # Override the Table class to make sure a table is drawn even if it is there is no data yet
        self.allow_empty = True

        # Set default settings for the flask_table class

        # Default table classes
        self.classes = get_default_classes()

        # Set table width (necessary to make the tables resize correctly when the window size
        # is changed (in addition to the responsive settings))
        self.html_attrs = {'style': "width:100%;"}


class ExtendedFlaskTable(DefaultFlaskTable):
    """Base class for Flask tables, used for keeping common default settings"""
    def __init__(self, *args, **kwargs):
        # Passing all arguments unchanged to the parent class
        super().__init__(*args, **kwargs)

        # Set default settings for the flask_table class

    def get_data_json(self):
        json_data = {'data': []}
        for item in self.items:
            # Generate the column fields
            # From tr function in table.py:
            #   row = [c.td(item, attr) for attr, c in self._cols.items() if c.show]
            # modified to allow stripping of the td tags
            row = []
            for attr, c in self._cols.items():
                if c.show:
                    val = c.td(item, attr)

                    # Remove the <td></td> tags created by the td function in columns.py
                    # TBD - modify/overload the Col class to output the data without the td tags
                    if val.startswith('<td>') and val.endswith('</td>'):
                        val = val[4:-5]
                    row.append(val)

            json_data['data'].append(row)
            json_data['id'] = self.table_id

        return json_data


class DefaultDataTable():
    """Base class for keeping common default settings for the Datatables javascript plugin"""
    def __init__(self, *args, **kwargs):
        self.id = ''
        self.data = []
        self.room_id = ''

        # Set special parameters for the DataTables javascript  plugin

        # Add the default Datatables configuration, this will be inserted as json in the DataTables
        # creator
        self.datatables_attr = get_default_datatables_attr()


class SnakeLengthTable(DefaultDataTable):
    def __init__(self, data, table_id=''):
        super().__init__()
        self.id = table_id
        self.data = data
        self.room_id = 'snake_length_table_room'       # Room name for the sockets

        # Update the data with generated columns before creating the table
        for row in self.data:
            # Insert the node id in the icon column, will be used for creating the url to the icon
            row['icon'] = row['node_id']

        # Create the flask table
        self.table = self.ft_table(self.data, table_id=self.id)

        # Set parameters for DataTables
        # Set default order colum
        self.datatables_attr['order'] = [[5, 'desc']]

        # Disable ordering for the icon column
        # centered_cols = [0, 2, 3, 4, 5, 6, 7, 8, 9]     # All cols except the name column
        self.datatables_attr['columnDefs'] = [{'targets': [0], 'orderable': False}]
        # TBD                                 {'targets': centered_cols, 'className': "dt-center"}]

    class ft_table(ExtendedFlaskTable):
        """Set up the flask table definitions"""
        # Specify the columns in the table
        # The name in attr must match a dict key in the data passed to the table. If attr is not
        # specified the parameter assigned to ft.Col must match the dict key in the data
        snake_icon = IconCol('', height=16, width=16, attr='icon')
        snake_name = ft.Col('Name', attr='name')
        rounds = ThousandSepCol('Rounds played', attr='rounds')
        deaths = ThousandSepCol('Deaths', attr='deaths')
        length_current = ThousandSepCol('Current', attr='length_current')
        eol_length_max = ThousandSepCol('Max', attr='eol_length_max')
        eol_length_q3 = ft.Col('3rd quartile', attr='eol_length_quartile3')
        eol_length_median = ft.Col('Median', attr='eol_length_median')
        eol_length_q1 = ft.Col('1st quartile', attr='eol_length_quartile1')
        eol_length_min = ft.Col('Min', attr='eol_length_min')
        eol_length_sum = ThousandSepCol('Sum (max values)', attr='eol_length_sum')
        length_acc = ThousandSepCol('Accumulated snake mass', attr='acc_snake_mass')


class MaxSnakeLengthTable(DefaultDataTable):
    def __init__(self, data, table_id=''):
        super().__init__()
        self.id = table_id
        self.data = data
        self.room_id = 'max_snake_length_table_room'       # Room name for the sockets

        # Update the data with generated columns before creating the table
        for row in self.data:
            # Insert the node id in the icon column, will be used for creating the url to the icon
            row['icon'] = row['node_id']

        # Create the flask table
        self.table = self.ft_table(self.data, table_id=self.id)

        # Set parameters for DataTables
        # Set default order colum
        self.datatables_attr['order'] = [[5, 'desc']]

        # Disable ordering for the icon column
        # centered_cols = [0, 2, 3, 4, 5, 6, 7, 8, 9]     # All cols except the name column
        self.datatables_attr['columnDefs'] = [{'targets': [0], 'orderable': False}]
        # TBD                                 {'targets': centered_cols, 'className': "dt-center"}]

    class ft_table(ExtendedFlaskTable):
        """Set up the flask table definitions"""
        # Specify the columns in the table
        # The name in attr must match a dict key in the data passed to the table. If attr is not
        # specified the parameter assigned to ft.Col must match the dict key in the data
        snake_icon = IconCol('', height=16, width=16, attr='icon')
        snake_name = ft.Col('Name', attr='name')
        rounds = ThousandSepCol('Rounds played', attr='rounds')
        deaths = ThousandSepCol('Deaths', attr='deaths')
        length_current = ThousandSepCol('Current', attr='length_current')
        max_length_current = ThousandSepCol('Current max', attr='max_length_current')
        max_length_max = ThousandSepCol('Max', attr='max_length_max')
        max_length_q3 = ft.Col('3rd quartile', attr='max_length_quartile3')
        max_length_median = ft.Col('Median', attr='max_length_median')
        max_length_q1 = ft.Col('1st quartile', attr='max_length_quartile1')
        max_length_min = ft.Col('Min', attr='max_length_min')
        max_length_sum = ThousandSepCol('Sum (max values)', attr='max_length_sum')


class SnakeAliveTimeTable(DefaultDataTable):
    def __init__(self, data, table_id=''):
        super().__init__()
        self.id = table_id
        self.data = data
        self.room_id = 'snake_alive_time_table_room'       # Room name for the sockets

        # Update the data with generated columns before creating the table
        for row in self.data:
            # Insert the node id in the icon column, will be used for creating the url to the icon
            row['icon'] = row['node_id']

        # Create the flask table
        self.table = self.ft_table(self.data, table_id=self.id)

        # Set parameters for DataTables
        # Set default order colum
        self.datatables_attr['order'] = [[5, 'desc']]
        # Disable ordering for the icon column
        self.datatables_attr['columnDefs'] = [{'targets': [0], 'orderable': False}]

    class ft_table(ExtendedFlaskTable):
        """Set up the flask table definitions"""
        # Specify the columns in the table
        # The name in attr must match a dict key in the data passed to the table. If attr is not
        # specified the parameter assigned to ft.Col must match the dict key in the data
        snake_icon = IconCol('', height=16, width=16, attr='icon')
        snake_name = ft.Col('Name', attr='name')
        rounds = ThousandSepCol('Rounds played', attr='rounds')
        deaths = ThousandSepCol('Deaths', attr='deaths')
        alive_time_current = ThousandSepCol('Current', attr='alive_time_current')
        alive_time_max = ThousandSepCol('Max', attr='alive_time_max')
        alive_time_q3 = ft.Col('3rd quartile', attr='alive_time_quartile3')
        alive_time_median = ft.Col('Median', attr='alive_time_median')
        alive_time_q1 = ft.Col('1st quartile', attr='alive_time_quartile1')
        alive_time_min = ft.Col('Min', attr='alive_time_min')
        alive_time_sum = ThousandSepCol('Sum', attr='alive_time_sum')


class SnakeResponseTimeTable(DefaultDataTable):
    def __init__(self, data, table_id=''):
        super().__init__()
        self.id = table_id
        self.data = data
        self.room_id = 'snake_response_time_table_room'       # Room name for the sockets

        # Update the data with generated columns before creating the table
        for row in self.data:
            # Insert the node id in the icon column, will be used for creating the url to the icon
            row['icon'] = row['node_id']

        # Create the flask table
        self.table = self.ft_table(self.data, table_id=self.id)

        # Set parameters for DataTables

        # Set default order colum
        self.datatables_attr['order'] = [[7, 'asc']]
        # Disable ordering for the icon column
        self.datatables_attr['columnDefs'] = [{'targets': [0], 'orderable': False}]

    class ft_table(ExtendedFlaskTable):
        """Set up the flask table definitions"""

        # Specify the columns in the table
        # The name in attr must match a dict key in the data passed to the table. If attr is not
        # specified the parameter assigned to ft.Col must match the dict key in the data
        snake_icon = IconCol('', height=16, width=16, attr='icon')
        snake_name = ft.Col('Name', attr='name')
        rounds = ThousandSepCol('Rounds played', attr='rounds')
        deaths = ThousandSepCol('Deaths', attr='deaths')
        response_sum_valid = ScaledValueCol('Total response time [s]',
                                            scale_factor=1,
                                            decimals=2,
                                            neg_val='NA',
                                            attr='response_time_sum_valid')
        response_max = ScaledValueCol('Max [µs]',
                                      scale_factor=1e6,
                                      decimals=2,
                                      neg_val='NA',
                                      attr='response_time_max')
        response_avg = ScaledValueCol('Average [µs]',
                                      scale_factor=1e6,
                                      decimals=2,
                                      neg_val='NA',
                                      attr='response_time_avg')
        response_min = ScaledValueCol('Min [µs]',
                                      scale_factor=1e6,
                                      decimals=2,
                                      neg_val='NA',
                                      attr='response_time_min')
        response_num_valid = ThousandSepCol('# valid', attr='response_time_num_valid')
        response_num_missed = ThousandSepCol('# missed', attr='response_time_num_missed')


class SnakeMovesTable(DefaultDataTable):
    def __init__(self, data, table_id=''):
        super().__init__()
        self.id = table_id
        self.data = data
        self.room_id = 'snake_moves_table_room'       # Room name for the sockets

        # Update the data with generated columns before creating the table
        for row in self.data:
            # Insert the node id in the icon column, will be used for creating the url to the icon
            row['icon'] = row['node_id']
            row['distance'] = (row['move_lefts'] + row['move_rights'] +
                               row['move_forwards'] + row['move_fast_forwards'] * 2)

        # Create the flask table
        self.table = self.ft_table(self.data, table_id=self.id)

        # Set parameters for DataTables

        # Set default order colum
        self.datatables_attr['order'] = [[3, 'desc']]
        # Disable ordering for the icon column
        self.datatables_attr['columnDefs'] = [{'targets': [0], 'orderable': False}]

    class ft_table(ExtendedFlaskTable):
        """Set up the flask table definitions"""
        # Specify the columns in the table
        # The name in attr must match a dict key in the data passed to the table. If attr is not
        # specified the parameter assigned to ft.Col must match the dict key in the data
        snake_icon = IconCol('', height=16, width=16, attr='icon')
        snake_name = ft.Col('Name', attr='name')
        rounds = ThousandSepCol('Rounds played', attr='rounds')
        distance = ThousandSepCol('Distance travelled', attr='distance')
        move_fast_forward = ThousandSepCol('Fast forward', attr='move_fast_forwards')
        move_forward = ThousandSepCol('Forward', attr='move_forwards')
        move_left = ThousandSepCol('Left', attr='move_lefts')
        move_right = ThousandSepCol('Right', attr='move_rights')


class SnakeKillsDeathsTable(DefaultDataTable):
    def __init__(self, data, table_id=''):
        super().__init__()
        self.id = table_id
        self.data = data
        self.room_id = 'kill_death_table_room'

        # Update the data with generated columns before creating the table
        for row in self.data:
            # Insert the node id in the icon column, will be used for creating the url to the icon
            row['icon'] = row['node_id']

        # Create the flask table
        self.table = self.ft_table(self.data, table_id=self.id)

        # Set parameters for DataTables

        # Set default order colum
        self.datatables_attr['order'] = [[4, 'asc']]
        # Disable ordering for the icon column
        self.datatables_attr['columnDefs'] = [{'targets': [0], 'orderable': False}]

    class ft_table(ExtendedFlaskTable):
        """Set up the flask table definitions"""
        # Specify the columns in the table
        # The name in attr must match a dict key in the data passed to the table. If attr is not
        # specified the parameter assigned to ft.Col must match the dict key in the data
        snake_icon = IconCol('', height=16, width=16, attr='icon')
        snake_name = ft.Col('Name', attr='name')
        rounds = ThousandSepCol('Rounds played', attr='rounds')
        kills = ThousandSepCol('Kills', attr='kills')
        deaths = ThousandSepCol('Deaths', attr='deaths')


class SnakeScoreTable(DefaultDataTable):
    def __init__(self, data, table_id=''):
        super().__init__()
        self.id = table_id
        self.data = data
        self.room_id = 'score_table_room'       # Room name for the sockets

        # Add generated columns to the data
        self.add_generated_data_values()

        # Create the flask table
        self.table = self.ft_table(self.data, table_id=self.id)

        # Set parameters for DataTables

        # Set default order colum
        self.datatables_attr['order'] = [[0, 'asc']]
        # Disable ordering for the icon column
        self.datatables_attr['columnDefs'] = [{'targets': [0], 'orderable': False},
                                              {'targets': [1], 'orderable': False}]
        self.datatables_attr['columnDefs'].append({'className': 'text-center',
                                                   'targets': [0, 1, 2]})
        # self.datatables_attr['columnDefs'].append({'responsivePriority': 0, 'targets': 0})
        # self.datatables_attr['columnDefs'].append({'responsivePriority': 1, 'targets': 4})
        # self.datatables_attr['columnDefs'].append({'responsivePriority': 2, 'targets': 5})
        # self.datatables_attr['columnDefs'].append({'responsivePriority': 3, 'targets': 7})

    def add_generated_data_values(self):
        """Update the data with generated columns"""
        for row in self.data:
            # Insert the node id in the icon column, will be used for creating the url to the icon
            row['icon'] = row['node_id']
            # Calculate kill/death ratio
            if row['deaths'] > 0:
                row['kd_ratio'] = row['kills'] / row['deaths']
            else:
                row['kd_ratio'] = 0
            # Calculate food pr round
            if row['rounds'] > 0:
                row['food_per_round'] = row['food'] / row['rounds']
            else:
                row['food_per_round'] = 0

    class ft_table(ExtendedFlaskTable):
        """Set up the flask table definitions"""

        # Specify the columns in the table
        # The name in attr must match a dict key in the data passed to the table. If attr is not
        # specified the parameter assigned to ft.Col must match the dict key in the data
        snake_rank = ft.Col('', attr='rank')
        status = StatusCol('', attr='status')
        snake_icon = IconCol('', height=16, width=16, attr='icon')
        snake_name = ft.Col('Name', attr='name')
        rounds = ThousandSepCol('Rounds played', attr='rounds')
        response_avg = ScaledValueCol('Avg resp time [µs]',
                                      scale_factor=1e6,
                                      decimals=2,
                                      neg_val='NA',
                                      attr='response_time_avg')
        response_num_missed = ThousandSepCol('Missed resp', attr='response_time_num_missed')
        kills = ThousandSepCol('Kills', attr='kills')
        deaths = ThousandSepCol('Deaths', attr='deaths')
        kd_ratio = FloatCol('Kill/ death', attr='kd_ratio')
        food = ft.Col('Food', attr='food')
        food_per_round = FloatCol('Food/ round', attr='food_per_round')
        length_acc = ThousandSepCol('Accumulated snake mass', attr='acc_snake_mass')
        length_current = ThousandSepCol('Current length', attr='length_current')
        max_length_current = ThousandSepCol('Current max length', attr='max_length_current')
        max_length_max = ThousandSepCol('Max length', attr='max_length_max')
        max_length_median = ft.Col('Median length', attr='max_length_median')
        alive_time_current = ThousandSepCol('Current age', attr='alive_time_current')
        alive_time_max = ThousandSepCol('Max age', attr='alive_time_max')
        alive_time_median = ft.Col('Median age', attr='alive_time_median')


####################################################################################################


class DashboardMaxLengthTable(DefaultDataTable):
    def __init__(self, data, table_id=''):
        super().__init__()
        self.id = table_id
        self.data = data
        self.room_id = 'dashboard_table_max_length_room'       # Room name for the sockets

        # Add generated columns to the data
        self.add_generated_data_values()

        # Create the flask table
        self.table = self.ft_table(self.data, table_id=self.id)

        # Set parameters for DataTables

        # Override the default responsive settings
        # - Disable the details row (ie the option for showing data incolumns that have been hidden)
        self.datatables_attr['responsive'] = {'details': False}

        # Set default order colum
        self.datatables_attr['order'] = [[0, 'asc']]

        # Disable ordering for the status and icon columns
        self.datatables_attr['columnDefs'] = [{'targets': [1], 'orderable': False},
                                              {'targets': [2], 'orderable': False}]
        # Specify which column are hidden first on smaller screens
        # - default is 10000, set a higher values to hide column before the rest
        # self.datatables_attr['columnDefs'].append({'responsivePriority': 20000, 'targets': 3})
        self.datatables_attr['columnDefs'].append({'responsivePriority': 20005, 'targets': 6})
        # self.datatables_attr['columnDefs'].append({'responsivePriority': 20010, 'targets': 8})
        self.datatables_attr['columnDefs'].append({'className': 'text-center',
                                                   'targets': [0, 1, 2]})

    def add_generated_data_values(self):
        """Update the data with generated columns"""
        for row in self.data:
            # Insert the node id in the icon column, will be used for creating the url to the icon
            row['icon'] = row['node_id']

    class ft_table(ExtendedFlaskTable):
        """Set up the flask table definitions"""
        # Specify the columns in the table
        # The name in attr must match a dict key in the data passed to the table. If attr is not
        # specified the parameter assigned to ft.Col must match the dict key in the data
        snake_rank = ft.Col('', attr='rank')
        status = StatusCol('', attr='status')
        snake_icon = IconCol('', height=16, width=16, attr='icon')
        snake_name = ft.Col('Name', attr='name')
        max_length_max = ThousandSepCol('Max length', attr='max_length_max')
        rounds = ThousandSepCol('Rounds played', attr='rounds')
        deaths = ThousandSepCol('Deaths', attr='deaths')
        kills = ThousandSepCol('Kills', attr='kills')
        response_avg = ScaledValueCol('Avg resp time [µs]',
                                      scale_factor=1e6,
                                      decimals=2,
                                      neg_val='NA',
                                      attr='response_time_avg')
        response_num_missed = ThousandSepCol('Missed rounds', attr='response_time_num_missed')
        # kills = ThousandSepCol('Kills', attr='kills')
        # kd_ratio = FloatCol('Kill/ death', attr='kd_ratio')
        # food = ft.Col('Food', attr='food')
        # food_per_round = FloatCol('Food/ round', attr='food_per_round')


class DashboardMaxAgeTable(DefaultDataTable):
    def __init__(self, data, table_id=''):
        super().__init__()
        self.id = table_id
        self.data = data
        self.room_id = 'dashboard_table_max_age_room'       # Room name for the sockets

        # Add generated columns to the data
        self.add_generated_data_values()

        # Create the flask table
        self.table = self.ft_table(self.data, table_id=self.id)

        # Set parameters for DataTables

        # Override the default responsive settings
        # - Disable the details row (ie the option for showing data incolumns that have been hidden)
        self.datatables_attr['responsive'] = {'details': False}

        # Set default order colum
        self.datatables_attr['order'] = [[0, 'asc']]

        # Disable ordering for the status and icon columns
        self.datatables_attr['columnDefs'] = [{'targets': [1], 'orderable': False},
                                              {'targets': [2], 'orderable': False}]
        # Specify which column are hidden first on smaller screens
        # - default is 10000, set a higher values to hide column before the rest
        # self.datatables_attr['columnDefs'].append({'responsivePriority': 20000, 'targets': 3})
        self.datatables_attr['columnDefs'].append({'responsivePriority': 20005, 'targets': 6})
        # self.datatables_attr['columnDefs'].append({'responsivePriority': 20010, 'targets': 8})
        self.datatables_attr['columnDefs'].append({'className': 'text-center',
                                                   'targets': [0, 1, 2]})

    def add_generated_data_values(self):
        """Update the data with generated columns"""
        for row in self.data:
            # Insert the node id in the icon column, will be used for creating the url to the icon
            row['icon'] = row['node_id']

    class ft_table(ExtendedFlaskTable):
        """Set up the flask table definitions"""
        # Specify the columns in the table
        # The name in attr must match a dict key in the data passed to the table. If attr is not
        # specified the parameter assigned to ft.Col must match the dict key in the data
        snake_rank = ft.Col('', attr='rank')
        status = StatusCol('', attr='status')
        snake_icon = IconCol('', height=16, width=16, attr='icon')
        snake_name = ft.Col('Name', attr='name')
        age_max = ThousandSepCol('Max age', attr='alive_time_max')
        rounds = ThousandSepCol('Rounds played', attr='rounds')
        deaths = ThousandSepCol('Deaths', attr='deaths')
        kills = ThousandSepCol('Kills', attr='kills')
        response_avg = ScaledValueCol('Avg resp time [µs]',
                                      scale_factor=1e6,
                                      decimals=2,
                                      neg_val='NA',
                                      attr='response_time_avg')
        response_num_missed = ThousandSepCol('Missed rounds', attr='response_time_num_missed')
        # kills = ThousandSepCol('Kills', attr='kills')
        # kd_ratio = FloatCol('Kill/ death', attr='kd_ratio')
        # food = ft.Col('Food', attr='food')
        # food_per_round = FloatCol('Food/ round', attr='food_per_round')


class DashboardMaxAccMassTable(DefaultDataTable):
    def __init__(self, data, table_id=''):
        super().__init__()
        self.id = table_id
        self.data = data
        self.room_id = 'dashboard_table_max_acc_mass_room'       # Room name for the sockets

        # Add generated columns to the data
        self.add_generated_data_values()

        # Create the flask table
        self.table = self.ft_table(self.data, table_id=self.id)

        # Set parameters for DataTables

        # Override the default responsive settings
        # - Disable the details row (ie the option for showing data incolumns that have been hidden)
        self.datatables_attr['responsive'] = {'details': False}

        # Set default order colum
        self.datatables_attr['order'] = [[0, 'asc']]

        # Disable ordering for the status and icon columns
        self.datatables_attr['columnDefs'] = [{'targets': [1], 'orderable': False},
                                              {'targets': [2], 'orderable': False}]
        # Specify which column are hidden first on smaller screens
        # - default is 10000, set a higher values to hide column before the rest
        # self.datatables_attr['columnDefs'].append({'responsivePriority': 20000, 'targets': 3})
        self.datatables_attr['columnDefs'].append({'responsivePriority': 20005, 'targets': 6})
        # self.datatables_attr['columnDefs'].append({'responsivePriority': 20010, 'targets': 8})
        self.datatables_attr['columnDefs'].append({'className': 'text-center',
                                                   'targets': [0, 1, 2]})

    def add_generated_data_values(self):
        """Update the data with generated columns"""
        for row in self.data:
            # Insert the node id in the icon column, will be used for creating the url to the icon
            row['icon'] = row['node_id']

            if row['rounds'] > 0:
                row['mass_pr_round'] = row['acc_snake_mass'] / row['rounds']
            else:
                row['mass_pr_round'] = 0

    class ft_table(ExtendedFlaskTable):
        """Set up the flask table definitions"""
        # Specify the columns in the table
        # The name in attr must match a dict key in the data passed to the table. If attr is not
        # specified the parameter assigned to ft.Col must match the dict key in the data
        snake_rank = ft.Col('', attr='rank')
        status = StatusCol('', attr='status')
        snake_icon = IconCol('', height=16, width=16, attr='icon')
        snake_name = ft.Col('Name', attr='name')
        length_acc = ThousandSepCol('Acc snake mass', attr='acc_snake_mass')
        mass_pr_round = ScaledValueCol('Avg snake mass pr round',
                                       scale_factor=1,
                                       decimals=3,
                                       neg_val='NA',
                                       attr='mass_pr_round')
        max_length_max = ThousandSepCol('Max length', attr='max_length_max')
        rounds = ThousandSepCol('Rounds played', attr='rounds')
        deaths = ThousandSepCol('Deaths', attr='deaths')
        kills = ThousandSepCol('Kills', attr='kills')
        response_avg = ScaledValueCol('Avg resp time [µs]',
                                      scale_factor=1e6,
                                      decimals=2,
                                      neg_val='NA',
                                      attr='response_time_avg')
        response_num_missed = ThousandSepCol('Missed rounds', attr='response_time_num_missed')
        # kills = ThousandSepCol('Kills', attr='kills')
        # kd_ratio = FloatCol('Kill/ death', attr='kd_ratio')
        # food = ft.Col('Food', attr='food')
        # food_per_round = FloatCol('Food/ round', attr='food_per_round')


####################################################################################################


class PlayerInfoTable(DefaultDataTable):
    def __init__(self, data, table_id=''):
        super().__init__()
        self.id = table_id
        self.data = data
        self.room_id = 'player_info_table_room'

        # Update the data with generated columns before creating the table
        for row in self.data:
            # Insert the node id in the image columns, will be used for creating the url
            row['icon'] = row['node_id']
            row['head_img'] = row['node_id']

        # Create the flask table
        self.table = self.ft_table(self.data, table_id=self.id)

        # Set parameters for DataTables

        # Set default order colum
        self.datatables_attr['order'] = [[1, 'asc']]
        # Disable ordering for the icon column
        self.datatables_attr['columnDefs'] = [{'targets': [0], 'orderable': False}]

    class ft_table(ExtendedFlaskTable):
        """Set up the flask table definitions"""
        # Specify the columns in the table
        # The name in attr must match a dict key in the data passed to the table. If attr is not
        # specified the parameter assigned to ft.Col must match the dict key in the data
        # snake_icon = IconCol('', height=24, width=24, attr='icon')
        snake_head_img = SnakeImgCol('', height=30, width=30, attr='head_img')
        snake_name = ft.Col('Name', attr='name')
        owner = ft.Col('Created by', attr='owner')
        info = ft.Col('Info', attr='info')
        # error_msg = ft.Col('Last error message', attr='last_error_msg')


class GameWinnersTable(DefaultDataTable):
    def __init__(self, data, table_id=''):
        super().__init__()
        self.id = table_id
        self.data = data
        self.room_id = 'game_winners_table_room'

        # Create the flask table
        self.table = self.ft_table(self.data, table_id=self.id)

        # Set parameters for DataTables

        # Set default order colum
        self.datatables_attr['order'] = [[0, 'desc']]
        # Disable ordering for the icon column
        # self.datatables_attr['columnDefs'] = [{'targets': [0], 'orderable': False}]

    class ft_table(ExtendedFlaskTable):
        """Set up the flask table definitions"""
        # Specify the columns in the table
        # The name in attr must match a dict key in the data passed to the table. If attr is not
        # specified the parameter assigned to ft.Col must match the dict key in the data
        # snake_icon = IconCol('', height=24, width=24, attr='icon')
        # snake_head_img = SnakeImgCol('', height=30, width=30, attr='head_img')
        started = StringToDateCol('Start date', attr='started')
        snake_name = ft.Col('Snake name', attr='name')
        owner = ft.Col('Player name', attr='owner')
        title = ft.Col('Game', attr='title')


class GameInfoTable(DefaultDataTable):
    def __init__(self, data, table_id=''):
        super().__init__()
        self.id = table_id
        self.data = data
        self.room_id = 'game_info_table_room'

        # Create the flask table
        self.table = self.ft_table(self.data, table_id=self.id)

        # Set parameters for DataTables
        self.datatables_attr['ordering'] = False
        self.datatables_attr['info'] = False        # Hide the "Showing n to m of N entries" text

        # Set default order colum
        # self.datatables_attr['order'] = [[0, 'desc']]
        # Disable ordering for the icon column
        # self.datatables_attr['columnDefs'] = [{'targets': [0], 'orderable': False}]

    class ft_table(ExtendedFlaskTable):
        """Set up the flask table definitions"""
        # Specify the columns in the table
        # The name in attr must match a dict key in the data passed to the table. If attr is not
        # specified the parameter assigned to ft.Col must match the dict key in the data
        key = ft.Col('Key', attr='key')
        value = ft.Col('Value', attr='value')

        def __init__(self, *args, **kwargs):
            # Passing all arguments unchanged to the parent class
            super().__init__(*args, **kwargs)

            # Add class for hiding the table header
            self.classes.append('hide_table_header')
