##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# # Main class for handling board generator

# #Import Modules
import os, sys
import pickle
import math
import pygame
from pygame.locals import *
import numpy as np
import tkinter as tk
from tkinter import filedialog
from tkinter import simpledialog
from tkinter import messagebox

from Options_IE import *

main_dir = os.path.split(os.path.abspath(__file__))[0]
icon_dir = os.path.join(main_dir, 'icons')
data_dir = os.path.join(main_dir, 'data')
img_dir  = os.path.join(main_dir, 'image')
color_dir  = os.path.join(main_dir, 'colors')


class Interface():
    draw_modes = ['free','line','box','box_f','circle','circle_f'] #'select'}
    draw_cursors = [[(8, 8), (4, 4), ( 24, 24, 24,231,231, 24, 24, 24), (0, 0, 0, 0, 0, 0, 0, 0)]]
    
    def __init__(self, rgb = False):
        if rgb:
            self.mode = 'rgb'
            self.options = Options(True)
            self.init_rgb_palett()
            self.active_color = 0
            self.pal_num = 0
        else:
            self.mode = 'ega'
            self.options = Options()
            self.active_color = 2
            
        self.icon_bg = self.options.icon_bg
        self.show_help_screen = False
        self.alpha = 2
        
        self.active_color_icon = 0
        self.active_mode = 0
        self.valid_start = False
        
        self.icon_map = np.zeros( (self.options.icon_pixel_x, self.options.icon_pixel_y), dtype=np.uint32 )
        self.actual_map = self.icon_map.copy()
        self.map_list = []
        self.update_undo_map(clear = True)
        
        pygame.init()
        pygame.mouse.set_visible(True)
        
        pygame.mouse.set_cursor(*self.draw_cursors[0])
        
        display_info = pygame.display.Info()
        self.screen = pygame.display.set_mode(self.options.size, DOUBLEBUF | HWSURFACE )
        
        self.save_warnings = self.options.save_warnings
        self.changes = False
  
        self.file_opt = options_file = {}
        options_file['defaultextension'] = '.pkl'
        options_file['initialdir'] = icon_dir
        
        self.file_opt_png = options_file_png = {}
        options_file_png['defaultextension'] = '.png'
        options_file_png['initialdir'] = img_dir
        
        self.file_opt_txt = options_file_txt = {}
        options_file_txt['defaultextension'] = '.txt'
        options_file_txt['initialdir'] = color_dir
        
        self.root = tk.Tk()
        self.root.withdraw()
        
        self.load_draw_images()        
        self.update_caption()
        self.tooltip = False
        self.tooltip_special = False
        self.tooltip_msg = ''
        
        self.font = pygame.font.Font( self.options.font, 13 )
        
        
    def init_rgb_palett(self):
        self.rgb_pal = []
        for r in range(0,8):
            red = int(r*255/7)
            for g in range(0,8):
                green = int(g*255/7)
                for b in range(0,8):
                    blue = int(b*255/7)
                    pal_col = ((red<<16) + (green<<8) + blue)
                    self.rgb_pal.append(pal_col)
                          
            
    def load_draw_images(self):        
        self.images = []
        for img in range(0, len(self.draw_modes)):
            file_name = str(img)+'.png'
            fullname = os.path.join(data_dir, file_name)
            try:
                image = pygame.image.load(fullname)
            except pygame.error:
                print ('Cannot load image:', fullname)
                raise SystemExit(str(geterror()))
            image = image.convert()

            self.images.append(image)
            
        fullname = os.path.join(data_dir, self.options.tooltip_screen)
        try:
            self.tt_image = pygame.image.load(fullname)
        except pygame.error:
            print ('Cannot load image:', fullname)
            raise SystemExit(str(geterror()))
        
    def draw_draw_modes(self):        
        x_size = self.options.draw_options_h * len(self.draw_modes)
        y_size = self.options.draw_options_h
        surface_size = (x_size, y_size)
        mode_surface = pygame.Surface(surface_size)
        
        image_pos = [0,0]
        for image in self.images:
            mode_surface.blit(image, image_pos)
            image_pos[0] += self.options.draw_options_h
        
        rect = pygame.Rect(self.active_mode*y_size,0,y_size-2,y_size-2)
        pygame.draw.rect(mode_surface, self.options.highlight, rect, 2)
       
        self.screen.blit( mode_surface, self.options.draw_options )
        
    def update_undo_map(self, clear = False):
        if clear:
            del self.map_list[:]
            self.map_id = 0
            self.numchanges = 0

        while len(self.map_list) > (self.map_id+1):
            del self.map_list[(len(self.map_list)-1)]
            
        self.map_list.append(self.icon_map.copy())
        if len(self.map_list) > self.options.undo_depth:
            del self.map_list[0]
        self.map_id = (len(self.map_list)-1)
        self.numchanges = 0 
        
    def undo(self):
        if self.numchanges > 0:
            self.update_undo_map()
        if len(self.map_list) > 0:
            if self.map_id > 0 and self.map_id <= len(self.map_list):
                self.map_id -= 1
                self.icon_map = []
                self.icon_map = self.map_list[self.map_id].copy()
        
    def redo(self):
        if len(self.map_list) > (self.map_id+1) and self.map_id >=0:
            self.map_id += 1
            self.icon_map = []
            self.icon_map = self.map_list[self.map_id].copy()
            
        
    def get_color(self, num):
        if self.mode == 'ega':
            r = (num >> 6)&3
            g = (num >> 4)&3
            b = (num >> 2)&3
            a = num&3
            red   = int( ( r * a/3 *85) + ( self.icon_bg[0]*(3-a) /3 ) )
            green = int( ( g * a/3 *85) + ( self.icon_bg[1]*(3-a) /3 ) )
            blue  = int( ( b * a/3 *85) + ( self.icon_bg[2]*(3-a) /3 ) )
        else:
            red = (num >> 16)&255
            green = (num >> 8)&255
            blue = (num >> 0)&255
        
        return((red, green, blue))
        
    def get_color_from_palett(self, color_icon):
        if self.mode == 'ega':
            red = (((color_icon >> 4) & 3) << 6)
            green = (((color_icon >> 2) & 3) << 4)
            blue = (color_icon&3)<<2
            color = (red + green + blue + self.alpha)
        else:
            if (color_icon+int(64*self.pal_num)) >= len(self.rgb_pal):
                color_icon = int((len(self.rgb_pal)-1)%64)
                self.active_color_icon = color_icon
                self.pal_num = int((len(self.rgb_pal)/64))
            color = self.rgb_pal[color_icon+int(64*self.pal_num)]
        return( color  )

    def set_palette_and_active_color(self, color_id):
        if self.mode == 'ega':
            self.alpha = int(color_id%4)
            self.active_color_icon = color_id >> 2
            self.active_color = color_id
        else:
            self.active_color = color_id
            self.active_color_icon = 0
            id = 0
            for color in self.rgb_pal:
                if color == color_id:
                    self.active_color_icon = int(id%64)
                    self.pal_num = int(id/64)
                    break
                id += 1
        self.update_caption()
        
        
    def draw_all_palettes(self):
        if self.mode=='rgb':
            mod = 4*int(self.pal_num/4)
        else: mod = 0
        
        p0 = 0 + mod
        p1 = 1 + mod
        p2 = 2 + mod
        p3 = 3 + mod
            
        if (self.mode=='ega' and self.alpha == 0) or (self.mode=='rgb' and self.pal_num%4 == 0):
            self.draw_palette(p0, self.options.set_a0_palette, 64, 0.5)
            self.draw_palette(p1, self.options.set_a1_palette, -1, 0.5)
            self.draw_palette(p2, self.options.set_a2_palette, -1, 0.5)
            self.draw_palette(p3, self.options.set_a3_palette, -1, 0.5)
        elif (self.mode=='ega' and self.alpha == 1) or (self.mode=='rgb' and self.pal_num%4 == 1):
            self.draw_palette(p0, self.options.set_a0_palette, -1, 0.5)
            self.draw_palette(p1, self.options.set_a1_palette, 64, 0.5)
            self.draw_palette(p2, self.options.set_a2_palette, -1, 0.5)
            self.draw_palette(p3, self.options.set_a3_palette, -1, 0.5)
        elif (self.mode=='ega' and self.alpha == 2) or (self.mode=='rgb' and self.pal_num%4 == 2):
            self.draw_palette(p0, self.options.set_a0_palette, -1, 0.5)
            self.draw_palette(p1, self.options.set_a1_palette, -1, 0.5)
            self.draw_palette(p2, self.options.set_a2_palette, 64, 0.5)
            self.draw_palette(p3, self.options.set_a3_palette, -1, 0.5)
        else:
            self.draw_palette(p0, self.options.set_a0_palette, -1, 0.5)
            self.draw_palette(p1, self.options.set_a1_palette, -1, 0.5)
            self.draw_palette(p2, self.options.set_a2_palette, -1, 0.5)
            self.draw_palette(p3, self.options.set_a3_palette, 64, 0.5)            
        
        
    def draw_palette(self, alpha, place, hl = 0, resize = 1 ):
        size = int(resize * self.options.icon_size_pal)
        border = int(resize * self.options.icon_border_pal)
        side_size = 8*(size+border)+border
        surface_size = (side_size , side_size )
        icon_surface = pygame.Surface( surface_size )

        a = alpha
        if a < 0: a = 0
        if a > 3: a = 3
        
        palett = []
        
        if self.mode == 'ega':
            for r in range(0, 4):
                for g in range(0, 4):
                    for b in range(0, 4):
                        red   = int( ( r * a/3 *85) + ( self.icon_bg[0]*(3-a) /3 ) )
                        green = int( ( g * a/3 *85) + ( self.icon_bg[1]*(3-a) /3 ) )
                        blue  = int( ( b * a/3 *85) + ( self.icon_bg[2]*(3-a) /3 ) )
                        co = (red, green, blue)
                        palett.append(co)
        else:
            for loc_id in range(0,64):
                id = loc_id + (64*int(alpha))
                if (id) < len(self.rgb_pal):
                    co = (((self.rgb_pal[id]>>16)&255),((self.rgb_pal[id]>>8)&255),((self.rgb_pal[id])&255))
                else: co = (0,0,0)
                palett.append(co)
                    
        for x in range(0, 8):
            for y in range(0, 8):
                rect = pygame.Rect((size+border)*x+border,(size+border)*y+border,size,size)
                index = 8*x+y
                pygame.draw.rect(icon_surface, palett[index], rect)
            
        if hl>-1 and hl<64:
            hl_y = (hl)%8
            hl_x = int((hl)/8)
            rect = pygame.Rect((size+border)*hl_x,(size+border)*hl_y,size+2*border-2,size+2*border-2)
            pygame.draw.rect(icon_surface, self.options.highlight, rect, border+1)
        elif hl == 64:
            rect = pygame.Rect(0,0,side_size-2,side_size-2)
            pygame.draw.rect(icon_surface, self.options.highlight, rect, 2*(border+1))
            
            
        self.screen.blit( icon_surface, place )
        
    def draw_icon_bg(self):
        if (self.icon_bg[0] > 127): r = 0 
        else: r = 255
        if (self.icon_bg[1] > 127): g = 0 
        else: g = 255
        if (self.icon_bg[2] > 127): b = 0 
        else: b = 255
        
        surface_size = self.options.rgb_bg_text_size
        rgb_surface = pygame.Surface(surface_size)
        
        rgb_surface.fill( (self.icon_bg[0], 0,0) )
        text = 'R:' + str(self.icon_bg[0])
        rgb_surface.blit( self.font.render(text, 1, (r,255,255)), (4,8) )       
        self.screen.blit( rgb_surface, self.options.set_r_text )   
        
        rgb_surface.fill( (0, self.icon_bg[1],0) )
        text = 'G:' + str(self.icon_bg[1])
        rgb_surface.blit( self.font.render(text, 1, (255,g,255)), (4,8) )       
        self.screen.blit( rgb_surface, self.options.set_g_text )   
        
        rgb_surface.fill( (0,0,self.icon_bg[2]) )
        text = 'B:' + str(self.icon_bg[2])
        rgb_surface.blit( self.font.render(text, 1, (255,255,b)), (4,8) )       
        self.screen.blit( rgb_surface, self.options.set_b_text )   
        
        
        icon_surface = pygame.Surface( self.options.rgb_bg_size )
        icon_surface.fill( self.icon_bg)
        text = 'RGB(' + str(self.icon_bg[0]) + ',' + str(self.icon_bg[1]) + ',' +str(self.icon_bg[2]) + ')'
        icon_surface.blit( self.font.render(text, 1, (r,g,b)), (4,8) )
        self.screen.blit( icon_surface, self.options.set_icon_bg )
        
    def draw_large_icon(self):
        x = self.options.icon_pixel_x
        y = self.options.icon_pixel_y
        size = self.options.icon_size_draw
        border = self.options.icon_border_draw
        
        x_size = x*size + (x+1)*border
        y_size = y*size + (y+1)*border
        surface_size = (x_size, y_size)
        icon_surface = pygame.Surface(surface_size)
        
        for a in range(0,x+1):
            rect = pygame.Rect(a*(size+border),0,border,y_size)
            pygame.draw.rect(icon_surface, self.options.icon_border_color, rect)
            
        for b in range(0,y+1):
            rect = pygame.Rect(0,b*(size+border),x_size, border)
            pygame.draw.rect(icon_surface, self.options.icon_border_color, rect)
            
        for a in range(0,x):
            for b in range(0,y):
                rect = pygame.Rect(border + a*(size+border), border + b*(size+border), size, size)
                color = self.get_color(self.icon_map[a,b])
                pygame.draw.rect(icon_surface, color, rect)
       
        self.screen.blit( icon_surface, self.options.set_icon )   
        
    def draw_normal_icon(self):
        surface_size = self.options.preview_text_size
        rgb_surface = pygame.Surface(surface_size)
        
        text = 'Preview:'
        rgb_surface.blit( self.font.render(text, 1, (128,128,128)), (4,8) )
       
        self.screen.blit( rgb_surface, self.options.set_preview_text )   
        
        x = self.options.icon_pixel_x
        y = self.options.icon_pixel_y
        
        surface_size_y = max(2*y, self.options.rgb_size)
        surface_size = (4*x, surface_size_y)
        icon_surface = pygame.Surface(surface_size)
        if self.options.rgb_size > y:
            y_offset = int((self.options.rgb_size - 2*y)/2)
        else:
            y_offset = 0
            
        for a in range(0,x):
            for b in range(0,y):
                color = self.get_color(self.icon_map[a,b])
                icon_surface.set_at((a, b+y_offset), color)
                
                icon_surface.set_at((2*x +     2*a, 2*b     + y_offset), color)
                icon_surface.set_at((2*x + 1 + 2*a, 2*b     + y_offset), color)
                icon_surface.set_at((2*x +     2*a, 2*b + 1 + y_offset), color)
                icon_surface.set_at((2*x + 1 + 2*a, 2*b + 1 + y_offset), color)
       
        self.screen.blit( icon_surface, self.options.set_preview_icon )   
        
    def draw_pal_info(self):
        info_surface = pygame.Surface( self.options.rgb_bg_size )
        
        max_pal = int((len(self.rgb_pal)-1)/64)+1
        pal_num_low = int(self.pal_num/4)*4+1
        pal_num_high = min(max_pal,pal_num_low+3)
        
        text = 'Palette ' + str(pal_num_low) + ' - ' + str(pal_num_high) + ' of ' + str(max_pal)
        info_surface.blit( self.font.render(text, 1, (128,128,128)), (4,8) )
        self.screen.blit( info_surface, self.options.set_icon_bg )
        
    def update_caption(self):
        output = 'uWars Icon Editor (v1.01): '
        if self.mode == 'ega':
            output += 'Alpha = %d (%d %%) ' % (self.alpha, int(round(self.alpha*33.3)))
        color = self.get_color(self.active_color)
        output +=' Marked color = 0x%x (%d, %d, %d)' %(self.active_color, color[0], color[1], color[2])

        if self.changes:
            output += ' (*)'
        
        pygame.display.set_caption(output)
    
    def save_file(self):        
        file = tk.filedialog.asksaveasfilename(**self.file_opt)   
        if (file != ''):
            self.changes = False
            output = open(file, 'wb')
            pickle.dump(self.icon_bg, output)
            pickle.dump(self.icon_map, output)
            output.close()
            self.changes = False
            self.update_caption()
            
            
    def save_png_file(self):        
        file = tk.filedialog.asksaveasfilename(**self.file_opt_png)   
        if (file != ''):
            x = self.options.icon_pixel_x
            y = self.options.icon_pixel_y
            surface_size = (x,y)
            png_surface = pygame.Surface(surface_size)
            for a in range(0,x):
                for b in range(0,y):
                    color = self.get_color(self.icon_map[a,b])
                    png_surface.set_at((a, b), color)
                
            pygame.image.save(png_surface, file)
            self.changes = False
            self.update_caption()
            
    def load_png_file(self):        
        file = tk.filedialog.askopenfilename(**self.file_opt_png)   
        if (file != ''):
            try:
                image = pygame.image.load( file )
            except pygame.error:
                print ('Cannot load image:', fullname)
                raise SystemExit(str(geterror()))
                
            self.icon_map = []
            self.changes = False
            
            image = image.convert()
            pxarray = pygame.PixelArray(image)
            
            x = pxarray.shape[0]
            y = pxarray.shape[1]
            
            self.icon_map = np.zeros( (x, y), dtype=np.uint32 )
            
            for a in range(0,x):
                for b in range(0,y):
                    color = pxarray[a,b]
                    self.icon_map[a,b] = color
            
            self.options.icon_pixel_x = x
            self.options.icon_pixel_y = y
            self.options.update()
            self.screen = pygame.display.set_mode(self.options.size, DOUBLEBUF | HWSURFACE )
            
            self.update_caption()
            self.update_undo_map(clear = True)

    def import_colors(self, replace = False):
        file = tk.filedialog.askopenfilename(**self.file_opt_txt)
        if file:
            if replace:
                self.rgb_pal = []
                self.pal_num = 0
                self.active_color_icon = 0
                
            with open(file, 'r') as f:        
                for line in f:
                    r,g,b = [int(x) for x in line.split()]
                    pal_col = ((r<<16) + (g<<8) + b)
                    self.rgb_pal.append(pal_col)
        
    def export_colors(self):
        file = tk.filedialog.asksaveasfilename(**self.file_opt_txt)
        if file:
            with open(file, 'w') as f:
                for img in self.rgb_pal:
                    color = self.get_color(img)
                    f.write(str(color[0]))
                    f.write(' ')
                    f.write(str(color[1]))
                    f.write(' ')
                    f.write(str(color[2]))
                    f.write('\n')
        
    def write_text_files(self, cb = 0):
        p    = open('icons/python_snake.txt', 'w')
        c    = open('icons/hw_c_snake.txt', 'w')
        clip = tk.Tk()
        clip.withdraw()
        clip.clipboard_clear()        
        
        
        p_string = '# This code can be copied into your python snake class\n\n'
        p_string +='    icon = np.array( [\n'
        
        c_string = '// This code can be copied into HW snake\n\n'
        c_string +='const uint8_t snake_icon[144] PROGMEM = {\n'
        
        p.write(p_string)
        c.write(c_string)
        
        if cb == 0: clip.clipboard_append(c_string)
        else:       clip.clipboard_append(p_string)
        
        for b in range(0,self.options.icon_pixel_y):
            p_string = '        ['
            c_string = '    '
            for a in range(0,self.options.icon_pixel_x):
                p_string += str(self.icon_map[a,b])
                c_string += str(self.icon_map[a,b])                
                if a < (self.options.icon_pixel_x-1):
                    p_string += ','
                    c_string += ','
                   
            p_string += ']'
            if b < (self.options.icon_pixel_y-1):
                p_string += ',\n'
                c_string += ',\\ \n'
            else:
                p_string += ' ] )\n\n'
                c_string += '\n'
            p.write(p_string)
            c.write(c_string)
            if cb == 0: clip.clipboard_append(c_string)
            else:       clip.clipboard_append(p_string)

        p_string = '    colors = [(' + str(self.icon_bg[0]) + ',' + str(self.icon_bg[1]) + ',' +str(self.icon_bg[2]) + '),(' + str(self.icon_bg[0]) + ',' + str(self.icon_bg[1]) + ',' +str(self.icon_bg[2]) + ')]\n\n'
        p.write(p_string)
        
        c_string = '}; \n\n'
        c_string +='const uint8_t colors[6] PROGMEM = { ' + str(self.icon_bg[0]) + ',' + str(self.icon_bg[1]) + ',' +str(self.icon_bg[2]) +',' + str(self.icon_bg[0]) + ',' + str(self.icon_bg[1]) + ',' +str(self.icon_bg[2] )+' }; \n\n'
        c.write(c_string)
        
        if cb == 0: clip.clipboard_append(c_string)
        else:       clip.clipboard_append(p_string)
        
        p.close()
        c.close()
        
        self.tooltip = True
        self.tooltip_special = True
        self.tooltip_msg = []
        if cb == 0: self.tooltip_msg.append('Put c-code icon on clipboard')
        else: self.tooltip_msg.append('Put python icon on clipboard')
        self.tooltip_msg.append('Wrote c and pyton file (/icon)')
 
    def get_confirmation(self):
        if not self.save_warnings:
            confirmation = True
        elif (self.changes == True):
            confirmation = tk.messagebox.askyesno("Changes made since last save will be lost", "You have unsaved changes that will be lost if you continue. Proceed in any case?")
        else: 
            confirmation = True
        return confirmation

    def load_file(self):
        ask = self.get_confirmation()    
        if ask == True:
            file = tk.filedialog.askopenfilename(**self.file_opt)  
            if (file != ''):
                self.icon_map = []
                
                input = open(file, 'rb' )
                self.icon_bg = pickle.load(input)
                self.icon_map = pickle.load(input)
                input.close()
                
                self.changes = False
                
                self.options.icon_pixel_x = self.icon_map.shape[0]
                self.options.icon_pixel_y = self.icon_map.shape[1]
                self.options.update()
                self.screen = pygame.display.set_mode(self.options.size, DOUBLEBUF | HWSURFACE )
                
                self.update_caption()
                self.update_undo_map(clear = True)
        
    def new_map(self, skip_ask = False):
        if skip_ask:
            ask = True
        else:
            ask = self.get_confirmation()    
        if ask == True:
            self.icon_map = np.zeros( (self.options.icon_pixel_x, self.options.icon_pixel_y), dtype=np.uint8 )
            self.changes = False
            self.update_undo_map(clear = True)
            self.active_color = 0
            self.pal_num = 0
            self.active_color_icon = 0

    def update_active_color(self,x,y):
        obj_x = (int)(np.floor((x-self.options.set_active_palette[0])/(self.options.icon_size_pal+self.options.icon_border_pal)))
        obj_y = (int)(np.floor((y-self.options.set_active_palette[1])/(self.options.icon_size_pal+self.options.icon_border_pal)))
        if (obj_x >= 0) and (obj_x < 8) and (obj_y >= 0) and (obj_y < 8):
            self.set_active_color(obj_y + 8*obj_x, True)
        
    def update_icon(self,x,y, val, ctrl = False, move = False, up = False):
        obj_x = (int)(np.floor((x-self.options.set_icon[0])/(self.options.icon_size_draw+self.options.icon_border_draw)))
        obj_y = (int)(np.floor((y-self.options.set_icon[1])/(self.options.icon_size_draw+self.options.icon_border_draw)))
        if (obj_x >= 0) and (obj_x < self.options.icon_pixel_x) and (obj_y >= 0) and (obj_y < self.options.icon_pixel_y):
            if val==2:
                if not up:
                    replace_color = self.icon_map[obj_x, obj_y]
                    if replace_color != self.active_color:
                        if ctrl:
                            for a in range(0,self.options.icon_pixel_x):
                                for b in range(0,self.options.icon_pixel_y):
                                    if self.icon_map[a,b] == replace_color:
                                        self.icon_map[a,b] = self.active_color
                        else:
                            self.recursive_fill(obj_x, obj_y, replace_color)
                        self.numchanges += (self.options.undo_update_freq+1)
            else:
                if ctrl:
                    if not up:
                        self.set_palette_and_active_color(self.icon_map[obj_x, obj_y])
                else:
                    mode = self.draw_modes[self.active_mode]
                    if mode == 'line' or mode == 'box' or mode == 'box_f' or mode == 'circle' or mode == 'circle_f':
                        if move or up:
                            if self.valid_start:
                                if mode == 'line':
                                    self.add_line(obj_x, obj_y, up)
                                elif mode == 'box':
                                    self.add_box(obj_x, obj_y, up)
                                elif mode == 'box_f':
                                    self.add_box(obj_x, obj_y, up, True)    
                                elif mode == 'circle':
                                    self.add_circle(obj_x, obj_y, up)
                                elif mode == 'circle_f':
                                    self.add_circle(obj_x, obj_y, up, True)
                                if up:
                                    self.numchanges += 2
                        else:
                            self.start_x=obj_x
                            self.start_y=obj_y
                            self.valid_start = True
                            self.actual_map = []
                            self.actual_map = self.icon_map.copy()                          
                    else:
                        if not up:
                            if self.icon_map[obj_x, obj_y] != self.active_color:
                                self.numchanges += 1
                                self.icon_map[obj_x, obj_y] = self.active_color
            self.changes = True
            
            if self.numchanges > self.options.undo_update_freq:
                self.update_undo_map()

    def add_box(self, x, y, done = False, filled = False):    
        self.icon_map = []
        self.icon_map = self.actual_map.copy()
                
        min_x = min(x, self.start_x)
        max_x = max(x, self.start_x)
        min_y = min(y, self.start_y)
        max_y = max(y, self.start_y)
        if filled:
            for x in range(min_x, max_x + 1):
                for y in range(min_y, max_y + 1):
                    self.icon_map[x, y] = self.active_color
        else:
            if (max_x > min_x) and (max_y > min_y):
                self.traverse_line(min_x,min_y,max_x,min_y,True)
                self.traverse_line(min_y,min_x,max_y,min_x,False)
                self.traverse_line(min_y,max_x,max_y,max_x,False)
                self.traverse_line(min_x,max_y,max_x,max_y,True)
            
        if done:
            self.valid_start = False
            self.start_x = x
            self.start_y = y
            self.changes = True  
            self.update_caption()
        
    def add_circle(self, x_in, y_in, done = False, filled = False):
        self.icon_map = []
        self.icon_map = self.actual_map.copy()
        
        radius = int(round(math.sqrt((abs(x_in-self.start_x))**2+abs((y_in-self.start_y))**2)))
        x = 0
        y = radius
        f = 1 - radius
        ddf_x = 1
        ddf_y = -2 * radius
        
        if (self.start_x - radius) >= 0 and (self.start_y - radius) >=0 and (self.start_x + radius)<self.options.icon_pixel_x and (self.start_y + radius) < self.options.icon_pixel_y:
            self.icon_map[self.start_x, self.start_y+radius] = self.active_color
            self.icon_map[self.start_x, self.start_y-radius] = self.active_color
            self.icon_map[self.start_x+radius, self.start_y] = self.active_color
            self.icon_map[self.start_x-radius, self.start_y] = self.active_color
            if filled:
                if radius > 0:
                    self.traverse_line(self.start_x-radius, self.start_y, self.start_x + radius, self.start_y, True)
            
            while ( x < y):
                if f >= 0:
                    y -= 1
                    ddf_y += 2
                    f+= ddf_y
                x += 1
                ddf_x += 2
                f += ddf_x
                
                if filled:
                    if x > 0:
                        self.traverse_line(self.start_x-x, self.start_y + y, self.start_x +x, self.start_y + y, True)
                        self.traverse_line(self.start_x-x, self.start_y - y, self.start_x +x, self.start_y - y, True)
                    else:
                        self.icon_map[self.start_x, self.start_y+y] = self.active_color
                        self.icon_map[self.start_x, self.start_y-y] = self.active_color
                    if y > 0:
                        self.traverse_line(self.start_x-y, self.start_y + x, self.start_x+y, self.start_y + x, True)
                        self.traverse_line(self.start_x-y, self.start_y - x, self.start_x+y, self.start_y - x, True)
                    else:
                        self.icon_map[self.start_x, self.start_y+x] = self.active_color
                        self.icon_map[self.start_x, self.start_y-x] = self.active_color
                else:
                    self.icon_map[self.start_x + x, self.start_y + y] = self.active_color
                    self.icon_map[self.start_x - x, self.start_y + y] = self.active_color
                    
                    self.icon_map[self.start_x + x, self.start_y - y] = self.active_color
                    self.icon_map[self.start_x - x, self.start_y - y] = self.active_color
                    
                    self.icon_map[self.start_x + y, self.start_y + x] = self.active_color
                    self.icon_map[self.start_x - y, self.start_y + x] = self.active_color
                    
                    self.icon_map[self.start_x + y, self.start_y - x] = self.active_color
                    self.icon_map[self.start_x - y, self.start_y - x] = self.active_color
                
            if done:
                self.valid_start = False
                self.start_x = x
                self.start_y = y
                self.changes = True  
                self.update_caption()
            
    def add_line(self, x, y, done = False):
        self.icon_map = []
        self.icon_map = self.actual_map.copy()
         
        if abs(x-self.start_x) > abs (y-self.start_y):
            if x > self.start_x:
                self.traverse_line(self.start_x, self.start_y, x, y, True)
            elif self.start_x > x:
                self.traverse_line(x, y, self.start_x, self.start_y, True)
        else:
            if y > self.start_y:
                self.traverse_line(self.start_y, self.start_x, y, x, False)
            elif self.start_y > y:
                self.traverse_line(y, x, self.start_y, self.start_x, False)                    
                
        if done:
            self.valid_start = False
            self.start_x = x
            self.start_y = y
            self.changes = True  
            self.update_caption()
            
    # Drawing line of walls from point (a,b) to (c,d) if order True, otherwise from (b,a) to (d,c)
    def traverse_line(self, a, b, c, d, order):
        e = a
        done = False
        while (done == False):
            if e > c:
                e = c
            f = b + int(np.floor(((d - b) * (a-e) / (a - c) ) + 0.5 ))
            if order == True:
                self.icon_map[e, f] = self.active_color
            else:
                self.icon_map[f, e] = self.active_color
            if e >= c:
                done = True
            e += 1
         
    def recursive_fill(self, x, y, color):
        if x<0 or y <0 or x>(self.options.icon_pixel_x-1) or y>(self.options.icon_pixel_y-1):
            return
        elif not (self.icon_map[x,y] == color):
            return
        else:
            self.icon_map[x,y] = self.active_color
            self.recursive_fill(x-1, y, color)
            self.recursive_fill(x+1, y, color)
            self.recursive_fill(x, y-1, color)
            self.recursive_fill(x, y+1, color)
            
    def update_alpha(self,x,y, scroll = False):
        obj_x = (int)(np.floor((x-self.options.set_a0_palette[0])/(self.options.palette_size*0.5)))
        obj_y = (int)(np.floor((y-self.options.set_a0_palette[1])/(self.options.palette_size*0.5)))
        if (obj_x >= 0) and (obj_x <= 1) and (obj_y >= 0) and (obj_y <= 1):
            if self.mode == 'ega':
                self.set_alpha((2*obj_y+obj_x), True)
            else:
                self.pal_num = int(self.pal_num/4)*4 + 2*obj_y + obj_x
                self.active_color = self.get_color_from_palett(self.active_color_icon)
            self.update_caption()

    def update_mode(self,x,y, inc = 0, scroll = False):
        obj_x = (int)(np.floor((x-self.options.draw_options[0])/(self.options.draw_options_h)))
        obj_y = (int)(np.floor((y-self.options.draw_options[1])/(self.options.draw_options_h)))
        if (obj_x >= 0) and (obj_x < len(self.draw_modes)) and (obj_y >= 0) and (obj_y < 1):
            if scroll:
                self.active_mode += inc
                if self.active_mode < 0: self.active_mode = 0
                elif self.active_mode >= len(self.draw_modes): self.active_mode = len(self.draw_modes)-1
            else:
                self.active_mode = obj_x
        
    def mouse_click(self, val = 0, ctrl = False, move = False, up = False):
        x, y = pygame.mouse.get_pos()
        self.update_icon(x,y,val, ctrl, move, up)
        if not up:
            self.update_active_color(x,y)
            self.update_alpha(x,y)
            self.update_mode(x,y)
        if self.mode == 'ega' and up:
            if (x >= self.options.set_r_text[0] and x < (self.options.set_r_text[0] + self.options.palette_size/3) and
                y >= self.options.set_r_text[1] and y < (self.options.set_r_text[1]+self.options.rgb_size)):        
                self.get_input_color(0)
            elif (x >= self.options.set_g_text[0] and x < (self.options.set_g_text[0] + self.options.palette_size/3) and
                y >= self.options.set_g_text[1] and y < (self.options.set_g_text[1]+self.options.rgb_size)):        
                self.get_input_color(1)
            elif (x >= self.options.set_b_text[0] and x < (self.options.set_b_text[0] + self.options.palette_size/3) and
                y >= self.options.set_b_text[1] and y < (self.options.set_b_text[1]+self.options.rgb_size)):        
                self.get_input_color(2)
        
    def draw_background(self):
        bg = pygame.Surface(self.screen.get_size())
        bg.fill( self.options.background )
        self.screen.blit(bg, (0,0))
        
    def draw(self):
        self.draw_background()
        self.draw_draw_modes()
        self.draw_all_palettes()
        if self.mode == 'ega':
            self.draw_icon_bg()
            self.draw_palette(self.alpha, self.options.set_active_palette, self.active_color_icon)
        else:
            self.draw_pal_info()
            self.draw_palette(self.pal_num, self.options.set_active_palette, self.active_color_icon)
        self.draw_large_icon()
        self.draw_normal_icon()
        self.draw_tooltip()
        self.draw_help_screen()
        pygame.display.flip()
        
    def set_alpha(self, inc, abs = False, flip = False):
        if self.mode == 'ega':
            if abs:
                self.alpha = inc
            else:
                if inc > 0: inc = 1
                else: inc = -1
                self.alpha += inc
                
            if self.alpha < 0: self.alpha = 0
            if self.alpha > 3: self.alpha = 3
            self.active_color = (self.active_color >> 2)
            self.active_color = (self.active_color << 2) + self.alpha
        else:
            if flip:
                self.pal_num += (inc*4)
            elif abs:
                self.pal_num = int(self.pal_num/4)*4 + inc
            else:
                if inc > 0: inc = 1
                else: inc = -1
                self.pal_num += inc
                
            if self.pal_num < 0: self.pal_num = 0
            if self.pal_num > int((len(self.rgb_pal)-1)/64): self.pal_num = int((len(self.rgb_pal)-1)/64)
            self.active_color = self.get_color_from_palett(self.active_color_icon)
        self.update_caption()
        
    def set_active_color(self, inc, abs = False):
        if abs:
            self.active_color_icon = inc
        else:
            self.active_color_icon += inc
            
        if self.mode == 'ega':
            if self.active_color_icon < 0:
                if self.alpha > 0:
                    self.alpha -= 1
                    self.active_color_icon += 64
                else:
                    self.active_color_icon = 0
            if self.active_color_icon > 63:
                if self.alpha < 3:
                    self.alpha += 1
                    self.active_color_icon -= 64
                else:
                    self.active_color_icon = 63
        else:
            if self.active_color_icon < 0:
                if self.pal_num >0:
                    self.active_color_icon = 63
                    self.pal_num -= 1
                else:
                    self.active_color_icon = 0
            if self.active_color_icon > 63:
                if self.pal_num < int((len(self.rgb_pal)-1)/64):
                    self.active_color_icon = 0
                    self.pal_num += 1
                else:
                    self.active_color_icon = 63
                
        self.active_color = self.get_color_from_palett(self.active_color_icon)
        self.update_caption()    
        
    def set_color(self, index, inc, abs = False):
        if abs:
            self.icon_bg[index] = inc
        else:
            self.icon_bg[index] += inc
        if self.icon_bg[index] < 0: self.icon_bg[index] = 0
        if self.icon_bg[index] > 255: self.icon_bg[index] = 255
        self.update_caption()
        
    def get_input_color(self, id):        
        local_root = tk.Tk()
        local_root.withdraw
        local_root.lift()
        local_root.grab_set()
        local_root.initial_focus = self.root
        local_root.focus_force()
            
        rgb = RGBDialog(local_root, "Background")
        local_root.destroy()
        if rgb.result:
            try:
                r = int(rgb.result[0])
                g = int(rgb.result[1])
                b = int(rgb.result[2])
            except TypeError:
                print('No value from input box, not updating background')
            else:
                self.set_color(0, r, True)
                self.set_color(1, g, True)
                self.set_color(2, b, True)
            
     
    def area(self,x,y,id):
        if id == 'alpha':
            if (x >= self.options.set_a0_palette[0] and x < (self.options.set_a0_palette[0] + self.options.palette_size) and
                y >= self.options.set_a0_palette[1] and y < (self.options.set_a0_palette[1]+self.options.palette_size)):        
                return True
        elif id == 'pal':
            if (x >= self.options.set_active_palette[0] and x < (self.options.set_active_palette[0] + self.options.palette_size) and
                y >= self.options.set_active_palette[1] and y < (self.options.set_active_palette[1]+self.options.palette_size)):
                return True
        return False
        
    def scroll_mouse(self, inc):
        x, y = pygame.mouse.get_pos()
        self.update_mode(x,y,inc,True)
        
        if self.area(x,y,'alpha'):
            self.set_alpha(inc)
        elif self.area(x,y,'pal'):
            self.set_active_color(inc)
        elif (x >= self.options.set_r_text[0] and x < (self.options.set_r_text[0] + self.options.palette_size/3) and
                y >= self.options.set_r_text[1] and y < (self.options.set_r_text[1]+self.options.rgb_size)):        
            self.set_color(0,inc)
        elif (x >= self.options.set_g_text[0] and x < (self.options.set_g_text[0] + self.options.palette_size/3) and
                y >= self.options.set_g_text[1] and y < (self.options.set_g_text[1]+self.options.rgb_size)):        
            self.set_color(1,inc)
        elif (x >= self.options.set_b_text[0] and x < (self.options.set_b_text[0] + self.options.palette_size/3) and
                y >= self.options.set_b_text[1] and y < (self.options.set_b_text[1]+self.options.rgb_size)):        
            self.set_color(2,inc)
        else:    
            obj_x = (int)(np.floor((x-self.options.set_icon[0])/(self.options.icon_size_draw+self.options.icon_border_draw)))
            obj_y = (int)(np.floor((y-self.options.set_icon[1])/(self.options.icon_size_draw+self.options.icon_border_draw)))
            if (obj_x >= 0) and (obj_x < self.options.icon_pixel_x) and (obj_y >= 0) and (obj_y < self.options.icon_pixel_y):
                replace_color = self.icon_map[obj_x, obj_y]
                old_color = replace_color
                if inc > 0 : replace_color += 4
                else: replace_color -= 4
                if replace_color < 0: 
                    replace_color += 4
                    if replace_color > 0:
                        replace_color += 251
                elif replace_color > 255: 
                    replace_color -= 4
                    if replace_color < 255:
                        replace_color -= 251
                self.set_palette_and_active_color(replace_color)
                if abs(inc) > 1:
                    for a in range(0,self.options.icon_pixel_x):
                        for b in range(0,self.options.icon_pixel_y):
                            if self.icon_map[a,b] == old_color:
                                self.icon_map[a,b] = replace_color
                    self.numchanges += (self.options.undo_update_freq+1)
                else:
                    self.icon_map[obj_x, obj_y] = replace_color
                    self.numchanges += 1
                if self.numchanges > self.options.undo_update_freq:
                    self.update_undo_map()
                    
        
    def draw_help_screen( self ):
        if self.show_help_screen:            
            try:
                image = pygame.image.load( self.options.board_generator_help_screen )
            except pygame.error:
                print ('Cannot load image:', fullname)
                raise SystemExit(str(geterror()))
                
            x = int( self.screen.get_width()/2 - image.get_width()/2 )
            y = int( self.screen.get_height()/2 - image.get_height()/2 )
            self.screen.blit( image, (x,y) )
            
    def draw_tooltip(self ):
        if self.tooltip:
            if self.tooltip_special:
                image = self.tt_image.copy()
                x = int( self.screen.get_width()/2 - image.get_width()/2 )
                y = int( self.screen.get_height()/2 - image.get_height()/2 )                    
                self.tooltip_msg
                image.blit( self.font.render(self.tooltip_msg[0], 1, (128,128,128)), (4,8) )
                image.blit( self.font.render(self.tooltip_msg[1], 1, (128,128,128)), (4,25) )
                show = True
            else:
                tt_text = 'Debug testing'
                show = False
            
            if show:
                self.screen.blit( image, (x,y) )
        
    def toggle_help_screen(self):
        self.show_help_screen = not self.show_help_screen
        

    def toggle_draw_mode(self, back = False):
        self.active_mode += 1
        if self.active_mode >= len(self.draw_modes):
            self.active_mode = 0
        
class RGBDialog(tk.simpledialog.Dialog):

    def body(self, master):
        tk.Label(master, text="Red:").grid(row=0)
        tk.Label(master, text="Green:").grid(row=1)
        tk.Label(master, text="Blue:").grid(row=2)
        
        self.e1 = tk.Entry(master)
        self.e2 = tk.Entry(master)
        self.e3 = tk.Entry(master)
        
        self.e1.grid(row=0, column = 1)
        self.e2.grid(row=1, column = 1)
        self.e3.grid(row=2, column = 1)
                
        return self.e1
        
        
    def apply(self):
        red   = int(self.e1.get())
        green = int(self.e2.get())
        blue  = int(self.e3.get())
        self.result = red, green, blue
  