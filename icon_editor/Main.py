##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

# Main loop

#Import Modules
import pygame
from pygame.locals import *
if not pygame.font: print ('Warning, fonts disabled')

from Interface import *

def main(vga = False):   
    interface = Interface(vga)
    clock = pygame.time.Clock()
    going = True
    
    time = clock.get_time()
    
    # Main board generator loop
    while going:
        clock.tick(50)
        events = pygame.event.get()
        
        if interface.tooltip_special == True:
            time_special += clock.get_time()
            if time_special > 4000:
                interface.tooltip_special = False
        else:
            if events: 
                time = 0
                interface.tooltip = False
            time += clock.get_time()        
            if time > 2000:
                interface.tooltip = True
      
        # All input controls. Numbers to change walltype
        for event in events:
            if event.type == KEYDOWN:   
                if event.key == K_ESCAPE:
                    ask = interface.get_confirmation()
                    if ask == True:
                        going = False
                elif event.key == K_d:    
                    interface.toggle_draw_mode()  
                elif event.key == K_s and pygame.key.get_mods() & pygame.KMOD_CTRL:
                    if interface.mode == 'ega':       
                        interface.save_file()
                    else:
                        interface.save_png_file()
                elif event.key == K_i and pygame.key.get_mods() & pygame.KMOD_CTRL:
                    if interface.mode == 'ega':
                        interface.save_png_file()
                    else:
                        interface.import_colors()
                elif event.key == K_r and pygame.key.get_mods() & pygame.KMOD_CTRL:
                    if interface.mode == 'rgb':
                        interface.import_colors(True)
                elif event.key == K_e and pygame.key.get_mods() & pygame.KMOD_CTRL:
                    if interface.mode == 'rgb':
                        interface.export_colors()    
                elif event.key == K_o and pygame.key.get_mods() & pygame.KMOD_CTRL:
                    if interface.mode == 'ega':
                        interface.load_file()
                    else:
                        interface.load_png_file()
                elif event.key == K_n and pygame.key.get_mods() & pygame.KMOD_CTRL:     
                    interface.new_map()
                elif event.key == K_z and pygame.key.get_mods() & pygame.KMOD_CTRL:     
                    interface.undo()
                elif event.key == K_y and pygame.key.get_mods() & pygame.KMOD_CTRL:     
                    interface.redo()
                elif event.key == K_h:    
                    interface.toggle_help_screen()
                elif event.key == K_p:
                    if interface.mode == 'ega':
                        interface.write_text_files(1)
                        time_special = 0
                elif event.key == K_c:
                    if interface.mode == 'ega':
                        interface.write_text_files(0)
                        time_special = 0
                elif event.key == K_0 or event.key == K_KP0:
                    interface.set_alpha(0, True)
                elif event.key == K_1 or event.key == K_KP1:
                    interface.set_alpha(1, True)
                elif event.key == K_2 or event.key == K_KP2:
                    interface.set_alpha(2, True)
                elif event.key == K_3 or event.key == K_KP3:
                    interface.set_alpha(3, True)
                elif event.key == K_r:
                    if interface.mode == 'ega':
                        interface.get_input_color(0)
                elif event.key == K_g:
                    if interface.mode == 'ega':
                        interface.get_input_color(1)
                elif event.key == K_b:
                    if interface.mode == 'ega':
                        interface.get_input_color(2)
                elif (event.key == K_PLUS) or (event.key == K_KP_PLUS):
                    if interface.mode == 'rgb':
                        interface.set_alpha(1, True, True)
                elif (event.key == K_MINUS) or (event.key == K_KP_MINUS):
                    if interface.mode == 'rgb':
                        interface.set_alpha(-1, True, True) 
                    
            if event.type == QUIT:
                ask = interface.get_confirmation()
                if ask == True:
                    going = False
                
            if event.type == MOUSEBUTTONDOWN:
                if pygame.mouse.get_pressed()==(1,0,0):
                    if pygame.key.get_mods() & pygame.KMOD_CTRL:
                        interface.mouse_click(0, True)
                    else:
                        interface.mouse_click(0)
                elif pygame.mouse.get_pressed()==(0,0,1):
                    if pygame.key.get_mods() & pygame.KMOD_CTRL:
                        interface.mouse_click(2, True)
                    else:
                        interface.mouse_click(2)
                elif event.button == 4 and pygame.key.get_mods() & pygame.KMOD_CTRL: interface.scroll_mouse(10)
                elif event.button == 5 and pygame.key.get_mods() & pygame.KMOD_CTRL: interface.scroll_mouse(-10)     
                elif event.button == 4 : interface.scroll_mouse(1)
                elif event.button == 5 : interface.scroll_mouse(-1)   
                    
            if event.type == MOUSEMOTION:
                if pygame.mouse.get_pressed()==(1,0,0):
                    interface.mouse_click(0, move = True)
                    
                    
            if event.type == MOUSEBUTTONUP:
                if event.button == 1:
                    interface.mouse_click(0, move = True, up = True)
                        
        interface.draw()        
                        
    pygame.quit()
    


           