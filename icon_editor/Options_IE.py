##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

 
        
class Options():
    def __init__(self, rgb = False):
        #configure these parameters:
        if rgb:
            self.mode             = 'rgb'
            self.icon_size_draw   = 13
            self.icon_border_draw =  2
            self.icon_size_pal    = 22
            self.icon_border_pal  =  3
            
            self.border_size      = 30
            self.rgb_size         = 25
            
            self.icon_pixel_x     = 30
            self.icon_pixel_y     = 30  
            
            self.icon_bg          =  [0, 255,0] # not implemented atm
        else:
            self.mode             = 'ega'
            self.icon_size_draw   = 22
            self.icon_border_draw =  3
            self.icon_size_pal    = 22
            self.icon_border_pal  =  3
            
            self.border_size      = 30
            self.rgb_size         = 25
            
            self.icon_pixel_x     = 12
            self.icon_pixel_y     = 12  
            
            self.icon_bg          = [63,63,63] 
        
        self.save_warnings     = True  
        
        self.icon_border_color = (127,127,127)   
        self.highlight         = (255,255,255)
          # must use [] since program can change values
        self.background        = (0,0,0)   
        
        self.undo_depth        = 100
        self.undo_update_freq  = 1
        
        self.update()
        
        
    def update(self):    
        #do not configure the ones below
        if self.mode == 'rgb':
            self.board_generator_help_screen = "help_rgb.png"
        else:
            self.board_generator_help_screen = "help.png"
        self.font = 'novem___.ttf'
        self.tooltip_screen = "tooltip.png"
        
        self.draw_options       = (self.border_size, self.border_size)
        self.draw_options_h     = 30
        self.first_row          = (self.border_size * 2 + self.draw_options_h )
        self.set_icon           = (self.border_size, self.first_row)
        self.second_row         = self.first_row * 2 + self.icon_pixel_y * self.icon_size_draw + (self.icon_pixel_y+1)*self.icon_border_draw
        self.set_preview_text   = (self.border_size, self.second_row)
        self.icon_pre_offset    = 80
        self.preview_text_size  = (self.icon_pre_offset, self.rgb_size)
        self.set_preview_icon   = (self.border_size + self.icon_pre_offset, self.second_row)
        self.set_preview_icon_2 = (self.border_size + self.icon_pre_offset + 2*self.icon_pixel_x, self.second_row)
        
        self.second_col         = max(2*self.border_size + self.icon_pixel_x * self.icon_size_draw + (self.icon_pixel_x+1)*self.icon_border_draw,
                                      2*self.border_size + self.icon_pre_offset + 4 * self.icon_pixel_x ) 
        self.set_active_palette = (self.second_col,self.first_row)
        self.palette_size       = 8*self.icon_size_pal+7*self.icon_border_pal
        
        self.rgb_bg_size        = (self.palette_size, self.rgb_size)
        self.rgb_bg_text_size   = (1/3*self.palette_size, self.rgb_size)
        self.set_r_text         = (self.second_col+self.icon_border_pal,self.first_row+self.border_size+self.palette_size)
        self.set_g_text         = (self.second_col+1/3*self.palette_size+self.icon_border_pal,self.first_row+self.border_size+self.palette_size)
        self.set_b_text         = (self.second_col+2/3*self.palette_size+self.icon_border_pal,self.first_row+self.border_size+self.palette_size)
        self.set_icon_bg        = (self.second_col+self.icon_border_pal,self.first_row+self.border_size+self.palette_size+self.rgb_size)
        
        self.set_a0_palette     = (self.second_col,self.first_row+2*self.border_size+self.palette_size+2*self.rgb_size)
        self.set_a1_palette     = (self.second_col + int(0.5*self.palette_size),self.first_row+2*self.border_size+self.palette_size+2*self.rgb_size)
        self.set_a2_palette     = (self.second_col,self.first_row+2*self.border_size+self.palette_size+2*self.rgb_size + int(0.5*self.palette_size))
        self.set_a3_palette     = (self.second_col + int(0.5*self.palette_size),self.first_row+2*self.border_size+self.palette_size+2*self.rgb_size + int(0.5*self.palette_size))
        
        self.window_size_x      = self.second_col + self.palette_size + self.border_size
        self.window_size_y      = max((self.first_row+3*self.border_size+2*self.palette_size+2*self.rgb_size),
                                      (self.border_size + 2 * self.icon_pixel_y + self.second_row))
        self.size               = (self.window_size_x, self.window_size_y)

        
        