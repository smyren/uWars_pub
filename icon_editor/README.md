# How to use the Icon Editor

{- TODO: Explain a bit more about the differences -}

## RGB Image Editor

```
\icon_editor\run_rgb.bat
```

![rgb_editor](icon_editor/doc/RGB_image_editor.png "RGB Image Editor")


## EGA Icon Editor

```
\icon_editor\run.bat
```

![ega_editor](icon_editor/doc/EGA_icon_editor.png "EGA Icon Editor")
