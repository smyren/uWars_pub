/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/io.h>
#include <stdio.h>
#include <math.h>
//#include <inttypes.h>
#include <stdlib.h>
#include <stdint.h>
#include "adc.h"

/*********************************************************************************
	ADC setup
*********************************************************************************/
void adc_setup(void)
{
	ADCA.CTRLB = (1<<ADC_CONMODE_bp) | 1<<ADC_FREERUN_bp | ADC_RESOLUTION_12BIT_gc;		//Set ADC Free run conversion mode and resolution; Signed Diff, 12Bit
	
	ADCA.CH0.CTRL = ADC_CH_INPUTMODE_DIFF_gc;								//Set CHANNEL0 conversion mode; Diff
	ADCA.CH0.MUXCTRL = ADC_CH_MUXPOS_PIN1_gc | ADC_CH_MUXNEG_PIN2_gc; 		//ADC1 as NEG and ADC2 as POS
	
	ADCA.REFCTRL = ADC_REFSEL_AREFA_gc;										//Set reference; Ext Ref on PA0
	ADCA.PRESCALER =  ADC_PRESCALER_DIV256_gc;								//Set ADC clock speed and conversion time. fsampling = 32MHz/256 -> 125ks 
																			//max zin = ca. 10k
	ADCA.CALL = 0x44;														//Load ADC calibration values from Signature Row
	ADCA.CALH = 0x04;
	ADCA.CTRLA = 0x01;														//Enable ADC

}

