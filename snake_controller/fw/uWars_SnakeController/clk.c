/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "clk.h"

uint8_t rtc_time = 0;

/*********************************************************************************
System Clock setup
*********************************************************************************/
void clk_init(void)
 {
 
 	/*Start the external oscillator, 32.768kHz crystal*/
//  OSC.XOSCCTRL = OSC_XOSCSEL_32KHz_gc;					//Enable external 32.768kHz crystal
//  OSC.CTRL |= OSC_XOSCEN_bm | OSC_RC32MEN_bm;				//Enable External oscillator and 32MHz internal RC osc
//  do {													//Wait for the 32kHz oscillator crystal to stabilize.
//  } while((OSC.STATUS & OSC_XOSCRDY_bm)==0);
//  
//  OSC.DFLLCTRL = OSC_RC2MCREF_XOSC32K_gc;					//select external 32.768kHz crystal as 2MHz DFLL calibration ref
//  DFLLRC2M_CTRL |= DFLL_ENABLE_bm;						//enable DFLL for autocalibration of 2MHz RC oscillator
// 
// 	OSC.PLLCTRL = OSC_PLLSRC_RC2M_gc | 16;					//Set PLL reference to 2MHz RC, multiply PLL by 16 to get 32MHz system clock
// 	OSC.CTRL |= OSC_PLLEN_bm;
// 	while(!(OSC.STATUS & OSC_PLLRDY_bm));
// 	
// 	CCP = CCP_IOREG_gc;
// 	CLK.CTRL = CLK_SCLKSEL_PLL_gc;
//  	
// 	do {													//Wait for the 32MHz internal RC osc to stabilize.
// 	} while((OSC.STATUS & OSC_RC32MRDY_bm)==0);
// 	OSC.DFLLCTRL = OSC_RC32MCREF_XOSC32K_gc;					//select external 32.768kHz crystal as 32MHz DFLL calibration ref
// 	DFLLRC32M_COMP1 = 0x1B;
// 	DFLLRC32M_COMP2 = 0xB7;
// 	DFLLRC32M_CTRL |= DFLL_ENABLE_bm;						//enable DFLL for autocalibration of 32MHz RC oscillator
// 	 
//  	CLK.USBCTRL = CLK_USBSEN_bm;							//enable USB clock
}

/*********************************************************************************
	Real Time Clock Setup 
	Starts the RTC clock at 1.024kHz derived from external 32.768kHz crystal
	Generates an Overflow every 500ms
*********************************************************************************/
void rtc_init(void)
{
	RTC.PER = 512;					// PER register to 512 will give a CNT overflow every 500ms
	RTC.CNT = 0x00;					// reset CNT register 

	RTC.INTCTRL = RTC_OVFINTLVL_LO_gc;									// Low level overflow interrupt
	CLK.RTCCTRL = CLK_RTCSRC_TOSC_gc | CLK_RTCEN_bm;					// Set RTC source, 1.024kHz from external 32.768kHz xtal
	RTC.CTRL = CLK_RTCEN_bm;										    // start RTC, no prescaling

	do {												// Wait for RTC to start.
		} while ( RTC.STATUS & RTC_SYNCBUSY_bm );
    
	PMIC.CTRL |= PMIC_LOLVLEX_bm;						//Enable low level interrupts for OVF interrupt
}

/*********************************************************************************
	RTC overflow interrupt handler.	Overflows every 500ms
*********************************************************************************/
ISR (RTC_OVF_vect)					//RTC overflow interrupt
{
	uint8_t sSREG;
	sSREG = SREG;
	
	rtc_time++;
	
	SN_LED_PORT.OUTTGL = ( sn_awaiting_connection & SNAKE_NODE_MASK );

	SREG = sSREG;
}

