/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#include "command.h"
#include "gpio.h"

/*********************************************************************************
 Process data commands from Game host
*********************************************************************************/
void process_data_packet(int usb_rx_nbr_of_bytes)
{
	if ( default_packet_ptr->cmd == D_CMD_PING)
	{
		send_host_packet(0, default_packet_ptr->cmd, (void*)NULL );
	}
	
	if ( default_packet_ptr->cmd == D_CMD_ID)
	{
		uint8_t send_data[] = "Snake Controller";
		send_host_packet(sizeof(send_data)-1, default_packet_ptr->cmd, send_data ); // -1 to remove '0' termination of string
	}
	
	// Snake node connection status
	if ( default_packet_ptr->cmd == D_CMD_STATUS)
	{
		uint8_t send_data[] = {0,0,0,0,0};
		
		check_connected_snakes();
		get_presence_status(send_data);
		send_host_packet(sizeof(send_data), default_packet_ptr->cmd, send_data );
	}

	// Snake command
	if ( default_packet_ptr->cmd == D_CMD_SN_COMMAND)
	{
		uint8_t packet_size;
		uint8_t spi_exchange_status = 0;
		
		default_sn_rx_packet.target = default_packet_ptr->target;
		default_sn_rx_packet.power = 0;
		
		// send data to SN through SPI
		spi_exchange_status = spi_sn_data_exchange( default_packet_ptr, &default_sn_rx_packet );
		
		// If spi exchange had a timed response, set up and transmit data to game host
		if ( spi_exchange_status )
		{
			packet_size = ((uint16_t)default_sn_rx_packet.data - (uint16_t)&default_sn_rx_packet) + default_sn_rx_packet.data[0];
			
			send_host_packet(packet_size, default_packet_ptr->cmd, (uint8_t*)&default_sn_rx_packet );
		}
		else     // Snake Node had a timeout, so set up and transmit a simpler package
		{
			
			default_sn_rx_packet.target = default_packet_ptr->target;
			default_sn_rx_packet.timing = 0xFFFF;
			default_sn_rx_packet.power = 0;
			
			packet_size = ((uint16_t)default_sn_rx_packet.data - (uint16_t)&default_sn_rx_packet) + default_sn_rx_packet.data[0];
			
			send_host_packet(packet_size, default_packet_ptr->cmd, (uint8_t*)&default_sn_rx_packet );
		}
	}
}


/*********************************************************************************
 Send packet to host over usb CDC
*********************************************************************************/
void send_host_packet(int size, uint8_t cmd, uint8_t* buffer)
{
	putchar( (size + 2) );
	putchar( cmd );
	for (int i=0; i<size; i++)
	{
		putchar( buffer[i] );
	}
}
