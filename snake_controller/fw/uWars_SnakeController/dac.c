/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#include "dac.h"

// Definition for AREF. 2.048V.
#define VREF_DAC	2048

// Definition for gain in buffer opamp. 
#define OPAMP_GAIN	3


/*********************************************************************************
	DAC setup
*********************************************************************************/
void dac_setup(void)
{
	
	DACB.CTRLA = 1<<DAC_CH0EN_bp | 1<<DAC_ENABLE_bp;			//enable DACB ch0
	DACB.CTRLC = DAC_REFSEL_AREFB_gc;							//select AREF on port B as reference
	
	// load calibration values
	DACB.CH0OFFSETCAL = 0x07;
	DACB.CH0GAINCAL = 0x0C;
	
}

/*********************************************************************************
	Set DAC output voltage.
	
	Setpoint is in mV. 
	Max value is 6144 mV, but we clamp it to max 6000 so not to exceed IC abs. max.
*********************************************************************************/
bool set_sn_vdd(uint16_t setpoint)
{	
	// return a 0 if setpoint is out of boundaries
	if (setpoint > 6000) { return 0; }
		
	// scale setpoint value to dac resolution and opamp gain. 
	DACB.CH0DATA = ( (setpoint * 2) / OPAMP_GAIN );
	
	return 1;
}
