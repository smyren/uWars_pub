/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "eeprom.h"
#include <nvm.h>

struct eeprom_addresses ee_addr;

void load_eeprom_values(void)
{
	uint16_t temp1 = 0x11ab;
	uint16_t temp2 = 0xbb66;
	uint16_t temp3 = 0x77cc;
	uint16_t temp4 = 0x0011;
	uint16_t temp5 = 0x33dd;
	uint16_t temp6 = 0x44ee;
		
	EEPROM_Store((uint8_t *)&temp1, sizeof(temp1), (uint16_t)&ee_addr1.cal_dac.gain);
	EEPROM_Store((uint8_t *)&temp2, sizeof(temp2), (uint16_t)&ee_addr1.cal_adc[1].offset);
	EEPROM_Store((uint8_t *)&temp3, sizeof(temp3), (uint16_t)&ee_addr1.cal_adc[2].offset);
	EEPROM_Store((uint8_t *)&temp4, sizeof(temp4), (uint16_t)&ee_addr1.cal_adc[3].offset);
	EEPROM_Store((uint8_t *)&temp5, sizeof(temp5), (uint16_t)&ee_addr1.cal_adc[4].offset);
	EEPROM_Store((uint8_t *)&temp6, sizeof(temp6), (uint16_t)&ee_addr1.cal_adc[0].offset);		
	
	eeprom_enable_mapping();
	uint16_t temp = 0;
	temp = *(uint16_t*)(&ee_addr1.cal_adc[0].offset);
	
	
	//uint16_t temp = 0;
	//EEPROM_Load((uint8_t *)&temp, sizeof(temp),  (uint16_t)&ee_addr1.cal_adc[0].offset );	
	
}


uint8_t EEPROM_Store(uint8_t *adrPointer, uint8_t size, uint16_t eepromAddr)    // Store anything by passing the address and size.
{
	uint8_t n;
	for(n=0;n < size; n++)
	{
		uint16_t addr = eepromAddr+n;
		nvm_eeprom_write_byte(addr, *(adrPointer+n));
	}
	return eepromAddr + size;
}

uint8_t EEPROM_Load(uint8_t *adrPointer, uint8_t size, uint16_t eepromAddr)    // Load anything by passing the address and size.
{
	uint8_t n;
	for(n=0;n < size; n++)
	{
		uint16_t addr = eepromAddr+n;
		*(adrPointer+n)=nvm_eeprom_read_byte(addr);
	}
	return eepromAddr + size;
}