/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EEPROM_H
#define EEPROM_H

#include <avr/io.h>
#include <stdint.h>

void load_eeprom_values(void);
uint8_t EEPROM_Store(uint8_t *adrPointer, uint8_t size, uint16_t eepromAddr);
uint8_t EEPROM_Load(uint8_t *adrPointer, uint8_t size, uint16_t eepromAddr);

// struct for calibration values for ADC and DAC
typedef struct u_cal_val_t
{
	uint16_t gain;
	uint16_t offset;
	
} u_cal_val_t;

// struct to hold all eeprom addresses
typedef struct eeprom_addresses
{
	u_cal_val_t cal_dac;
	u_cal_val_t cal_adc[5];
}eeprom_addresses;

// declare global struct for eeprom variables
//struct eeprom_addresses ee_addr1;

#define ee_addr1                (*(eeprom_addresses *)MAPPED_EEPROM_START)			//

#endif