/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include "gpio.h"

uint8_t rtc_time;

// declare two registers with bit information on connection status for quick access
uint8_t sn_awaiting_connection = 0;
uint8_t sn_currently_connected = 0;		// used for RTC interrupt LED blinking

// declare array of five Snake node slots
u_snakenode_t snakenode[5];

/*********************************************************************************
 Get connected snakenodes. A connected snake pulls SNOP low when not in active transmission
 
*********************************************************************************/
void check_connected_snakes(void)
{
	uint8_t sn_present = 0;
		
	 // read input status on all SNOP. Invert so that a present SN is '1' instead of '0' for easier checking later on
 	sn_present = ~( PORTE.IN ) & SNOP_MASK_ALL;

	// see if there are any nodes awaiting connection
	if ( sn_awaiting_connection > 0 )
	{
		check_awaiting_nodes(sn_present);
	}
	
	// if number of SNOPs are greater than already connected SN's, we have a new node wanting to join. If lower, we have less nodes
	if (sn_present > sn_currently_connected)
	{
		register_new_nodes(sn_present);
	}

	if (sn_present < sn_currently_connected)
	{
		// we have fewer nodes than before, so we do a quick de-registering
		de_register_nodes(sn_present);
	}
		
return;		
}


/*********************************************************************************
  De-registers removed snakenodes simply by updating SN presence structs 
   from SNOP pin status
*********************************************************************************/
void de_register_nodes(uint8_t nodes)
{
	u_snakenode_t* snakenode_ptr = snakenode;	// declare a pointer for global presence struct	

	for (uint8_t i=0; i<5; i++)
	{
			// update connected status from SNOP pin status
			snakenode_ptr->connected = ( nodes & (1<<i) );
			
			// for any SNOP pin that is low, update global connected status register
			if ( snakenode_ptr->connected == 0 )
			{
				sn_currently_connected &= ~(1<<i);
			}
		snakenode_ptr++;
	}
// finally, update LED port status
SN_LED_PORT.OUT = sn_currently_connected;
}

/*********************************************************************************
 Registers new snakenodes that have SNOP low. Sets new snakenodes as "awaiting" 
  and takes a timestamp from the RTC tick
*********************************************************************************/
void register_new_nodes(uint8_t nodes)
{
	u_snakenode_t* snakenode_ptr = snakenode;	// declare a pointer for global presence struct
		
	for (uint8_t i=0; i<5; i++)
	{
		// if SN snop is present, but not awaiting connection and not already connected, then we register it as awaiting and take a timestamp
		if ( ( snakenode_ptr->connected == 0 ) && ( snakenode_ptr->awaiting == 0 ) )
		{
			if ( ( nodes & (1<<i) ) ) // mask out and check SNOP pin for given SN
			{
				sn_awaiting_connection |= (1<<i);		// update interrupt status
				snakenode_ptr->awaiting = 1;			// set as awaiting to join
				snakenode_ptr->timestamp = rtc_time;	// make a timestamp using RTC			
			}
		}		
		snakenode_ptr++;
	}
}

/*********************************************************************************
 Checks timing for snakenodes awaiting to join the fun. Compares timestamp for 
	each node to current time from the RTC tick
*********************************************************************************/
void check_awaiting_nodes(uint8_t nodes)
{
	uint8_t current_timestamp = 0;
	uint8_t temp;

	u_snakenode_t* snakenode_ptr = snakenode;	// declare a pointer for global presence struct

	for (uint8_t i=0; i<5; i++)
	{
		// check nodes awaiting to join
		if ( snakenode_ptr->awaiting == 1 )
		{
			//read current time
			current_timestamp = rtc_time;
			
			// if flagged as awaiting to join,  check timestamp and calculate difference (dirty way but it works)
			if (current_timestamp > snakenode_ptr->timestamp)
			{
				current_timestamp -= snakenode_ptr->timestamp;
			}
			else
				{
					current_timestamp = snakenode_ptr->timestamp - current_timestamp;
				}
						
			// check if time since first discovery have passed the limit
			if ( ( current_timestamp >= (SN_JOIN_TIME*2) ) )
			{
				
				// read SNOP presence again to be sure there is a node there
				temp = ~( PORTE.IN ) & SNOP_MASK_ALL;
				
				// check SNOP pin status
				if ( ( temp & (1<<i) ) )
				{
					// if snakenode is still present after timeout, then we allow it to join 
					sn_awaiting_connection &= ~(1<<i);
					snakenode_ptr->awaiting = 0;
					snakenode_ptr->connected = 1;
					sn_currently_connected |= (1<<i);
					SN_LED_PORT.OUT = sn_currently_connected;
				}
				else
					{	//if snake node is not present after timeout, reset connection status completely
						sn_awaiting_connection &= ~(1<<i);
						snakenode_ptr->awaiting = 0;	// reset awaiting flag
						snakenode_ptr->connected = 0;	// reset connected flag just to be sure 				
						sn_currently_connected &= ~(1<<i);
						SN_LED_PORT.OUT = sn_currently_connected;
						
					}
			}
		}
		snakenode_ptr++;
	}
}

/*********************************************************************************
  Just a simple function to collect and append connection status for Snakenodes 
	into the send data array
*********************************************************************************/
void get_presence_status(uint8_t* active_nodes)
{
	u_snakenode_t* snakenode_ptr = snakenode;	// declare a pointer for global presence struct	
	
	active_nodes[0] = snakenode_ptr->connected;
	snakenode_ptr++;
	active_nodes[1] = snakenode_ptr->connected;
	snakenode_ptr++;
	active_nodes[2] = snakenode_ptr->connected;
	snakenode_ptr++;
	active_nodes[3] = snakenode_ptr->connected;
	snakenode_ptr++;
	active_nodes[4] = snakenode_ptr->connected;
}

/*********************************************************************************
GPIO pin setup
*********************************************************************************/
void gpio_setup(void)
{
	// turn on all modules in the PRP registers... ASF turns everything off in sysclk_init
	PR.PRGEN = 0x00;
	PR.PRPA = 0x00;
	PR.PRPB = 0x00;
	PR.PRPC = 0x00;
	PR.PRPD = 0x00;
	PR.PRPE = 0x00;
	PR.PRPF = 0x00;
	
	// Port E are all inputs PE0-4 are connected to Snake Node Output (SNOP) pins
	PORTE.DIRCLR = 0x01F;
	// Set Input sense configuration to Falling Edge for SNOP pins, enable pullup resistor
	PORTE.PIN0CTRL = PORT_ISC_RISING_gc | PORT_OPC_PULLUP_gc;
	PORTE.PIN1CTRL = PORT_ISC_RISING_gc | PORT_OPC_PULLUP_gc;
	PORTE.PIN2CTRL = PORT_ISC_RISING_gc | PORT_OPC_PULLUP_gc;
	PORTE.PIN3CTRL = PORT_ISC_RISING_gc | PORT_OPC_PULLUP_gc;
	PORTE.PIN4CTRL = PORT_ISC_RISING_gc | PORT_OPC_PULLUP_gc;
	
	PORTB.OUTCLR = PIN2_bm;					//make sure that PB2 (DAC0) is low
	
	// PORT	
	PORTB.DIRSET = PIN2_bm;					// set PB2 DAC0 as output
	PORTD.DIRSET = PIN4_bm | PIN1_bm;		// set PD4 and PD1 (LED) as output 
	PORTF.DIRSET = 0x3F;					// set all PortF LED pins as output
	PORTF.OUTSET = 0x00;
	
	// enable slewrate limiting for all output pins for better emi
	PORTB.PIN2CTRL = PORT_SRLEN_bm;	
	PORTD.PIN1CTRL = PORT_SRLEN_bm;
	PORTD.PIN4CTRL = PORT_SRLEN_bm;
	PORTF.PIN0CTRL = PORT_SRLEN_bm;
	PORTF.PIN1CTRL = PORT_SRLEN_bm;
	PORTF.PIN2CTRL = PORT_SRLEN_bm;
	PORTF.PIN3CTRL = PORT_SRLEN_bm;
	PORTF.PIN4CTRL = PORT_SRLEN_bm;
	PORTF.PIN5CTRL = PORT_SRLEN_bm;
	
	// enable clock output on PD4
	//PORTCFG_CLKEVOUT = PORTCFG_CLKEVPIN_bm | PORTCFG_CLKOUT_PD7_gc;
	
	// initialize snakenode struct
	u_snakenode_t* snakenode_ptr = snakenode;	// declare a pointer for global presence struct
	
	// initialize snake node struct	
	for (uint8_t i=0; i<5; i++)
	{
		snakenode_ptr->timestamp = 0;
		snakenode_ptr->awaiting = 0;	
		snakenode_ptr->connected = 0;	
		snakenode_ptr++;
	}
	
}
