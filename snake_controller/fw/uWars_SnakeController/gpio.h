/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GPIO_H
#define GPIO_H

#include <avr/io.h>
#include "spi.h"

//defienitions for debug LED on Ardicino board PD1
#define LED_HIGH			PORTF_OUTSET = PIN5_bm
#define LED_LOW				PORTF_OUTCLR = PIN5_bm
#define LED_TOGGLE			PORTF_OUTTGL = PIN5_bm

#define SN_LED_PORT			PORTF

#define SNAKE_NODE_MASK		0x1F

#define SNOP_MASK_ALL		0x1F
#define SNIP_MASK_ALL		0x1F

#define SN_JOIN_TIME		5			//time in seconds that a snake node needs to wait before getting asserted

extern uint8_t sn_awaiting_connection;
extern uint8_t sn_currently_connected;

// define a struct for snake node connection status
typedef struct u_snakenode
{
	bool connected;
	bool awaiting;
	uint8_t timestamp;
	
}u_snakenode_t;

extern u_snakenode_t snakenode[5];

void gpio_setup(void);
void check_connected_snakes(void);
void register_new_nodes(uint8_t nodes);
void de_register_nodes(uint8_t nodes);
void check_awaiting_nodes(uint8_t nodes);
void get_presence_status(uint8_t* active_nodes);

#endif