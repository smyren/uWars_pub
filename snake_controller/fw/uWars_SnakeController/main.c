/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * uWars_SnakeController.c
 *
 * Created: 26.09.2016 20:55:19
 * Author : Stig
 
 ----Timing Window----
	
	using 16-bit timer and 32MHz we have base resolution of 31.25ns. Can be extended to 32-bits using event system and a second timer
	
	Max timeout and resolution for all prescaler settings (timer0)
	
	Prescaler	Max timout	Resolution
	1			  2.048 ms	  31.25 ns
	2			  4.096 ms	  62.50 ns

 ----Issues to look into----


 ----ToDo's----
	
*	Make MCU run from PLL autocalibrated from external 32kHz crystal	DONE!
*	EEPROM storage of cal values
	ADC measurments of IDD	
	Calibration of VDD power
	Normalize timing measurement
	Commands for setting Power, timing window
	
*/

#include <avr/io.h>
#include <asf.h>
#include "main.h"
#include "conf_board.h"
#include "conf_usb.h"
#include "adc.h"
#include "dac.h"
#include "gpio.h"
#include "clk.h"
#include "usb.h"
#include "spi.h"
#include "command.h"
#include "eeprom.h"

static volatile bool main_b_cdc_enable = false;
extern bool host_cmd_available;
uint8_t* usb_data_buffer_ptr;

// input data formats
u_default_packet* default_packet_ptr = (void*)usb_tx_buffer;
u_default_sn_rx_packet default_sn_rx_packet;

int main(void)
{
	int usb_rx_nbr_of_bytes;
	
	irq_initialize_vectors();
	cpu_irq_enable();	
	sysclk_init();
	sleepmgr_init();
	stdio_usb_init();
	board_init();
	gpio_setup();
	rtc_init();
	dac_setup();
	spi_setup();


	
	// set SnakeNode VDD to 5.0V
	set_sn_vdd(5000);
	
	//load_eeprom_values();

    while (true) 
	{
		// poll for available data from Snake Host. Returns with operation byte and data in buffer if new data available
		usb_rx_nbr_of_bytes = usb_rx_poll();
		
		if( usb_rx_nbr_of_bytes )	
		{						
			LED_TOGGLE; // toggle LED
			process_data_packet(usb_rx_nbr_of_bytes);
	
		}	
	}
}