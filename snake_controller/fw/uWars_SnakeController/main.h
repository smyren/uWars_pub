/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAIN_H
#define MAIN_H

#include <stdbool.h>
#include <stdio.h>

// fw version number
#define FW_VERSION			0x01

// sc action commands
#define D_CMD_PING			0
#define D_CMD_ID			1
#define D_CMD_STATUS		2
#define D_CMD_SN_COMMAND	3
#define D_CMD_ERROR			66

// System related commands
#define S_CMD_CALIBRATE		254		// command to set controller in calibration mode for doing ADC and DAC calibration

// Error types
#define D_ERROR_SNAKE_NODE_NOT_CONNCETED 100

#define USB_RX_BUFFER_SIZE 256

typedef struct u_default_packet
{
	uint8_t packet_size;
	uint8_t	cmd;
	uint8_t target;
	uint16_t timing;
	uint8_t bytes;
	uint8_t data[USB_RX_BUFFER_SIZE];
} u_default_packet;

typedef struct u_default_sn_rx_packet
{
	uint8_t target;
	uint16_t timing;
	uint16_t power;
	uint8_t data[USB_RX_BUFFER_SIZE];
} u_default_sn_rx_packet;

extern u_default_packet* default_packet_ptr;
extern u_default_sn_rx_packet default_sn_rx_packet;


#endif