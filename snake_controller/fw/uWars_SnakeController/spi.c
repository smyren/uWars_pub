/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "spi.h"

uint8_t	spi_done_timer_started_flag = 0;

/*********************************************************************************
SPI TX and RX - With response time measurement
	
*********************************************************************************/
uint8_t spi_sn_data_exchange (u_default_packet *tx_packet, u_default_sn_rx_packet *rx_packet)
{
	
	init_timer_event(tx_packet->target);
	send_spi_data (tx_packet);
	measure_response_time(rx_packet);

	// if timing measurement reports anything lower than 0xffff, we have a valid timing measurement and response from sn
	if ( ( rx_packet->timing < 0xFFFF ) )
		{						
			recieve_spi_data (rx_packet);
						
			// return a 1 to indicate a valid, timed response was received
			return 1;
		}
	
	// return any SS high again
	PORTC.OUTSET = SS_ALL_SN_MASK;
	
	// set data size byte to 1 byte for SN timeout
	rx_packet->data[0] = 1;
	
	// return a zero to indicate 'no response' timeout
	return 0;
}

/*********************************************************************************
	Send data to snakenode through spi 
*********************************************************************************/
void recieve_spi_data (u_default_sn_rx_packet *packet)
{
	uint8_t i;
	
	SPIC.STATUS = 0xFF;		//reset SPI flags
			
	SPIC.DATA;	//flush SPI data register
	
	// Flush first received byte from sn. It might be corrupt
	// send one dummy byte
	SPIC.DATA =  0x00;
	// wait for tx to complete
	while ( !( SPIC.STATUS & SPI_IF_bm ) );
	packet->data[0] = SPIC.DATA;
	
	//set data0 to an initial value before starting transmission. Data0 contains number-of-bytes to receive
	packet->data[0] = 2;
	
	// start shifting in bytes from SN
	for( i=0; i < (packet->data[0]); i++)
	{
		// send one dummy byte
		SPIC.DATA =  0x00;
				
		// wait for tx to complete
		while ( !( SPIC.STATUS & SPI_IF_bm ) );
				
		// read out data from reception into receive buffer
		packet->data[i] = SPIC.DATA;
	}
			
	// return any SS high again
	PORTC.OUTSET = SS_ALL_SN_MASK;
}

/*********************************************************************************
	Send data to snakenode through spi 
*********************************************************************************/
void send_spi_data (u_default_packet *packet)
{
	uint8_t i;
	
	SPIC.STATUS = 0xFF;		//reset SPI flags
	
	// set low correct SS pin from target byte. SSpins are on PC0-4
	PORTC.OUTCLR = ( 1 << packet->target );
	
	// start sending data bytes with SPI
	for( i=0; ( i < (packet->data[0]) ); i++ )
	{
		// send one byte
		SPIC.DATA =  packet->data[i];
		
		// wait for tx to complete. If this is the last byte sent, we need to start the timer from SPI TX complete interrupt to make sure the timer starts without getting interrupted from USB
		if( i == ( packet->data[0] - 1))
			{									
				// enable SPI TX complete interrupt, High level. Static pri of SPI is higher than that of USB. ISR will start timer for measuring time.
				SPIC.INTCTRL = SPI_INTLVL_HI_gc;
				
				// wait for flag in interrupt to be set
				while ( !( spi_done_timer_started_flag ) );
				
				// reset flag for next time
				spi_done_timer_started_flag = 0;
				
				// disable SPI interrupt
				SPIC.INTCTRL = 0;
			}
			else
			{
				while ( !( SPIC.STATUS & SPI_IF_bm ) );
			}
	}
}

/*********************************************************************************
	Measure response time from snakenode. 
*********************************************************************************/
void measure_response_time (u_default_sn_rx_packet *packet)
{
	
	// wait for SNOP rising edge (CCA interrupt flag) or TC0 overflow or SNOP pin LOW
	while ( !( TCC0.INTFLAGS & TC0_OVFIF_bm ) && !( TCC0.INTFLAGS & TC0_CCAIF_bm ) )
	
	// flush capture buffer registers (for some reason...)
	TCC0.CCABUF;
	
	// stop TC0
	TCC0.CTRLA = 0x00;
	EVSYS.CH0MUX = 0x00;	
	
	// check if we had a valid CCA buffer update
	if ( ( TCC0.INTFLAGS & TC0_CCAIF_bm ) )
		{
			// CCA has been updated, this means we had a timed response from SN
			// save measured response time. Using BUF registers works best, it seems...
			packet->timing = TCC0.CCABUF;
		}
	else
		{				
			packet->timing = 0xFFFF;
		}
}


/*********************************************************************************
	Initialize timer and event system for SPI timing measurement
*********************************************************************************/
void init_timer_event (uint8_t target)
{
	// configure appropriate input pin to trigger event CH0 mapped to CCA. Pins already have ISC set to Rising edge in gpio_setup
	// Input pins (snake node output, SNOP) are located on PE0-4. SNOP must be driven high by the target snake to indicate that
	// it is ready to transmit data. This then triggers Event CH0 which makes TimerC0 put current count value into CCA
	// 0x70 sets 'PORTE' and bits 3:0 sets input pin
	EVSYS.CH0MUX = 0x70 | target;
		
	// issue a RESET command to TimerC0
	TCC0.CTRLFSET = TC_TC0_CMD_RESET_gc;
	
	// preload the CNT register with lost cycles due to enable/start/interrupt handling time. 3x cycles for pushing to stack, 3x for int.vect. jump, and one for starting timer, 7x in total
	// current value is hand tuned... does not seem to make sense to need as many as 32x.... oh well. 
	TCC0.CNT = 32;
		
	// set TimerC0 to input capture mode and select Event channel 0 as input source to CCA
	TCC0.CTRLD = TC_TC0_EVACT_CAPT_gc | TC_TC0_EVSEL_CH0_gc;
		
	// initialize TOP value of TimerC0 to max possible.
	TCC0.PER = 0xFFFF;
		
	// enable CCA input capture
	TCC0.CTRLB = TC0_CCAEN_bm;
		
	// clear all flags
	TCC0.INTFLAGS = 0xFF;
}


/*********************************************************************************
	SPI transmit complete interrupt
*********************************************************************************/
ISR (SPIC_INT_vect)
{
	// Start TimerC0 at full speed	(32MHz, Tper=31.25ns, timeout = 2.048ms)
	TCC0.CTRLA = TC_TC0_CLKSEL_DIV1_gc;
	
	// set spi done flag
	spi_done_timer_started_flag = -1;
}


/*********************************************************************************
	SPI setup, port C, Pin 3-7.
*********************************************************************************/
void spi_setup (void)
{
// set PC5 (MOSI) and PC7 (SCK) as outputs, and PC0-4 (SS) also as outputs
PORTC.DIR = PIN7_bm | PIN5_bm | PIN4_bm | PIN3_bm | PIN2_bm | PIN1_bm | PIN0_bm; 
PORTC.DIRCLR = PIN6_bm; // PC6 (MISO) as input

// enable slewrate control for all SPI pins for better signal integrity
PORTC.PIN0CTRL = PORT_SRLEN_bm;
PORTC.PIN1CTRL = PORT_SRLEN_bm;
PORTC.PIN2CTRL = PORT_SRLEN_bm;
PORTC.PIN3CTRL = PORT_SRLEN_bm;
PORTC.PIN4CTRL = PORT_SRLEN_bm;
PORTC.PIN5CTRL = PORT_SRLEN_bm;
PORTC.PIN7CTRL = PORT_SRLEN_bm;

// set all SS high
PORTC.OUTSET = 0x1F;	
	
// Set SPI mode: (CLKper/64 = 500kHz), Master, CLKmode 00, LSB first
SPIC.CTRL = SPI_ENABLE_bm | SPI_MASTER_bm | SPI_PRESCALER_DIV64_gc;		
SPIC.INTCTRL = 0x00; // ensure SPI interrupts are disabled

//Enable high level interrupts for SPI tx complete interrupt
PMIC.CTRL |= PMIC_HILVLEX_bm;
}
