/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPI_H
#define SPI_H

#include <avr/io.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <asf.h>
#include "main.h"


#define SS_ALL_SN_MASK	0x1F
#define START_TIMER		TCC0.CTRLA = TC_TC0_CLKSEL_DIV1_gc

extern uint8_t	spi_done_timer_started_flag;

void spi_setup(void);
void spi_tx_data (uint8_t *tx_data_ptr);
uint8_t spi_sn_data_exchange (u_default_packet *tx_packet, u_default_sn_rx_packet *rx_packet);
void recieve_spi_data (u_default_sn_rx_packet *packet);
void send_spi_data (u_default_packet *packet);
void measure_response_time (u_default_sn_rx_packet *packet);
void init_timer_event (uint8_t target);

#endif