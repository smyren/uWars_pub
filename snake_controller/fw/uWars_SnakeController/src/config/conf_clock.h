/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONF_CLOCK_H_INCLUDED
#define CONF_CLOCK_H_INCLUDED

#define BOARD_XOSC_HZ							32768UL
#define BOARD_XOSC_TYPE							XOSC_TYPE_32KHZ
#define BOARD_XOSC_STARTUP_US					XOSC_STARTUP_16384
												
//#define CONFIG_SYSCLK_SOURCE					SYSCLK_SRC_RC2MHZ
//#define CONFIG_SYSCLK_SOURCE					SYSCLK_SRC_RC32MHZ	
//#define CONFIG_SYSCLK_SOURCE					SYSCLK_SRC_RC32KHZ
//#define CONFIG_SYSCLK_SOURCE					SYSCLK_SRC_XOSC
#define CONFIG_SYSCLK_SOURCE					SYSCLK_SRC_PLL
												
/* Fbus = Fsys / (2 ^ BUS_div) */				
//#define CONFIG_SYSCLK_PSADIV					SYSCLK_PSADIV_2					
#define CONFIG_SYSCLK_PSADIV					SYSCLK_PSADIV_1					// PLL run at 32MHz.
#define CONFIG_SYSCLK_PSBCDIV					SYSCLK_PSBCDIV_1_1
												
//#define CONFIG_PLL0_SOURCE					PLL_SRC_XOSC
#define CONFIG_PLL0_SOURCE						PLL_SRC_RC2MHZ
//#define CONFIG_PLL0_SOURCE					PLL_SRC_RC32MHZ
												
/* Fpll = (Fclk * PLL_mul) / PLL_div */			
#define CONFIG_PLL0_MUL							(32000000UL / 2000000UL)
#define CONFIG_PLL0_DIV							1

/* DFLL autocalibration */
#define CONFIG_OSC_AUTOCAL_RC2MHZ_REF_OSC  OSC_ID_XOSC
//#define CONFIG_OSC_AUTOCAL_RC32MHZ_REF_OSC OSC_ID_XOSC

#define CONFIG_USBCLK_SOURCE					USBCLK_SRC_RCOSC
#define CONFIG_OSC_RC32_CAL						48000000UL
#define CONFIG_OSC_AUTOCAL_RC32MHZ_REF_OSC		OSC_ID_USBSOF
//#define CONFIG_OSC_AUTOCAL_RC32MHZ_REF_OSC	OSC_ID_XOSC

/* Use to enable and select RTC clock source */
#define CONFIG_RTC_SOURCE						SYSCLK_RTCSRC_TOSC

#endif /* CONF_CLOCK_H_INCLUDED */
