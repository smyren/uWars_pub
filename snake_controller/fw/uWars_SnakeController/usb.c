/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdbool.h>
#include <asf.h>
#include "main.h"
#include "usb.h"
#include "gpio.h"

#define USB_RX_START_SIGN 0

volatile bool usb_data_ready = false;
volatile int usb_rx_nbr_of_bytes = -1;
volatile int usb_rx_expect_nbr_of_bytes = -1;
volatile int usb_rx_timeout = -1;
uint8_t* usb_tx_buffer_ptr = usb_tx_buffer;

// Interrupt to code flags
volatile bool usb_data_packet_complete = true;

// Debug
int usb_rx_timeout_counter = 0;

/*********************************************************************************
USB receive notification 

This is used by the cdc stack in the background, interrupt/event style
*********************************************************************************/
void usb_rx_notify(void)
{
	uint8_t temp;
	bool flush = false;
	
	if (usb_data_packet_complete)
	{
		// Always require more than 2 data in a packet
		if (udi_cdc_get_nb_received_data() > 1){
			// First data (first package)
			usb_rx_nbr_of_bytes = 1;
			usb_tx_buffer_ptr = usb_tx_buffer;
			// Get LSB size
			temp = udi_cdc_getc();
			
			if (temp > USB_RX_BUFFER_SIZE)
			{
				flush = true;
			}else
				{	// first byte contains number of total bytes in transfer
					usb_rx_expect_nbr_of_bytes = temp;
					// save received data to buffer
					*usb_tx_buffer_ptr++ = temp;
				}
		}else
			{
				flush = true;
			}
	}
	
	while ( udi_cdc_is_rx_ready() && (usb_rx_nbr_of_bytes < USB_RX_BUFFER_SIZE) )
	{
		// Read all data in usb buffer to local buffer
		*usb_tx_buffer_ptr++ = udi_cdc_getc();
		usb_rx_nbr_of_bytes++;
	}
	
	if (usb_rx_nbr_of_bytes == usb_rx_expect_nbr_of_bytes)
	{
		// All data received
		usb_data_ready = true;
	}else if (usb_rx_nbr_of_bytes > usb_rx_expect_nbr_of_bytes){
		// Too much data received
		flush = true;
	}else{
		// Expecting more data.
		usb_data_packet_complete = false;
	}
	
	if (flush)
	{
		// Flush out data
		while ( udi_cdc_is_rx_ready() )
		{
			udi_cdc_getc();
		}
	}
}

/*********************************************************************************
USB receive data transfer 

*********************************************************************************/
int usb_rx_poll(void)
{
	if ( !usb_data_packet_complete )
	{
		if (usb_rx_timeout > 0)
		{
			usb_rx_timeout--; // Update timeout counter
		}
	
		// Need to make sure there is no interrupt since this might cause a conflicting update of usb_data_packet_complete
		cpu_irq_disable();
		if (usb_rx_timeout == 0)
		{
			usb_rx_timeout_counter += 1;  // For debug. Should never occur...
			usb_data_packet_complete = true;
			// Could flag an error here so device could tell the host that an error occurred
		}
		cpu_irq_enable();
	}else
	{
		usb_rx_timeout = 1000; // Wait for more data
		
	}
	
	if (usb_data_ready)
	{
		// Do not expect usb interrupt here since device has not answered on previous command
		usb_data_ready = false; 
		usb_data_packet_complete = true;
		
		// Return packet size ready for processing
		return usb_rx_nbr_of_bytes; 
	}
	return 0;
}
