##
## Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
## 
## This file is part of uWars - a microcontroller snake game 
## (see https://gitlab.com/smyren/uWars_pub).
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

import serial
import serial.tools.list_ports

ws = [chr(2)+chr(1),chr(8)+chr(3)+chr(0)+chr(0)+chr(0)+chr(0)+chr(0)+chr(100)]

ports = serial.tools.list_ports.comports()
for port in ports:
    print(port.description)
    if port.description.find("ASF") != -1:
        ser = serial.Serial(port.device, timeout = 1.0, write_timeout = 0)
        ser.write(ws[0].encode())
        print( ser.read(20) )
        ser.write(ws[1].encode())
        print( ser.read(20) )
        ser.write(ws[0].encode())
        print( ser.read(20) )
    if port.description.find("mEDBG") != -1:
        ser = serial.Serial(port.device, timeout = 1.0, write_timeout = 0)
        ser.write(ws[0].encode())
        print( ser.read(20) )
        
        ser.close()