# How to program and debug your own uWars snake node

If you know how to use Atmel Studio then steps 2, 3, 4 and 6 are the ones you are most interested in

## Step 1: 
### Install Atmel Studio 7

Download the [latest release here](http://www.microchip.com/development-tools/atmel-studio-7). It will take a while to install.

## Step 2: 
### Open a snake node simple template solution

In the folder `\snake_node\snake_node\simple_template\` there are three template solutions to choose between.
All of them are using the same files, so changing main.c in one solution will change it for all.

* #### `simple_template_libs.atsln`

    This is the recommended one to start out with, containing the project `simple_template_libs.cproj`.  
    It has everything you need to write your snake algorithm and program/debug it using the communication libraries in one project.

* #### `simple_template_libs_full.atsln`

    Identical to simple_template_libs, however you also have the two static library projects for UART and SPI communication.  
    Good for looking at the implementation of the communication, and if you want to tweak stuff.

* #### `simple_template.atsln`

    This one contains a project `simple_template.cproj` without static libraries, so it includes the communication files directly.  
	Useful if you don't care about libraries and/or want to do a large rewrite to optimize the handler.

## Step 3: 
### Configure user_settings.h

Edit `user_settings.h` to personalize your snake as you wish.  
See the [Icon Editor Readme](icon_editor/README.md) for info on how to generate the variables for snake icon and colors.

## Step 4:
### Write your snake algorithm

In `main.c` you will find the functions needed for writing your algorithm.  
All the info you need should be found in the code and comments for `main.c`, `protocol.h` and `simple_protocol_handler.h`.
If not, let us know!

## Step 5:
### Connect and configure device

Connect your board to the computer, and if needed (no embedded debugger) connect a programmer/debugger to the computer and your device.  
In the project properties, go to the Tool pane and find your devices programmer/debugger under `Selected debugger/programmer`.

#### Tool menu
![Tool_select](snake_node/doc/select_debugger.png "Selected debugger/programmer")

## Step 6: 
### Compile and start debugging

Choose the `Debug` solution configuration and click `Start Debugging (F5)`.  
This will use the UART static library and use low optimization level when compiling to make debugging easier.

{- NB! -} Putting breakpoints outside of `calculate_movement()` will cause the device to miss package transfers and the game host will hang and need a restart. However sometimes it is necessary to do this when looking for a bug in postprocessing.

#### Solutions Configurations menu
![Compile_Debug](snake_node/doc/compile_debug.png "Solutions configuration")

Then run the game host to debug in-game:
```
\game_host\run.bat
```
See the [Game Host Readme](game_host/README.md) for more info on how to configure the game host.

## Step 7:
### Compile without debugging

If you want to run your snake node directy connected to a game host, select the `Release_UART` solutions configuration as shown above.  
If your snake node will be connected to a snake controller, select the `Release_SPI` solutions configuration.  
This will select the correct communication static library and a higher optimization level to hopefully make the code run faster.

Press `Start Without Debugging (Ctrl+Alt+F5)` to compile and program the device.

#### Start Without Debugging button
![without_debug](snake_node/doc/start_nodebug.png "Start Without Debugging")

### Good luck!