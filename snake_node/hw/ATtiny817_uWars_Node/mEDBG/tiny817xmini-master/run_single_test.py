'''
This script is used to run a single test from the 'common' folder.
Since a submodule cannot import files beyond top level it has to be
run from one level above the settings file.

Usage:
run_single_test.py test_name
run_single_test.py package_name test_name

'''

if __name__ == '__main__':
    import scanparser
    import os
    import upa
    import sys
    
    print "Import test"
    if len(sys.argv) == 3:
        import_string = "import src.common.%s.%s as test_egg" % ( sys.argv[1], sys.argv[2] )
        from src.settings import *
    else:
        print "Wrong number of arguments!"
        sys.exit(1)
    
    print "-> %s" % import_string
    try:
        exec import_string
        exec settings_import_string
        print "imported ok"
    except:
        print "Error: Could not import egg!"
    

    scanString = TEST_SCANSTRING
    scanData = scanparser.parse(scanString)

    info = {}
    info['scanData'] = scanData
    info['scanString'] = scanString
    info['interactive'] = False
    info['verbose'] = True
    
    upas_found = upa.detect()
    if len(upas_found) < NUMBER_UPAS:
        print ("Number of UPAs found (%d) does not match requirements (%d)" % (len(upas_found), 1))
        sys.exit(1)
    
    info['upas'] = []
    for found_upa in upas_found:
        info['upas'].append(upa.upa(found_upa[0]))

    for upa in info['upas']:
        upa.power_on()

    from plm_db import testlog

    testlogger = testlog.create(host=os.getenv("ATMEL_PLM_HOST"),
                                port=os.getenv("ATMEL_PLM_PORT"),
                                server=os.getenv("ATMEL_PLM_SERVER"),
                                testan=TEST_AN, testrev='1',
                                uutan=scanData['an'], uutrev=scanData['rev'])
    testlogger.new_record(scanData['serial'])
    info['testlogger'] = testlogger

    test = test_egg.test(info)
    result = test.Run()
    
    for upa in info['upas']:
        upa.power_off()

    # Commenting out as no need to log to PLM in default case
    if result:
        print "Test passed"
        #testlogger.close_record("pass")
    else:
        print "Test failed with message: '%s'" % test.getMessage()
        #testlogger.close_record("fail")

    #testlogger.close_record('invalid')
    #testlogger.close()

    print "Done"