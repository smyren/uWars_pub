import os
import sys
from setuptools import setup
from src.settings import EGG_NAME, KIT_NAME

packman = EGG_NAME

try:
    from src.version import version
except:
    version = '0.0'

# sdist is used to build the egg
if sys.argv[1] == 'sdist':
    # Major test version, must match version of test component in PLM
    MAJOR_VERSION = "1"

    # Get the build jumber from Jenkins
    if os.getenv("BUILD_NUMBER"):
        TEST_BUILD = os.getenv("BUILD_NUMBER")
    else:
        TEST_BUILD = "0"

    version = "{}.{}".format(MAJOR_VERSION, TEST_BUILD)

    # Write the version to a file so the source code can know its own version
    f = open('{}/version.py'.format('src'), 'w')
    f.write("# This file was generated when the egg was built.\n")
    f.write("version = '{}'\n".format(version))
    f.close()

setup(name=packman,
      version=version,
      description='Production of %s evaluation kit' % KIT_NAME,
      url='http://www.atmel.com',
      include_package_data=True,
      package_dir={packman : 'src'},
      packages=[packman],
      package_data={packman:['src/data/*.*'],'common':['src/common/*.*']}
      )
