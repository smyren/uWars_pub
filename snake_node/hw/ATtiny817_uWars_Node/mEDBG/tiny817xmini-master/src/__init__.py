""" eggTester compliant test package"""
from settings import *

# Import version from version.py generated when the package is built
# If the file isn't found use version 0.0
try:
    from version import version
except:
    version = "0.0"
    pass

class testSequence:
    """
    Test sequence class for eggTester
    """
    testAn = TEST_AN  # Xplained Pro test component
    testRev = version

    def __init__(self, info):
        self.myTests = []  # Holds a list of tests
        self.info = info  # Global 'info' struct
        self.setupTests()  # Configure the test list
        self.setupCleanup()  # Configure the cleanup script
        self.setupTestOptions()  # Configure the options


    def getTestDetails(self):
        """Returns test a-number and revision"""
        return self.testAn, self.testRev

    def getTestList(self):
        """Returns the list of tests"""
        return self.myTests

    def getTestOptions(self):
        """Returns the options provided"""
        return self.myOptions

    def getCleanup(self):
        """Returns the cleanup script"""
        return self.cleanup

    def addTest(self, name, run):
        """Adds a test to the test list"""
        newTest = {}
        newTest['name'] = name
        newTest['run'] = run
        newTest['passed'] = False
        newTest['message'] = ""
        self.myTests.append(newTest)

    # Test developer should edit the following sections
    def setupTests(self):
        """Sets up the test list"""
        # Check that a correct product was scanned
        self.addTest(name="common.general.check_scan_data", run=True)

        # Download latest mEDBG firmware from PLM
        self.addTest(name="common.medbg.update_firmware", run=True)

        # Check power and turn on UPA
        self.addTest(name="common.general.power_test", run=True)

        # Chip Erase
        self.addTest(name="common.medbg.chiperase", run=True)

        # FLASH bootloader
        self.addTest(name="common.medbg.program_bootloader", run=True)

        # FLASH mEDBG
        self.addTest(name="common.medbg.program_firmware", run=True)

        # Program config on mega32U4
        self.addTest(name="common.medbg.program_config", run=True)

        # Program Fuses on mega32U4
        self.addTest(name="common.medbg.program_fuses", run=True)

        # Program Fuses on target
        self.addTest(name="common.general.target_program_fuses", run=True)

        # Program test application into target device using mEDBG UPDI
        self.addTest(name="common.general.target_program_selftest", run=True)

        # Run target test
        self.addTest(name="selftest", run=True)

        # Program serial number into EEPROM (mega32U4)
        self.addTest(name="common.medbg.program_serial", run=True)

    def setupCleanup(self):
        """Sets up the cleanup script"""
        self.cleanup = {}
        self.cleanup['name'] = "common.general.cleanup"
        self.cleanup['run'] = True
        self.cleanup['passed'] = False
        self.cleanup['status'] = ''
        self.cleanup['message'] = ""

    def setupTestOptions(self):
        """Sets up the eggTester options"""
        self.myOptions = {}
        self.myOptions['autorun'] = False               # 
        self.myOptions['superuser'] = True              # Allows pressing some extra buttons in the eggTester GUI, not ment for mass production
        self.myOptions['allow_keyboard_input'] = True   # Allows a board serial number to be typed in the eggTester
        self.myOptions['cleanup'] = "common.src.cleanup"           # The name of the "cleanup" function
        self.myOptions['upa'] = NUMBER_UPAS             # Number of UPAs required by the test (set to 0 if no UPA, or old version of UPA is used
        self.myOptions['interactive'] = False           # Use this flag to trigger pauses for driver installation throughout the test
        self.myOptions['verbose'] = True                # Set this flag to True for extra debug output

    # This function is run if the test is updated from PLM / Installed for the first time
    def onInstall (self):
        from settings import STUDIO_PATH
        TIMEOUT = 10
        
        print "onInstall routine"
        
        # atbackend relies on a "codeCache" at runtime.  This is automatically generated on first-time start of atbackend.exe
        # To minimise the chance of timeouts later, we would like to generate the codeCache on install.
        # atbackend is thus started with two parameters:
        # /clean will re-build the codeCache regardless of its state
        # /exit will automatically exit afterwards, so we should not need to kill the process
        ATBACKEND = r'%s\\atbackend.exe /clean /exit'% (STUDIO_PATH)
        command = "%s" % (ATBACKEND)
        
        self.execute_command(command, TIMEOUT)
        
        # atbackend uses part packs to support different devices. The part packs needed for this test
        # should be supplied in the /data/packs folder. We want to install these packs after installation
        # just in case.
        
        ATPACMAN = r'%s\\atpackcli.exe -i'% (STUDIO_PATH)
        
        packs = [os.path.join(DATA_PATH + "/packs",fn) for fn in next(os.walk(DATA_PATH + "/packs"))[2]]
        for pack in packs:
            command = "%s %s" % (ATPACMAN, pack)
            self.execute_command(command, TIMEOUT)
        
        # Pack Manager relies on a database of supported devices.  This database will either be empty or contain pointers to the packs on the 
        # machine used to construct the test egg.  For atbackend to work correctly, we need to reconstruct the pack database using the -t option
        # atpackcli.exe exits quietly after doing this.
        ATPACMAN = r'%s\\atpackcli.exe -t'% (STUDIO_PATH)
        command = "%s" % (ATPACMAN)
        self.execute_command(command, TIMEOUT)
    
    def execute_command(self, command, timeout):
        import subprocess
        import time
        import win32api
        print "Executing command '%s'" % command
        t0 = time.time()
        process = subprocess.Popen (command, shell=False, stderr=subprocess.PIPE, stdout=subprocess.PIPE, stdin=subprocess.PIPE, cwd=STUDIO_PATH)
        print "Waiting %s seconds for command to complete" % timeout
        while (process.poll() == None):
            if time.time() - t0 > timeout:
                print "Timeout waiting for command to terminate"
                win32api.TerminateProcess(int(process._handle), -1)
                break
            time.sleep(1)
        print "command terminated"
