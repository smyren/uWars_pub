from settings import *
from usbdevice import medbg

testname = "target_cdc_test"
testdescription = "Checks that the CDC interface to the %s works." % DEVICE_NAME

PROGRAM_TOOL = r'%s\\Studio\\atprogram.exe' % (DATA_PATH)


class test:
    def __init__(self, info):
        self.info = info
        self.message = ""
        self.testlogger = info['testlogger']

    def getMessage(self):
        return self.message

    def setMessage(self, message):
        self.message = message

    def run_target_self_test(self):
        cdc = medbg.medbg()
        cdc.connect("", timeout=2)
        if self.info['verbose']:
            print "CDC on %s" % cdc.comport
            print "CDC connected"

        result = cdc.command("hello")

        print "CDC test returned '%s'" % result
        if result == "good day":
            self.testlogger.add_parameter (parameter="CDC",  value = "%s" % result, status='pass')
        else:
            self.setMessage("CDC error: %s" % result)
            self.testlogger.add_parameter (parameter="CDC",  value = "%s" % result, status='fail')
            cdc.disconnect()
            return False

        cdc.command("led on")

        chiprev = cdc.command ("chiprev")
        if self.info['verbose']:
            print "CDC chiprev test returned '%s'" % chiprev
        if chiprev == 'nak':
            self.testlogger.add_parameter (parameter="chiprev", value = chiprev, status='fail')
        else:
            self.testlogger.add_parameter (parameter="chiprev", value = chiprev, status='pass')

        chipid = cdc.command ("chipid")
        if self.info['verbose']:
            print "CDC chipid test returned '%s'" % chipid
        if chipid == 'nak':
            self.testlogger.add_parameter (parameter="chipid", value = chipid, status='fail')
        else:
            self.testlogger.add_parameter (parameter="chipid", value = chipid, status='pass')

        unique = cdc.command ("unique")
        if self.info['verbose']:
            print "CDC unique test returned '%s'" % unique
        if unique == 'nak':
            self.testlogger.add_parameter (parameter="unique", value = unique, status='fail')
        else:
            self.testlogger.add_parameter (parameter="unique", value = unique, status='pass')

        cdc.command("led off")

        cdc.disconnect()
        cdc = None
        del cdc

        print "CDC disconnected"

        return True

    def Run(self):
        print "Running: '%s'" % testname
        print "This test does: '%s'" % testdescription
        print "Scanned data is '%s'" % self.info['scanData']

        # The application is already programmed
        result = self.run_target_self_test()

        if not result:
            self.setMessage("Selftest failed")
            self.testlogger.add_parameter(parameter="target_selftest", value="%s" % result, status='fail')
        else:
            self.setMessage("OK")
            self.testlogger.add_parameter(parameter="target_selftest", value="%s" % result, status='pass')

        return result
