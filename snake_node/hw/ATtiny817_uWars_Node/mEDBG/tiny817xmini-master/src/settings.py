import os
from common.edbgcli.driver.medbg_factory import *

EGG_NAME = "tiny817xmini"  # eggTest name
TEST_AN = "A11-0331"  # eggTest PLM component
A_NUMBER = "A09-2658"  # PCBA number
TEST_SCANSTRING = "2658030200000031"  # Used for testing single files
NUMBER_UPAS = 1  # How many UPAs are required for the test

# ############# USER PAGE CONFIG BLOCK START #####################
# Userpage info for programming into the mEDBG
KIT_NAME = r'ATtiny817 Xplained Mini'  # Kit name
DEVICE_NAME = r"ATtiny817"  # Target device name, has to match Studio XML name
MANUFACTURER_NAME = r"Atmel"
DEVICE_SIGNATURE = [0x00, 0x1E, 0x90, 0x0B]
MEDBG_TYPE = MEDBG_CONFIG_FIRE_UPDI
"""
See the medbg_set_fuse_mask function documentation for details.
The three first bytes of the fuse mask settings depends on the target firmware (isp/dw, swd or TPI)
The last two (AND_mask and OR_mask) defines if fuses are allowed to be changed, forced to 1 or forced to 0
"""
MEDBG_FUSE_MASK = [[0x04, 0x3F, 0x40, 0x0E, 0xF1]]
# ############# USER PAGE CONFIG BLOCK END #####################

# Power test settings

power_test_settings = {}
power_test_settings[0] = {}  # Settings for UPA with hardware id 0
power_test_settings[0]['name'] = r'medbg' # The name will be shown in printouts and test logs
# These are the limits on the UPA power supply input in mV.
# The test will stop if UPA is powered not within these limits
power_test_settings[0]['v_psu_min'] = 5500
power_test_settings[0]['v_psu_max'] = 17000

# These limits define the current range that is expected for the UPA that is connected to the DUT.
# If UPA measures a current that is outside this range the test will be terminated.
# The defined values are in mA.
power_test_settings[0]['v_usb_min'] = 4800
power_test_settings[0]['v_usb_max'] = 5200

# Voltage limits on the USB bus. If the UPA measures a USB bus voltage that is outside these limits
# the test will be terminated and an error message will be logged. The values are in mV.
power_test_settings[0]['min_current'] = 1
power_test_settings[0]['max_current'] = 100

# Delay in seconds before we start measuring voltages on the DUT after we have turned on power to the DUT
POWER_STAB_DELAY = 0.5

# Session settings
PRE_CHIPERASE = False  # Useful to get rid of DFU, or program with -e
POST_FAILED_CHIP_ERASE = True  # TODO - should be True in production
POST_PASSED_CHIP_ERASE = False  # TODO - should be False in production
POST_POWER_OFF = True  # TODO - should be True in production

#  Very static stuff goes here
PRODUCT_NAME = 'MEDBG'  # Data/Folder name
USB_WAIT = 10  # maximum number of seconds to wait for USB devices
ATMEL_VID = 0x03EB  # Atmel USB vendor id
DEBUGGER_PID = 0x2145  # MEDBG USB product id
PRODUCTION_SERIAL_NUMBER = "FFFFFFFFFFFFFFFFFFFF"

# General settings
DATA_PATH = os.path.normpath("%s\\data" % (os.path.abspath(os.path.dirname(__file__))))
BOOTLOADER_PATH = os.path.normpath("%s\\bootloader" % DATA_PATH)
STUDIO_PATH = os.path.normpath("%s\\studio\\backend" % DATA_PATH)
TARGET_PATH = os.path.normpath("%s\\target" % DATA_PATH)
PROGRAM_TOOL = os.path.normpath("%s\\atprogram.exe" % STUDIO_PATH)
UPGRADE_TOOL = os.path.normpath("%s\\atfw.exe" % STUDIO_PATH)

# Get enviroment variable in a safe way
if os.getenv("APPDATA"):
    APPDATA = os.getenv("APPDATA")
else:
    APPDATA = 'FOOBAR'

# Used by update firmware
# FIRMWARE_COMPONENT = "A11-0252"  # MEDBG firmware component
# FIRMWARE_REVISION = "1"  # EDBG revision override
APP_PATH = APPDATA + "\\Atmel\\" + "EDBG_Initialisation_SW"
DBG_FW_PATH = os.path.normpath("%s\\mEDBG_factory") % APP_PATH

# Used by tests for the mEDBG
PROGRAMMER = "atmelice"
DEBUGGER_DEVICE = r'ATmega32U4'
DEBUGGER_INTERFACE = r'JTAG'
MEDBG_FUSE_SETTING_NO_EESAVE = r'1E99F6'
MEDBG_FUSE_SETTINGS = r'1E91F6'  # FUSE settings for mEDBG (LOW,HIGH,EXTENDED)
BOOTLOADER_BIN = "ATMega32U4-usbdevice_dfu-1_0_0.hex"
BOOTLOADER_BIN_FILE = os.path.normpath("%s\\%s" % (BOOTLOADER_PATH, BOOTLOADER_BIN))
MEDBG_BIN = "medbgupdi.hex"  # medbgtpi.hex, medbgsam.hex, medbgupdi.hex, or medbg.hex
MEDBG_BIN_FILE = os.path.normpath("%s\\temp\\%s" % (DBG_FW_PATH, MEDBG_BIN))

# Used by selftest and Program demo
TOOL_NAME = 'medbg'
INTERFACE_NAME = r'UPDI'  #Interface from EDBG to target device
TEST_FIRMWARE_NAME = r'XMINI_TINY817_verification.hex'
TEST_FIRMWARE_FILE = os.path.normpath("%s\\%s" % (TARGET_PATH, TEST_FIRMWARE_NAME))

TARGET_OSCSEL_FUSE = "0x02"
TARGET_FUSES_PREFIX = "write -fs -o 2 --values %s" % TARGET_OSCSEL_FUSE

TARGET_FUSES_ADDRESS = "-o 5"
TARGET_FUSES = "0xF4"
