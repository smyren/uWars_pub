/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * tiny817_uwars_node_revA.h
 *
 * Hardware related defines and simple functions for the initial snake node board 
 *
 */ 

#ifndef _TINY817_UWARS_NODE_REVA_H_
#define _TINY817_UWARS_NODE_REVA_H_

#include <avr/io.h>
#include "../protocol.h"

/* PORTMUX - Alternate peripheral locations */
//SPI is always in alternate location
#define portmux_spi()           (PORTMUX.CTRLB |= PORTMUX_SPI0_ALTERNATE_gc)
//USART is in default position (0) when working as SNIP/SNOP, alternate position (1) when connected to debugger?
#define PORTMUX_USART_DBGIO		PORTMUX_USART0_ALTERNATE_gc
#define PORTMUX_USART_SNIO		PORTMUX_USART0_DEFAULT_gc
#define portmux_usart(mode)     (PORTMUX.CTRLB = (PORTMUX.CTRLB & ~PORTMUX_USART0_bm) | mode)

#define portmux_init()          do {                                      \
                                    portmux_spi();                        \
                                    portmux_usart(PORTMUX_USART_SNIO);    \
                                } while (0)



/* LEDs - Four 0603 LEDs on PORTB[4:7], active low */
#define LEDPORT     PORTB
#define LED_gp      4
#define LED_gm      (0x0f << LED_gp)

typedef enum LED_MASK_enum
{ 
    LED_0_gc = (0x01<<LED_gp),    
    LED_1_gc = (0x02<<LED_gp),    
    LED_2_gc = (0x04<<LED_gp),    
    LED_3_gc = (0x08<<LED_gp),    
    LED_01_gc = (0x03<<LED_gp),
    LED_12_gc = (0x06<<LED_gp),  
    LED_03_gc = (0x09<<LED_gp),
    LED_23_gc = (0x0C<<LED_gp),
    LED_ALL_gc = (0x0f<<LED_gp),  
} LED_MASK_t;


//Control functions for the LEDs
#define led_init()              do {                            \
                                    LEDPORT.OUTSET = LED_gm;    \
                                    LEDPORT.DIRSET = LED_gm;    \
                                } while (0)
#define led_enable(mask)        (LEDPORT.OUTCLR = (mask) & LED_gm)
#define led_disable(mask)       (LEDPORT.OUTSET = (mask) & LED_gm)

//Function to display the node's latest move using the LEDs
#define led_display_move(move)  do {                                            \
                                    led_disable(LED_ALL_gc);                    \
                                    LEDPORT.OUTCLR = (1 << move) << LED_gp;     \
                                } while (0)
//#define led_display_move(move)    (LEDPORT.OUT = ((1 << move) << LED_gp) | (LEDPORT.OUT & (~LED_gm)))



/* Button - One tactile button on PORTC[4], active low */
#define BTNPORT     PORTC
#define BTNCTRL     PIN4CTRL
#define BTN_bp      4
#define BTN_bm      (1 << BTN_bp)

//Control functions for the button
#define button_init()           do {                                    \
                                    BTNPORT.DIRCLR = BTN_bm;            \
                                    BTNPORT.BTNCTRL = PORT_PULLUPEN_bm; \
                                } while (0)
#define is_button_pushed()      (BTNPORT.IN & BTN_bm)



/* Initialize board - default settings*/
#define	board_init()    do {                   \
							portmux_init();    \
	                        led_init();        \
	                        button_init();     \
	                    } while (0)


/* Functions to add for pre- and post-command blinking */
#define pre_command_blink() led_enable(LED_3_gc)

static inline void post_command_blink( uint8_t move ) {
    led_disable(LED_ALL_gc);
    switch (move) {
	    case MOVE_FORWARD:
	    led_enable(LED_1_gc);
	    break;
	    case MOVE_LEFT:
	    led_enable(LED_0_gc);
	    break;
	    case MOVE_RIGHT:
	    led_enable(LED_2_gc);
	    break;
	    case MOVE_FAST_FORWARD:
	    led_enable(LED_0_gc);
	    led_enable(LED_2_gc);
	    break;
	    default:
	    led_enable(LED_ALL_gc);
	    break;
    }	
}

#endif //_TINY817_UWARS_NODE_REVA_H_
