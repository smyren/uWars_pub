/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This file contains data describing the communication protocol
 *
 * Changing these defines and structs may mess with communication with the game host!
 *
 */
#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <stdint.h>
#include <stdbool.h>
#include <avr/pgmspace.h>

// Definitions of movement
enum moves {
    MOVE_FORWARD     ,
    MOVE_LEFT        ,
    MOVE_RIGHT       ,
    MOVE_FAST_FORWARD
};

// Definitions of board elements
enum elements {
    BOARD_EMPTY         = 0x00,
    BOARD_SNAKE_MASK    = (1 << 7) - 1, 	//BOARD_SNAKE_HEAD == BOARD_SNAKE_MASK && (1 << 0)
    BOARD_FOOD          = (1 << 7),
    BOARD_WALL          = (1 << 7) + 1,
    BOARD_EDGE          = 0xff
};

// This macro may be used to store user strings in flash for sending with
// the send_response. The user must count the bytes manually.
// It will add the flash offset address to your location.
#define SSTR(a) (const uint8_t*)PSTR(a)+MAPPED_PROGMEM_START

// This define sets total RX receive buffer size
#define RX_BUFFER_SIZE          160     // TODO: Calculate actual size needed when structures are settled

// Snake head is always in the middle of the RX_MAX_XY_SIZE * RX_MAX_XY_SIZE array.  
// Vertical axis in the radar array is flipped to have the forward positions incremental to the head position
// Radar image is rotated according to the movement so that the snake head always looks "up" the y-axis.
// Example, 5x5 view: (rX/rY shows positions relative to snake head)
//                                                       Y:   rY:
//                                                       10      
//                                                        9       
//                                                        8       
//                                |  -  F  -  |           7     2 
//                                |     f     |           6     1 - FfLR marks the possible moves
//                                |  L  X  R  |           5     0 - X marks snake head position
//                                |           |           4    -1 
//                                |  -  -  -  |           3    -2 
//                                                        2       
//                                                        1
//                                                        0       
//                    X: 0  1  2  3  4  5  6  7  8  9 10          
//                                                                
//                   rX:         -2 -1  0  1  2                   
//
// This define is the size of the buffer reserved for radar image, independent of actual radar view
// (The remaining is reserved for meta data)
#define RX_MAX_XY_SIZE          (11)

// Response data type for a RADAR_FRAME packet type
union radar_frame_response {
    // This is only one byte, always
    uint8_t array[4];
    struct {
        uint8_t command;
        uint8_t movement            : 3;
        uint8_t slow_move           : 1;
        uint8_t reserved            : 3;
        uint8_t has_status_update   : 1;
        const uint8_t* comment;
    };
};

// Contents of a radar frame command
struct radar_frame_packet {
    uint8_t     size;                   // Packet size
    uint8_t     command;                // Packet command
    uint8_t     respawn;                // True if snake just respawned
    uint8_t     rotation;               // Snake rotation
    uint16_t    length;                 // Snake length
    uint8_t     active_snakes;          // Number of active snakes
    uint8_t     ranking;
    uint8_t     radar_shape1;           // ????
    uint8_t     radar_shape0;           // ????

    // The above data should match up to 16 bytes
    uint8_t     radar[RX_MAX_XY_SIZE][RX_MAX_XY_SIZE];
    uint8_t     unused[2];              // To be determined
};

// Generic command packet with up to 14 bytes payload
struct command_packet {
    uint8_t     size;                   // Packet size
    uint8_t     command;                // Packet command
    uint8_t     data[13];               // Must total up to 16 bytes
};

// packet_data union gives different representations received packages, to
// make it easier (more readable) for the users to parse the data.
union packet_data {
    uint8_t                     array[RX_BUFFER_SIZE];      // Raw access to all bytes received
    struct radar_frame_packet   radar;                      // Used for decoding data as radar frame
    struct command_packet       command;                    // Used for getting command byte and (possibly) data
};

// packet_type_t is a readable representation of the currently implemented
// commands.
enum packet_type_t {
    D_NO_PACKET = 0,
    D_CMD_ID = 1,

    D_CMD_SN_ID = 100,
    D_CMD_SN_NAME = 101,
    D_CMD_SN_COMMENT = 102,
    D_CMD_SN_ICON = 103,
    D_CMD_SN_COLORS = 104,
    D_CMD_SN_OWNER = 105,
    D_CMD_SN_INFO = 106,
    D_CMD_SN_TIMING = 107,

    D_CMD_SN_MOVE = 110
};

// The user can access the received packet through this packet define.
// NOTE: Unless get_packet() returns something different from NO_PACKET, the
// data found here might be incoherent.
extern union packet_data packet;

// Just for convenience, we make a global variable for users
extern union radar_frame_response radar_response;

#endif // PROTOCOL_H