/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Simple protocol handler for snake_nodes
 *
 * This file simplifies communication, and presents a simple set of functions
 * for the user to implement to interface with the rest of the system.
 *
 * Feel free to replace this, or dig into the snake_com.c/h and spi_snake.c or
 * uart_snake.c if you roll something different or more optimized :)
 *
 * If you have suggestions for improvements, or better code with improvements
 * let us know. It can be added...
 *
 * The goal of this file (and .h file interface) is to provide an easy
 * entry point to making snake nodes. Please have a look at the
 * snake_node_template to see an example snake using these functions.
 *
 * Important changes (these are changes that might break existing snakes):
 *  2016-12-11: Initial version
 */

// The important include for everything AVR related
#include <avr/io.h>
#include "hw/tiny817_uwars_node_revA.h"

// Some snake related headers
#include "simple_protocol_handler.h"
#include "snake_com.h"

/*
    Initialize HW, and communication

    Initializes communication buffers and interrupts needed for communication.
    Do NOT permanently disable interrupts after calling this, also be very
    careful if disabling interrupts for any length of time, communication speed
    gives very little overhead for interrupts...

    LEDs are initialized, and OFF.

    Initial snake comment is "".
*/
void simple_protocol_handler_init(uint32_t f_cpu) {
    // TODO: Update this function to use the board functions from tiny817_uwars_node_revA.h

    // Initialize snake_com objects (this also enables interrupts)
    snake_com_init(f_cpu);
    led_init();
    radar_response.comment = SSTR("");
}

/*
    Main communication loop

    Waits for a packet, and responds to all we have an easy reply for. For all
    the others, we call user functions to get the input needed.

    There are some callbacks that may be used to get more interactivity:
        Function pointer
        void idle_loop_hook(void)
        void pre_command_parse(void)
        void post_command_parse(void)

    idle_loop_hook() - Called continously while waiting for the next packet
        to arrive. You should periodically check if get_packet() == D_NO_PACKET
        and return promptly from whatever you are doing, so you do not timeout
        on your response. It could be a movement packet that just arrived.
    pre_command_parse() - Called before figuring out what action to take on a
        packet that just arrived. Any time you spend in this function increases
        your response time... Can be useful if there are some commands you wish
        to handle without rolling your own handler. Send the response manually.
    post_command_parse() - Called just after sending the response to your
        command (the response might stil be transmitting (interrupt driven) when
        this is called, but unless you disable interrupts you probably have
        some time before the next command is incomming.
*/
void simple_protocol_handler_main(const uint8_t *snake_name,
                                  const uint8_t *snake_owner,
                                  const uint8_t *snake_info,
                                  const uint8_t *snake_colors,
                                  const uint8_t *snake_icon,
                                  void (*idle_loop_hook)(void),
                                  void (*pre_command_parse)(void),
                                  void (*post_response_hook)(void),
                                  void (*post_command_parse)(void)) {
#ifdef RUN_UART_SNAKE
    uint16_t timing = 0;
    //Run internal timing measurement when running UART snake
    internal_timing_init();
#endif

    while(1) {
        while(get_packet() == D_NO_PACKET) {
            // Optional idle_loop_hook callback
            if (idle_loop_hook)
            {
                (*idle_loop_hook)();
                
            }
        }

#ifdef RUN_UART_SNAKE
        //Reset internal timing measurement when running UART snake
        internal_timing_reset();
#endif

        // Optional pre_command_parse callback
        if (pre_command_parse)
        {
            (*pre_command_parse)();
        }

// Here we are explicitly ignoring the array out of bound warning we are getting
// from the SSTR(...) lines. The macro is adding 0x8000 (to get in flash range),
// the compiler understands that any access to this array after this will end up
// far beyond the array, but does not know that with this offset we will reading
// from the correct location in the end. (Only visible with high optimizations.)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Warray-bounds"
        switch(get_packet()) {
        case D_NO_PACKET:
            // Bugger, try again (can happen if user "erases" packet in pre_command_parse())
            break;
        case D_CMD_SN_MOVE:
            // Great! The snake moves
            radar_response.has_status_update = 0;
            radar_response.command = D_CMD_SN_MOVE;
            calculate_movement();
            
#ifdef RUN_UART_SNAKE
            //Read out internal timing measurement when running UART snake
            timing = internal_timing_readout();
#endif
            send_response(radar_response.array, 2);
            
            // Optional post_response_hook callback
            if (post_response_hook)
            {
                (*post_response_hook)();
            }
            break;
        case D_CMD_ID:
            // We are supposed to return something, but what?
            send_response_cmd_str(D_CMD_ID, SSTR("Snake Node"));
            break;
        case D_CMD_SN_ID:
            // Sending HW serial number from sigrow
            send_response_cmd(D_CMD_SN_ID, (const uint8_t *)&SIGROW.SERNUM0, 10);
            break;
        case D_CMD_SN_NAME:
            // Whats your name?
            send_response_cmd_str(D_CMD_SN_NAME, snake_name);
            break;
        case D_CMD_SN_COMMENT:
            // Whats up?
            send_response_cmd_str(D_CMD_SN_COMMENT, radar_response.comment);
            break;
        case D_CMD_SN_ICON:
            // What do you look like?
            send_response_cmd( D_CMD_SN_ICON, snake_icon + MAPPED_PROGMEM_START, 144 );   // Must add flash offset (otherwise done through SSTR macro)
            break;
        case D_CMD_SN_COLORS:
            // It doesn't matter if you're black or white?
            send_response_cmd( D_CMD_SN_COLORS, snake_colors + MAPPED_PROGMEM_START, 6 );   // Must add flash offset (otherwise done through SSTR macro)
            break;
        case D_CMD_SN_OWNER:
            // Who you gonna call?
            send_response_cmd_str(D_CMD_SN_OWNER, snake_owner);
            break;
        case D_CMD_SN_INFO:
            // What are you like?
            send_response_cmd_str(D_CMD_SN_INFO, snake_info);
            break;
        case D_CMD_SN_TIMING:
#ifdef RUN_UART_SNAKE
            // Do we need some calculation first?
            send_response_cmd( D_CMD_SN_TIMING, (const uint8_t *)&timing, sizeof(timing) );
            break;
#endif
        default:
            // Oops! this is a real problem
            send_response_cmd_str(D_NO_PACKET, SSTR("ERROR: Unknown command"));
            break;
        }
#pragma GCC diagnostic pop

        // Optional post_command_parse callback
        if (post_command_parse)
        {
            (*post_command_parse)();
        }
    }
}

uint8_t get_board_data(int8_t ahead, int8_t right) {
    uint8_t *head = &packet.radar.radar[RX_MAX_XY_SIZE / 2][RX_MAX_XY_SIZE / 2];

    head += right;
    head += ahead * RX_MAX_XY_SIZE;

    return *head;
}

