/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This is the interface that defines how to use the simple_protocol_handler interface
 *
 * The functions declared here are utility functions implemented by 
 * simple_protocol_handler, that the user can/should leverage to get started
 * quickly :)
 *
 */
#ifndef SIMPLE_PROTOCOL_HANDLER_H
#define SIMPLE_PROTOCOL_HANDLER_H

// Including the protocol variables and defines
// Examine this to understand how the different structs are built
#include "protocol.h"

// Implement these functions in your code. They will be called from the simple_protocol_handler_main()
// There are some documentation and hints on how to use them in the snake_node_template file, for now.
void calculate_movement();          //THE function, just setup your response in 

// Call these functions from your code, or roll your own more efficient versions. Your choice :)
// Have a look at the .c file for more documentation on the functions
void simple_protocol_handler_init(uint32_t f_cpu);               // Initialize HW, and communication (needs cpu clock)
void simple_protocol_handler_main(const uint8_t *snake_name,
                                  const uint8_t *snake_owner,
                                  const uint8_t *snake_info,
                                  const uint8_t *snake_colors,
                                  const uint8_t *snake_icon,
                                  void (*idle_loop_hook)(void),
                                  void (*pre_command_parse)(void),
                                  void (*post_response_hook)(void),
                                  void (*post_command_parse)(void)
                                  );                             // Main loop, call this from your main(), it does not return

// Fast and easy way to get the board element located at an x,y location where 0,0 is the snakes head in the middle of the radar.
uint8_t get_board_data(int8_t ahead, int8_t right);


#endif // SIMPLE_PROTOCOL_HANDLER_H
