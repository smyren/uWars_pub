/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Generic communication code for snake node communication
 *
 * UART / SPI specific code can be found in uart_snake.c or spi_snake.c
 *
 */
#include <avr/interrupt.h>

// SNAKE_COM_PRIVATE define before including snake_com.h to get
// definitions for low level communication functions.
#define SNAKE_COM_PRIVATE
#include "snake_com.h"

// This is the input buffer, and also all the interpretations of the input data.
union packet_data packet;

// This is the radar response buffer (declared here, we are certain that it is
// a global variable, and does not go out of scope too early).
union radar_frame_response radar_response;

enum packet_type_t get_packet() {
    if(packet.command.size == com_get_rx_count()) {
        return packet.command.command;
    }
    return D_NO_PACKET;
}

enum packet_type_t wait_for_packet() {
    enum packet_type_t packet_type = D_NO_PACKET;
    while(packet_type == D_NO_PACKET) {
        packet_type = get_packet();
    }
    return packet_type;
}

void send_response(const uint8_t* data, uint8_t size) {
    com_send_byte(size + 1);        // Send data size + 1 for size byte
    com_send_bytes(data, size);     // Send prepared buffer

    // Invalidate received data
    com_set_rx_buffer(packet.array, RX_BUFFER_SIZE);
}

void send_response_cmd(uint8_t cmd, const uint8_t* data, uint8_t size) {
    com_send_byte( size + 1 + 1 );  // Send string size +1 for size byte +1 for command byte
    com_send_byte(cmd);             // Include command in reply
    com_send_bytes(data, size);     // Send prepared buffer

    // Invalidate received data
    com_set_rx_buffer(packet.array, RX_BUFFER_SIZE);
}

void send_response_cmd_str(uint8_t cmd, const uint8_t* str) {
    uint8_t size = 0;
    for(; str[size] != '\0'; ++size);       // Count size
    send_response_cmd(cmd, str, size);
}

void snake_com_init(uint32_t f_cpu) {
    // Initialize HW communication module
    com_init(f_cpu);

    // Setup RX buffer
    com_set_rx_buffer(packet.array, RX_BUFFER_SIZE);

    // Enable interrupts to start receiving data
    sei();
}