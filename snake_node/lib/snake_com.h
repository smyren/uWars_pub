/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Generic communication code for snake node communication
 *
 * Definitions relating to host communication. This file defines the package
 * types as seen from the snake node, and also support functions for getting
 * and sending packages.
 *
 */
#ifndef SNAKE_COM_H
#define SNAKE_COM_H

#include <stdint.h>
#include <stdbool.h>

#include "protocol.h"

enum packet_type_t get_packet();
enum packet_type_t wait_for_packet();
void send_response(const uint8_t *data, uint8_t size);
void send_response_cmd(uint8_t cmd, const uint8_t *data, uint8_t size);
void send_response_cmd_str(uint8_t cmd, const uint8_t *str);
void snake_com_init(uint32_t f_cpu);

// This SNAKE_COM_PRIVATE hides some definitions that are not intended to be
// used by other than library functions. If you feel they are useful, let us
// discuss how we can make the functionality available without exposing these
// directly.
#ifdef SNAKE_COM_PRIVATE
// Low level communication functions, these are implemented in
// uart_snake.c or spi_snake.c.
void    com_send_bytes(const uint8_t *buffer, uint8_t buffer_size);
void    com_send_byte(uint8_t data);
void    com_set_rx_buffer(uint8_t *buffer, uint8_t buffer_size);
uint8_t com_get_rx_count();
void    com_init(uint32_t f_cpu);
#endif // SNAKE_COM_PRIVATE

// These defines are for the internal timing measurements done when running
// UART protocol, since this does not have the same timing as the 
// SPI Motherboard.
#ifdef RUN_UART_SNAKE
void internal_timing_init();
void internal_timing_reset();
uint16_t internal_timing_readout();
#endif // RUN_UART_SNAKE

#endif // SNAKE_COM_H