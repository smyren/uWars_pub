/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Low level SPI communication routines
 *
 * This is The communication interface between the snake node and the snake controller. If you
 * modify it and mess it up. Good luck debugging :)
 *
 * To use the SPI interface, please define the RUN_SPI_SNAKE in the project settings.
 *
 */
#ifdef RUN_SPI_SNAKE

#include "snake_com.h"

#include <avr/interrupt.h>
#include <util/atomic.h>

// We will need some pointers for buffers, and some counters to see how much
// data we have received and sent.
const uint8_t *spi_tx_buffer = 0;
volatile uint8_t spi_tx_buffer_size = 0;
volatile uint8_t spi_tx_count = 0;
uint8_t *spi_rx_buffer = 0;
volatile uint8_t spi_rx_buffer_size = 0;
volatile uint8_t spi_rx_count = 0;
//? volatile uint8_t receive_timeout = 0;


/*?
ISR( RTC_PIT_vect ) {
    RTC.PITINTFLAGS = RTC_PI_bm;
    receive_timeout++;
    if ( (receive_timeout > 16) & (usart_rx_count != usart_rx_buffer[0]) ) {
        // Resetting this reception
        usart_rx_buffer[0] = 0;
        usart_rx_buffer[1] = D_NO_PACKET;
        usart_rx_count = 0;
    }
}
?*/

ISR( PORTC_PORT_vect ) {
    // This interrupt is setup for resetting ongoing communications
    PORTB.OUTCLR = PIN2_bm;             // SNOP low
    PORTC.INTFLAGS |= PIN3_bm;
    // Abort any ongoing transmissions
    SPI0.INTCTRL &= ~SPI_DREIE_bm;
    spi_tx_buffer_size = 0;
    spi_tx_count = 0;

    // Rest data reception register
    SPI0.INTFLAGS = SPI_RXCIF_bm;
    spi_rx_buffer[0] = 0;
    spi_rx_buffer[1] = D_NO_PACKET;
    spi_rx_count = 0;
}


// Communication is interrupt based, but this will only fill and empty the
// buffers for you, you still have to poll to find out if the transfer is complete.
//
// Also of note is the fact that we are double buffered in the transmit
// direction. TODO CHECK: This might cause us to send a dummy byte when sending replys.
ISR( SPI0_INT_vect ) {
    // First send any data pending in the output queue
    if(spi_tx_count >= spi_tx_buffer_size) {
        SPI0.INTCTRL &= ~SPI_DREIE_bm;              // Last byte sent disable tx interrupt
    } else {
        SPI0.DATA = spi_tx_buffer[spi_tx_count++];  // Send next byte
    }

    // Then accept data received
    if(spi_rx_count >= spi_rx_buffer_size) {
        SPI0.INTFLAGS = SPI_RXCIF_bm;               // Clear interrupt, drop data, buffer full
    } else {
        //receive_timeout = 0;
        spi_rx_buffer[spi_rx_count++] = SPI0.DATA;  // Read last received byte
    }
}

// NOTE: This buffer cannot be reused by the application until all data is sent.
void com_send_bytes( const uint8_t *buffer, uint8_t buffer_size ) {
    PORTB.OUTSET = PIN2_bm;             // SNOP high

    while ( (SPI0.INTCTRL & SPI_DREIE_bm) == SPI_DREIE_bm ); // Wait for previous transmission

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        spi_tx_buffer = buffer;
        spi_tx_buffer_size = buffer_size;
        spi_tx_count = 0;
        SPI0.INTCTRL |= SPI_DREIE_bm;               // Re-enable interrupts to start sending buffer
    }
}

void com_send_byte( uint8_t data ) {
    PORTB.OUTSET = PIN2_bm;             // SNOP high

    while ( (SPI0.INTCTRL & SPI_DREIE_bm) == SPI_DREIE_bm ); // Wait for previous transmission

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        spi_tx_buffer = 0;                          // No buffer, just what we put in the queue
        spi_tx_buffer_size = 0;
        spi_tx_count = 0;

        SPI0.DATA = data;                           // Queue for sending
        SPI0.INTCTRL |= SPI_DREIE_bm;               // Re-enable interrupts (to tell we are busy)
    }
}


// Call this to start filling a new buffer / restart filling this buffer
void com_set_rx_buffer( uint8_t *buffer, uint8_t buffer_size ) {
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        spi_rx_buffer = buffer;
        // Setting the 'count' location to maximum to prevent the reception
        // from being seen as complete before anything has been received.
        // (first buffer location could have started out as 0)
        spi_rx_buffer[0] = 0;
        spi_rx_buffer[1] = D_NO_PACKET;
        spi_rx_buffer_size = buffer_size;
        spi_rx_count = 0;
    }
}

// Check how many bytes has been received
uint8_t com_get_rx_count() {
    return spi_rx_count;
}

void com_init(uint32_t f_cpu) {
    // Setup SPI
    SPI0.CTRLB = SPI_BUFEN_bm;          // Buffered mode enabled, mode 0
    SPI0.CTRLA = SPI_ENABLE_bm;         // MSB first, Slave mode, enable ;)
    SPI0.INTCTRL = SPI_RXCIF_bm;        // Enable receive interrupts
    // DREIE is used for transmission, but will be enabled when needed

    // Setup SPI for alternate port
    PORTMUX.CTRLB = PORTMUX_SPI0_ALTERNATE_gc;

    // Some pin directions used during communication
    PORTB.DIRSET = PIN2_bm;             // SNOP as output
    PORTB.OUTCLR = PIN2_bm;             // SNOP low
    PORTC.DIRCLR = PIN3_bm;             // SS as input
    PORTC.DIRSET = PIN1_bm;             // MISO as output

    // Setup interrupt on rising edge for SS
    PORTC.PIN3CTRL = PORT_ISC_RISING_gc;

    // Enable PIT for timing out communication in case of bad communication
    // Interrupt period should be 31.25 ms
    // RTC.PITINTCTRL = RTC_PI_bm;
    // RTC.PITCTRLA = RTC_PERIOD_CYC1024_gc | RTC_PITEN_bm;
}

#endif  // RUN_SPI_SNAKE
