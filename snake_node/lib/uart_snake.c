/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Low level UART communication routines
 *
 * This is not for debug strings, but actually running a snake node with a
 * direct connection to the game host.
 *
 * To use the UART, please define the RUN_UART_SNAKE in the project settings.
 *
 */
#ifdef RUN_UART_SNAKE

#include "snake_com.h"

#include <avr/interrupt.h>
#include <util/atomic.h>

// We will need some pointers for buffers, and some counters to see how much
// data we have received and sent.
const uint8_t *usart_tx_buffer = 0;
volatile uint8_t usart_tx_buffer_size = 0;
volatile uint8_t usart_tx_count = 0;
uint8_t *usart_rx_buffer = 0;
volatile uint8_t usart_rx_buffer_size = 0;
volatile uint8_t usart_rx_count = 0;
volatile uint8_t receive_timeout = 0;

//variable for calculating timing based on F_CPU
uint8_t f_cpu_compensate = 0;

ISR( RTC_PIT_vect ) {
    RTC.PITINTFLAGS = RTC_PI_bm;
    receive_timeout++;
    if((receive_timeout > 16) & (usart_rx_count != usart_rx_buffer[0])) {
        // Resetting this reception
        usart_rx_buffer[0] = 0;
        usart_rx_buffer[1] = D_NO_PACKET;
        usart_rx_count = 0;
    }
}

// Communication is interrupt based, and these are the interrupt vectors.
// Still you will have to poll to find out if the transfer is complete.
ISR( USART0_RXC_vect ) {
    if(usart_rx_count >= usart_rx_buffer_size) {
        USART0.STATUS = USART_RXCIF_bm;                     // Clear interrupt
        return;                                             // Buffer full, dropping data
    }

    receive_timeout = 0;
    usart_rx_buffer[usart_rx_count++] = USART0.RXDATAL;
}

ISR( USART0_DRE_vect ) {
    if(usart_tx_count >= usart_tx_buffer_size) {
        USART0.CTRLA &= ~USART_DREIE_bm;                    // Last byte sent, disable interrupt
        return;
    }
    USART0.TXDATAL = usart_tx_buffer[usart_tx_count++];
}

// NOTE: This buffer cannot be reused by the application until all data is sent.
void com_send_bytes( const uint8_t *buffer, uint8_t buffer_size ) {
    while ( (USART0.CTRLA & USART_DREIE_bm) == USART_DREIE_bm ); // Wait for previous transmission

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        usart_tx_buffer = buffer;
        usart_tx_buffer_size = buffer_size;
        usart_tx_count = 0;
        USART0.CTRLA |= USART_DREIE_bm;                     // Re-enable interrupts to start sending buffer
    }
}

void com_send_byte( uint8_t data ) {
    while ( (USART0.CTRLA & USART_DREIE_bm) == USART_DREIE_bm ); // Wait for previous transmission

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        usart_tx_buffer = 0;                                // No buffer, just what we put in the queue
        usart_tx_buffer_size = 0;
        usart_tx_count = 0;

        USART0.TXDATAL = data;                              // Start sending
        USART0.CTRLA |= USART_DREIE_bm;                     // Re-enable interrupts (to tell we are busy)
    }
}


// Call this to start filling a new buffer / restart filling this buffer
void com_set_rx_buffer( uint8_t *buffer, uint8_t buffer_size ) {
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        usart_rx_buffer = buffer;
        // Setting the 'count' location to maximum to prevent the reception
        // from being seen as complete before anything has been received.
        // (first buffer location could have started out as 0)
        usart_rx_buffer[0] = 0;
        usart_rx_buffer[1] = D_NO_PACKET;
        usart_rx_buffer_size = buffer_size;
        usart_rx_count = 0;
    }
}

// Check how many bytes has been received
uint8_t com_get_rx_count() {
    return usart_rx_count;
}

void com_init(uint32_t f_cpu) {
    int8_t correction = SIGROW.OSC20ERR5V;

    uint32_t baud = (348*(f_cpu / 1000000ul))/10;         // 1389 is for 10 MHz@28800, 4167 is 10MHz@9600 , 115200 is 347@10 MHz
    baud = (baud * (1024 + (correction))) / 1024;

    USART0.BAUD = (uint16_t)baud;
    USART0.CTRLC = USART_CHSIZE_8BIT_gc | USART_SBMODE_1BIT_gc | USART_PMODE_DISABLED_gc;
    USART0.CTRLB = USART_TXEN_bm | USART_RXEN_bm;
    USART0.CTRLA = USART_RXCIE_bm;
    // DREIE is used for transmission, but will be enabled when needed

    PORTB.DIRSET = 0x04;                // TODO: Select Normal/Alternate port

    // Enable PIT for timing out communication in case of bad communication
    // Interrupt period should be 31.25 ms
    RTC.PITINTCTRL = RTC_PI_bm;
    RTC.PITCTRLA = RTC_PERIOD_CYC1024_gc | RTC_PITEN_bm;

    f_cpu_compensate = (f_cpu / 1000000ul);
}

void internal_timing_init(){
    //Start timer at full speed
    TCA0.SINGLE.CTRLA = TCA_SINGLE_CLKSEL_DIV1_gc | TCA_SINGLE_ENABLE_bm;
}

void internal_timing_reset(){
    //Start new measurement
    TCA0.SINGLE.CNT = 0;	
}

uint16_t internal_timing_readout(){
    uint32_t count = TCA0.SINGLE.CNT;
    
    //Compensate for clock frequency based on F_CPU
    count = (count * 32) / f_cpu_compensate;
    
    return (uint16_t) count;
}

#endif  // RUN_UART_SNAKE
