/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Snake node template project.
 *
 * This main file together with the project files, and lib folder should
 * provide the basic framework for users to generate their own snakes.
 *
 * Goal: The users should just have to make a copy of this project and rename
 *       some files (or make a branch, so they can easily pull updates later on)
 *       to have a (stupid) snake up and running, they can then add the missing
 *       smartness, and rule the board!
 *
 * Setup snake name, owner, description and icon in user_settings.h
 *
 * You must select what communication protocol you are going to run.
 * The easiest way is to go to the "Solution Configurations" pulldown menu
 * and select Debug, Release_UART or Release_SPI.
 * By default Debug is set to use the UART library, however this can be changed
 * Go to the project properties (ALT+F7). Under Toolchain select 
 * AVR/GNU C Linker -> Libraries. Change the included lib_uart_com.a to lib_spi_com.a
 * One of these MUST be setup
 *
 */
#include <avr/io.h>
#include <stdlib.h>

/* Include uWars libraries */
#include <simple_protocol_handler.h>
#include "hw/tiny817_uwars_node_revA.h"

/* Include user flavoring*/
#include "user_settings.h"

/* Internal prototypes */
void idle_loop_hook(void);
void pre_command_parse(void);
void post_response_hook(void);
void post_command_parse(void);

/*  calculate_movement()
    The function that determines how your snake will move

    Use the get_board_data(ahead, right) function to peek at the value of
    different board locations, and determine where you wish to move. Set the
    radar_response.command field to one of the following defines:
        MOVE_FORWARD
        MOVE_FAST_FORWARD
        MOVE_LEFT
        MOVE_RIGHT
    and (optionally) set the radar_response.has_status_update bit to 1 if you
    wish to update your comment. If you want to update your comment, you must
    also setup the radar_response.comment pointer to the new comment. The
    game host will request this after your movement response has been sent, and
    timing is stopped...
        radar_response.comment = SSTR("My new comment");

    Just return from this function when you are done, your response will be
    sent immediately ;)
*/
void calculate_movement(void)
{
    // Second quickest snake ever :)
    uint8_t ahead = get_board_data(1, 0);
    uint8_t left = get_board_data(0, -1);
    uint8_t right = get_board_data(0, 1);

    if(ahead == BOARD_FOOD) {
        radar_response.movement = MOVE_FORWARD;
        return;
    } else if(left == BOARD_FOOD) {
        radar_response.movement = MOVE_LEFT;
        radar_response.comment = SSTR("FOOD   LEFT");
        radar_response.has_status_update = 1;
        return;
    } else if(right == BOARD_FOOD) {
        radar_response.movement = MOVE_RIGHT;
        radar_response.comment = SSTR("FOOD   RIGHT");
        radar_response.has_status_update = 1;
        return;
    }

    if(ahead == BOARD_EMPTY) {
        radar_response.movement = MOVE_FORWARD;
        return;
    } else if(left == BOARD_EMPTY) {
        radar_response.movement = MOVE_LEFT;
        return;
    } else if(right == BOARD_EMPTY) {
        radar_response.movement = MOVE_RIGHT;
        return;
    }

    radar_response.movement = MOVE_FAST_FORWARD;
    return;
}

/*  main
    You are responsible for declaring a main function.

    To use the simple_protocol handler_for communication, just call the
    simple_protocol_handler_init() and simple_protocol_handler_main()
    it will call functions in your code when you need to do stuff.

    When using the UART communication, F_CPU is needed by the
    simple_protocol_handler_init() for Baud calculation.
    F_CPU is declared in the project properties (Alt+F7) under 
    Toolchain -> AVR/GNU C Compiler -> Symbols.

    In addition to passing the user_settings to simple_protocol_handler_main
    you can choose to hook other functions to be called at different stages 
    in the processing loop. They are used by default, however you can 
    disable them by replacing the function pointer with NULL. 
    The protocol handler loops the user configurable functions in this order:
        *idle_loop_hook is called continously while nothing is received
        *pre_command_parse is called before any packet is processed
        calculate_movement() is called if the packet is a radar packet
        *post_response_hook is called if the packet is a radar 
        *post_command_parse is called after any packet is processed

    Of course, even though this is a nice implementation, you might
    want to roll your own at some point in the feature. We are,
    after all, counting cycles ;)
*/
int main(void)
{
    /* Configure clock settings */
    _PROTECTED_WRITE(CLKCTRL_MCLKCTRLB, CLKCTRL_PEN_bm);    // Set system clock prescaler to 2 (=10 MHz)

    /* Init communication interface */
    simple_protocol_handler_init(F_CPU);

    /* Start the game handler */
    simple_protocol_handler_main(SSTR(SNAKE_NAME),
                                 SSTR(SNAKE_OWNER),
                                 SSTR(SNAKE_INFO),
                                 snake_colors,
                                 snake_icon,
                                 &idle_loop_hook,
                                 &pre_command_parse,
                                 &post_response_hook,
                                 &post_command_parse);
}

/*
    Do stuff in the while loop waiting for next data from host.
*/
void idle_loop_hook(void)
{

}

/*
    A command has been received, but not yet parsed.
*/
void pre_command_parse(void)
{
    // Blink LED(s) to show activity
    pre_command_blink();
}

/*
    Response has been sent.
*/
void post_response_hook(void)
{

}

/*
    A command has been parsed.
*/
void post_command_parse(void)
{
    // Blink LED(s) to show movement activity
    post_command_blink(radar_response.movement);
}

