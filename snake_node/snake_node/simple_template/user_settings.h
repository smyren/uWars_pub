/*
 * Copyright (c) 2017 Steinar Myren, Stig Larssen, Carl Petter Levy, Johan R. Karlsen, Ingebrigt Hole, Ole Einar Salvesen, Egil Rotevatn, Magne T�rresen.
 * 
 * This file is part of uWars - a microcontroller snake game 
 * (see https://gitlab.com/smyren/uWars_pub).
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
    This is the user settings file for configuring your snake data.
    
    Customize this to personalize (snakealize?) your snake :)    
    
    Please do not include this file in other places than simple_protocol_handler.c.
    Since we are defining variables, you will either get linker errors, or waste flash.
*/
 
#ifndef USER_SETTINGS_H
#define USER_SETTINGS_H

/*
    Name of your snake.
*/
#define SNAKE_NAME		"YOUR SNAKE-NAME HERE"

/*
    Name of snake owner.
*/
#define SNAKE_OWNER		"YOUR NAME HERE"

/*
    Background story (flavor text) for your snake. This MUST be less than some
    (not yet precisely determined) number. Max packet size is 255 bytes, and
    there is some protocol overhead. Keep below 240 bytes, and you should be OK.
*/
#define SNAKE_INFO		"Nobody has taken the time to update my story... No one loves ME!"

/*
    Colors of your snake, [format documentation goes here].
    Colors from the icon editor can be pasted directly into this file.
    The response needs to be 6 bytes, exactly.
*/
const uint8_t snake_colors[6] PROGMEM = {0,255,0,255,255,0};

/*
    Icon of your snake, [format documentation goes here].
    We recommend pasting from the icon editor (like the colors above).
    The array MUST be 144 bytes (12x12), exactly...
*/
const uint8_t snake_icon[144] PROGMEM = {
    34,162,194,194,186,250,250,186,158,158,158,34,
    34,162,194,194,186,250,250,158,178,170,162,34,
    34,162,194,194,186,250,250,158,178,170,162,34,
    34,162,194,194,186,250,250,158,178,170,162,34,
    34,162,194,194,186,250,250,186,158,158,158,34,
    34,162,170,178,186,250,250,186,178,170,162,34,
    34,162,222,222,222,250,250,30,178,170,30,34,
    34,222,170,178,186,222,186,30,30,170,30,34,
    34,222,170,178,178,222,178,30,30,30,30,34,
    34,222,170,170,170,222,170,30,170,30,30,34,
    34,162,222,222,222,162,162,30,162,162,30,34,
    34,34,34,34,34,34,34,34,34,34,34,34
};

#endif  // USER_SETTINGS_H